import React from 'react'
import { Form, Formik } from 'formik';
import CustomInput from '../CustomInput';
import * as Yup from 'yup';

const BaseHashtagForm = ({ hashtag, submitCallback, resetFormHandler }) => {
    let initialValues = 
    {
        hashtag: hashtag?.hashtag || '',
    };

    const validationSchema = Yup.object({
        hashtag: Yup.string().required('Hashtag is required'),
    })

    return (
        <>
            <Formik
                initialValues={initialValues}
                enableReinitialize={true}
                validationSchema={validationSchema}
                onSubmit={submitCallback}
            >
                <Form>
                    <div className="form-group">
                        <CustomInput name={"hashtag"} label={"Hashtag"} class_name={"form-control"}/>
                    </div>
                    <div className="form-group">
                        <button type='submit' className="mb-2 mr-2 btn btn-primary">Save</button>
                        <button type='reset' onClick={() => resetFormHandler()} className="mb-2 mr-2 btn btn-secondary">Cancel</button>
                    </div>
                </Form>
            </Formik>
        </>
    )
}

export default BaseHashtagForm