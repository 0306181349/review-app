import React, { useEffect, useState } from 'react';
import * as Yup from 'yup';
import { Field, Form, Formik } from 'formik';
import CustomInput from '../CustomInput';
import { loadJWT } from '../../../utility/LocalStorage';
import { makeRequest } from '../../../utility/API';
import SelectLocation from '../../input/SelectLocation';
import SelectCategory from '../../input/SelectCategory';
import SelectHashtag from '../../input/SelectHashtag';
import CKEditorInput from '../CKEditorInput';
import ImageDragAndDropInput from '../../input/ImageDragAndDropInput';
import EditPostImageInput from '../../input/EditPostImageInput';

const BasePostForm = ({ post, setRedirectParam, setErrors }) => {
    const [category, setCategory] = useState([]);
    useEffect(() => {
        makeRequest({
            url: `select/category`,
            successCallback: (data) => {
                const { category_data } = data;
                console.log(category_data);
                setCategory(category_data.map(i => ( {label: i.category_name, value: i.category_id} )));
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'GET',
            // authorization: loadJWT(),
        });
    }, []);
    let initialValues = 
    {
        title: post?.title || '',
        content: post?.content || '',
        rating: post?.rating || '',
        hashtags: (post !=  null && post.hashtag != undefined) ? post.hashtag.map((hashtag) => {
            return hashtag.value;
        }) : '',
        location: post?.location_id || '',
        remain_images: post?.post_image || '',
    };

    const submitCallback = (values) => {
        makeRequest({
            url: `admin/post/${post != null ? `update/${post.post_id}` : 'add'}`,
            values,
            successCallback: (data) => {
                const { id } = data;
                setRedirectParam(id);
            },
            failureCallback: (error) => {
                setErrors(error);
            },
            requestType: "POST",
            authorization: loadJWT(),
        });
    };

    const validationSchema = Yup.object({
        title: Yup.string().required('Title is required'),
        content: Yup.string().required('Content is required'),
        rating: Yup.number().required("Rating is required"),
        // location: Yup.number().required('Location is required'),
    })

    return (
        <>
            <Formik
                    initialValues={initialValues}
                    enableReinitialize={true}
                    validationSchema={validationSchema}
                    onSubmit={(values) => {
                        let data = new FormData();
                        // if(values.images != undefined){
                        //     Array.from(values.images).forEach((file, index) => {
                        //         data.append(`images[${index}]`, file);
                        //     });
                        // }
                        for(const key in values){
                            console.log(values[key]);
                            if(key == 'images' && values.images != undefined){
                                Array.from(values.images).forEach((file, index) => {
                                    data.append(`images[${index}]`, file);
                                });
                            }else{
                                data.append(`${key}`, `${values[key]}`);
                            }
                        }
                        submitCallback(data);
                    }}
                >
                    <Form>
                        <div className="form-group">
                            <CustomInput name={"title"} label={"Title"} class_name={"form-control"}/>
                        </div>
                        <div className="form-group">
                            <SelectLocation name={"location"} label={"Location"} class_name={"form-control"} selected={post == null ? null : {label: post.location_name, value: post.location_id}} />
                        </div>
                        <div className="form-group">
                            <SelectCategory name={"category"} label={"Category"} class_name={"form-control"} selected={post == null ? null : {label: post.category_name, value: post.category_id}} selectOptions={category} />
                        </div>
                        <div className="form-group">
                            <CustomInput name={"rating"} label={"Rating"} class_name={"form-control"}/>
                        </div>
                        <div className="form-group">
                            <SelectHashtag name={"hashtags"} label={"Hashtag"} class_name={"form-control"} selected={post == null ? null : post.hashtag} />
                        </div>
                        <div className="form-group">
                            <CKEditorInput name={"content"} label={"Content"} />
                        </div>
                        {
                            post == null ? 
                            <div className="form-group">
                                <ImageDragAndDropInput name={"images"} label={"Images"} />
                            </div>
                            :
                            <div className="form-group">
                                <EditPostImageInput uploadedImages={post.post_image || []} name={"images"} label={"Images"} />
                            </div>
                        }
                        <div className="form-group">
                            <button type='submit' className="mb-2 mr-2 btn btn-primary">Save</button>
                        </div>
                    </Form>
            </Formik>
        </>
    )
}

export default BasePostForm
