import React, { useEffect, useState } from 'react';
import { Form, Formik } from 'formik';
import CustomInput from '../CustomInput';
import { loadJWT } from '../../../utility/LocalStorage';
import { makeRequest } from '../../../utility/API';
import SelectProvince from '../../input/SelectProvince';
import TimePickerInput from '..//TimePickerInput';
import * as Yup from 'yup';

const BaseLocationForm = ({ location, setRedirectParam ,setErrors}) => {

    const [province, setProvince] = useState([]);
    useEffect(() => {
        makeRequest({
            url: `select/province`,
            successCallback: (data) => {
                const { province_data } = data;
                console.log(province_data);
                setProvince(province_data.map(i => ( {label: i.province_name, value: i.province_id} )));
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'GET',
            // authorization: loadJWT(),
        });
    }, []);
    let initialValues = 
    {
        location_name: location?.location_name || '',
        open_time: location?.open_time || '',
        closed_time: location?.closed_time || '',
        lowest_price: location?.lowest_price || '',
        highest_price: location?.highest_price || '',
        rating: location?.rating || '',
        address: location?.address || '',
        phone_number: location?.phone_number || '',
    };

    const submitCallback = (values) => {
        makeRequest({
            url: `admin/location/${location != null ? `update/${location.id}` : 'add'}`,
            values,
            successCallback: ( data ) => {
                const { id } = data;
                setRedirectParam(id)
            },
            failureCallback: (error) => {
                setErrors(error);
            },
            requestType: "POST",
            authorization: loadJWT(),
        });
    };

    const validationSchema = Yup.object({
        location_name: Yup.string().required('Location name is required'),
        open_time: Yup.string().required('Open time is required'),
        closed_time: Yup.string().required("Closed time is required"),
        // lowest_price: Yup.number().required('Lowest price is required'),
        // hightgest_price: Yup.number().required('Hightgest price is required'),
        address: Yup.string().required('Address is required'),
        // phone_number: Yup.string().required('Phone is required'),
    })

    return (
        <>
            <Formik
                    initialValues={initialValues}
                    enableReinitialize={true}
                    validationSchema={validationSchema}
                    onSubmit={submitCallback}
                >
                    <Form>
                        <div className="form-group">
                            <CustomInput name={"location_name"} label={"Location name"} class_name={"form-control"}/>
                        </div>
                        <div className="form-group">
                            <TimePickerInput name={"open_time"} label={"Open time"} class_name={"form-control"}/>
                        </div>
                        <div className="form-group">
                            <TimePickerInput name={"closed_time"} label={"Closed time"} class_name={"form-control"}/>
                        </div>
                        <div className="form-group">
                            <CustomInput name={"lowest_price"} label={"Lowest price"} class_name={"form-control"}/>
                        </div>
                        <div className="form-group">
                            <CustomInput name={"highest_price"} label={"Highest price"} class_name={"form-control"}/>
                        </div>
                        <div className="form-group">
                            <SelectProvince name={"province"} label={"Province"} class_name={"form-control"} selected={location == null ? null : {label: location.province_name, value: location.province_id}} selectOptions={province} />
                        </div>
                        <div className="form-group">
                            <CustomInput name={"address"} label={"Address"} class_name={"form-control"}/>
                        </div>
                        <div className="form-group">
                            <CustomInput name={"phone_number"} label={"Phone"} class_name={"form-control"}/>
                        </div>
                        <div className="form-group">
                            <button type='submit' className="mb-2 mr-2 btn btn-primary">Save</button>
                        </div>
                    </Form>
            </Formik>
        </>
    )
}

export default BaseLocationForm
