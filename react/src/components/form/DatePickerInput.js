import React from 'react';
import DatePicker from "react-datepicker";
import {ErrorMessage, Field} from 'formik';
import "react-datepicker/dist/react-datepicker.css";

const getErrorDiv = message => {
    return (
        <div style={{color: '#dc3545'}}>
            {message}
        </div>
    );
};

const DatePickerInput = ({ class_name, name, label, type = 'text' }) => {
    return (
        <>
            <label htmlFor={name}>{label}</label>
            <br />
            <Field name={name}>
                {
                    ({form, field}) => {
                        const { setFieldValue } = form;
                        const { value } = field;
                        return (
                        <>
                            <DatePicker 
                                id={name} 
                                name={name} 
                                {...field} 
                                selected={Date.parse(value)}
                                onChange={ 
                                    (val) => {
                                        console.log(val);
                                        setFieldValue(name, val)
                                    }
                                }
                                className={class_name}
                            />
                            <ErrorMessage
                                name={name}
                                render={getErrorDiv}
                            />
                        </>)
                    }
                }
            </Field>
        </>
    )
}

export default DatePickerInput
