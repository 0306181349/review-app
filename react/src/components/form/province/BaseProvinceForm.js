import React from 'react'
import { Form, Formik } from 'formik';
import CustomInput from '../CustomInput';
import * as Yup from 'yup';

const BaseProvinceForm = ({ province, submitCallback, resetFormHandler }) => {
    let initialValues = 
    {
        province_name: province?.province_name || '',
    };

    const validationSchema = Yup.object({
        province_name: Yup.string().required('Province name is required'),
    })

    return (
        <>
            <Formik
                initialValues={initialValues}
                enableReinitialize={true}
                validationSchema={validationSchema}
                onSubmit={submitCallback}
            >
                <Form>
                    <div className="form-group">
                        <CustomInput name={"province_name"} label={"Province name"} class_name={"form-control"}/>
                    </div>
                    <div className="form-group">
                        <button type='submit' className="mb-2 mr-2 btn btn-primary">Save</button>
                        <button type='reset' onClick={() => resetFormHandler()} className="mb-2 mr-2 btn btn-secondary">Cancel</button>
                    </div>
                </Form>
            </Formik>
        </>
    )
}

export default BaseProvinceForm