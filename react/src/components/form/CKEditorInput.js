import React from 'react';
import {ErrorMessage, Field} from 'formik';
import { CKEditor } from '@ckeditor/ckeditor5-react';   
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

const getErrorDiv = message => {
    return (
        <div style={{color: '#dc3545'}}>
            {message}
        </div>
    );
};

const CKEditorInput = ({ name, label}) => {
    return (
        <>
            <label htmlFor={name}>{label}</label>
            <br />
            <Field name={name}>
                    {({form, field}) => {
                        const { setFieldValue } = form;
                        const { value } = field;
                        return (
                            <>
                                <CKEditor
                                    editor={ ClassicEditor }
                                    data={value}
                                    onChange={(event, editor) => {
                                        setFieldValue(name, editor.getData());
                                    }}
                                />
                                <ErrorMessage
                                    name={name}
                                    render={getErrorDiv}
                                />
                            </>
                        )
                    }}
            </Field>
        </>
    )
}

export default CKEditorInput
