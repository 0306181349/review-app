import React, { useState } from 'react';
import * as Yup from 'yup';
import { Field, Form, Formik } from 'formik';
import CustomInput from '../CustomInput';
import DatePickerInput from '../DatePickerInput';
import { loadJWT } from '../../../utility/LocalStorage';
import { makeRequest } from '../../../utility/API';
import AvatarInput from '../../input/AvatarInput';

const BaseUserForm = ({ user, setRedirectParam, setErrors }) => {
    const [responseMessage, setResponseMessage] = useState('');

    let initialValues = 
    {
        first_name: user?.first_name || '',
        last_name: user?.last_name || '',
        email: user?.email || '',
        phone: user?.phone || '',
        birthday: user ? new Date(user.birthday) : new Date(),
        user_name: user?.user_name || '',
        gender: user?.gender || '',
    };

    const submitCallback = (values) => {
        makeRequest({
            url: `admin/users/${user != null ? `update/${user.id}` : 'add'}`,
            values,
            successCallback: (data) => {
                const { id } = data;
                setRedirectParam(id);
            },
            failureCallback: (error) => {
                setErrors(error);
            },
            requestType: "POST",
            authorization: loadJWT(),
        });
    };

    const validationSchema = user == null ? Yup.object({
        first_name: Yup.string().required('First name is required'),
        last_name: Yup.string().required('Last name is required'),
        email: Yup.string()
                  .email("Invalid email address")
                  .required("Email address is required")
                  .min(6, 'Email must be at least 6 characters')
                  .max(50, 'Email cannot exceed 50 characters'),
        user_name: Yup.string().required("User name is required"),
        birthday: Yup.date().required('Birthday is required').nullable(),
        password: Yup.string().required('Password is required'),
        // password_confirmation: Yup.string()
        //    .oneOf([Yup.ref('password'), null], 'Passwords must match')
    }) 
    :
    Yup.object({
        first_name: Yup.string().required('First name is required'),
        last_name: Yup.string().required('Last name is required'),
        email: Yup.string()
                  .email("Invalid email address")
                  .required("Email address is required")
                  .min(6, 'Email must be at least 6 characters')
                  .max(50, 'Email cannot exceed 50 characters'),
        user_name: Yup.string().required("User name is required"),
        birthday: Yup.string().required('Birthday is required'),
    });

    return (
        <>
            <Formik
                    initialValues={initialValues}
                    enableReinitialize={true}
                    validationSchema={validationSchema}
                    onSubmit={(values) => {
                        let data = new FormData();
                        // if(values.images != undefined){
                        //     Array.from(values.images).forEach((file, index) => {
                        //         data.append(`images[${index}]`, file);
                        //     });
                        // }
                        for(const key in values){
                            // console.log(values[key]);
                            if(key === 'birthday'){
                                data.append(key, values[key].getFullYear() +'-'+ (values[key].getMonth()+1) +'-'+ values[key].getDate());
                                continue;
                            }
                            data.append(key, values[key]);
                        }
                        submitCallback(data);
                    }}
                >
                <Form>
                    <div className="row">
                        <div className="col-md-9">
                            <div className="form-group">
                                <CustomInput name={"first_name"} label={"First name"} class_name={"form-control"}/>
                            </div>
                            <div className="form-group">
                                <CustomInput name={"last_name"} label={"Last name"} class_name={"form-control"}/>
                            </div>
                            <div className="form-group">
                                <CustomInput name={"user_name"} label={"User name"} class_name={"form-control"}/>
                            </div>
                        </div>
                        <div className="col-md-3" style={{flex: 1,}}>
                            <AvatarInput name={"avatar"} class_name={"form-control"} avatarSrc={user?.avatar || null} />
                        </div>
                    </div>
                    <div className="form-group">
                        <label>Gender</label>
                        <Field as="select" className="form-control" name="gender">
                            <option value="Female">Female</option>
                            <option value="Male">Male</option>
                        </Field>
                    </div>
                    <div className="form-group">
                        <CustomInput name={"email"} label={"Email"} class_name={"form-control"}/>
                    </div>
                    <div className="form-group">
                        <CustomInput name={"phone"} label={"Phone"} class_name={"form-control"}/>
                    </div>
                    <div className="form-group">
                        <CustomInput name={"password"} label={"Password"} class_name={"form-control"}/>
                    </div>
                    <div className="form-group">
                        <DatePickerInput name={"birthday"} label={"Birthday"} class_name={"form-control"}/>
                    </div>
                    <div className="form-group">
                        <button type='submit' className="mb-2 mr-2 btn btn-primary">Save</button>
                    </div>
                </Form>
            </Formik>
        </>
    )
}

export default BaseUserForm
