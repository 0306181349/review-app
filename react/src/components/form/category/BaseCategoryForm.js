import React from 'react'
import { Form, Formik } from 'formik';
import CustomInput from '../CustomInput';
import * as Yup from 'yup';

const BaseCategoryForm = ({ category, submitCallback, resetFormHandler }) => {
    let initialValues = 
    {
        category_name: category?.category_name || '',
    };

    const validationSchema = Yup.object({
        category_name: Yup.string().required('Category name is required'),
    })

    return (
        <>
            <Formik
                initialValues={initialValues}
                enableReinitialize={true}
                validationSchema={validationSchema}
                onSubmit={submitCallback}
            >
                <Form>
                    <div className="form-group">
                        <CustomInput name={"category_name"} label={"Category name"} class_name={"form-control"}/>
                    </div>
                    <div className="form-group">
                        <button type='submit' className="mb-2 mr-2 btn btn-primary">Save</button>
                        <button type='reset' onClick={() => resetFormHandler()} className="mb-2 mr-2 btn btn-secondary">Cancel</button>
                    </div>
                </Form>
            </Formik>
        </>
    )
}

export default BaseCategoryForm