import React from 'react'
import { Form, Formik } from 'formik';
import CustomInput from '../CustomInput';
import * as Yup from 'yup';

const BaseReportTitleForm = ({ reportTitle, submitCallback, resetFormHandler }) => {
    let initialValues = 
    {
        report_title: reportTitle?.report_title || '',
    };

    const validationSchema = Yup.object({
        report_title: Yup.string().required('Report title is required'),
    })

    return (
        <>
            <Formik
                initialValues={initialValues}
                enableReinitialize={true}
                validationSchema={validationSchema}
                onSubmit={submitCallback}
            >
                <Form>
                    <div className="form-group">
                        <CustomInput name={"report_title"} label={"Report title"} class_name={"form-control"}/>
                    </div>
                    <div className="form-group">
                        <button type='submit' className="mb-2 mr-2 btn btn-primary">Save</button>
                        <button type='reset' onClick={() => resetFormHandler()} className="mb-2 mr-2 btn btn-secondary">Cancel</button>
                    </div>
                </Form>
            </Formik>
        </>
    )
}

export default BaseReportTitleForm