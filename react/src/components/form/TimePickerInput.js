import React from 'react';
import TimePicker from 'react-time-picker';
import {ErrorMessage, Field} from 'formik';

const getErrorDiv = message => {
    return (
        <div style={{color: '#dc3545'}}>
            {message}
        </div>
    );
};

const TimePickerInput = ({ class_name, name, label }) => {
    return (
        <>
            <label htmlFor={name}>{label}</label>
            <br />
            <Field name={name}>
                {
                    ({form, field}) => {
                        const { setFieldValue } = form;
                        const { value } = field;
                        return (
                        <>
                            <TimePicker
                              onChange = {val => setFieldValue(name, val)}
                              value = {value}
                              locale="sv-sv"
                              className={class_name}
                            />
                            <ErrorMessage
                                name={name}
                                render={getErrorDiv}
                            />
                        </>)
                    }
                }
            </Field>
        </>
    )
}

export default TimePickerInput
