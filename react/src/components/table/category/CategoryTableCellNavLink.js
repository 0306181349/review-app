import React from 'react'
import { Link } from 'react-router-dom'

const CategoryTableCellNavLink = ({id, text}) => {
    return (
        <div>
            <Link to={'/admin/category/'+id}> {text} </Link>
        </div>
    )
}

export default CategoryTableCellNavLink
