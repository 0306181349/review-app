import CategoryTableCellNavLink from './CategoryTableCellNavLink';
import React from 'react'

export const COLUMNS = [
    {
        Header: "Category name",
        accessor: "category_name",
        Cell: ({ cell: { value }, row: {original} }) => <CategoryTableCellNavLink id={original.id} text={value} />
    },
    {
        Header: "Created at",
        accessor: "created_at",
    }
]