import ReportTileCellNavLink from './ReportTitleCellNavLink'
import React from 'react'

export const COLUMNS = [
    {
        Header: "Report title",
        accessor: "report_title",
    },
    {
        Header: "Num of report",
        accessor: "report_num"
    },
]