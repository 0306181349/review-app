import LocationTableCellNavLink from './LocationTableCellNavLink';
import LocationCellAction from './LocationCellAction';
import React from 'react'

export const COLUMNS = [
    {
        Header: "Location name",
        accessor: "location_name",
        Cell: ({ cell: { value }, row: {original} }) => <LocationTableCellNavLink id={original.id} text={value} />
    },
    {
        Header: "Open time",
        accessor: "open_time"
    },
    {
        Header: "Closed time",
        accessor: "closed_time"
    },
    {
        Header: "Rating",
        accessor: "rating",
    },
    {
        Header: "",
        accessor: "id",
        Cell: ({ cell: { value }, row: {original} }) => <LocationCellAction id={original.id}/>,
    },
];