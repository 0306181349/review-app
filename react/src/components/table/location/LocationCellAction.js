import React from 'react'
import { Link } from 'react-router-dom'

const LocationCellAction = ({ id }) => {
    return (
        <div>
            <Link to={'/admin/location/edit/'+id} className="btn btn-outline-primary" style={{margin: "5px"}}> <i className="fas fa-edit"></i> </Link>
            <Link to={'/admin/location/'+id} className="btn btn-outline-danger" style={{margin: "5px"}}> <i className="fas fa-trash"></i> </Link>
        </div>
    )
}

export default LocationCellAction
