import PostTableCellNavLink from './PostTableCellNavLink';
import PostTableCellAction from './PostTableCellAction';
import React from 'react'

export const COLUMNS = [
    {
        Header: "Post title",
        accessor: "title",
        Cell: ({ cell: { value }, row: {original} }) => <Link to={`/admin/post/${original.id}`}> {original.title} </Link>
    },
    {
        Header: "Location",
        accessor: "location"
    },
    {
        Header: "View quantity",
        accessor: "view_quantity"
    },
    {
        Header: "Like quantity",
        accessor: "like_quantity",
    },
    {
        Header: "",
        accessor: "id",
        Cell: ({ cell: { value }, row: {original} }) => <PostTableCellAction id={original.id} />
    }
]