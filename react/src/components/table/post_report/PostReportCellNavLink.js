import React from 'react'
import { Link } from 'react-router-dom'

const PostReportCellNavLink = ({id}) => {
    return (
        <>
            <Link to={'/admin/report/post/show/'+id} className="btn btn-outline-primary"> <i className="fas fa-edit"></i> </Link>
        </>
    )
}

export default PostReportCellNavLink
