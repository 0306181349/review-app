import PostReportCellNavLink from './PostReportCellNavLink';
import React from 'react'

export const COLUMNS = [
    {
        Header: "Post",
        accessor: "post_title",
    },
    {
        Header: "Num of report",
        accessor: "post_report_num"
    },
    {
        Header: "",
        accessor: "post_id",
        Cell: ({ cell: { value }, row: {original} }) => <PostReportCellNavLink id={original.post_id}/>
    }
]