import LockedPostTableCellNavLink from './LockedPostTableCellNavLink';
import LockedPostTableCellAction from './LockedPostTableCellAction';
import React from 'react'

export const COLUMNS = [
    {
        Header: "Post title",
        accessor: "title",
        Cell: ({ cell: { value }, row: {original} }) => <LockedPostTableCellNavLink id={original.id} text={value} />
    },
    {
        Header: "Locked at",
        accessor: "locked_at"
    },
    {
        Header: "Locked by",
        accessor: "locked_by"
    },
    {
        Header: "",
        accessor: "id",
        Cell: ({ cell: { value }, row: {original} }) => <LockedPostTableCellAction id={original.id} />
    }
]