import React from 'react';
import { Link } from 'react-router-dom';

const LockedPostTableCellAction = ({id, text}) => {
    return (
        <div>
            <Link to={'/admin/locked-post/'+id} className="btn btn-outline-primary" style={{margin: "5px"}}> <i className="fas fa-edit"></i> </Link>
        </div>
    )
}

export default LockedPostTableCellAction
