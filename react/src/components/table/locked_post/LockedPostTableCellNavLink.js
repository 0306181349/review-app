import React from 'react'
import { Link } from 'react-router-dom'

const LockedPostTableCellNavLink = ({id, text}) => {
    return (
        <div>
            <Link to={'/admin/locked-post/'+id}> {text} </Link>
        </div>
    )
}

export default LockedPostTableCellNavLink
