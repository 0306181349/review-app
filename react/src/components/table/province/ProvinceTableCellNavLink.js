import React from 'react'
import { Link } from 'react-router-dom'

const ProvinceTableCellNavLink = ({id, text}) => {
    return (
        <div>
            <Link to={'/admin/province/'+id}> {text} </Link>
        </div>
    )
}

export default ProvinceTableCellNavLink
