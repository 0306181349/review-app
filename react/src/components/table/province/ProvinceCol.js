import ProvinceTableCellNavLink from './ProvinceTableCellNavLink';
import React from 'react'

export const COLUMNS = [
    {
        Header: "Province name",
        accessor: "province_name",
        Cell: ({ cell: { value }, row: {original} }) => <ProvinceTableCellNavLink id={original.id} text={value} />
    },
    {
        Header: "Created at",
        accessor: "created_at",
    }
]