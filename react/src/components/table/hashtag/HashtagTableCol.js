import React from 'react'

export const COLUMNS = [
    {
        Header: "Hashtag",
        accessor: "hashtag",
    },
    {
        Header: "Post",
        accessor: "post_quantity"
    },
    {
        Header: "Created at",
        accessor: "created_at"
    },
    {
        Header: "Created by",
        accessor: "created_by",
    }
]