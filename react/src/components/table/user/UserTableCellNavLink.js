import React from 'react'
import { Link } from 'react-router-dom'

const UserTableCellNavLink = ({id, text, className}) => {
    return (
        <div>
            <Link to={'/admin/users/'+id} className={className}> {text} </Link>
        </div>
    )
}

export default UserTableCellNavLink
