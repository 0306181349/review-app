import UserTableCellNavLink from './UserTableCellNavLink';
import UserTableCellAction from './UserTableCellAction';
import React from 'react'

export const COLUMNS = [
    {
        Header: "User Name",
        accessor: "user_name",
        Cell: ({ cell: { value }, row: {original} }) => 
        <Link to={`/admin/users/${original.id}`} className={className}> {text} </Link>
    },
    {
        Header: "Gender",
        accessor: "gender",
        Cell: ({ cell: { value }}) => {
            if(value == 0){
                return "Male"
            }else {
                return "Female"
            }
        }
    },
    {
        Header: "Email",
        accessor: "email"
    },
    {
        Header: "Rank",
        accessor: "rank",
    },
    {
        Header: "",
        accessor: "id",
        Cell: ({ cell: { value }, row: {original} }) => (
            <>
                <Link to={`/admin/users/edit/${original.id}`} className="btn btn-outline-primary" style={{margin: "5px"}}> <i className="fas fa-edit"></i> </Link>
                <Link to={`/admin/users/${original.id}`} className="btn btn-outline-danger" style={{margin: "5px"}}> <i className="fas fa-trash"></i> </Link>
            </>
        ),
        
        minWidth: "50"
    }
]