import React, { useState, useEffect, useMemo } from 'react';
import {ErrorMessage, Field} from 'formik';
import {useDropzone} from 'react-dropzone';

const getErrorDiv = message => {
    return (
        <div style={{color: '#dc3545'}}>
            {message}
        </div>
    );
};

const EditPostImageInput = ({ uploadedImages, name, label }) => {

    const thumbsContainer = {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 16
    };
    
    const thumb = {
      display: 'inline-flex',
      borderRadius: 2,
      border: '1px solid #eaeaea',
      margin: 8,
      width: 100,
      height: 100,
      padding: 4,
      boxSizing: 'border-box',
      backgroundColor: '#f2f2f2',
    };
    
    const thumbInner = {
      display: 'flex',
      minWidth: 0,
      overflow: 'hidden',
      position: 'relative',
    };
    
    const img = {
      display: 'block',
      width: 'auto',
      height: '100%'
    };

    const closeBtn = {
      position: 'absolute',
      top: '2px',
      right: '2px',
      zIndex: '100',
      cursor: 'pointer',
      backgroundColor: '#ff0000',
      color: '#fff',
      borderRadius: '50%',
      height: '24px',
      width: '24px',
    };
    
    const [files, setFiles] = useState([]);
    const [images, setImages] = useState([]);

    const removeImageHandler = (i, setFieldValue) => {
      const newFiles = [...files];
      newFiles.splice(i, 1);
      setFiles(newFiles);
      setFieldValue(name, newFiles);
    }

    const removeUploadedImageHandler = (i, setFieldValue) => {
        const newFiles = [...images];
        newFiles.splice(i, 1);
        setImages(newFiles);
        setFieldValue('remain_images', newFiles);
    }

    useEffect(() => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach(file => URL.revokeObjectURL(file.preview));
    }, [files, images]);

    useEffect(() => {
        setImages(uploadedImages);
    }, [uploadedImages]);

    return (
        <>
        <label htmlFor={name}>{label}</label>
            <br />
            <Field name={name}>
                {
                    ({ form }) => {
                        const { setFieldValue } = form;
                        const {getRootProps, getInputProps} = useDropzone({
                            accept: 'image/*',
                            onDrop: acceptedFiles => {
                              let uploadImg = new Array();
                              let uploadedImg = files;
                              uploadImg = acceptedFiles.map(file => Object.assign(file, {
                                preview: URL.createObjectURL(file)
                              }));
                              
                              if(uploadedImg.length != 0){
                                for(const i in uploadImg){
                                  if(uploadedImg.find(image => image.path == uploadImg[i].path) != undefined){
                                    continue;
                                  }else{
                                    uploadedImg.push(uploadImg[i]);
                                  }
                                }
                              }else{
                                for(const i in uploadImg){
                                  uploadedImg.push(uploadImg[i]);
                                }
                              }
                              // console.log(uploadImg);
                              // console.log(uploadedImg);
                              // console.log(files);
                              setFiles( 
                                uploadedImg
                                // acceptedFiles.map(file => Object.assign(file, {
                                //   preview: URL.createObjectURL(file)
                                // })) 
                              );
                              setFieldValue(name, uploadedImg);
                            }
                        });
                        return (
                          <>
                              <section className="container" style={{
                                  backgroundColor: '#fff',
                                  textAlign: 'center',
                                  cursor: 'pointer',
                                  border: '1px solid #ced4da',
                                  borderRadius: '.25rem',
                              }}>
                                <div {...getRootProps({className: 'dropzone'})}>
                                  <input {...getInputProps()} name="images" />
                                  <input hidden name="remain_images" />
                                  <p>Drag and drop some files here, or click to select files</p>
                                </div>
                                <aside style={thumbsContainer}>
                                  {
                                  images.length != 0 ?
                                      images.map((file, i) => (
                                          <div style={thumb} key={file.name} key={i}>
                                            <div style={thumbInner}>
                                              <span style={closeBtn} onClick={() => removeUploadedImageHandler(i, setFieldValue)}>&times;</span>
                                              <img
                                                src={file}
                                                style={img}
                                              />
                                            </div>
                                          </div>
                                      ))
                                      :
                                      ''
                                  }
                                  {
                                    files.map((file, i) => (
                                      <div style={thumb} key={file.name}>
                                        <div style={thumbInner}>
                                          <span style={closeBtn} onClick={() => removeImageHandler(i, setFieldValue)}>&times;</span>
                                          <img
                                            src={file.preview}
                                            style={img}
                                          />
                                        </div>
                                      </div>
                                    ))
                                  }
                                </aside>
                              </section>
                              <ErrorMessage
                                  name={name}
                                  render={getErrorDiv}
                              />
                          </>
                        )
                    }
                }
            </Field>
        </>
    )
}

export default EditPostImageInput