import React, { useState } from 'react';
import AsyncSelect from 'react-select/async';
import { makeRequest } from '../../utility/API';
import {ErrorMessage, Field} from 'formik';
import axios from 'axios';
import { baseURL } from '../../utility/API'
const getErrorDiv = message => {
    return (
        <div style={{color: '#dc3545'}}>
            {message}
        </div>
    );
};

const SelectLocation = ({ name, label, selected }) => {
    const [select, setSelect] = useState([]);

    const loadOptions = async (inputText, callback) => {
        let selectData = await axios.get(`${baseURL}/select/location?key=${inputText}`);
        const { data } = selectData; // get the data part from http response

        callback(data.location_data.map(i => ({label: i.location_name, value: i.location_id})));
    }

    // const loadOptions = inputValue => new Promise(resolve => {
    //     resolve(
    //         makeRequest({
    //             url: `http://review-app.local/select/location?key=${inputValue}`,
    //             successCallback: (data) => {
    //                 const { location_data } = data;
    //                 console.log(location_data.map(i => ({label: i.location_name, value: i.location_id})));
    //                 return location_data.map(i => ({label: i.location_name, value: i.location_id}));
    //             },
    //             failureCallback: (error) => {
    //                 console.log(error);
    //             },
    //             requestType: 'GET',
    //             // authorization: loadJWT(),
    //         })
    //     )
    // })

    return (
        <>
            <label htmlFor={name}>{label}</label>
            <br />
            <Field name={name}>
                {
                    ({ form }) => {
                        const { setFieldValue } = form;
                        return (
                            <>
                                <AsyncSelect 
                                    styles={{
                                      // Fixes the overlapping problem of the component
                                      menu: provided => ({ ...provided, zIndex: 9999 })
                                    }}
                                    maxMenuHeight={250}
                                    name={name}
                                    value={(select.value == undefined && selected != undefined && selected != null) ? selected : select}
                                    placeholder={"Type to search location"}
                                    onChange={(opt) => {
                                        setSelect(opt);
                                        setFieldValue(name, opt.value);
                                    }}
                                    loadOptions={loadOptions}
                                />
                                <ErrorMessage
                                    name={name}
                                    render={getErrorDiv}
                                />
                            </>
                        )
                    }
                }
            </Field>
        </>
    )
}

export default SelectLocation
