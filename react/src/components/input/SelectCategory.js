import React, { useState, useEffect, useMemo } from 'react';
import AsyncSelect from 'react-select/async';
import {ErrorMessage, Field} from 'formik';
import { makeRequest } from '../../utility/API';

const getErrorDiv = message => {
    return (
        <div style={{color: '#dc3545'}}>
            {message}
        </div>
    );
};

const SelectCategory = ({ name, label, selected, selectOptions }) => {
    const [select, setSelect] = useState([]);
    // const [category, setCategory] = useState([
    //     {"label" : "Ăn uống", 'value' : '1'},
    //     {"label" : "Ngủ nghỉ", 'value' : '2'},
    //     {"label" : "Du lịch", 'value' : '3'},
    // ]);

    const filterSelect = (inputValue) => {
        return selectOptions.filter(i =>
          i.label.toLowerCase().includes(inputValue.toLowerCase())
        );
    };
    
    const promiseOptions = inputValue =>
        new Promise(resolve => {
            resolve(filterSelect( inputValue ));
        });

    return (
        <>
        <label htmlFor={name}>{label}</label>
            <br />
            <Field name={name}>
                {
                    ({ form }) => {
                        const { setFieldValue } = form;
                        return (
                            <>
                                <AsyncSelect 
                                    styles={{
                                      // Fixes the overlapping problem of the component
                                      menu: provided => ({ ...provided, zIndex: 9999 })
                                    }}
                                    maxMenuHeight={250}
                                    name={name}
                                    value={(select.length == 0) ? selected : select}
                                    placeholder={"Type to search category"}
                                    onChange={(opt) => {
                                        setSelect(opt);
                                        setFieldValue(name, opt.value);
                                    }}
                                    loadOptions={promiseOptions}
                                />
                                <ErrorMessage
                                    name={name}
                                    render={getErrorDiv}
                                />
                            </>
                        )
                    }
                }
            </Field>
        </>
    )
}

export default SelectCategory