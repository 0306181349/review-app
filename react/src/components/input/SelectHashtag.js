import React, { useState, useEffect } from 'react';
import AsyncSelect from 'react-select/async';
import {ErrorMessage, Field} from 'formik';
import axios from 'axios';
import { baseURL } from '../../utility/API'

const getErrorDiv = message => {
    return (
        <div style={{color: '#dc3545'}}>
            {message}
        </div>
    );
};

const SelectHashtag = ({ name, label, selected }) => {
    const [select, setSelect] = useState([]);

    const loadOptions = async (inputText, callback) => {
        let selectData = await axios.get(`${baseURL}/select/hashtag?key=${inputText}`);
        const { data } = selectData; // get the data part from http response

        callback(data.hashtag_data.map(i => ({label: i.hashtag, value: i.hashtag_id})));
    }
  
      useEffect(() => {
        setSelect(selected);
      }, [selected]);

    // const loadOptions = inputValue => new Promise(resolve => {
    //     resolve(
    //         makeRequest({
    //             url: `http://review-app.local/select/location?key=${inputValue}`,
    //             successCallback: (data) => {
    //                 const { location_data } = data;
    //                 console.log(location_data.map(i => ({label: i.location_name, value: i.location_id})));
    //                 return location_data.map(i => ({label: i.location_name, value: i.location_id}));
    //             },
    //             failureCallback: (error) => {
    //                 console.log(error);
    //             },
    //             requestType: 'GET',
    //             // authorization: loadJWT(),
    //         })
    //     )
    // })

    return (
        <>
            <label htmlFor={name}>{label}</label>
            <br />
            <Field name={name}>
                {
                    ({ form }) => {
                        const { setFieldValue } = form;
                        return (
                            <>
                                <AsyncSelect 
                                    styles={{
                                      // Fixes the overlapping problem of the component
                                      menu: provided => ({ ...provided, zIndex: 9999 })
                                    }}
                                    maxMenuHeight={250}
                                    isMulti
                                    name={name}
                                    value={select}
                                    placeholder={"Type to search location"}
                                    onChange={(opts) => {
                                        console.log(select);
                                        console.log(opts);
                                        setSelect(opts);
                                        let hashTags = opts.map((opt) => {
                                            return opt.value;
                                        })
                                        setFieldValue(name, hashTags);
                                    }}
                                    loadOptions={loadOptions}
                                />
                                <ErrorMessage
                                    name={name}
                                    render={getErrorDiv}
                                />
                            </>
                        )
                    }
                }
            </Field>
        </>
    )
}

export default SelectHashtag
