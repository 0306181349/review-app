import React, { useState, useEffect, useMemo } from 'react';
import {ErrorMessage, Field} from 'formik';
import {useDropzone} from 'react-dropzone';

const getErrorDiv = message => {
    return (
        <div style={{color: '#dc3545'}}>
            {message}
        </div>
    );
};

const AvatarInput = ({ name, label, avatarSrc }) => {

    const thumbsContainer = {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 16
    };
    
    const thumb = {
      display: 'inline-flex',
      borderRadius: 2,
      border: '1px solid #eaeaea',
      margin: 8,
      height: '197px',
      width: '197px',
      padding: 4,
      boxSizing: 'border-box',
      backgroundColor: '#f2f2f2',
    };
    
    const thumbInner = {
      display: 'flex',
      overflow: 'hidden',
      position: 'relative',
    };
    
    const img = {
      display: 'block',
      width: 'auto',
      height: '100%',
    };

    const removeImgBtn = {
      position: 'absolute',
      top: '2px',
      right: '2px',
      cursor: 'pointer',
      backgroundColor: '#ff0000',
      color: '#fff',
      borderRadius: '50%',
      height: '24px',
      width: '24px',
    };
    
    const [file, setFile] = useState();

    const removeImageHandler = (setFieldValue) => {
      setFile();
      setFieldValue(name, "");
    }

    useEffect(() => {
      // Make sure to revoke the data uris to avoid memory leaks
      if(file != undefined){
        URL.revokeObjectURL(file.preview);
      }
    }, [file]);
    
    useEffect(() => {
        if(avatarSrc != undefined && avatarSrc != null){
            setFile({preview: avatarSrc});
        }
    }, [avatarSrc]);
    
    return (
        <>
        <label htmlFor={name}></label>
            <br />
            <Field name={name}>
                {
                    ({ form }) => {
                        const { setFieldValue } = form;
                        const {getRootProps, getInputProps} = useDropzone({
                            accept: 'image/*',
                            multiple: false,
                            onDrop: acceptedFiles => {
                              // let uploadedImg = file;
                              console.log(acceptedFiles[0]);
                              let uploadImg = Object.assign(acceptedFiles[0], {
                                preview: URL.createObjectURL(acceptedFiles[0])
                              });
                              
                            //   if(uploadedImg.length != 0){
                            //     for(const i in uploadImg){
                            //       if(uploadedImg.find(image => image.path == uploadImg[i].path) != undefined){
                            //         continue;
                            //       }else{
                            //         uploadedImg.push(uploadImg[i]);
                            //       }
                            //     }
                            //   }else{
                            //     for(const i in uploadImg){
                            //       uploadedImg.push(uploadImg[i]);
                            //     }
                            //   }
                              // console.log(uploadImg);
                              // console.log(uploadedImg);
                              // console.log(file);
                              setFile( 
                                uploadImg
                                // acceptedFiles.map(file => Object.assign(file, {
                                //   preview: URL.createObjectURL(file)
                                // })) 
                              );
                              setFieldValue(name, acceptedFiles[0]);
                            }
                        });
                        return (
                            <>
                                <div className="container" style={{
                                    backgroundColor: '#fff',
                                    textAlign: 'center',
                                    cursor: 'pointer',
                                    border: '1px solid #ced4da',
                                    borderRadius: '.25rem',
                                    height: '212px',
                                }}>
                                    
                                {
                                    file != undefined ?
                                    <div style={thumb} key={file.name}> 
                                        <div style={thumbInner}>
                                            <span style={removeImgBtn} onClick={() => removeImageHandler(setFieldValue)}>&times;</span>
                                            <img
                                              src={file.preview}
                                              style={img}
                                            />
                                        </div>
                                    </div>
                                    : 
                                    <div {...getRootProps({className: 'dropzone'})}>
                                        <input {...getInputProps()} name="images" />
                                        <p>Drag and drop some file here, or click to select file</p>
                                    </div>
                                }
                                  {/* <aside style={thumbsContainer}>
                                    {
                                      file.map((file, i) => (
                                        <div style={thumb} key={file.name}>
                                          <div style={thumbInner}>
                                            <span style={removeImgBtn} onClick={() => removeImageHandler(i, setFieldValue)}>&times;</span>
                                            <img
                                              src={file.preview}
                                              style={img}
                                            />
                                          </div>
                                        </div>
                                      ))
                                    }
                                  </aside> */}
                                </div>
                                <ErrorMessage
                                    name={name}
                                    render={getErrorDiv}
                                />
                            </>
                        )
                    }
                }
            </Field>
        </>
    )
}

export default AvatarInput