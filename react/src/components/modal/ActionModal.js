import React,{
    forwardRef,
    useState, 
    useImperativeHandle,
} from 'react'
import { motion, AnimatePresence } from 'framer-motion'
import ReactDOM from 'react-dom';

const ActionModal = forwardRef((props, ref) => {
    const [isOpen, setIsOpen] = useState(false);

    useImperativeHandle(ref, () => {
        return {
            open:  () => setIsOpen(true),
            close: () => setIsOpen(false),
        }
    })
    return ReactDOM.createPortal(
    (
        <AnimatePresence>
            {
                isOpen && (
                    <>
                        <motion.div
                          initial={{
                            opacity: 0
                          }}
                          animate={{
                            opacity: 1,
                            transition: {
                              duration: 0.3
                            }
                          }}
                          exit={{
                            opacity: 0,
                          }}
                          onClick={() => setIsOpen(false)}
                          style={{
                            position: 'fixed',
                            overflowY: 'hidden',
                            top: '0',
                            left: '0',
                            background: 'rgba(0, 0, 0, 0.6)',
                            zIndex: '1040',
                            width: '100vw',
                            height: '100vh',
                          }}
                        />
                        <motion.div // modal content
                          initial={{
                            scale: 0
                          }}
                          animate={{
                            scale: 1,
                            transition: {
                              duration: 0.3
                            }
                          }}
                          exit={{
                            scale: 0,
                          }}
                          style={{
                            position: 'fixed',
                            zIndex: "9999",
                            maxWidth: '500px',
                            width: '100%',
                            background: 'white',
                            margin: 'auto',
                            top: '0',
                            bottom: '0',
                            left: '0',
                            right: '0',
                            padding: '1rem',
                            border: '1px solid black',
                            borderRadius: '5px',
                            display: 'flex',
                            flexDirection: 'column',
                            height: 'max-content',
                          }}
                        >
                          <motion.div // modal header
                            style={{
                              display: 'flex',
                              alignItems: 'flex-start',
                              borderBottom: '1px solid #e9ecef',
                              padding: '.5rem',
                            }}
                          >
                            <h3>{props.header}</h3>
                          </motion.div>
                          <motion.div // modal body
                            style={{
                              display: 'flex',
                              borderBottom: '1px solid #e9ecef',
                              padding: '1rem',
                              overflowY: 'auto',
                              flexDirection: 'column',
                              maxHeight: '80vh',
                            }}
                          >
                            {props.children}
                          </motion.div>
                          <motion.div // modal footer
                            style={{
                              display: 'flex',
                              justifyContent: 'flex-end',
                              padding: '.5rem'
                            }}
                          >
                            <button
                              className="btn-transition btn btn-danger"
                              style={{
                                marginRight: '.5rem',
                              }}
                              onClick={() => {
                                setIsOpen(false);
                                props.confirmAction(props.actionId);
                              }}
                            >
                              Delete
                            </button>
                            <button
                              className="btn-transition btn btn-secondary"
                              onClick={() => {
                                setIsOpen(false);
                              }}
                            >
                              Close
                            </button>
                          </motion.div>
                        </motion.div>
                    </>
                )
            }
        </AnimatePresence>
    ), document.getElementById('portal'));
})

export default ActionModal
