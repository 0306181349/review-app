import React from 'react'
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

const CarouselItem = ({ post_image, post_video }) => {
    if(post_video != null){
      return (
        <Carousel 
        width={"100%"} 
        dynamicHeight={false} 
        showThumbs={false}
        centerMode={true}
        centerSlidePercentage={100}
        infiniteLoop={true}
        swipeable={true}
        >
          {
            post_image != undefined && post_image.map((image, index) => (
              <div style={{background: '#000000', border: '2px solid black', borderRadius: '5px'}} key={index}>
                <img src={image} style={{height: '500px', objectFit: 'contain'}} />
              </div>
            ))
          }
          <div style={{background: '#000000', border: '2px solid black', borderRadius: '5px'}} >
            <video controls style={{height: '500px', objectFit: 'contain'}} >
              <source src={post_video} type="video/mp4"/>
            </video>
          </div>
        </Carousel>
      )
    }
    return (
      <Carousel
        width={"100%"} 
        dynamicHeight={false} 
        showThumbs={false}
        centerMode={true}
        centerSlidePercentage={100}
        infiniteLoop={true}
        swipeable={true}
      >
        {
          post_image != undefined && post_image.map((image, index) => (
            <div style={{background: '#000000', border: '2px solid black', borderRadius: '5px'}} key={index}>
              <img src={image} style={{height: '500px', objectFit: 'contain'}} />
            </div>
          ))
        }
      </Carousel>
    )
}

export default CarouselItem
