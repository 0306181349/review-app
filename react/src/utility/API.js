import React from 'react';
import axios from 'axios';

export const baseURL = 'http://review-app.local';
// export const baseURL = 'http://27.68.125.22';
const API = axios.create({
    baseURL: baseURL,
    responseType: "json",
});
const getRequestConfiguration = (authorization) => {
    const headers = {
        "Content-Type": "application/json",
    };
    if(authorization) headers.authorization = `Bearer ${authorization}`;
    return { headers };
}
export const makeRequest = ({
    url,
    values,
    successCallback,
    failureCallback,
    requestType = 'POST',
    authorization = null,
}) => {
    const requestConfiguration = getRequestConfiguration(authorization);
    let promise;
    switch (requestType) {
        case 'GET':
            promise = API.get(url, requestConfiguration);
            break;
        case 'POST':
            promise = API.post(url, values ,requestConfiguration);
            break;
        case 'DELETE':
            promise = API.delete(url ,requestConfiguration);
            break;
        default: 
            return;
    }
    
    promise
        .then((response) => {
            // server return HTML => login session expired 
            // if(response.headers['content-type'] === "text/html; charset=UTF-8"){
            //     window.location.replace("http://review-app.local/admin/login");
            // }else{
                // get the data part of http response
                const { data } = response;
                console.log(response);
                // then use data to do what ever u want
                successCallback(data);
           // }
        })
        .catch((error) => {
            if (error.response) {
                switch (error.response.status){
                    case 401:
                        window.location.replace("http://review-app.local/admin/login");
                        failureCallback(error.response.status);
                        failureCallback(error.response.data);
                    default:
                        failureCallback(error.response.status);
                        failureCallback(error.response.data);
                }
            }
        });
};
