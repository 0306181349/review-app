import React from 'react';
import ReactDOM from 'react-dom';
import LoginPage from './LoginPage';


let container = document.getElementById('root');
let component = <LoginPage />

ReactDOM.render(component, container);
