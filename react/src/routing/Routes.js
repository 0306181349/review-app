import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Dashboard from '../pages/dashboard/Dashboard';
import PageNotFound from '../pages/PageNotFound';
import UsersTables from '../pages/user/UsersTables';
import ShowUser from '../pages/user/ShowUser';
import ChangePass from '../pages/user/ChangePass';
import PostTables from '../pages/post/PostTables';
import { PrivateRoutes } from './PrivateRoutes';
import EditUser from '../pages/user/EditUser';
import AddUser from '../pages/user/AddUser';
import LocationTable from '../pages/location/LocationTable';
import ShowLocation from '../pages/location/ShowLocation';
import AddLocation from '../pages/location/AddLocation';
import EditLocation from '../pages/location/EditLocation';
import ShowPost from '../pages/post/ShowPost';
import AddPost from '../pages/post/AddPost';
import EditPost from '../pages/post/EditPost';
import ProvinceTable from '../pages/province/ProvinceTable';
import CategoryTable from '../pages/category/CategoryTable';
import HashtagTable from '../pages/hashtag/HashtagTable'
import PostReportTable from '../pages/post_report/PostReportTable';
import ReportTitleTable from '../pages/report_title/ReportTitleTable';
import ShowPostReport from '../pages/post_report/ShowPostReport';
import LockedPostTable from '../pages/locked_post/LockedPostTable';
import PostUnlockRequestTable from '../pages/locked_post/PostUnlockRequestTable';
import ShowLockedPost from '../pages/locked_post/ShowLockedPost';
import PostByHashtagTable from '../pages/hashtag/PostByHashtagTable'

const Routes = () => (
    <Switch>
        <Route path="/admin/dashboard" exact component={Dashboard} />
        <Route path={"/admin/users"} exact component={UsersTables}/>
        <Route path={"/admin/users/edit/:id"} exact component={EditUser}/>
        <Route path={"/admin/users/create"} exact component={AddUser}/>
        <Route path={"/admin/users/:id"} exact component={ShowUser}/>
        <Route path={"/admin/users/changepass/:id"} exact component={ChangePass}/>
        
        <Route path={"/admin/post"} exact component={PostTables}/>
        <Route path={"/admin/post/create"} exact component={AddPost}/>
        <Route path={"/admin/post/:id"} exact component={ShowPost}/>
        <Route path={"/admin/post/edit/:id"} exact component={EditPost}/>

        <Route path={"/admin/location"} exact component={LocationTable} />
        <Route path={"/admin/location/create"} exact component={AddLocation} />
        <Route path={"/admin/location/:id"} exact component={ShowLocation}/>
        <Route path={"/admin/location/edit/:id"} exact component={EditLocation} />

        <Route path={"/admin/province"} exact component={ProvinceTable} />

        <Route path={"/admin/category"} exact component={CategoryTable} />

        <Route path={"/admin/hashtag"} exact component={HashtagTable} />
        <Route path={"/admin/hashtag/post/:id"} exact component={PostByHashtagTable} />
        <Route path={"/admin/report/post"} exact component={PostReportTable} />
        <Route path={"/admin/report/post/show/:id"} exact component={ShowPostReport} />
        <Route path={"/admin/report-title"} exact component={ReportTitleTable} />

        <Route path={"/admin/locked-post/"} exact component={LockedPostTable} />
        <Route path={"/admin/locked-post/unlock-request"} exact component={PostUnlockRequestTable} />
        <Route path={"/admin/locked-post/:id"} exact component={ShowLockedPost} />
        <Route path={'*'} component={PageNotFound}/>
    </Switch>
)
export default Routes