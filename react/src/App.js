import React from 'react'
import { BrowserRouter } from 'react-router-dom';
import Routes from './routing/Routes';
import Header from './layout/Header';
import Sidebar from './layout/Sidebar';
import Footer from './layout/Footer';
import {clearState, loadJWT, loadUser} from './utility/LocalStorage';
import { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { makeRequest } from './utility/API';

const App = () => {
    const [user, setUser] = useState([]);
    useEffect(() => {
        makeRequest({
        url: `admin/user-info`,
        successCallback: (data) => {
            console.log(data);
            const { user_data } = data;
            setUser(user_data);
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'GET',
      })
    }, []); 
    
    return (
        <>
            <div className="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
                <BrowserRouter>
                    <Header user={user} />
                        <div className="app-main">
                            <Sidebar />
                            <div className="app-main__outer">
                                <Routes />
                                <Footer />
                            </div>
                        </div>
                </BrowserRouter>
            </div>
        </>
    )
}

export default App
