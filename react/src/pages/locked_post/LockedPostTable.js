import React, {useMemo, useEffect, useState} from 'react';
import { makeRequest } from '../../utility/API';
import { useTable, useGlobalFilter, usePagination } from 'react-table';
import { COLUMNS } from '../../components/table/locked_post/LockedPostTableCol';
import TableSearchSpan from '../../components/table/TableSearchSpan';
import { loadJWT } from '../../utility/LocalStorage';
import { NavLink } from 'react-router-dom';

const LockedPostTable = () => {
  const [posts, setPost] = useState([]);
  useEffect(() => {
    makeRequest({
        url: 'admin/post/locked/all',
        successCallback: (data) => {
            const { posts } = data;
            setPost(posts);
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'GET',
        authorization: loadJWT(),
    });
  }, []);
  const col = useMemo(() => COLUMNS, []);
  
  const tableInstance = useTable({
    columns: col,
    data: posts,
    initialState: { pageSize: 10 }
  }, useGlobalFilter, usePagination)
  
  const { 
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    nextPage,
    previousPage,
    gotoPage,
    pageCount,
    prepareRow,
    state,
    canPreviousPage,
    canNextPage,
    pageOptions,
    setGlobalFilter,
  } = tableInstance;
  
  const { globalFilter } = state;
  const { pageIndex } = state;

  return (
      <div className="app-main__inner">
        <div className="app-page-title">
          <div className="page-title-wrapper">
            <div className="page-title-heading">
              <div className="page-title-icon">
                <i className="pe-7s-drawer icon-gradient bg-happy-itmeo">
                </i>
              </div>
              <div>Locked Post Table
              </div>
            </div>
          </div>
        </div>  
        <ul className="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
            <li className="nav-item">
                <NavLink className="nav-link" activeClassName="nav-link active" exact to={'/admin/locked-post'}>
                    <span>Locked Post</span>
                </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" activeClassName="nav-link active" exact to={'/admin/locked-post/unlock-request'}>
                    <span>Unlock Request</span>
                </NavLink>
            </li>
        </ul>       
        <div className="row">
          <div className="col-lg-12">
            <div className="main-card mb-3 card">
              <div className="card-body">
                <TableSearchSpan filter={ globalFilter } setFilter={ setGlobalFilter }/>
                <table {...getTableProps()} className="table table-hover table-bordered">
                  <thead> 
                    { 
                      headerGroups.map((headerGroup) => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                          {headerGroup.headers.map((col) => (
                            <th {...col.getHeaderProps()}>{ col.render("Header") }</th>
                          ))}
                        </tr>
                      )) 
                    }
                  </thead>
                  <tbody {...getTableBodyProps()}>
                    {
                      page.map(row => {
                        prepareRow(row)
                        return (
                          <tr {...row.getRowProps()}>
                            {
                              row.cells.map( cell => {
                                return (
                                  <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                )
                              })
                            }
                          </tr>
                        )
                      })
                    }
                  </tbody>
                </table>
                <span>
                  Page{' '} { pageIndex + 1 } of { pageOptions.length } {' '}
                </span>
                <span>
                  | Go to page: {' '}
                  <input type="number" defaultValue={pageIndex + 1} 
                    onChange={e => {
                    const pageNumber = e.target.value ? Number(e.target.value) - 1 : 0
                    gotoPage(pageNumber);
                  }} style={{ width: '50px', marginRight: '5px', marginLeft: '5px'}}/>
                </span>
                <div className="btn-group">
                  <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" onClick={() => gotoPage(0)} disabled={!canPreviousPage}>{'<<'}</button>
                  <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" disabled={!canPreviousPage} onClick={() => previousPage()}>Previous</button>
                  <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" disabled={!canNextPage} onClick={() => nextPage()}>Next</button>
                  <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" onClick={() => gotoPage( pageCount - 1 )} disabled={!canNextPage}>{'>>'}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>    

  )
}

export default LockedPostTable
