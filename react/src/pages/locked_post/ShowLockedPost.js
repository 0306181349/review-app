import React from 'react';
import { useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { makeRequest } from '../../utility/API';
import { Redirect } from 'react-router';
import { loadJWT } from '../../utility/LocalStorage';
// import CarouselItem from '../../components/carousel/CarouselItem';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

const ShowLockedPost = () => {
    let { id } = useParams();
    const [post, setPost] = useState([]);
    const [shouldRedirect, setShouldRedirect] = useState(false);
    
    useEffect(() => {
        makeRequest({
            url: `admin/post/locked/${id}`,
            successCallback: (data) => {
                const { post_data } = data;
                // console.log(post_data);
                setPost(post_data);
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'GET',
            authorization: loadJWT(),
        });
    },[]);
    
    const unlockPostHandler = () => {
        makeRequest({
            url: `admin/post/unlock/${id}`,
            successCallback: (data) => {
                const { post_data } = data;
                // console.log(post_data);
                setShouldRedirect(true);
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'GET',
            authorization: loadJWT(),
        });
    }

    const unlockRejectHandler = () => {
        makeRequest({
            url: `admin/post/unlock-reject/${id}`,
            successCallback: (data) => {
                const { post_data } = data;
                // console.log(post_data);
                setShouldRedirect(true);
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'GET',
            authorization: loadJWT(),
        });
    }
    return shouldRedirect ? ( <Redirect to='/admin/locked-post' /> ) : (
        <> 
              <div className="app-main__inner">
                <div className="app-page-title">
                  <div className="page-title-wrapper">
                    <div className="page-title-heading">
                      <div className="page-title-icon">
                        <i className="pe-7s-drawer icon-gradient bg-happy-itmeo">
                        </i>
                      </div>
                      <div>Post detail
                          <div className="page-title-subheading">
                      </div>
                    </div>
                   </div>
                  </div>
                </div>            
                <div className="row">
                  <div className="col-lg-12">
                    <div className="main-card mb-3 card">
                      <div className="card-body">
                              <div className="form-group">
                                <Carousel 
                                    width={"100%"} 
                                    dynamicHeight={false} 
                                    showThumbs={false}
                                    centerMode={true}
                                    centerSlidePercentage={100}
                                    infiniteLoop={true}
                                >
                                      {
                                          post.post_image != undefined ? post.post_image.map((image, index) => (
                                              <div style={{background: '#000000', border: '2px solid black', borderRadius: '5px'}} key={index}>
                                                <img src={image} style={{height: '500px', objectFit: 'contain'}} />
                                              </div>
                                          )) 
                                          : 
                                          ''
                                      }
                                </Carousel>
                              </div>
                              <div className="form-group">
	        		          	<label>Post title</label>
	        		          	<input type="text" className="form-control" value={(post.title || '')} readOnly />
	        		          </div>
	        		          <div className="form-group">
	        		          	<label>Hashtag</label>
	        		          	<input type="text" className="form-control" value={(post.show_hashtag || '')} readOnly />
	        		          </div>
	        		          <div className="form-group">
	        		          	<label>Rating</label>
	        		          	<input type="text" className="form-control" value={(post.rating || '')} readOnly />
	        		          </div>
	        		          <div className="form-group">
	        		          	<label>Location</label>
	        		          	<input type="text" className="form-control" value={(post.location_name || '')} readOnly />
	        		          </div>
	        		          <div className="form-group">
	        		          	<label>Category</label>
	        		          	<input type="text" className="form-control" value={(post.category_name || '')} readOnly />
	        		          </div>
	        		          <div className="form-group">
	        		          	<label>View quantity</label>
	        		          	<input type="text" className="form-control" value={(post.view_quantity || '')} readOnly />
	        		          </div>
	        		          <div className="form-group">
	        		          	<label>Content</label>
	        		          	<div style = {{
                                  backgroundColor: "#e9ecef",
                                  border: "1px solid #e9ecef",
                                  borderRadius: ".25rem",
                                  padding: "10px",
                                }} dangerouslySetInnerHTML={{__html: (post.content || '') }} />
	        		          </div>
	        		          <div className="form-group">
	        		          	<label>Locked by</label>
	        		          	<input type="text" className="form-control" value={(post.locked_by || '')} readOnly />
	        		          </div>
	        		          <div className="form-group">
	        		          	<label>Violation</label>
	        		          	<div style = {{
                                  backgroundColor: "#e9ecef",
                                  border: "1px solid #e9ecef",
                                  borderRadius: ".25rem",
                                  padding: "10px",
                                }}>
                                    <ul>
                                    {
                                        post.post_violation?.map((item, index) => (
                                            <li key={index}>{item}</li>
                                        ))
                                    }
                                    </ul>
                                </div>
	        		          </div>
                              <div className="form-group">
                                <button className="mb-2 mr-2 btn-transition btn btn-outline-primary" onClick={() => {unlockPostHandler(post.post_id)}}>Unlock</button>
                                {
                                    post.unlock_requested == true ? <button className="mb-2 mr-2 btn-transition btn btn-outline-secondary" onClick={() => {unlockRejectHandler(post.post_id)}}>Reject</button> : ''
                                }
                              </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
        </>
    )
}

export default ShowLockedPost
