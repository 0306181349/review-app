import React from 'react';
import { Link, useParams } from 'react-router-dom';
import { useEffect, useState, useRef } from 'react';
import { makeRequest } from '../../utility/API';
import { Redirect } from 'react-router';
import { loadJWT } from '../../utility/LocalStorage';
import ActionModal from '../../components/modal/ActionModal';

const ShowLocation = () => {
    let { id } = useParams();
    const [location, setLocation] = useState([]);
    const [shouldRedirect, setShouldRedirect] = useState(false);
    const [verify, setVerify] = useState(false);
    const [verifyBy, setVerifyBy] = useState('');
    const delModalRef = useRef();

    useEffect(() => {
        makeRequest({
            url: `admin/location/show/${id}`,
            successCallback: (data) => {
                const { location_data } = data;
                setLocation(location_data);
                setVerify(location_data.is_verify);
                setVerifyBy(location_data.verified_by || '');
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'GET',
            authorization: loadJWT(),
        });
    }, []);

    const deleteLocation = (id) => {
        makeRequest({
            url: `admin/location/delete/${id}`,
            successCallback: (data) => {
                setShouldRedirect(true);
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'DELETE',
            authorization: loadJWT(),
        });
    };

    const verifyLocation = () => {
      makeRequest({
        url: `admin/location/verify/${id}`,
        successCallback: (data) => {
          setVerify(data.is_verify);
          setVerifyBy(data.verify_by || '');
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'GET',
        authorization: loadJWT(),
    });
    }

    return shouldRedirect ? ( <Redirect to='/admin/location' /> ) : (
        <> 
              <div className="app-main__inner">
                <div className="app-page-title">
                  <div className="page-title-wrapper">
                    <div className="page-title-heading">
                      <div className="page-title-icon">
                        <i className="pe-7s-drawer icon-gradient bg-happy-itmeo">
                        </i>
                      </div>
                      <div>Location detail
                          <div className="page-title-subheading">
                      </div>
                    </div>
                   </div>
                  </div>
                </div>            
                <div className="row">
                  <div className="col-lg-12">
                    <div className="main-card mb-3 card">
                      <div className="card-body">
                              <div className="form-group">
	        		          	<label>Location name</label>
	        		          	<input type="text" className="form-control" value={(location.location_name || '')} readOnly />
	        		          </div>
	        		          <div className="form-group">
	        		          	<label>Address</label>
	        		          	<input type="text" className="form-control" value={(location.address || '')} readOnly />
	        		          </div>
	        		          <div className="form-group">
	        		          	<label>Phone number</label>
	        		          	<input type="text" className="form-control" value={(location.phone_number || '')} readOnly />
	        		          </div>
                              <div className="row">
                                <div className="col-md-6">
	        		              <div className="form-group">
	        		              	<label>Open time</label>
	        		              	<input type="text" className="form-control" value={(location.open_time || '')} readOnly />
	        		              </div>
                                </div>
                                <div className="col-md-6">
	        		              <div className="form-group">
	        		              	<label>Closed time</label>
	        		              	<input type="text" className="form-control" value={(location.closed_time || '')} readOnly />
	        		              </div>
                                </div>
                              </div>
                              <div className="row">
                                <div className="col-md-6">
	        		              <div className="form-group">
	        		              	<label>Lowest price</label>
	        		              	<input type="text" className="form-control" value={(location.lowest_price || '')} readOnly />
	        		              </div>
                                </div>
                                <div className="col-md-6">
	        		              <div className="form-group">
	        		              	<label>Highest price</label>
	        		              	<input type="text" className="form-control" value={(location.highest_price || '')} readOnly />
	        		              </div>
                                </div>
                              </div>
	        		            <div className="form-group">
	        		            	<label>Province</label>
	        		            	<input type="text" className="form-control" value={(location.province_name || '')} readOnly />
	        		            </div>
                          <div className="row">
                            <div className="col-md-1">
                              <div className="form-group">
	        		              	  <label>Verify</label>
	        		              	  <input type="checkbox" style={{
                                  height: 38,
                                  width: 38,
                                }} className="form-check" checked={verify ? true : false || false} readOnly />
	        		                </div>
                            </div>
                            <div className="col-md-11">
                              {
                                verify ? (
                                    <div className="form-group">
                                      <label>Verified By</label>
                                      <input type="text" className="form-control" value={(verifyBy || '')} readOnly />
                                    </div>
                                ) : ''
                              }
                            </div>
                          </div>
                              <div className="form-group">
                                <Link to={`/admin/location/edit/${location.id}`}> <button className="mb-2 mr-2 btn-transition btn btn-outline-primary">Edit</button> </Link>
                                <button className="mb-2 mr-2 btn-transition btn btn-outline-success" onClick={() => {verifyLocation(location.id)}}>{verify ? 'Unverify' : 'Verify'}</button>
                                <button className="mb-2 mr-2 btn-transition btn btn-outline-danger" onClick={() => {delModalRef.current.open()}}>Delete</button>
	    		              </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ActionModal 
                    header={'Warning'} 
                    confirmAction={deleteLocation} 
                    actionId={location.id} 
                    ref={delModalRef}
                >
                    <h5>Do you really want to delete this location ? (this action can not be undo)</h5>
                </ActionModal>
        </>
    )
}

export default ShowLocation
