import React from 'react';
import BaseLocationForm from '../../components/form/location/BaseLocationForm';
import { Redirect } from 'react-router-dom';
import { useState } from 'react';
import FailureAlert from '../../components/alert/FailureAlert';


const AddLocation = () => {
    const [shouldRedirect, setShouldRedirect] = useState(false);
    const [redirectParam, setRedirectParam] = useState(0);
    const [errors, setErrors] = useState(null);

    return redirectParam!=0 ? ( <Redirect to={`/admin/location/${redirectParam}`} /> ) : (
        <> 
              <div className="app-main__inner">
                <div className="app-page-title">
                  <div className="page-title-wrapper">
                    <div className="page-title-heading">
                      <div className="page-title-icon">
                        <i className="pe-7s-drawer icon-gradient bg-happy-itmeo">
                        </i>
                      </div>
                      <div>Add location
                          <div className="page-title-subheading">
                      </div>
                    </div>
                   </div>
                  </div>
                </div>            
                <div className="row">
                  <div className="col-lg-12">
                    <div className="main-card mb-3 card">
                      {errors && <FailureAlert errors={errors}/>}
                      <div className="card-body">
                          <BaseLocationForm location={null} setRedirectParam={setRedirectParam} setErrors={setErrors} setShouldRedirect={setShouldRedirect} />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
        </>
    )
}

export default AddLocation
