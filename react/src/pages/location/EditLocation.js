import React from 'react';
import BaseLocationForm from '../../components/form/location/BaseLocationForm';
import { useParams, Redirect } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { makeRequest } from '../../utility/API';
import { loadJWT } from '../../utility/LocalStorage';
import FailureAlert from '../../components/alert/FailureAlert';

const EditLocation = () => {
    const { id } = useParams();
    const [errors, setErrors] = useState(null);
    const [location, setLocation] = useState([]);
    const [redirectParam, setRedirectParam] = useState(0);

    useEffect(() => {
        makeRequest({
            url: `admin/location/show/${id}`,
            successCallback: (data) => {
                const { location_data } = data;
                setLocation(location_data);
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'GET',
            authorization: loadJWT(),
        });
    },[]);


    return redirectParam!=0 ? ( <Redirect to={`/admin/location/${redirectParam}`} /> ) : (
        <>
          <div className="app-main__inner">
            <div className="app-page-title">
              <div className="page-title-wrapper">
                <div className="page-title-heading">
                  <div className="page-title-icon">
                    <i className="pe-7s-drawer icon-gradient bg-happy-itmeo">
                    </i>
                  </div>
                  <div>Edit location
                      <div className="page-title-subheading">
                  </div>
                </div>
               </div>
              </div>
            </div>            
            <div className="row">
              <div className="col-lg-12">
                <div className="main-card mb-3 card">
                {errors && <FailureAlert errors={errors}/>}
                  <div className="card-body">
                      <BaseLocationForm location={location} setRedirectParam={setRedirectParam} setErrors={setErrors} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </>
    )
}

export default EditLocation
