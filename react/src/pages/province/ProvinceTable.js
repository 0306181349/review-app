import React, {useMemo, useEffect, useState, useRef} from 'react';
import { makeRequest } from '../../utility/API';
import { useTable, useGlobalFilter, usePagination } from 'react-table';
// import { COLUMNS } from '../../components/table/province/ProvinceCol';
import TableSearchSpan from '../../components/table/TableSearchSpan';
import { loadJWT } from '../../utility/LocalStorage';
import { Link } from 'react-router-dom';
import BaseProvinceForm from '../../components/form/province/BaseProvinceForm';
import ActionModal from '../../components/modal/ActionModal';

const ProvinceTable = () => {
  const [province, setProvince] = useState([]);
  const [currentEditProvince, setCurrentEditProvince] = useState({});
  const [currentDeleteProvince, setCurrentDeleteProvince] = useState({});
  const delModalRef = useRef();
  const editProvinceHandler = (id) => {
    // console.log(id)
    let provinceData = [...province].filter((province) => province.id == id)[0];
    console.log(provinceData)
    // console.log(provinceData);
    setCurrentEditProvince(provinceData);
  }

  const resetFormHandler = () => {
    setCurrentEditProvince({});
  }

  useEffect(() => {
    makeRequest({
        url: 'admin/province/all',
        successCallback: (data) => {
            const { province } = data;
            setProvince(province);
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'GET',
        authorization: loadJWT(),
    });
  }, []);

  const submitCallback = (values, { resetForm }) => {
    currentEditProvince.id != undefined ?
    (
      makeRequest({
        url: `admin/province/edit/${currentEditProvince.id}`,
        values,
        successCallback: (data) => {
            const { province_data } = data;
            let newProvince = [...province];
            let indexOfEditeProvince = newProvince.findIndex((province) => province.id == province_data.id);
            newProvince[indexOfEditeProvince] = province_data;
            setProvince(newProvince);
            setCurrentEditProvince({});
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'POST',
        authorization: loadJWT(),
      })
    )
    :
    (
      makeRequest({
        url: `admin/province/add`,
        values,
        successCallback: (data) => {
            const { province_data } = data;
            let newProvince = [...province];
            newProvince.unshift(province_data);
            setProvince(newProvince);
            resetForm()
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'POST',
        authorization: loadJWT(),
      })
    )
  }
  
  const deleteProvinceHandler = (id) => {
    makeRequest({
        url: `admin/province/delete/${id}`,
        successCallback: (data) => {
          const provinceData = [...province].filter((province) => province.id != currentDeleteProvince.id);
          setCurrentDeleteProvince({});
          setProvince(provinceData);
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'DELETE',
        authorization: loadJWT(),
    });
  }

  const COLUMNS = [
    {
        Header: "Province name",
        accessor: "province_name",
    },
    {
        Header: "Created at",
        accessor: "created_at",
    },
    {
      Header: "",
      accessor: "id",
      Cell: ({ cell: { value }, row: {original} }) => (
        <>
          <button onClick={() => {editProvinceHandler(original.id)}} className="btn btn-outline-primary" style={{margin: "5px"}}> <i className="fas fa-edit"></i> </button>
          <button onClick={() => {setCurrentDeleteProvince(original); delModalRef.current.open()}} className="btn btn-outline-danger" style={{margin: "5px"}}> <i className="fas fa-trash"></i> </button>
        </>
      )
    }
  ]
  const col = useMemo(() => COLUMNS, [province]);
  const tableInstance = useTable({
    columns: col,
    data: province,
    initialState: { pageSize: 5 }
  }, useGlobalFilter, usePagination)
  const { 
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    nextPage,
    previousPage,
    gotoPage,
    pageCount,
    prepareRow,
    state,
    canPreviousPage,
    canNextPage,
    pageOptions,
    setGlobalFilter,
  } = tableInstance;

  const { globalFilter } = state;
  const { pageIndex } = state;

  return (
    <>
      <div className="app-main__inner">
        <div className="app-page-title">
          <div className="page-title-wrapper">
            <div className="page-title-heading">
              <div className="page-title-icon">
                <i className="pe-7s-drawer icon-gradient bg-happy-itmeo">
                </i>
              </div>
              <div>Province Table
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <div className="main-card mb-3 card">
              <div className="row">
                <div className="col-md-4">
                  <div className="card-body">
                    <BaseProvinceForm 
                      province={currentEditProvince}
                      submitCallback={submitCallback}
                      resetFormHandler={resetFormHandler}
                    />
                  </div>
                </div>
                <div className="col-md-8">
                  <div className="card-body">
                    <TableSearchSpan filter={ globalFilter } setFilter={ setGlobalFilter }/>
                    <table {...getTableProps()} className="table table-hover table-bordered">
                      <thead> 
                        { 
                          headerGroups.map((headerGroup) => (
                            <tr {...headerGroup.getHeaderGroupProps()}>
                              {headerGroup.headers.map((col) => (
                                <th {...col.getHeaderProps()}>{ col.render("Header") }</th>
                              ))}
                            </tr>
                          )) 
                        }
                      </thead>
                      <tbody {...getTableBodyProps()}>
                        {
                          page.map(row => {
                            prepareRow(row)
                            return (
                              <tr {...row.getRowProps()}>
                                {
                                  row.cells.map( cell => {
                                    return (
                                    <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                    )
                                  })
                                }
                              </tr>
                            )
                          })
                        }
                      </tbody>
                    </table>
                    <span>
                      Page{' '} { pageIndex + 1 } of { pageOptions.length } {' '}
                    </span>
                    <span>
                      | Go to page: {' '}
                      <input type="number" defaultValue={pageIndex + 1} 
                        onChange={e => {
                        const pageNumber = e.target.value ? Number(e.target.value) - 1 : 0
                        gotoPage(pageNumber);
                      }} style={{ width: '50px', marginRight: '5px', marginLeft: '5px'}}/>
                    </span>
                    <div className="btn-group">
                      <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" onClick={() => gotoPage(0)} disabled={!canPreviousPage}>{'<<'}</button>
                      <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" disabled={!canPreviousPage} onClick={() => previousPage()}>Previous</button>
                      <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" disabled={!canNextPage} onClick={() => nextPage()}>Next</button>
                      <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" onClick={() => gotoPage( pageCount - 1 )} disabled={!canNextPage}>{'>>'}</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ActionModal
                header={'Warning'}
                confirmAction={deleteProvinceHandler} 
                actionId={currentDeleteProvince.id} 
                ref={delModalRef}
      >
        <h5>Do you really want to delete this province ? (this action can not be undo)</h5>
      </ActionModal>
    </>
  )
}

export default ProvinceTable
