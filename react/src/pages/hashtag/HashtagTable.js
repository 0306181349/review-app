import React, {useMemo, useEffect, useState, useRef} from 'react';
import { makeRequest } from '../../utility/API';
import { useTable, useGlobalFilter, usePagination } from 'react-table';
// import { COLUMNS } from '../../components/table/hashtag/HashtagTableCol';
import TableSearchSpan from '../../components/table/TableSearchSpan';
import { loadJWT } from '../../utility/LocalStorage';
import BaseHashtagForm from '../../components/form/hashtag/BaseHashtagForm';
import { Link } from 'react-router-dom';
import ActionModal from '../../components/modal/ActionModal';

const HashtagTable = () => {
  const [hashtags, setHashtag] = useState([]);
  const [currentEditHashtag, setCurrentEditHashtag] = useState({});
  const [currentDeleteHashtag, setCurrentDeleteHashtag] = useState({});
  const delModalRef = useRef();

  const editHashtagHandler = (id) => {
    let hashtagData = [...hashtags].filter((hashtag) => hashtag.hashtag_id == id)[0];
    setCurrentEditHashtag(hashtagData);
  }
  const resetFormHandler = () => {
    setCurrentEditHashtag({});
  }

  const deleteHashtagHandler = (id) => {
    makeRequest({
        url: `admin/hashtag/delete/${id}`,
        successCallback: (data) => {
          const hashtagData = [...hashtags].filter((hashtag) => hashtag.hashtag_id != currentDeleteHashtag.hashtag_id);
          setCurrentDeleteHashtag({});
          setHashtag(hashtagData);
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'DELETE',
        authorization: loadJWT(),
    });
  }

  const submitCallback = (values, { resetForm }) => {
    currentEditHashtag.hashtag_id != undefined ? 
    (  
      makeRequest({
        url: `admin/hashtag/edit/${currentEditHashtag.hashtag_id}`,
        values,
        successCallback: (data) => {
            const { hashtag_data } = data;
            let newHashtagData = [...hashtags];
            let indexOfEditedHashtag = newHashtagData.findIndex((hashtag) => hashtag.hashtag_id == hashtag_data.hashtag_id);
            newHashtagData[indexOfEditedHashtag] = hashtag_data;
            setHashtag(newHashtagData);
            setCurrentEditHashtag({});
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'POST',
        authorization: loadJWT(),
      })
    ) 
    :
    (
      makeRequest({
        url: `admin/hashtag/add`,
        values,
        successCallback: (data) => {
            const { hashtag_data } = data;
            let newHashtagData = [...hashtags];
            newHashtagData.unshift(hashtag_data);
            setHashtag(newHashtagData);
            resetForm();
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'POST',
        authorization: loadJWT(),
      })
    )
  }

  const COLUMNS = [
    {
        Header: "Hashtag",
        accessor: "hashtag",
        Cell: ({ cell: { value }, row: {original} }) => (original.post_quantity > 0 ? <Link to={`/admin/hashtag/post/${original.hashtag_id}`}> {original.hashtag} </Link> : original.hashtag)
    },
    {
        Header: "Post",
        accessor: "post_quantity"
    },
    {
        Header: "Created at",
        accessor: "created_at"
    },
    {
        Header: "Created by",
        accessor: "created_by",
    },
    {
        Header: "",
        accessor: "hashtag_id",
        Cell: ({ cell: { value }, row: { original } }) => (
          <>
            <button onClick={() => {editHashtagHandler(original.hashtag_id)}} className="btn btn-outline-primary" style={{margin: "5px"}}> <i className="fas fa-edit"></i> </button>
            <button onClick={() => {setCurrentDeleteHashtag(original); delModalRef.current.open()}} className="btn btn-outline-danger" style={{margin: "5px"}}> <i className="fas fa-trash"></i> </button>
          </>
        )
    }
  ]

  useEffect(() => {
    makeRequest({
        url: 'admin/hashtag/all',
        successCallback: (data) => {
            const { hashtags } = data;
            setHashtag(hashtags);
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'GET',
        authorization: loadJWT(),
    });
  }, []);

  const col = useMemo(() => COLUMNS, [hashtags]);
  
  const tableInstance = useTable({
    columns: col,
    data: hashtags,
    initialState: { pageSize: 5 }
  }, useGlobalFilter, usePagination)
  
  const { 
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    nextPage,
    previousPage,
    gotoPage,
    pageCount,
    prepareRow,
    state,
    canPreviousPage,
    canNextPage,
    pageOptions,
    setGlobalFilter,
  } = tableInstance;
  
  const { globalFilter } = state;
  const { pageIndex } = state;

  return (
    <>
      <div className="app-main__inner">
        <div className="app-page-title">
          <div className="page-title-wrapper">
            <div className="page-title-heading">
              <div className="page-title-icon">
                <i className="pe-7s-drawer icon-gradient bg-happy-itmeo">
                </i>
              </div>
              <div>Hashtag Table
                <div className="page-title-subheading">
                </div>
              </div>
            </div>
          </div>
        </div>            
        <div className="row">
          <div className="col-lg-12">
            <div className="main-card mb-3 card">
              <div className="card-body">
                <div className="row">
                  <div className="col-md-4">
                    <div className="card-body">
                      <BaseHashtagForm 
                        hashtag={currentEditHashtag} 
                        submitCallback={submitCallback} 
                        resetFormHandler={resetFormHandler} 
                      />
                    </div>
                  </div>
                  <div className="col-md-8">
                    <TableSearchSpan filter={ globalFilter } setFilter={ setGlobalFilter }/>
                    <table {...getTableProps()} className="table table-hover table-bordered">
                      <thead> 
                        { 
                          headerGroups.map((headerGroup) => (
                            <tr {...headerGroup.getHeaderGroupProps()}>
                              {headerGroup.headers.map((col) => (
                                <th {...col.getHeaderProps()}>{ col.render("Header") }</th>
                              ))}
                            </tr>
                          )) 
                        }
                      </thead>
                      <tbody {...getTableBodyProps()}>
                        {
                          page.map(row => {
                            prepareRow(row)
                            return (
                              <tr {...row.getRowProps()}>
                                {
                                  row.cells.map( cell => {
                                    return (
                                      <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                    )
                                  })
                                }
                              </tr>
                            )
                          })
                        }
                      </tbody>
                    </table>
                    <span>
                      Page{' '} { pageIndex + 1 } of { pageOptions.length } {' '}
                    </span>
                    <span>
                      | Go to page: {' '}
                      <input type="number" defaultValue={pageIndex + 1} 
                        onChange={e => {
                        const pageNumber = e.target.value ? Number(e.target.value) - 1 : 0
                        gotoPage(pageNumber);
                      }} style={{ width: '50px', marginRight: '5px', marginLeft: '5px'}}/>
                    </span>
                    <div className="btn-group">
                      <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" onClick={() => gotoPage(0)} disabled={!canPreviousPage}>{'<<'}</button>
                      <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" disabled={!canPreviousPage} onClick={() => previousPage()}>Previous</button>
                      <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" disabled={!canNextPage} onClick={() => nextPage()}>Next</button>
                      <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" onClick={() => gotoPage( pageCount - 1 )} disabled={!canNextPage}>{'>>'}</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ActionModal
                header={'Warning'} 
                confirmAction={deleteHashtagHandler} 
                actionId={currentDeleteHashtag.hashtag_id} 
                ref={delModalRef}
      >
        <h5>Do you really want to delete this hashtag ? (this action can not be undo)</h5>
      </ActionModal> 
    </>
  )
}



export default HashtagTable
