import React, {useMemo, useEffect, useState, useRef} from 'react';
import { makeRequest } from '../../utility/API';
import { useTable, useGlobalFilter, usePagination } from 'react-table';
import { COLUMNS } from '../../components/table/post/PostTableCol';
import TableSearchSpan from '../../components/table/TableSearchSpan';
import { loadJWT } from '../../utility/LocalStorage';
import { Link, useParams } from 'react-router-dom';
import ActionModal from '../../components/modal/ActionModal';

const PostByHashtagTable = () => {
  let { id } = useParams();
  const [posts, setPost] = useState([]);
  const [currentDeletePost, setCurrentDeletePost] = useState([]);
  const delModalRef = useRef();
  useEffect(() => {
    makeRequest({
        url: `admin/hashtag/post/${id}`,
        successCallback: (data) => {
            const { post_data } = data;
            setPost(post_data);
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'GET',
        authorization: loadJWT(),
    });
  }, []);

  const deletePostHandler = (id) => {
    makeRequest({
      url: `admin/post/delete/${id}`,
      successCallback: (data) => {
        const postData = [...posts].filter((post) => post.id != currentDeletePost.id);
        setCurrentDeletePost({});
        setPost(postData);
      },
      failureCallback: (error) => {
          console.log(error);
      },
      requestType: 'DELETE',
      authorization: loadJWT(),
    });
  }

  const COLUMNS = [
    {
        Header: "Post title",
        accessor: "title",
        Cell: ({ cell: { value }, row: {original} }) => <Link to={`/admin/post/${original.id}`}> {original.title} </Link>
    },
    {
        Header: "Location",
        accessor: "location"
    },
    {
        Header: "View",
        accessor: "view_quantity"
    },
    {
        Header: "Like",
        accessor: "like_quantity",
    },
    {
        Header: "",
        accessor: "id",
        Cell: ({ cell: { value }, row: {original} }) => (
          <>
            <Link to={`/admin/post/edit/${original.id}`} className="btn btn-outline-primary" style={{margin: "5px"}}> <i className="fas fa-edit"></i> </Link>
            <button onClick={() => {setCurrentDeletePost(original); delModalRef.current.open()}} className="btn btn-outline-danger" style={{margin: "5px"}}> <i className="fas fa-trash"></i> </button>
          </>
        )
    }
  ]

  const col = useMemo(() => COLUMNS, [posts]);
  
  const tableInstance = useTable({
    columns: col,
    data: posts,
    initialState: { pageSize: 10 }
  }, useGlobalFilter, usePagination)
  
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    nextPage,
    previousPage,
    gotoPage,
    pageCount,
    prepareRow,
    state,
    canPreviousPage,
    canNextPage,
    pageOptions,
    setGlobalFilter,
  } = tableInstance;
  
  const { globalFilter } = state;
  const { pageIndex } = state;

  return (
    <>
      <div className="app-main__inner">
        <div className="app-page-title">
          <div className="page-title-wrapper">
            <div className="page-title-heading">
              <div className="page-title-icon">
                <i className="pe-7s-drawer icon-gradient bg-happy-itmeo">
                </i>
              </div>
              <div>Post Tables
              </div>
            </div>
          </div>
        </div>            
        <div className="row">
          <div className="col-lg-12">
            <div className="main-card mb-3 card">
              <div className="card-body">
                <Link to={`/admin/post/create`}><button className="mb-2 mr-2 btn btn-primary">Add post</button></Link>
                <TableSearchSpan filter={ globalFilter } setFilter={ setGlobalFilter }/>
                <table {...getTableProps()} className="table table-hover table-bordered">
                  <thead> 
                    { 
                      headerGroups.map((headerGroup) => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                          {headerGroup.headers.map((col) => (
                            <th {...col.getHeaderProps()}>{ col.render("Header") }</th>
                          ))}
                        </tr>
                      )) 
                    }
                  </thead>
                  <tbody {...getTableBodyProps()}>
                    {
                      page.map(row => {
                        prepareRow(row)
                        return (
                          <tr {...row.getRowProps()}>
                            {
                              row.cells.map( cell => {
                                return (
                                  <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                )
                              })
                            }
                          </tr>
                        )
                      })
                    }
                  </tbody>
                </table>
                <span>
                  Page{' '} { pageIndex + 1 } of { pageOptions.length } {' '}
                </span>
                <span>
                  | Go to page: {' '}
                  <input type="number" defaultValue={pageIndex + 1} 
                    onChange={e => {
                    const pageNumber = e.target.value ? Number(e.target.value) - 1 : 0
                    gotoPage(pageNumber);
                  }} style={{ width: '50px', marginRight: '5px', marginLeft: '5px'}}/>
                </span>
                <div className="btn-group">
                  <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" onClick={() => gotoPage(0)} disabled={!canPreviousPage}>{'<<'}</button>
                  <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" disabled={!canPreviousPage} onClick={() => previousPage()}>Previous</button>
                  <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" disabled={!canNextPage} onClick={() => nextPage()}>Next</button>
                  <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" onClick={() => gotoPage( pageCount - 1 )} disabled={!canNextPage}>{'>>'}</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ActionModal
                header={'Warning'}
                confirmAction={deletePostHandler}
                actionId={currentDeletePost.id}
                ref={delModalRef}
      >
        <h5>Do you really want to delete this post ? (this action can not be undo)</h5>
      </ActionModal>
    </>
  )
}

export default PostByHashtagTable
