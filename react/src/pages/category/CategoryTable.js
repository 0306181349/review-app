import React, {useMemo, useEffect, useState, useRef} from 'react';
import { makeRequest } from '../../utility/API';
import { useTable, useGlobalFilter, usePagination } from 'react-table';
// import { COLUMNS } from '../../components/table/category/CategoryCol';
import TableSearchSpan from '../../components/table/TableSearchSpan';
import { loadJWT } from '../../utility/LocalStorage';
import { Link } from 'react-router-dom';
import BaseCategoryForm from '../../components/form/category/BaseCategoryForm';
import ActionModal from '../../components/modal/ActionModal';

const CategoryTable = () => {
  const [category, setCategory] = useState([]);
  const [currentEditCategory, setCurrentEditCategory] = useState({});
  const [currentDeleteCategory, setCurrentDeleteCategory] = useState({});
  const delModalRef = useRef();

  const editCategoryCategory = (id) => {
    let categoryData = [...category].filter((category) => category.id == id)[0];
    setCurrentEditCategory(categoryData);
  }
  const resetFormHandler = () => {
    setCurrentEditCategory({});
  }

  const deleteCategoryHandler = (id) => {
    makeRequest({
        url: `admin/category/delete/${id}`,
        successCallback: (data) => {
          const categoryData = [...category].filter((category) => category.id != currentDeleteCategory.id);
          setCurrentDeleteCategory({});
          setCategory(categoryData);
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'DELETE',
        authorization: loadJWT(),
    });
  }

  const submitCallback = (values, { resetForm }) => {
    currentEditCategory.id != undefined ? 
    (  
      makeRequest({
        url: `admin/category/edit/${currentEditCategory.id}`,
        values,
        successCallback: (data) => {
            const { category_data } = data;
            console.log("response ",category_data);
            let newCategoryData = [...category];
            console.log("newCate", newCategoryData)
            let indexOfEditedCategory = newCategoryData.findIndex((category) => category.id == category_data.id);
            newCategoryData[indexOfEditedCategory] = category_data;
            console.log(indexOfEditedCategory);
            console.log(newCategoryData[indexOfEditedCategory]);
            setCategory(newCategoryData);
            setCurrentEditCategory({});
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'POST',
        authorization: loadJWT(),
      })
    ) 
    :
    (
      makeRequest({
        url: `admin/category/add`,
        values,
        successCallback: (data) => {
            const { category_data } = data;
            let newCategoryData = [...category];
            newCategoryData.unshift(category_data);
            setCategory(newCategoryData);
            resetForm();
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'POST',
        authorization: loadJWT(),
      })
    )
  }

  useEffect(() => {
    makeRequest({
        url: 'admin/category/all',
        successCallback: (data) => {
            const { category } = data;
            setCategory(category);
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'GET',
        authorization: loadJWT(),
    });
  }, []);

  const COLUMNS = [
    {
        Header: "Category name",
        accessor: "category_name",
        // Cell: ({ cell: { value }, row: {original} }) => <CategoryTableCellNavLink id={original.id} text={value} />
    },
    {
        Header: "Created at",
        accessor: "created_at",
    },
    {
        Header: "",
        accessor: "id",
        Cell: ({ cell: { value }, row: { original } }) => (
          <>
            <button onClick={() => {editCategoryCategory(original.id)}} className="btn btn-outline-primary" style={{margin: "5px"}}> <i className="fas fa-edit"></i> </button>
            <button onClick={() => {setCurrentDeleteCategory(original); delModalRef.current.open()}} className="btn btn-outline-danger" style={{margin: "5px"}}> <i className="fas fa-trash"></i> </button>
          </>
        )
    }
  ];

  const col = useMemo(() => COLUMNS, [category]);
  const tableInstance = useTable({
    columns: col,
    data: category,
    initialState: { pageSize: 10 }
  }, useGlobalFilter, usePagination)
  const { 
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    nextPage,
    previousPage,
    gotoPage,
    pageCount,
    prepareRow,
    state,
    canPreviousPage,
    canNextPage,
    pageOptions,
    setGlobalFilter,
  } = tableInstance;

  const { globalFilter } = state;
  const { pageIndex } = state;

  return (
    <>
      <div className="app-main__inner">
        <div className="app-page-title">
          <div className="page-title-wrapper">
            <div className="page-title-heading">
              <div className="page-title-icon">
                <i className="pe-7s-drawer icon-gradient bg-happy-itmeo">
                </i>
              </div>
              <div>Category Table
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <div className="main-card mb-3 card">
              <div className="card-body">
                <div className="row">
                  <div className="col-md-4">
                    <div className="card-body">
                      <BaseCategoryForm 
                        category={currentEditCategory}
                        submitCallback={submitCallback}
                        resetFormHandler={resetFormHandler}
                      />
                    </div>
                  </div>
                  <div className="col-md-8">
                    <div className="card-body">
                      <TableSearchSpan filter={ globalFilter } setFilter={ setGlobalFilter }/>
                      <table {...getTableProps()} className="table table-hover table-bordered">
                        <thead> 
                          { 
                            headerGroups.map((headerGroup) => (
                              <tr {...headerGroup.getHeaderGroupProps()}>
                                {headerGroup.headers.map((col) => (
                                  <th {...col.getHeaderProps()}>{ col.render("Header") }</th>
                                ))}
                              </tr>
                            )) 
                          }
                        </thead>
                        <tbody {...getTableBodyProps()}>
                          {
                            page.map(row => {
                              prepareRow(row)
                              return (
                                <tr {...row.getRowProps()}>
                                  {
                                    row.cells.map( cell => {
                                      return (
                                      <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                      )
                                    })
                                  }
                                </tr>
                              )
                            })
                          }
                        </tbody>
                      </table>
                      <span>
                        Page{' '} { pageIndex + 1 } of { pageOptions.length } {' '}
                      </span>
                      <span>
                        | Go to page: {' '}
                        <input type="number" defaultValue={pageIndex + 1} 
                          onChange={e => {
                          const pageNumber = e.target.value ? Number(e.target.value) - 1 : 0
                          gotoPage(pageNumber);
                        }} style={{ width: '50px', marginRight: '5px', marginLeft: '5px'}}/>
                      </span>
                      <div className="btn-group">
                        <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" onClick={() => gotoPage(0)} disabled={!canPreviousPage}>{'<<'}</button>
                        <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" disabled={!canPreviousPage} onClick={() => previousPage()}>Previous</button>
                        <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" disabled={!canNextPage} onClick={() => nextPage()}>Next</button>
                        <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" onClick={() => gotoPage( pageCount - 1 )} disabled={!canNextPage}>{'>>'}</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ActionModal
                header={'Warning'} 
                confirmAction={deleteCategoryHandler} 
                actionId={currentDeleteCategory.id} 
                ref={delModalRef}
      >
        <h5>Do you really want to delete this hashtag ? (this action can not be undo)</h5>
      </ActionModal> 
    </>  
  )
}

export default CategoryTable
