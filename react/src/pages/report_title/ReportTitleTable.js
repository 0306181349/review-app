import React, {useMemo, useEffect, useState, useRef} from 'react';
import { makeRequest } from '../../utility/API';
import { useTable, useGlobalFilter, usePagination } from 'react-table';
// import { COLUMNS } from '../../components/table/report_title/ReportTitleCol';
import TableSearchSpan from '../../components/table/TableSearchSpan';
import { loadJWT } from '../../utility/LocalStorage';
import { Link, useParams } from 'react-router-dom';
import BaseReportTitleForm from '../../components/form/report_title/BaseReportTitleForm';
import ActionModal from '../../components/modal/ActionModal';

const ReportTitleTable = () => {
  const [reportTitle, setReportTitle] = useState([]);
  const [currentEditReportTitle, setCurrentEditReportTitle] = useState({});
  const [currentDeleteReportTitle, setCurrentDeleteReportTitle] = useState({});
  const delModalRef = useRef();
  
  const editReportTitleHandler = (id) => {
    let reportTitleData = [...reportTitle].filter((reportTitle) => reportTitle.report_title_id == id)[0];
    // console.log('id', id);
    // console.log('reportTitleData', reportTitleData);
    setCurrentEditReportTitle(reportTitleData);
  }
  
  const resetFormHandler = () => {
    setCurrentEditReportTitle({});
  }

  const deleteReportTitleHandler = (id) => {
    makeRequest({
        url: `admin/report-title/delete/${id}`,
        successCallback: (data) => {
          const reportTitleData = [...reportTitle].filter((reportTitle) => reportTitle.report_title_id != currentDeleteReportTitle.report_title_id);
          setCurrentDeleteReportTitle({});
          setReportTitle(reportTitleData);
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'DELETE',
        authorization: loadJWT(),
    });
  }
  
  const submitCallback = (values, { resetForm }) => {
    currentEditReportTitle.report_title_id != undefined ? 
    (
      makeRequest({
        url: `admin/report-title/edit/${currentEditReportTitle.report_title_id}`,
        values,
        successCallback: (data) => {
            const { report_title_data } = data;
            let newReportTitleData = [...reportTitle];
            let indexOfEditedReportTitle = newReportTitleData.findIndex((reportTitle) => reportTitle.report_title_id == report_title_data.report_title_id);
            newReportTitleData[indexOfEditedReportTitle] = report_title_data;
            setReportTitle(newReportTitleData);
            setCurrentEditReportTitle({});
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'POST',
        authorization: loadJWT(),
      })
    ) 
    :
    (
      makeRequest({
        url: `admin/report-title/add`,
        values,
        successCallback: (data) => {
            const { report_title_data } = data;
            let newReportTitleData = [...reportTitle];
            newReportTitleData.unshift(report_title_data);
            setReportTitle(newReportTitleData);
            resetForm();
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'POST',
        authorization: loadJWT(),
      })
    )
  }

  useEffect(() => {
    makeRequest({
        url: 'admin/report-title/all',
        successCallback: (data) => {
            const { report_title_data } = data;
            setReportTitle(report_title_data);
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'GET',
        authorization: loadJWT(),
    });
  }, []);
  const COLUMNS = [
    {
        Header: "Report title",
        accessor: "report_title",
    },
    {
        Header: "",
        accessor: "report_title_id",
        Cell: ({ cell: { value }, row: { original } }) => (
          <>
            <button onClick={() => {editReportTitleHandler(original.report_title_id)}} className="btn btn-outline-primary" style={{margin: "5px"}}> <i className="fas fa-edit"></i> </button>
            <button onClick={() => {setCurrentDeleteReportTitle(original); delModalRef.current.open()}} className="btn btn-outline-danger" style={{margin: "5px"}}> <i className="fas fa-trash"></i> </button>
          </>
        )
    }
]
  const col = useMemo(() => COLUMNS, [reportTitle]);
  
  const tableInstance = useTable({
    columns: col,
    data: reportTitle,
    initialState: { pageSize: 10 }
  }, useGlobalFilter, usePagination)
  
  const { 
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    nextPage,
    previousPage,
    gotoPage,
    pageCount,
    prepareRow,
    state,
    canPreviousPage,
    canNextPage,
    pageOptions,
    setGlobalFilter,
  } = tableInstance;
  
  const { globalFilter } = state;
  const { pageIndex } = state;

  return (
    <>
      <div className="app-main__inner">
        <div className="app-page-title">
          <div className="page-title-wrapper">
            <div className="page-title-heading">
              <div className="page-title-icon">
                <i className="pe-7s-drawer icon-gradient bg-happy-itmeo">
                </i>
              </div>
              <div>Report Title Table
              </div>
            </div>
          </div>
        </div>            
        <div className="row">
          <div className="col-lg-12">
            <div className="main-card mb-3 card">
              <div className="card-body">
                <div className="row">
                  <div className="col-md-4">
                    <BaseReportTitleForm 
                      reportTitle={currentEditReportTitle}
                      submitCallback={submitCallback}
                      resetFormHandler={resetFormHandler}
                    />
                  </div>
                  <div className="col-md-8">
                    <TableSearchSpan filter={ globalFilter } setFilter={ setGlobalFilter }/>
                    <table {...getTableProps()} className="table table-hover table-bordered">
                      <thead> 
                        { 
                          headerGroups.map((headerGroup) => (
                            <tr {...headerGroup.getHeaderGroupProps()}>
                              {headerGroup.headers.map((col) => (
                                <th {...col.getHeaderProps()}>{ col.render("Header") }</th>
                              ))}
                            </tr>
                          )) 
                        }
                      </thead>
                      <tbody {...getTableBodyProps()}>
                        {
                          page.map(row => {
                            prepareRow(row)
                            return (
                              <tr {...row.getRowProps()}>
                                {
                                  row.cells.map( cell => {
                                    return (
                                      <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                    )
                                  })
                                }
                              </tr>
                            )
                          })
                        }
                      </tbody>
                    </table>
                    <span>
                      Page{' '} { pageIndex + 1 } of { pageOptions.length } {' '}
                    </span>
                    <span>
                      | Go to page: {' '}
                      <input type="number" defaultValue={pageIndex + 1} 
                        onChange={e => {
                        const pageNumber = e.target.value ? Number(e.target.value) - 1 : 0
                        gotoPage(pageNumber);
                      }} style={{ width: '50px', marginRight: '5px', marginLeft: '5px'}}/>
                    </span>
                    <div className="btn-group">
                      <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" onClick={() => gotoPage(0)} disabled={!canPreviousPage}>{'<<'}</button>
                      <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" disabled={!canPreviousPage} onClick={() => previousPage()}>Previous</button>
                      <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" disabled={!canNextPage} onClick={() => nextPage()}>Next</button>
                      <button className="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary" onClick={() => gotoPage( pageCount - 1 )} disabled={!canNextPage}>{'>>'}</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>    
      <ActionModal
                header={'Warning'}
                confirmAction={deleteReportTitleHandler}
                actionId={currentDeleteReportTitle.report_title_id}
                ref={delModalRef}
      >
        <h5>Do you really want to delete this report title ? (this action can not be undo)</h5>
      </ActionModal>
    </>
  )
}

export default ReportTitleTable
