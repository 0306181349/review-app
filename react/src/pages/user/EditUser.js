import React from 'react';
import BaseUserForm from '../../components/form/user/BaseUserForm';
import { useParams, Redirect } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { makeRequest } from '../../utility/API';
import { loadJWT } from '../../utility/LocalStorage';
import FailureAlert from '../../components/alert/FailureAlert';

const EditUser = () => {
    const { id } = useParams();
    const [redirectParam, setRedirectParam] = useState(0);
    const [user, setUser] = useState([]);
    const [errors, setErrors] = useState(null);

    useEffect(() => {
        makeRequest({
            url: `admin/users/show/${id}`,
            successCallback: (data) => {
                const { user_data } = data;
                setUser(user_data);
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'GET',
            authorization: loadJWT(),
        });
    },[]);
    
    return redirectParam!=0 ? ( <Redirect to={`/admin/users/${redirectParam}`} /> ) : (
        <>
          <div className="app-main__inner">
            <div className="app-page-title">
              <div className="page-title-wrapper">
                <div className="page-title-heading">
                  <div className="page-title-icon">
                    <i className="pe-7s-drawer icon-gradient bg-happy-itmeo">
                    </i>
                  </div>
                  <div>Edit account
                      <div className="page-title-subheading">
                  </div>
                </div>
               </div>
              </div>
            </div>            
            <div className="row">
              <div className="col-lg-12">
                <div className="main-card mb-3 card">
                  {errors && <FailureAlert errors={errors}/>}
                  <div className="card-body">
                      <BaseUserForm user={user} setErrors={setErrors} setRedirectParam={setRedirectParam} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </>
    )
}

export default EditUser
