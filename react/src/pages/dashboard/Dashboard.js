import React from 'react';
import { makeRequest } from '../../utility/API';
import { useEffect, useState } from 'react';
import { loadJWT } from '../../utility/LocalStorage';
import { Line } from 'react-chartjs-2';
import { Link } from 'react-router-dom';
import { Bar } from 'react-chartjs-2';

const Dashboard = () => {
  const [dashboardData, setDashboardData] = useState([]);

  const data = {
    labels: dashboardData.top_province_label,
    datasets: [
      {
        label: '# of Post',
        data: dashboardData.top_province_values,
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
        ],
        borderWidth: 1,
      },
    ],
  };

  useEffect(() => {
    makeRequest({
        url: `admin/dashboard/all`,
        successCallback: (data) => {
            const { dashboard_data } = data;
            setDashboardData(dashboard_data);
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'GET',
        authorization: loadJWT(),
    });
  },[]);

  const userData = {
    labels: dashboardData.user_report_labels,
    datasets: [
      {
        label: '# of New User',
        data: dashboardData.user_report_values,
        fill: false,
        backgroundColor: 'rgb(255, 99, 132)',
        borderColor: 'rgba(255, 99, 132, 0.2)',
      },
    ],
  };

  const postData = {
    labels: dashboardData.post_report_labels,
    datasets: [
      {
        label: '# of New Post',
        data: dashboardData.post_report_values,
        fill: false,
        backgroundColor: 'rgb(63, 106, 216)',
        borderColor: 'rgba(63, 106, 216, 0.2)',
      },
    ],
  }
  const options = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
            min: 0,
          },
        },
      ],
    },
  };

  return (
    <>
    <div className="app-main__inner">
      <div className="app-page-title">
        <div className="page-title-wrapper">
          <div className="page-title-heading">
            <div className="page-title-icon">
              <i className="pe-7s-car icon-gradient bg-mean-fruit">
              </i>
            </div>
            <div>Analytics Dashboard
              <div className="page-title-subheading">
              </div>
            </div>
          </div>
        </div>
      </div>          
      <div className="row">
        <div className="col-md-6 col-xl-4">
          <div className="card mb-3 widget-content bg-midnight-bloom">
            <div className="widget-content-wrapper text-white">
              <div className="widget-content-left">
                <div className="widget-heading">Total Post</div>
                <div className="widget-subheading">Last month { dashboardData.this_month_post }</div>
              </div>
              <div className="widget-content-right">
                <div className="widget-numbers text-white"><span>{ dashboardData.total_post }</span></div>
                
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-6 col-xl-4">
          <div className="card mb-3 widget-content bg-arielle-smile">
            <div className="widget-content-wrapper text-white">
              <div className="widget-content-left">
                <div className="widget-heading">Total user</div>
                <div className="widget-subheading">Last month { dashboardData.this_month_user }</div>
              </div>
              <div className="widget-content-right">
                <div className="widget-numbers text-white"><span>{ dashboardData.total_user }</span></div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-6 col-xl-4">
          <div className="card mb-3 widget-content bg-grow-early">
            <div className="widget-content-wrapper text-white">
              <div className="widget-content-left">
                <div className="widget-heading">Total location</div>
                <div className="widget-subheading">Last month { dashboardData.this_month_location }</div>
              </div>
              <div className="widget-content-right">
                <div className="widget-numbers text-white"><span>{ dashboardData.total_location }</span></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-12 col-lg-6">
          <div className="mb-3 card">
            <div className="card-header-tab card-header">
              <div className="card-header-title">
                <i className="header-icon lnr-apartment icon-gradient bg-love-kiss"> </i>
                New User Monthly Report
              </div>
              {/* <ul className="nav">
                <li className="nav-item"><a href="#" className="active nav-link">Last</a></li>
                <li className="nav-item"><a href="#" className="nav-link second-tab-toggle">Current</a></li>
              </ul> */}
            </div>
            <div className="card-body">
              <div className="tab-content">
                <div className="tab-pane fade show active" id="tabs-eg-77">
                  <div className="card mb-3 widget-chart widget-chart2 text-left w-100">
                    <div className="widget-chat-wrapper-outer">
                      <div className="widget-chart-wrapper widget-chart-wrapper-lg opacity-10 m-0">
                        <Line data={userData} options={options} />
                      </div>
                    </div>
                  </div>
                  <h6 className="text-muted text-uppercase font-size-md opacity-5 font-weight-normal">Top Users By Post</h6>
                  <ul className="rm-list-borders rm-list-borders-scroll list-group list-group-flush">
                    {
                      dashboardData.top_user != undefined ? (dashboardData.top_user).map((user, index) => {
                        return (
                          <li className="list-group-item" key={index}>
                            <div className="widget-content p-0">
                              <div className="widget-content-wrapper">
                                <div className="widget-content-left mr-3">
                                  <div className="img-round-div">
                                    <img width={42} className="rounded-circle" src={user.avatar} />
                                  </div>
                                </div>
                                <div className="widget-content-left">
                                <Link to={'/admin/users/' + user.account_id}> <div className="widget-heading">{user.user_name}</div> </Link>
                                  <div className="widget-subheading">Created at: {user.created_at}</div>
                                </div>
                                <div className="widget-content-right">
                                  <div className="font-size-xlg text-muted">
                                    <span>{user.post_quantity}</span>
                                    <small className="text-primary pl-2">
                                      POSTS
                                    </small>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </li>
                        );
                      }) : ''
                    }
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-12 col-lg-6">
          <div className="mb-3 card">
            <div className="card-header-tab card-header">
              <div className="card-header-title">
                <i className="header-icon lnr-apartment icon-gradient bg-love-kiss"> </i>
                New Post Monthly Report
              </div>
              {/* <ul className="nav">
                <li className="nav-item"><a href="#" className="active nav-link">Last</a></li>
                <li className="nav-item"><a href="#" className="nav-link second-tab-toggle">Current</a></li>
              </ul> */}
            </div>
            <div className="card-body">
              <div className="tab-content">
                <div className="tab-pane fade show active" id="tabs-eg-77">
                  <div className="card mb-3 widget-chart widget-chart2 text-left w-100">
                    <div className="widget-chat-wrapper-outer">
                      <div className="widget-chart-wrapper widget-chart-wrapper-lg opacity-10 m-0">
                        <Line data={postData} options={options} />
                      </div>
                    </div>
                  </div>
                  <h6 className="text-muted text-uppercase font-size-md opacity-5 font-weight-normal">Top Posts By Like</h6>
                  <ul className="rm-list-borders rm-list-borders-scroll list-group list-group-flush">
                    {
                      dashboardData.top_post != undefined ? (dashboardData.top_post).map((post, index) => {
                        return (
                          <li className="list-group-item" key={index}>
                            <div className="widget-content p-0">
                              <div className="widget-content-wrapper">
                                <div className="widget-content-left mr-3">
                                  <div className='img-round-div'>
                                    <img width={42} className="rounded-circle" src={post.post_image} />
                                  </div>
                                </div>
                                <div className="widget-content-left">
                                <Link to={'/admin/post/' + post.post_id}> <div className="widget-heading">{post.title}</div> </Link>
                                  <div className="widget-subheading">
                                    Created at: {post.created_at}
                                  </div>
                                </div>
                                <div className="widget-content-right">
                                  <div className="font-size-xlg text-muted">
                                    <span>{post.like_quantity}</span>
                                    <small className="text-primary pl-2">
                                      LIKES
                                    </small>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </li>
                        );
                      }) : ''
                    }
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-12">
          <div className="main-card mb-3 card">
            <div className="card-header">Top Provinces By Post 
              <div className="btn-actions-pane-right">
                <div role="group" className="btn-group-sm btn-group">
                </div>
              </div>
            </div>
            <div className="table-responsive">
              <Bar data={data} />
            </div>
          </div>
        </div>
      </div>
    </div>
  </>
  )
}

export default Dashboard;
