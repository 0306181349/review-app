import React from 'react';
import { Link, useParams } from 'react-router-dom';
import { useEffect, useState, useRef } from 'react';
import { makeRequest } from '../../utility/API';
import { Redirect } from 'react-router';
import { loadJWT } from '../../utility/LocalStorage';
// import CarouselItem from '../../components/carousel/CarouselItem';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import ActionModal from '../../components/modal/ActionModal';
import LockPostModal from './LockPostModal';
import * as Yup from 'yup';

const ShowPostReport = () => {
    let { id } = useParams();
    const [shouldRedirect, setShouldRedirect] = useState(false);
    const [postReport, setPostReport] = useState([]);
    const [post, setPost] = useState([]);
    const [reportTitle, setReportTitle] = useState([]);
    const delReporModalRef = useRef();
    const delPostModalRef = useRef();
    const lockPostModalRef = useRef();

    useEffect(() => {
        makeRequest({
            url: `admin/post/show/${id}`,
            successCallback: (data) => {
                const { post_data } = data;
                // console.log(post_data);
                setPost(post_data);
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'GET',
        });
        makeRequest({
            url: `admin/report/post/${id}`,
            successCallback: (data) => {
                const { report_data } = data;
                // console.log(post_data);
                setPostReport(report_data);
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'GET',
        });
        makeRequest({
            url: `admin/report-title/all`,
            successCallback: (data) => {
                const { report_title_data } = data;
                // console.log(post_data);
                setReportTitle(report_title_data);
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'GET',
        });
    },[]);

    const deletePost = (id) => {
        makeRequest({
            url: `admin/post/delete/${id}`,
            successCallback: (data) => {
                setShouldRedirect(true);
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'DELETE',
            authorization: loadJWT(),
        });
    };

    const deleteReportHandler = (id, index) => {
        makeRequest({
            url: `admin/report/post/delete-report/${id}`,
            successCallback: (data) => {
                let newPostReport = [...postReport];
                newPostReport.splice(index, 1);
                setPostReport(newPostReport);
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'DELETE',
            authorization: loadJWT(),
        });
    }

    const deleteAllReportHandler = (postId) => {
        makeRequest({
            url: `admin/report/post/delete-all/${postId}`,
            successCallback: (data) => {
                setShouldRedirect(true);
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'DELETE',
            authorization: loadJWT(),
        });
    }

    const lockPostHandler = (values) => {
        makeRequest({
            url: `admin/post/lock/${post.post_id}`,
            values,
            successCallback: ( data ) => {
                setShouldRedirect(true);
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: "POST",
            authorization: loadJWT(),
        });
    };

    return shouldRedirect ? ( <Redirect to='/admin/report/post' /> ) : (
        <>
            <div className="app-main__inner">
                <div className="app-page-title">
                  <div className="page-title-wrapper">
                    <div className="page-title-heading">
                      <div className="page-title-icon">
                        <i className="pe-7s-drawer icon-gradient bg-happy-itmeo">
                        </i>
                      </div>
                      <div>Post detail
                          <div className="page-title-subheading">
                      </div>
                    </div>
                   </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="main-card mb-3 card">
                      <div className="card-body">
                          
                      <div className="form-group">
                                <Carousel 
                                    width={"100%"} 
                                    dynamicHeight={false} 
                                    showThumbs={false}
                                    centerMode={true}
                                    centerSlidePercentage={100}
                                    infiniteLoop={true}
                                >
                                      {
                                          post.post_image != undefined ? post.post_image.map((image, index) => (
                                              <div style={{background: '#000000', border: '2px solid black', borderRadius: '5px'}} key={index}>
                                                <img src={image} style={{height: '500px', objectFit: 'contain'}} />
                                              </div>
                                          )) 
                                          : 
                                          ''
                                      }
                                </Carousel>
                              </div>
                              <div className="form-group">
	        		          	<label>Post title</label>
	        		          	<input type="text" className="form-control" value={(post.title || '')} readOnly />
	        		          </div>
	        		          <div className="form-group">
	        		          	<label>Created by</label>
	        		          	<input type="text" className="form-control" value={(post.created_by || '')} readOnly />
	        		          </div>
	        		          <div className="form-group">
	        		          	<label>Hashtag</label>
	        		          	<input type="text" className="form-control" value={(post.show_hashtag || '')} readOnly />
	        		          </div>
	        		          <div className="form-group">
	        		          	<label>Rating</label>
	        		          	<input type="text" className="form-control" value={(post.rating || '')} readOnly />
	        		          </div>
	        		          <div className="form-group">
	        		          	<label>Location</label>
	        		          	<input type="text" className="form-control" value={(post.location_name || '')} readOnly />
	        		          </div>
	        		          <div className="form-group">
	        		          	<label>Category</label>
	        		          	<input type="text" className="form-control" value={(post.category_name || '')} readOnly />
	        		          </div>
	        		          <div className="form-group">
	        		          	<label>View quantity</label>
	        		          	<input type="text" className="form-control" value={(post.view_quantity || '')} readOnly />
	        		          </div>
	        		          <div className="form-group">
	        		          	<label>Like quantity</label>
	        		          	<input type="text" className="form-control" value={(post.like_quantity || '')} readOnly />
	        		          </div>
	        		          <div className="form-group">
	        		          	<label>Content</label>
	        		          	<div style = {{
                                      backgroundColor: "#e9ecef",
                                      border: "1px solid #e9ecef",
                                      borderRadius: ".25rem",
                                      padding: "10px",
                                    }} dangerouslySetInnerHTML={{__html: (post.content || '') }} 
                                />
	        		          </div>
                              <div className="form-group">
                                <Link to={`/admin/post/edit/${post.post_id}`}> <button className="mb-2 mr-2 btn-transition btn btn-outline-primary">Edit</button> </Link>
                                <button className="mb-2 mr-2 btn-transition btn btn-outline-warning" onClick={() => lockPostModalRef.current.open()}>Lock</button>
                                <button className="mb-2 mr-2 btn-transition btn btn-outline-danger" onClick={() => {delPostModalRef.current.open()}}>Delete</button>
	    		              </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                      <div className="main-card mb-3 card">
                        <div className="card-body">
                            {
                                postReport.map((item, index) => {
                                      return(
                                          <div className="main-card mb-3 card" key={index}>
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-md-3" style={{
                                                        textAlign: "center",
                                                    }}>
                                                        <div style={{paddingLeft: "28%"}}>
                                                            <div className='img-round-div'>
                                                                <Link to={`/admin/users/${item.account_id}`}>
                                                                    <img width={42} className="rounded-circle" src={item.avatar} />
                                                                </Link>
                                                            </div>
                                                        </div>
                                                        <span>
                                                          {item.user_name}
                                                        </span>
                                                    </div>
                                                    <div className="col-md-9" style={{
                                                        position: 'relative',
                                                    }}>
                                                        <h5 className="card-title">{item.report_title}</h5>
                                                        <p>{item.report_content}</p>
                                                        <button style={{
                                                                    position: 'absolute',
                                                                    top: '-10px',
                                                                    right: '5px',
                                                        }}
                                                                className="btn btn-outline-danger"
                                                                onClick={() => {deleteReportHandler(item.report_id, index)}}
                                                        >
                                                            <i className="fas fa-trash"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                          </div>
                                      )
                                })
                            }
                            <button className="btn btn-outline-danger" onClick={() => delReporModalRef.current.open()}>
                                Delete all report
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <ActionModal 
            header={'Warning'} 
            confirmAction={deleteAllReportHandler} 
            actionId={post.post_id} 
            ref={delReporModalRef}
        >
            <h5>Do you really want to delete all the report ? (this action can not be undo)</h5>
        </ActionModal>
        
        <ActionModal 
            header={'Warning'} 
            confirmAction={deletePost} 
            actionId={post.post_id} 
            ref={delPostModalRef}
        >
            <h5>Do you really want to delete this post ? (this action can not be undo)</h5>
        </ActionModal>
        
        <LockPostModal 
            header={'Lock the post'} 
            actionId={post.post_id} 
            ref={lockPostModalRef}
            lockPostHandler={lockPostHandler}
            reportTitle={reportTitle}
        >
        </LockPostModal>
        </>
    )
}

export default ShowPostReport
