import React from 'react'

const ShowComment = ({ comment, replies, loadRepliesHandler, deleteCommentHandler }) => {
    
    return (
        <div className="comment">
            <div className="img-round-div">
                <img className="rounded-circle" src={comment.avatar} style={{ width: 42 }}/>
            </div>
            <div className="comment-right-part">
                <div className="comment-content">
                    <div className="comment-author">
                        {comment.user_name}
                    </div>
                    <div className="created-at">{new Date(comment.comment_date).toLocaleDateString()}</div>
                </div>
                <div className="comment-text">
                    {comment.content}
                </div>
                <div className="comment-actions">
                    {
                        comment.reply_to == null && !comment.is_loaded && (
                            <div className="comment-action" onClick={() => {loadRepliesHandler(comment.comment_id)}}>
                                Load more
                            </div>
                        )
                    }
                    <div className="comment-action" onClick={() => {deleteCommentHandler(comment.comment_id, comment.reply_to != null)}}>
                        Delete
                    </div>
                </div>
                {
                    replies.length > 0 && (
                        <div className="replies">
                            {replies.map((comment) => (<ShowComment comment={comment} replies={[]} key={comment.comment_id} deleteCommentHandler={deleteCommentHandler} />))}
                        </div>
                    )
                }
            </div>
        </div>
    )
}

export default ShowComment
