import React from 'react';
import BasePostForm from '../../components/form/post/BasePostForm';
import { useParams, Redirect } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { makeRequest } from '../../utility/API';
import { loadJWT } from '../../utility/LocalStorage';
import FailureAlert from '../../components/alert/FailureAlert';

const EditPost = () => {
    const { id } = useParams();
    const [redirectParam, setRedirectParam] = useState(0);
    const [errors, setErrors] = useState(null);
    const [post, setPost] = useState([]);

    useEffect(() => {
        makeRequest({
            url: `admin/post/show/${id}`,
            successCallback: (data) => {
                const { post_data } = data;
                setPost(post_data);
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'GET',
            authorization: loadJWT(),
        });
    },[]);

    return redirectParam!=0 ? ( <Redirect to={`/admin/post/${redirectParam}`} /> ) : (
        <>
          <div className="app-main__inner">
            <div className="app-page-title">
              <div className="page-title-wrapper">
                <div className="page-title-heading">
                  <div className="page-title-icon">
                    <i className="pe-7s-drawer icon-gradient bg-happy-itmeo">
                    </i>
                  </div>
                  <div>Edit post
                      <div className="page-title-subheading">
                  </div>
                </div>
               </div>
              </div>
            </div>            
            <div className="row">
              <div className="col-lg-12">
                <div className="main-card mb-3 card">
                {errors && <FailureAlert errors={errors}/>}
                  <div className="card-body">
                      <BasePostForm post={post} setErrors={setErrors} setRedirectParam={setRedirectParam} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </>
    )
}

export default EditPost
