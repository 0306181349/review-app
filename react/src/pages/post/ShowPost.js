import React from 'react';
import { Link, useParams } from 'react-router-dom';
import { useEffect, useState, useRef } from 'react';
import { makeRequest } from '../../utility/API';
import { Redirect } from 'react-router';
import { loadJWT } from '../../utility/LocalStorage';
// import CarouselItem from '../../components/carousel/CarouselItem';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import CarouselItem from '../../components/carousel/CarouselItem';
import ShowComment from './ShowComment';
import ActionModal from '../../components/modal/ActionModal';

const ShowPost = () => {
    let { id } = useParams();
    const [post, setPost] = useState([]);
    const [shouldRedirect, setShouldRedirect] = useState(false);
    const [comments, setComments] = useState([]);
    const [rightDivHeigth, setRightDivHeigth] = useState(0);
    const [replies, setReplies] = useState([]);
    const delModalRef = useRef();

    const rootComment = comments.filter((comment) => {
      return comment.reply_to === null;
    });
    const getRepliesComment = (commentId) => {
      return replies.filter((comment) => (
        comment.reply_to !== null && comment.reply_to == commentId
      )).sort((a, b) => (a.id - b.id))
    }
    const fetchReplies = (id) => {
      makeRequest({
        url: `admin/comment/replies/${id}`,
        successCallback: (data) => {
            const { replies_data } = data;
            let commentData = [...comments];
            let repliesData = [...replies, ...replies_data];
            commentData[commentData.findIndex(comment => comment.comment_id == id)].is_loaded = true;
            setReplies(repliesData);
            setComments(comments);
            console.log(commentData[actionCommentIndex]);
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'GET',
        authorization: loadJWT(),
      });
    }

    const deleteCommentHandler = (id, isDeleteReplies) => {
      makeRequest({
        url: `admin/comment/delete/${id}`,
        successCallback: (data) => {
          if(isDeleteReplies){
            let repliesData = [...replies].filter(reply => reply.comment_id != id);
            setReplies(repliesData);
          }else{
            let commentData = [...comments].filter(comment => comment.comment_id != id);
            let repliesData = [...replies].filter(reply => reply.reply_to != id);
            setComments(commentData);
            setReplies(repliesData);
          }
        },
        failureCallback: (error) => {
            console.log(error);
        },
        requestType: 'DELETE',
        authorization: loadJWT(),
      });
    }

    useEffect(() => {
        makeRequest({
            url: `admin/post/show/${id}`,
            successCallback: (data) => {
                const { post_data } = data;
                // console.log(post_data);
                setPost(post_data);
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'GET',
            authorization: loadJWT(),
        });
    },[]);

    useEffect(() => {
      setRightDivHeigth(document.getElementById('left-container').offsetHeight);
    }, [post]);

    useEffect(() => {
      makeRequest({
          url: `admin/comment/all/${id}`,
          successCallback: (data) => {
              const { comment_data } = data;
              console.log(comment_data);
              setComments(comment_data);
          },
          failureCallback: (error) => {
              console.log(error);
          },
          requestType: 'GET',
          authorization: loadJWT(),
      });
    },[]);

    const deletePost = (id) => {
        makeRequest({
            url: `admin/post/delete/${id}`,
            successCallback: (data) => {
                setShouldRedirect(true);
            },
            failureCallback: (error) => {
                console.log(error);
            },
            requestType: 'DELETE',
            authorization: loadJWT(),
        });
    };

    return shouldRedirect ? ( <Redirect to='/admin/post' /> ) : (
        <> 
          <div className="app-main__inner">
            <div className="app-page-title">
              <div className="page-title-wrapper">
                <div className="page-title-heading">
                  <div className="page-title-icon">
                    <i className="pe-7s-drawer icon-gradient bg-happy-itmeo">
                    </i>
                  </div>
                  <div>Post detail
                  <div className="page-title-subheading">
                  </div>
                </div>
               </div>
              </div>
            </div>            
            <div className="row">
              <div className="col-lg-12">
                <div className="row">
                  <div className="col-md-6">
                    <div className="main-card mb-3 card">
                      <div className="card-body" id="left-container">
                        <div className="form-group">
                          <CarouselItem post_image={post.post_image || []} post_video={post.post_video || null} />
                        </div>
                        <div className="form-group">
	                      	<label>Post title</label>
	                      	<input type="text" className="form-control" value={(post.title || '')} readOnly />
	                      </div>
	        		          <div className="form-group">
	        		          	<label>Created by</label>
	        		          	<input type="text" className="form-control" value={(post.created_by || '')} readOnly />
	        		          </div>
	                      <div className="form-group">
	                      	<label>Hashtag</label>
	                      	<input type="text" className="form-control" value={(post.show_hashtag || '')} readOnly />
	                      </div>
	                      <div className="form-group">
	                      	<label>Rating</label>
	                      	<input type="text" className="form-control" value={(post.rating || '')} readOnly />
	                      </div>
	                      <div className="form-group">
	                      	<label>Location</label>
	                      	<input type="text" className="form-control" value={(post.location_name || '')} readOnly />
	                      </div>
	                      <div className="form-group">
	                      	<label>Category</label>
	                      	<input type="text" className="form-control" value={(post.category_name || '')} readOnly />
	                      </div>
	                      <div className="form-group">
	                      	<label>View quantity</label>
	                      	<input type="text" className="form-control" value={(post.view_quantity || '')} readOnly />
	                      </div>
	                      <div className="form-group">
	                      	<label>Like quantity</label>
	                      	<input type="text" className="form-control" value={(post.like_quantity || '')} readOnly />
	                      </div>
	                      <div className="form-group">
	                      	<label>Content</label>
	                      	<div style = {{
                            backgroundColor: "#e9ecef",
                            border: "1px solid #e9ecef",
                            borderRadius: ".25rem",
                            padding: "10px",
                          }} dangerouslySetInnerHTML={{__html: (post.content || '') }} />
	                      </div>
                        <div className="form-group">
                          <Link to={`/admin/post/edit/${post.post_id}`}> <button className="mb-2 mr-2 btn-transition btn btn-outline-primary">Edit</button> </Link>
                          <button className="mb-2 mr-2 btn-transition btn btn-outline-danger" onClick={() => delModalRef.current.open()}>Delete</button>
	    		              </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="main-card mb-3 card">
                      <div className="card-body">
                        <div className="comments" 
                          style={rightDivHeigth != 0 && comments.length > 0 ? {
                            maxHeight: rightDivHeigth,
                            overflowY: 'auto',
                            scrollbarWidth: 'thin',
                          } : {}}>
                            {
                              comments.length > 0 ? (
                                <div className="comment-container">
                                  {
                                    rootComment.map((comment, index) => (
                                      <div key={index}>
                                        <ShowComment 
                                          comment={comment} 
                                          replies={getRepliesComment(comment.comment_id)} 
                                          loadRepliesHandler={fetchReplies}
                                          deleteCommentHandler={deleteCommentHandler}
                                        />
                                      </div>
                                    ))
                                  }
                                </div>
                              ) : (
                                <h3 style={{textAlign: 'center'}}>No comment</h3>
                              )
                            }
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <ActionModal
                header={'Warning'} 
                confirmAction={deletePost} 
                actionId={post.post_id} 
                ref={delModalRef}
          >
            <h5>Do you really want to delete this post ? (this action can not be undo)</h5>
          </ActionModal>
        </>
    )
}

export default ShowPost
