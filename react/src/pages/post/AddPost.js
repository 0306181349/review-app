import React from 'react';
import BasePostForm from '../../components/form/post/BasePostForm';
import { Redirect } from 'react-router-dom';
import { useState } from 'react';
import FailureAlert from '../../components/alert/FailureAlert';

const AddPost = () => {
    const [redirectParam, setRedirectParam] = useState(0);
    const [errors, setErrors] = useState(null);

    return redirectParam!=0 ? ( <Redirect to={`/admin/post/${redirectParam}`} /> ) : (
        <> 
              <div className="app-main__inner">
                <div className="app-page-title">
                  <div className="page-title-wrapper">
                    <div className="page-title-heading">
                      <div className="page-title-icon">
                        <i className="pe-7s-drawer icon-gradient bg-happy-itmeo">
                        </i>
                      </div>
                      <div>Add post
                          <div className="page-title-subheading">
                      </div>
                    </div>
                   </div>
                  </div>
                </div>            
                <div className="row">
                  <div className="col-lg-12">
                    <div className="main-card mb-3 card">
                      {errors && <FailureAlert errors={errors}/>}
                      <div className="card-body">
                          <BasePostForm post={null} setErrors={setErrors} setRedirectParam={setRedirectParam} />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
        </>
    )
}

export default AddPost
