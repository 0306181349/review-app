import React from 'react'
import { NavLink } from 'react-router-dom'

const Sidebar = () => {
    return (
        <>
<div className="app-sidebar sidebar-shadow">
  <div className="app-header__logo">
    <div className="logo-src" />
    <div className="header__pane ml-auto">
      <div>
        <button type="button" className="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
          <span className="hamburger-box">
            <span className="hamburger-inner" />
          </span>
        </button>
      </div>
    </div>
  </div>
  <div className="app-header__mobile-menu">
    <div>
      <button type="button" className="hamburger hamburger--elastic mobile-toggle-nav">
        <span className="hamburger-box">
          <span className="hamburger-inner" />
        </span>
      </button>
    </div>
  </div>
  <div className="app-header__menu">
    <span>
      <button type="button" className="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
        <span className="btn-icon-wrapper">
          <i className="fa fa-ellipsis-v fa-w-6" />
        </span>
      </button>
    </span>
  </div>    
  <div className="scrollbar-sidebar">
    <div className="app-sidebar__inner">
      <ul className="vertical-nav-menu">
        <li className="app-sidebar__heading"></li>
          <li>
            <NavLink to={'/admin/dashboard'} activeClassName="mm-active" >
                <i className="metismenu-icon pe-7s-graph2" />
                Dashboard
            </NavLink>
          </li>
        <li className="app-sidebar__heading"></li>
          <li>
            <NavLink to={'/admin/users'} activeClassName="mm-active" >
                <i className="metismenu-icon pe-7s-user" />
                User
            </NavLink>
          </li>
        <li className="app-sidebar__heading"></li>
        <li>
            <NavLink to={'/admin/post'} activeClassName="mm-active" >
                <i className="metismenu-icon pe-7s-albums" />
                Post
            </NavLink>
        </li>
        <li className="app-sidebar__heading"></li>
        <li>
            <NavLink to={'/admin/location'} activeClassName="mm-active" >
                <i className="metismenu-icon pe-7s-map-marker" />
                Location
            </NavLink>
        </li>
        <li className="app-sidebar__heading"></li>
        <li>
            <NavLink to={'/admin/hashtag'} activeClassName="mm-active" >
                <i className="metismenu-icon  pe-7s-link" />
                Hashtag
            </NavLink>
        </li>
        <li className="app-sidebar__heading"></li>
        <li>
            <NavLink to={'/admin/report/post'} activeClassName="mm-active" >
                <i className="metismenu-icon pe-7s-attention" />
                Post report
            </NavLink>
        </li>
        <li className="app-sidebar__heading"></li>
        <li>
            <NavLink to={'/admin/locked-post'} activeClassName="mm-active" >
                <i className="metismenu-icon pe-7s-attention" />
                Locked post
            </NavLink>
        </li>
        <li className="app-sidebar__heading"></li>
        <li>
            <a href="#">
                <i className="metismenu-icon pe-7s-config"></i>
                Config
                <i className="metismenu-state-icon pe-7s-angle-down caret-left"></i>
            </a>
            <ul>
                <li>
                    <NavLink to={'/admin/category'} activeClassName="mm-active" >
                        <i className="metismenu-icon">
                        </i>Category
                    </NavLink>
                </li>
                <li>
                    <NavLink to={'/admin/province'} activeClassName="mm-active" >
                        <i className="metismenu-icon">
                        </i>Province
                    </NavLink>
                </li>
                <li>
                    <NavLink to={'/admin/report-title'} activeClassName="mm-active" >
                        <i className="metismenu-icon">
                        </i>Report title
                    </NavLink>
                </li>
            </ul>
        </li>
      </ul>
    </div>
  </div>
</div>
  
        </>
    )
}

export default Sidebar
