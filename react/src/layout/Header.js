import React from 'react';
import { makeRequest } from '../utility/API';
import { Link } from 'react-router-dom';
import { baseURL } from '../utility/API';

const Header = ({user}) => {
  const setLogOut = (data) => {
    window.location.replace("http://review-app.local/admin/login");
  }

  function sayHello() {
    alert('Hello!');
  } 

  const logOut = () => {
    makeRequest({
      url: `admin/logout`,
      successCallback: (data) => {
          window.location.replace(baseURL+"/login");
      },
      failureCallback: (error) => {
          console.log(error);
      },
      requestType: 'GET',
  });
  }

  return (
    <>
      <div className="app-header header-shadow">
        <div className="app-header__logo">
          <div className="logo-src" />
          <div className="header__pane ml-auto">
            <div>
              <button type="button" className="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                <span className="hamburger-box">
                  <span className="hamburger-inner" />
                </span>
              </button>
            </div>
          </div>
        </div>
        <div className="app-header__mobile-menu">
          <div>
            <button type="button" className="hamburger hamburger--elastic mobile-toggle-nav">
              <span className="hamburger-box">
                <span className="hamburger-inner" />
              </span>
            </button>
          </div>
        </div>
        <div className="app-header__menu">
          <span>
            <button type="button" className="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
              <span className="btn-icon-wrapper">
                <i className="fa fa-ellipsis-v fa-w-6" />
              </span>
            </button>
          </span>
        </div>    
        <div className="app-header__content">
          <div className="app-header-right">
            <div className="header-btn-lg pr-0">
              <div className="widget-content p-0">
                <div className="widget-content-wrapper">
                  <div className="widget-content-left">
                    <div className="btn-group">
                      <div className='img-round-div'>
                        {/* <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" className="p-0 btn">
                          <img width={42} className="rounded-circle" src={user.avatar}/>
                        </a> */}
                        <Link to={`/admin/users/${user.id}`}>
                          <img width={42} className="rounded-circle" src={user.avatar}/>
                        </Link>
                      </div>
                      {/* <div tabIndex={-1} role="menu" aria-hidden="true" className="dropdown-menu dropdown-menu-right">
                        <button type="button" tabIndex={0} className="dropdown-item">User Account</button>
                        <button type="button" tabIndex={0} onClick={sayHello} className="dropdown-item">Logout</button>
                        <div tabIndex={-1} className="dropdown-divider" />
                        <button type="button" tabIndex={0} className="dropdown-item">Settings</button>
                      </div> */}
                    </div>
                  </div>
                  <div className="widget-content-left  ml-3 header-user-info">
                    <div className="widget-heading">
                      {user.user_name}
                    </div>
                    <div className="widget-subheading">
                      Admin
                    </div>
                  </div>
                  <div className="widget-content-right header-user-info ml-3">
                    <button type="button" onClick={() => logOut()} className="btn-shadow p-1 btn btn-primary btn-sm">
                    <i className="fas fa-sign-out-alt"></i>
                    </button>
                  </div>
                </div>
              </div>
            </div>      
          </div>
        </div>
      </div>     
    </>
  )
}

export default Header
