-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 15, 2021 at 09:50 AM
-- Server version: 8.0.18
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `app_review`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id` bigint(20) NOT NULL,
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `phone` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `g_uid` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `f_uid` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `firebase_token` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `first_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `gender` tinyint(4) NOT NULL DEFAULT '0',
  `birthday` date DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `bio` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `province` int(11) DEFAULT NULL,
  `exp_point` bigint(20) NOT NULL DEFAULT '0',
  `rank` int(11) NOT NULL DEFAULT '0',
  `following` bigint(20) NOT NULL DEFAULT '0',
  `followers` bigint(20) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `is_locked` tinyint(4) NOT NULL DEFAULT '0',
  `is_admin` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `user_name`, `password`, `email`, `phone`, `g_uid`, `f_uid`, `firebase_token`, `first_name`, `last_name`, `gender`, `birthday`, `avatar`, `bio`, `province`, `exp_point`, `rank`, `following`, `followers`, `created_at`, `created_by`, `is_deleted`, `deleted_at`, `deleted_by`, `updated_at`, `updated_by`, `is_locked`, `is_admin`) VALUES
(1, 'VinhPhat', '$2y$10$7Jz37ctFhEewGGzuJ7ADPu79USd62F9GyPm/o2FHOcLaL4rwZtpD.', 'phat@gmail.com', '0352559972', NULL, NULL, 'ekAEQEWvT8mO7rBqlu1gim:APA91bGUMrAd1Hw-u4lgmor5Ye2QbqbKRet3CL95Y-09l1Km4DaxtPnGrl8hPmVB_bv9eUTiSQvRACxq1Wh5srHIGWLkYHBf1HYx5a0DbUycwCF51YOZdthptK01nzw4_KYIme_OuMHw', 'Huỳnh', 'Vinh Phát', 0, '2000-07-04', 'upload/users/1/avatar/1636371436_837b5e479cf70993fc48.jpg', NULL, 50, 2000, 2, 1, 1, '2021-06-09 06:00:00', 1, 0, NULL, NULL, '2021-11-09 07:55:46', 1, 0, 1),
(2, 'TamVipProoo', '$2y$10$Tzmbtf1oCJjo87buza9zXekBYpCVKB9hWVM7iUrHC1Q4CnspIDVBy', 'tam@gmail.com', '0377677830', NULL, NULL, 'dYo-VlDQQL6pk3ajp9Lakg:APA91bGxl6zbyjSIZ2c-wzMuU_SoDw-NryHHEt80yNNHFRS3fHYOezepkIkhEv4QsgYdLx4kvgfIPxILwTxrcgY-uwBv9MJTv4RqnDH03pWHQyYDrxC0q_Bsnju_2SUddk0hsPPmFMz3', 'Nguyễn', 'Thành Tâm', 0, '2000-07-21', 'upload/users/2/avatar/avatar.jpg', NULL, 50, 2000, 1, 2, 0, '2021-06-25 18:48:23', 1, 0, NULL, NULL, '2021-11-08 05:22:42', 1, 0, 0),
(3, 'ThongDauBuoi', '$2y$10$Tzmbtf1oCJjo87buza9zXekBYpCVKB9hWVM7iUrHC1Q4CnspIDVBy', 'thong@gmail.com', '0987654321', NULL, NULL, '', 'Cao Thị', 'Minh Thông ', 1, '2000-07-14', 'upload/users/3/avatar/avatar.jpg', NULL, 50, 2000, 2, 0, 2, '2021-06-01 18:48:23', 1, 0, NULL, NULL, '2021-11-02 11:07:15', 1, 0, 0),
(23, 'tam', '$2y$10$N/fjT6.eAAzWjO/3duzrQeTUfyZG3D2G59WGPF1amqaW8hNXO/oSC', '', '0352559973', '', NULL, '', '', '', 0, '0000-00-00', 'upload/users/23/avatar/avatar.jpg', NULL, 0, 0, 0, 0, 0, '2021-11-06 08:43:50', 23, 0, NULL, NULL, '2021-11-06 08:43:50', NULL, 0, 0),
(24, 'phat', '$2y$10$IVUUaFfzoA1xnOmz.vDh9.ZQwibkKwvXWd0UiD6EVlkjBiHo6qIhi', '', '0352559977', '', NULL, 'fo_KfomxTJWXz56SPwCOxE:APA91bG297f0ipmr-daZzN0r0L0xFRWs-HAMBVoZYwvkVMiG0j21rQTlL6liSD0xqC_iDHQYt0ht_ZUNhTACXwq9F_csEOe0-FfrHSjcldIQjj-adrFeocnxz8on6pZKJANSz_J1qRiz', 'phat', 'huynh', 0, '2000-11-11', 'upload/users/24/avatar/avatar.jpg', NULL, 0, 0, 0, 0, 0, '2021-11-06 09:06:58', 24, 0, NULL, NULL, '2021-11-06 09:06:58', NULL, 0, 0),
(25, 'tam', '$2y$10$MExZpvaN9t515CX3vF3K7u8kQ3tqSWKYabonNE276qdN7QkdoPW0i', '', '0352559979', '', NULL, 'fo_KfomxTJWXz56SPwCOxE:APA91bF2MQ8e2CY1Qw6KqaASTeyTqxKR9oK3IN1C_MUxIX-FW-uuzjA-s1WwVfaYtiLmgctpzMYB1ci5Vi_fVRLhCnalewv5tjK8qwGHJcjeMKfa9bY4VmUicG11xv23IrFLs_VyVwbS', 'tsam', 'nguyen', 0, '2021-11-03', 'assets/images/avatars/avatar.jpg', NULL, 0, 0, 0, 0, 0, '2021-11-06 09:58:58', 25, 0, NULL, NULL, '2021-11-06 09:58:58', NULL, 0, 0),
(26, 'tamthanh2000', '$2y$10$pK8RHoax9N0eZElykKtqbeZYuOMGb1v1gdVSQzoHPHfcVg/Hz9kHG', '', '0306181359', '', NULL, 'dYo-VlDQQL6pk3ajp9Lakg:APA91bGxl6zbyjSIZ2c-wzMuU_SoDw-NryHHEt80yNNHFRS3fHYOezepkIkhEv4QsgYdLx4kvgfIPxILwTxrcgY-uwBv9MJTv4RqnDH03pWHQyYDrxC0q_Bsnju_2SUddk0hsPPmFMz3', 'tam', 'thanh', 0, '2021-11-08', 'upload/users/26/avatar/avatar.jpg', NULL, 0, 0, 0, 0, 0, '2021-11-08 01:46:33', 26, 0, NULL, NULL, '2021-11-08 05:36:47', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `auth`
--

CREATE TABLE `auth` (
  `id` bigint(20) NOT NULL,
  `user` bigint(20) NOT NULL,
  `access_token` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `refresh_token` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `expired_at` datetime NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `auth`
--

INSERT INTO `auth` (`id`, `user`, `access_token`, `refresh_token`, `expired_at`, `created_at`, `updated_at`) VALUES
(3, 1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJpYXQiOjE2MzY0NjYxNDcsImV4cCI6MTYzNjQ2OTc0N30.RP8R8i_LJilMaK6NKe-45_SKqUicRG365yHFEUB_Jbw', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJpYXQiOjE2MzY0NjYxNDcsImV4cCI6MTYzNjgyNjE0N30.9il3L4Rdnp1d_9VtYVCiWyDXdwSeOyvifZQk9HXoyY4', '2021-11-13 11:55:47', '2021-11-09 07:55:47', '2021-11-09 07:55:47'),
(14, 2, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjIiLCJpYXQiOjE2MzYzNzA1NjIsImV4cCI6MTYzNjM3NDE2Mn0.sWOs8qcjN-ytIV3lb0EheXTGAYM5_ZK51RL0jYIFX54', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjIiLCJpYXQiOjE2MzYzNzA1NjIsImV4cCI6MTYzNjczMDU2Mn0.foLI8vqNth3qcpvf4h_b3dMIWBCUTrGlqnN2RQ3Gv7g', '2021-11-12 09:22:42', '2021-11-08 05:22:42', '2021-11-08 05:22:42'),
(15, 23, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjIzIiwiaWF0IjoxNjM2MjA2MjQ2LCJleHAiOjE2MzYyMDk4NDZ9.uPpQI8FbeBj5uIA5mUOZp6G96V-QD4WHVtCSmpqF8dM', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjIzIiwiaWF0IjoxNjM2MjA2MjQ2LCJleHAiOjE2MzY1NjYyNDZ9.x-Dtrf5eQ_qmfpcoe2erxgIIpy9wK7z_dC_RydeZb64', '2021-11-10 11:44:06', '2021-11-06 08:44:06', '2021-11-06 08:44:06'),
(16, 24, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjI0IiwiaWF0IjoxNjM2MjA4MDk4LCJleHAiOjE2MzYyMTE2OTh9.BsjKmmRAUrc3TanuwpGTDUKeromsNwzaFAj8OozJW5w', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjI0IiwiaWF0IjoxNjM2MjA4MDk4LCJleHAiOjE2MzY1NjgwOTh9.DENKCWgXE5-wHYSVByc2XTQLvTK1xIJG8sFitDvmYPk', '2021-11-10 12:14:58', '2021-11-06 09:14:58', '2021-11-06 09:14:58'),
(17, 25, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MjUsImlhdCI6MTYzNjIxMDczOCwiZXhwIjoxNjM2MjE0MzM4fQ.V0nEBkaDedU6YrC_gqlXJBgZad14xKvWGAhJWPnJp-c', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MjUsImlhdCI6MTYzNjIxMDczOCwiZXhwIjoxNjM2NTcwNzM4fQ.8l9bqLio79Lc07TNR3mHf2x4RMLDbAWyNycpkv404kg', '2021-11-10 12:58:58', '2021-11-06 09:58:58', '2021-11-06 09:58:58'),
(18, 26, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjI2IiwiaWF0IjoxNjM2MzYyOTgxLCJleHAiOjE2MzYzNjY1ODF9.Deao1XNQiQP5A1OD2RXvlPUYY4as7riEb_Ldj4uw78c', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjI2IiwiaWF0IjoxNjM2MzYyOTgxLCJleHAiOjE2MzY3MjI5ODF9.nh0j1eryOzh2Tv2pLnEVmCIPYqCDrAWDh0sgdl3rC-4', '2021-11-12 07:16:21', '2021-11-08 03:16:21', '2021-11-08 03:16:21');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL,
  `category_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_by` bigint(20) DEFAULT NULL,
  `deleted_at` date DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`, `is_deleted`, `deleted_by`, `deleted_at`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'Đồ ăn', 0, NULL, NULL, '2021-06-12 17:27:42', 1, NULL, NULL),
(2, 'Đồ uống', 0, NULL, NULL, '2021-06-12 17:28:03', 1, NULL, NULL),
(3, 'Du lịch', 0, NULL, NULL, '2021-06-12 17:29:24', 1, NULL, NULL),
(4, 'Làm đẹp', 0, NULL, NULL, '2021-06-12 17:29:24', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` bigint(11) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `comment_id` bigint(20) DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `comment_image` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `post_id`, `comment_id`, `content`, `comment_image`, `created_at`, `created_by`, `is_deleted`, `deleted_at`, `deleted_by`, `updated_at`, `updated_by`) VALUES
(1, 1, NULL, 'Test 2', NULL, '2021-07-20 13:51:38', 1, 0, NULL, NULL, NULL, NULL),
(2, 1, NULL, 'Test 2', NULL, '2021-07-20 13:52:37', 1, 0, NULL, NULL, NULL, NULL),
(3, 1, 1, 'Rep Test 2', NULL, '2021-07-20 13:57:07', 1, 0, NULL, NULL, NULL, NULL),
(4, 1, 1, 'Test 2', NULL, '2021-07-22 21:54:47', 1, 0, NULL, NULL, NULL, NULL),
(5, 1, 1, 'Test 2', NULL, '2021-07-22 22:07:57', 1, 0, NULL, NULL, NULL, NULL),
(6, 1, 1, 'Test 2', NULL, '2021-07-22 22:08:27', 1, 0, NULL, NULL, NULL, NULL),
(7, 1, 1, 'Test 2', NULL, '2021-07-22 22:10:56', 1, 0, NULL, NULL, NULL, NULL),
(8, 8, NULL, 'ok quá đó', NULL, '2021-07-24 09:08:10', 2, 0, NULL, NULL, NULL, NULL),
(9, 8, NULL, 'Chưa đi lần nào, chắc sẽ đi một lần cho biết', NULL, '2021-07-24 09:08:55', 2, 0, NULL, NULL, NULL, NULL),
(10, 7, NULL, 'Giá hơi cao nhỉ', NULL, '2021-07-24 09:09:16', 2, 0, NULL, NULL, NULL, NULL),
(11, 7, NULL, 'Có em này đi thì sao nhỉ.hh', 'upload/comment/11/1627092612_29599ce1e5373861fedc.jpg', '2021-07-24 09:10:12', 2, 0, NULL, NULL, NULL, NULL),
(12, 8, 1, 'như buồi', NULL, '2021-07-24 09:10:39', 2, 0, NULL, NULL, NULL, NULL),
(13, 8, 1, 'như buồi', NULL, '2021-07-24 09:12:47', 2, 0, NULL, NULL, NULL, NULL),
(14, 8, 1, 'hơi xa', NULL, '2021-07-24 09:12:57', 2, 0, NULL, NULL, NULL, NULL),
(15, 8, NULL, 'Nhìn sướng thể nhở', NULL, '2021-07-24 09:14:13', 1, 0, NULL, NULL, NULL, NULL),
(16, 7, NULL, 'chill phết', NULL, '2021-07-24 09:14:24', 1, 0, NULL, NULL, NULL, NULL),
(17, 8, 1, 'được đấy', NULL, '2021-07-24 09:14:36', 2, 0, NULL, NULL, NULL, NULL),
(18, 8, 1, 'được đấy', 'upload/comment/18/1627092890_ae93573a5714a4adb19b.jpg', '2021-07-24 09:14:50', 2, 0, NULL, NULL, NULL, NULL),
(19, 8, 15, 'được đấy', 'upload/comment/19/1627093102_6fdc149ef03b465e4a3e.jpg', '2021-07-24 09:18:22', 2, 0, NULL, NULL, NULL, NULL),
(20, 8, 15, 'ngon lành', NULL, '2021-07-24 09:18:33', 2, 0, NULL, NULL, NULL, NULL),
(21, 7, 16, 'comment test', NULL, '2021-07-24 10:24:10', 1, 0, NULL, NULL, NULL, NULL),
(22, 7, 16, 'fsfdffdf', NULL, '2021-07-24 10:25:59', 1, 0, NULL, NULL, NULL, NULL),
(23, 7, 11, 'dsfdsf', NULL, '2021-07-24 10:29:03', 1, 0, NULL, NULL, NULL, NULL),
(24, 7, 11, 'fdf', NULL, '2021-07-24 10:33:36', 1, 0, NULL, NULL, NULL, NULL),
(25, 7, 11, 'gfdfdg', NULL, '2021-07-24 10:43:35', 1, 0, NULL, NULL, NULL, NULL),
(26, 7, 16, 'dsfdfd', NULL, '2021-07-24 10:54:28', 1, 0, NULL, NULL, NULL, NULL),
(27, 7, NULL, 'dsadsd', NULL, '2021-07-24 11:01:17', 1, 0, NULL, NULL, NULL, NULL),
(28, 7, NULL, 'dsffd', NULL, '2021-07-24 11:01:33', 1, 0, NULL, NULL, NULL, NULL),
(29, 7, 27, 'fdfdffd', NULL, '2021-07-24 11:04:55', 1, 0, NULL, NULL, NULL, NULL),
(30, 7, 10, 'fdfdsfdsffdf', NULL, '2021-07-24 11:07:46', 1, 0, NULL, NULL, NULL, NULL),
(31, 7, NULL, 'fdfdffdf', NULL, '2021-07-24 11:09:37', 1, 0, NULL, NULL, NULL, NULL),
(32, 7, 27, 'fdfdgf', NULL, '2021-07-24 11:10:29', 1, 0, NULL, NULL, NULL, NULL),
(33, 7, 31, 'fdsfdffs', NULL, '2021-07-24 11:11:19', 1, 0, NULL, NULL, NULL, NULL),
(34, 7, NULL, 'aaaaaaaaaaaaa', NULL, '2021-07-24 11:11:52', 1, 0, NULL, NULL, NULL, NULL),
(35, 7, 27, '', NULL, '2021-07-24 11:12:38', 1, 0, NULL, NULL, NULL, NULL),
(36, 7, 31, 'fdsfdfd', NULL, '2021-07-24 11:15:12', 1, 0, NULL, NULL, NULL, NULL),
(37, 7, 34, 'dsdsads', NULL, '2021-07-24 11:15:42', 1, 0, NULL, NULL, NULL, NULL),
(38, 7, NULL, 'yrtytyty', NULL, '2021-07-24 11:22:42', 1, 0, NULL, NULL, NULL, NULL),
(39, 7, 38, 'dddd', NULL, '2021-07-24 11:24:44', 1, 0, NULL, NULL, NULL, NULL),
(40, 7, 38, 'ssssssssss', NULL, '2021-07-24 11:25:48', 1, 0, NULL, NULL, NULL, NULL),
(41, 7, 38, 'gffgfgfdg', NULL, '2021-07-24 11:28:28', 1, 0, NULL, NULL, NULL, NULL),
(42, 7, 38, 'fdfdf', NULL, '2021-07-24 11:30:34', 1, 0, NULL, NULL, NULL, NULL),
(43, 7, 34, 'dsdsds', NULL, '2021-07-24 11:31:21', 1, 0, NULL, NULL, NULL, NULL),
(44, 7, 34, 'ffdfdfd', NULL, '2021-07-24 11:31:48', 1, 0, NULL, NULL, NULL, NULL),
(45, 7, 38, 'gfgfgf', NULL, '2021-07-24 11:34:38', 1, 0, NULL, NULL, NULL, NULL),
(46, 7, 34, 'd', NULL, '2021-07-24 11:35:27', 1, 0, NULL, NULL, NULL, NULL),
(47, 7, NULL, 'sdsd', 'upload/comment/47/1627116769_914573afdf64b4f9b7b4.jpg', '2021-07-24 15:52:49', 1, 0, NULL, NULL, NULL, NULL),
(48, 7, 38, 'fghg', NULL, '2021-07-24 15:53:49', 1, 0, NULL, NULL, NULL, NULL),
(49, 7, 47, 'fdfdsfd', NULL, '2021-07-24 19:10:44', 1, 0, NULL, NULL, NULL, NULL),
(50, 7, 47, 'dsadsdsddsad', NULL, '2021-07-24 19:13:23', 1, 0, NULL, NULL, NULL, NULL),
(51, 7, 34, 'dd', NULL, '2021-07-24 19:25:35', 1, 0, NULL, NULL, NULL, NULL),
(52, 5, NULL, 'nhin ngon qua', NULL, '2021-07-24 19:28:47', 1, 0, NULL, NULL, NULL, NULL),
(53, 5, 52, 'fdfdeg', NULL, '2021-07-24 19:30:39', 1, 0, NULL, NULL, NULL, NULL),
(54, 8, NULL, 'phai di', NULL, '2021-07-24 19:33:17', 1, 0, NULL, NULL, NULL, NULL),
(55, 8, 8, 'co tien r di', NULL, '2021-07-24 19:33:36', 1, 0, NULL, NULL, NULL, NULL),
(56, 8, 54, 'aaaaaaaaaaaaaaaaa', NULL, '2021-07-24 19:50:27', 2, 0, NULL, NULL, NULL, NULL),
(57, 5, NULL, 'ddsdsdds', NULL, '2021-07-24 19:50:41', 1, 0, NULL, NULL, NULL, NULL),
(58, 16, NULL, 'Test thông báo', NULL, '2021-07-24 19:50:51', 2, 0, NULL, NULL, NULL, NULL),
(59, 7, NULL, 'haha', 'upload/comment/59/1635647568_2252742faeea5f929825.jpg', '2021-10-31 09:32:48', 1, 0, NULL, NULL, NULL, NULL),
(60, 7, 47, 'hello', 'upload/comment/60/1635648088_b87a063cf2f03b990aec.jpg', '2021-10-31 09:41:28', 1, 0, NULL, NULL, NULL, NULL),
(61, 7, 47, 'ok', NULL, '2021-10-31 09:42:27', 1, 0, NULL, NULL, NULL, NULL),
(62, 7, 59, 'okkkkkkkkkkkkkk', NULL, '2021-10-31 09:44:13', 1, 0, NULL, NULL, NULL, NULL),
(63, 7, 59, 'okkk', NULL, '2021-10-31 09:46:19', 1, 0, NULL, NULL, NULL, NULL),
(64, 7, NULL, 'oonr ap', NULL, '2021-10-31 09:47:38', 1, 0, NULL, NULL, NULL, NULL),
(65, 7, 59, 'a', 'upload/comment/65/1635648581_95b8942165b27ccca568.jpg', '2021-10-31 09:49:41', 1, 0, NULL, NULL, NULL, NULL),
(66, 7, NULL, 'for DF fds fds for DF for DS FD SF DF do FD for DF DS fds for FD for DF DS fds for DF do for DF do FD SF DF DS FD SF DF DS FD fds for DF DS for DF do FD fds for DF DS FD for DF DS for SF DS fds for DF DS FD SF DF DS FD fds for for ew ew great g for did GF gds feel feel. DS do for DF DS FD feel ', NULL, '2021-10-31 10:50:34', 1, 0, NULL, NULL, NULL, NULL),
(67, 7, NULL, 'hahaksaj. fds fgfd. FD fds fgfd RB d', NULL, '2021-10-31 10:55:39', 1, 0, NULL, NULL, NULL, NULL),
(68, 7, NULL, 'Hello', 'upload/comment/68/1635652562_e7459a00958491175112.jpg', '2021-10-31 10:56:02', 1, 0, NULL, NULL, '2021-10-31 07:53:20', 1),
(69, 7, 67, 'ok ok.kkk', NULL, '2021-10-31 12:34:20', 1, 0, NULL, NULL, NULL, NULL),
(70, 7, 67, 'xin chao', 'upload/comment/70/1635658568_fb548793bc1a28143408.jpg', '2021-10-31 12:36:08', 1, 0, NULL, NULL, NULL, NULL),
(71, 7, 67, 'test', NULL, '2021-10-31 12:57:23', 1, 0, NULL, NULL, NULL, NULL),
(72, 15, NULL, 'hello', NULL, '2021-11-08 14:12:42', 2, 0, NULL, NULL, NULL, NULL),
(73, 17, NULL, 'Hay qua', NULL, '2021-11-08 18:07:33', 1, 0, NULL, NULL, NULL, NULL),
(74, 7, 68, 'hi', NULL, '2021-11-08 18:08:11', 1, 0, NULL, NULL, NULL, NULL),
(75, 7, 67, 'hello', NULL, '2021-11-08 18:08:32', 1, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `follow`
--

CREATE TABLE `follow` (
  `id` bigint(20) NOT NULL,
  `account_id_1` bigint(20) NOT NULL,
  `account_id_2` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `follow`
--

INSERT INTO `follow` (`id`, `account_id_1`, `account_id_2`, `created_at`) VALUES
(3, 2, 1, '2021-07-06 12:43:39'),
(4, 2, 3, '2021-07-06 12:43:39'),
(6, 3, 2, '2021-07-06 12:43:39'),
(10, 1, 2, '2021-10-23 20:05:50'),
(11, 1, 3, '2021-11-02 23:07:15');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` bigint(20) NOT NULL,
  `gallery_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `cover_photo` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `is_private` tinyint(1) NOT NULL DEFAULT '0',
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `gallery_name`, `description`, `cover_photo`, `is_private`, `is_default`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'Xem sau', NULL, NULL, 1, 1, '2021-08-11 22:38:47', 1, NULL, NULL),
(2, 'Xem sau', NULL, NULL, 1, 1, '2021-08-11 22:38:47', 2, NULL, NULL),
(3, 'Xem sau', NULL, NULL, 1, 1, '2021-08-11 22:39:14', 3, NULL, NULL),
(5, 'food', 'thuc an ngon', 'upload/gallery/5/1636117489_77df8c967dad8fa3cffb.jpg', 0, 0, '2021-11-05 20:04:49', 1, '2021-11-06 22:44:06', 1),
(6, 'Xem sau', NULL, NULL, 0, 1, '2021-11-06 20:43:50', 23, NULL, NULL),
(7, 'Xem sau', NULL, NULL, 0, 1, '2021-11-06 21:06:58', 24, NULL, NULL),
(8, 'Xem sau', NULL, NULL, 0, 1, '2021-11-06 21:58:58', 25, NULL, NULL),
(9, 'Xem sau', NULL, NULL, 0, 1, '2021-11-08 14:46:33', 26, NULL, NULL),
(10, 'an uong', '', 'upload/gallery/10/1636357829_be1e54741f8a6a1253db.jpg', 0, 0, '2021-11-08 14:50:29', 26, NULL, NULL),
(11, 'money an', '', 'upload/gallery/11/1636369835_7bd0fc32a3989078d1e2.jpg', 1, 0, '2021-11-08 18:10:35', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_post`
--

CREATE TABLE `gallery_post` (
  `id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `gallery_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `gallery_post`
--

INSERT INTO `gallery_post` (`id`, `post_id`, `gallery_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(57, 3, 5),
(59, 8, 10),
(60, 17, 10),
(62, 7, 11),
(64, 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `hashtag`
--

CREATE TABLE `hashtag` (
  `id` bigint(20) NOT NULL,
  `hashtag` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_by` bigint(20) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `hashtag`
--

INSERT INTO `hashtag` (`id`, `hashtag`, `created_by`, `created_at`, `is_deleted`, `deleted_by`, `deleted_at`, `updated_at`, `updated_by`) VALUES
(1, 'dulich', 1, '2021-07-30 16:16:47', 0, NULL, NULL, NULL, NULL),
(2, 'anuong', 1, '2021-07-30 16:16:47', 0, NULL, NULL, NULL, NULL),
(3, 'spa', 1, '2021-07-30 16:36:21', 0, NULL, NULL, NULL, NULL),
(4, 'anvat', 1, '2021-08-02 11:42:30', 0, NULL, NULL, NULL, NULL),
(5, 'vuacua', 1, '2021-08-02 11:42:30', 0, NULL, NULL, NULL, NULL),
(6, 'bunbo', 1, '2021-08-02 11:47:45', 0, NULL, NULL, NULL, NULL),
(7, 'bunrieu', 1, '2021-08-02 11:47:45', 0, NULL, NULL, '2021-11-13 03:05:27', 1),
(8, 'matcha', 1, '2021-11-13 16:05:36', 0, NULL, NULL, '2021-11-14 08:54:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `like_comment`
--

CREATE TABLE `like_comment` (
  `id` bigint(20) NOT NULL,
  `comment_id` bigint(20) NOT NULL,
  `account_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `like_comment`
--

INSERT INTO `like_comment` (`id`, `comment_id`, `account_id`) VALUES
(2, 54, 2),
(15, 12, 1),
(16, 13, 1),
(17, 14, 1),
(18, 8, 1),
(34, 16, 1),
(35, 26, 1),
(36, 22, 1),
(37, 21, 1),
(38, 50, 1),
(41, 45, 1),
(42, 41, 1),
(44, 49, 1),
(45, 34, 1),
(46, 44, 1),
(47, 43, 1),
(48, 42, 1),
(49, 30, 1),
(50, 54, 1),
(52, 47, 1),
(54, 40, 1),
(59, 65, 1),
(60, 63, 1),
(61, 19, 1),
(62, 20, 1),
(63, 8, 1),
(65, 8, 2),
(66, 72, 2),
(69, 68, 1),
(70, 71, 1);

-- --------------------------------------------------------

--
-- Table structure for table `like_post`
--

CREATE TABLE `like_post` (
  `id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `account_id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `like_post`
--

INSERT INTO `like_post` (`id`, `post_id`, `account_id`, `created_at`) VALUES
(1, 8, 2, '2021-07-06 12:45:27'),
(2, 6, 1, '2021-07-06 12:45:27'),
(3, 6, 2, '2021-07-06 12:45:27'),
(5, 2, 1, '2021-10-09 19:54:09'),
(7, 17, 2, '2021-10-13 21:33:23'),
(8, 16, 2, '2021-10-13 21:33:44'),
(9, 16, 1, '2021-10-13 21:33:44'),
(10, 16, 3, '2021-10-13 21:33:57'),
(11, 15, 1, '2021-10-13 21:33:57'),
(12, 14, 1, '2021-10-13 21:34:08'),
(13, 13, 1, '2021-10-13 21:34:08'),
(38, 3, 1, '2021-11-07 13:39:17'),
(39, 1, 1, '2021-11-07 13:38:11'),
(40, 7, 1, '2021-10-23 20:54:41'),
(47, 8, 1, '2021-11-02 22:23:32'),
(51, 17, 25, '2021-11-06 21:59:29'),
(52, 16, 25, '2021-11-06 21:59:32'),
(53, 8, 25, '2021-11-06 22:00:05'),
(54, 14, 2, '2021-11-07 13:25:02'),
(59, 27, 26, '2021-11-08 14:49:39'),
(60, 5, 1, '2021-11-08 18:07:02'),
(63, 17, 1, '2021-11-08 18:20:54'),
(64, 15, 2, '2021-11-08 18:22:50');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` bigint(11) NOT NULL,
  `location_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `open_time` time NOT NULL,
  `closed_time` time NOT NULL,
  `lowest_price` decimal(10,0) DEFAULT NULL,
  `highest_price` decimal(10,0) DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `province` int(11) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longtitude` double DEFAULT NULL,
  `phone_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_verify` bigint(20) DEFAULT NULL,
  `verified_by` bigint(20) DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `location_name`, `open_time`, `closed_time`, `lowest_price`, `highest_price`, `address`, `province`, `latitude`, `longtitude`, `phone_number`, `is_verify`, `verified_by`, `is_deleted`, `deleted_at`, `deleted_by`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'Bếp Chef Hó-Nguyễn Trãi', '08:00:00', '13:00:00', '55000', '75000', '212B/D28/4 Nguyễn Trãi, Phường Nguyễn Cư Trinh, Quận 1, Hồ Chí Minh', 50, 123.123456, 123.123456, '', 0, NULL, 0, NULL, NULL, '2021-06-10 10:27:35', 1, '2021-06-25 21:04:47', NULL),
(2, 'WELAX - Gội Đầu Thư Giãn', '10:00:00', '20:00:00', NULL, NULL, '245 Tân Hương,Phường Tân Quý,Quận Tân Phú,Hồ Chí Minh', 50, 123.123456, 123.123456, '0767634639', 0, NULL, 0, NULL, NULL, '2021-06-13 01:49:34', 1, NULL, NULL),
(3, 'Nhà Hàng Las Vegas', '16:00:00', '23:00:00', '49000', '300000', '188 - 190 - 192 Nguyễn Văn Linh, Phường Tân Thuận Tây, Quận 7, Hồ Chí Minh', 50, 123.123456, 123.123456, '', 0, NULL, 0, NULL, NULL, '2021-06-13 01:57:58', 1, NULL, NULL),
(4, 'Texas Chicken - Big C Nguyễn Thị Thập', '10:00:00', '22:00:00', NULL, NULL, 'Shop 01, Quận 7, Hồ Chí Minh', 50, 123.123456, 123.123456, '1900636091', 0, NULL, 0, NULL, NULL, '2021-06-13 01:57:58', 1, NULL, NULL),
(5, 'Léon Bar', '08:00:00', '22:00:00', NULL, NULL, '122 Hồ Tùng Mậu,Phường Bến Nghé,Quận 1,Hồ Chí Minh', 50, 123.123456, 123.123456, '02632222239', 0, NULL, 0, NULL, NULL, '2021-06-13 08:13:56', 1, NULL, NULL),
(6, 'Buôn Ma Thuột', '01:00:00', '24:00:00', NULL, NULL, 'Thành phố BMT, Phường Thống Nhất, Thành phố Buôn Mê Thuột, Đắk Lắk', 50, 123.123456, 123.123456, '', 0, NULL, 0, NULL, NULL, '2021-06-13 08:17:09', 1, NULL, NULL),
(10, 'Bún Riêu 66', '11:00:00', '21:00:00', '25000', '40000', '287/66 Nguyễn Đình Chiểu, P.5, Q.3', 50, NULL, NULL, '', 0, NULL, 0, NULL, NULL, '2021-08-01 14:14:34', 1, NULL, NULL),
(11, 'Trà sữa Nọng - Vĩnh Viễn', '09:00:00', '22:00:00', '15000', '25000', '82 Vĩnh Viễn, Quận 10', 50, NULL, NULL, '', 0, NULL, 0, NULL, NULL, '2021-08-01 14:24:19', 1, NULL, NULL),
(12, 'Ốc Thanh Thúy - Chợ Phạm Văn Hai', '16:00:00', '23:00:00', '40000', '100000', '101 Tân Sơn , Phường 02, Quận Tân Bình, Hồ Chí Minh', 50, NULL, NULL, '', 0, NULL, 0, NULL, NULL, '2021-08-01 14:42:20', NULL, NULL, NULL),
(13, 'Ăn Vặt Chị Như', '17:00:00', '22:00:00', NULL, NULL, '533 Phạm Văn Bạch Quận Tân Bình', 50, NULL, NULL, '', 0, NULL, 0, NULL, NULL, '2021-08-01 14:50:37', NULL, NULL, NULL),
(14, 'Bò Cay Xóm Đất - Hủ Tiếu Mì Khô Bạc Liêu', '07:30:00', '22:00:00', '25000', '55000', '174A Xóm Đất, P9 Q11', 50, NULL, NULL, '', 0, NULL, 0, NULL, NULL, '2021-08-01 14:59:46', NULL, NULL, NULL),
(15, 'Nhà hàng Vua Cua - Vũ Huy Tấn', '10:00:00', '22:00:00', '0', '0', '30 Vũ Huy Tấn P3 Q Bình Thạnh ', 50, NULL, NULL, '1900636091', 0, NULL, 0, NULL, NULL, '2021-08-01 16:38:20', 1, '2021-08-01 16:38:20', NULL),
(16, 'Bún Bò Út Hằng - CMT8', '09:00:00', '22:00:00', '0', '0', '595/1 Cách Mạng Tháng 8 P15 Q10', 50, NULL, NULL, '0122345677', 0, NULL, 0, NULL, NULL, '2021-08-01 16:51:41', 1, '2021-08-01 16:51:41', NULL),
(17, 'Quan an Quan 7', '22:26:00', '00:00:00', '15000', NULL, '9/15, Truong Phuoc Phan', 50, NULL, NULL, '0352559972', 0, NULL, 0, NULL, NULL, '2021-11-05 00:27:53', 1, '2021-11-12 20:05:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` bigint(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1: another user followed u, 2: another user commented on your post, 3: another user replied your comment, 4: another user posted a new post, 5: another user liked your post, 6: another user liked your comment',
  `account_id` bigint(20) NOT NULL COMMENT 'user whose receive the noti',
  `post_id` bigint(20) DEFAULT NULL,
  `comment_id` bigint(20) DEFAULT NULL,
  `follow_id` bigint(20) DEFAULT NULL,
  `like_post_id` bigint(20) DEFAULT NULL,
  `like_comment_id` bigint(20) DEFAULT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `clicked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` bigint(20) NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `title`, `type`, `account_id`, `post_id`, `comment_id`, `follow_id`, `like_post_id`, `like_comment_id`, `seen`, `clicked`, `created_at`, `created_by`, `is_deleted`, `deleted_at`, `deleted_by`, `updated_at`, `updated_by`) VALUES
(1, 'đã theo dõi bạn', 1, 1, NULL, NULL, 3, NULL, NULL, 1, 1, '2021-08-03 19:12:17', 2, 0, NULL, 0, NULL, NULL),
(2, 'đã thêm một bài viết mới', 4, 1, 1, NULL, NULL, NULL, NULL, 1, 1, '2021-08-03 21:16:19', 2, 0, NULL, 0, NULL, NULL),
(3, 'đã thích bài viết của bạn', 5, 1, NULL, NULL, NULL, 1, NULL, 1, 1, '2021-08-03 21:25:40', 2, 0, NULL, 0, NULL, NULL),
(4, 'đã bình luận về bài viết của bạn', 2, 1, NULL, 58, NULL, NULL, NULL, 1, 1, '2021-08-26 14:44:53', 2, 0, NULL, 0, NULL, NULL),
(5, 'đã trả lời bình luận của bạn ', 3, 1, NULL, 56, NULL, NULL, NULL, 1, 1, '2021-08-26 14:44:53', 2, 0, NULL, 0, NULL, NULL),
(6, 'đã thích bình luận của bạn', 6, 1, NULL, NULL, NULL, NULL, 2, 1, 1, '2021-08-26 14:47:24', 2, 0, NULL, 0, NULL, NULL),
(7, 'đã thích bài viết của bạn', 5, 3, NULL, NULL, NULL, 14, NULL, 0, 0, '2021-10-14 02:21:31', 1, 0, NULL, 0, NULL, NULL),
(8, 'đã thích bài viết của bạn', 5, 3, NULL, NULL, NULL, 15, NULL, 0, 0, '2021-10-14 21:28:07', 1, 0, NULL, 0, NULL, NULL),
(9, 'đã thích bài viết của bạn', 5, 3, NULL, NULL, NULL, 16, NULL, 0, 0, '2021-10-14 21:28:10', 1, 0, NULL, 0, NULL, NULL),
(10, 'đã thích bài viết của bạn', 5, 3, NULL, NULL, NULL, 17, NULL, 0, 0, '2021-10-14 21:44:53', 1, 0, NULL, 0, NULL, NULL),
(11, 'đã thích bài viết của bạn', 5, 3, NULL, NULL, NULL, 18, NULL, 0, 0, '2021-10-15 02:40:20', 1, 0, NULL, 0, NULL, NULL),
(13, 'đã thích bình luận của bạn', 6, 2, NULL, NULL, NULL, NULL, 16, 1, 0, '2021-10-17 08:54:20', 1, 0, NULL, 0, NULL, NULL),
(14, 'đã thích bình luận của bạn', 6, 2, NULL, NULL, NULL, NULL, 17, 1, 0, '2021-10-17 08:54:22', 1, 0, NULL, 0, NULL, NULL),
(15, 'đã thích bình luận của bạn', 6, 2, NULL, NULL, NULL, NULL, 18, 1, 0, '2021-10-17 08:54:27', 1, 0, NULL, 0, NULL, NULL),
(19, 'đã theo dõi bạn', 1, 2, NULL, NULL, 7, NULL, NULL, 1, 0, '2021-10-18 09:18:41', 1, 0, NULL, 0, NULL, NULL),
(31, 'đã theo dõi bạn', 1, 2, NULL, NULL, 8, NULL, NULL, 1, 0, '2021-10-23 07:49:49', 1, 0, NULL, 0, NULL, NULL),
(32, 'đã theo dõi bạn', 1, 2, NULL, NULL, 9, NULL, NULL, 1, 0, '2021-10-23 08:01:42', 1, 0, NULL, 0, NULL, NULL),
(33, 'đã theo dõi bạn', 1, 2, NULL, NULL, 10, NULL, NULL, 1, 0, '2021-10-23 08:05:50', 1, 0, NULL, 0, NULL, NULL),
(39, 'đã thích bài viết của bạn', 5, 2, NULL, NULL, NULL, 38, NULL, 1, 0, '2021-10-23 08:54:34', 1, 0, NULL, 0, NULL, NULL),
(40, 'đã thích bài viết của bạn', 5, 2, NULL, NULL, NULL, 39, NULL, 1, 0, '2021-10-23 08:54:36', 1, 0, NULL, 0, NULL, NULL),
(41, 'đã thích bài viết của bạn', 5, 2, NULL, NULL, NULL, 40, NULL, 1, 0, '2021-10-23 08:54:41', 1, 0, NULL, 0, NULL, NULL),
(42, 'đã thích bài viết của bạn', 5, 3, NULL, NULL, NULL, 41, NULL, 0, 0, '2021-10-29 22:42:06', 1, 0, NULL, 0, NULL, NULL),
(43, 'đã thích bài viết của bạn', 5, 3, NULL, NULL, NULL, 44, NULL, 0, 0, '2021-10-30 01:19:47', 1, 0, NULL, 0, NULL, NULL),
(44, 'đã bình luận về bài viết của bạn', 2, 2, NULL, 59, NULL, NULL, NULL, 1, 0, '2021-10-30 21:32:48', 1, 0, NULL, 0, NULL, NULL),
(45, 'đã bình luận về bài viết của bạn', 2, 2, NULL, 64, NULL, NULL, NULL, 1, 0, '2021-10-30 21:47:38', 1, 0, NULL, 0, NULL, NULL),
(46, 'đã bình luận về bài viết của bạn', 2, 2, NULL, 66, NULL, NULL, NULL, 1, 0, '2021-10-30 22:50:34', 1, 0, NULL, 0, NULL, NULL),
(47, 'đã bình luận về bài viết của bạn', 2, 2, NULL, 67, NULL, NULL, NULL, 1, 0, '2021-10-30 22:55:39', 1, 0, NULL, 0, NULL, NULL),
(48, 'đã bình luận về bài viết của bạn', 2, 2, NULL, 68, NULL, NULL, NULL, 1, 0, '2021-10-30 22:56:02', 1, 0, NULL, 0, NULL, NULL),
(49, 'đã thích bình luận của bạn', 6, 2, NULL, NULL, NULL, NULL, 61, 1, 0, '2021-11-02 10:14:10', 1, 0, NULL, 0, NULL, NULL),
(50, 'đã thích bình luận của bạn', 6, 2, NULL, NULL, NULL, NULL, 62, 1, 0, '2021-11-02 10:14:12', 1, 0, NULL, 0, NULL, NULL),
(51, 'đã thích bình luận của bạn', 6, 2, NULL, NULL, NULL, NULL, 63, 1, 0, '2021-11-02 10:14:14', 1, 0, NULL, 0, NULL, NULL),
(52, 'đã theo dõi bạn', 1, 3, NULL, NULL, 11, NULL, NULL, 0, 0, '2021-11-02 11:07:15', 1, 0, NULL, 0, NULL, NULL),
(53, 'đã thích bài viết của bạn', 5, 3, NULL, NULL, NULL, 48, NULL, 0, 0, '2021-11-02 11:08:29', 1, 1, '2021-11-08 05:07:07', 1, NULL, NULL),
(54, 'đã thích bình luận của bạn', 6, 2, NULL, NULL, NULL, NULL, 63, 1, 1, '2021-11-06 08:06:49', 1, 0, NULL, 0, NULL, NULL),
(55, 'đã thích bài viết của bạn', 5, 3, NULL, NULL, NULL, 49, NULL, 0, 0, '2021-11-06 09:16:15', 24, 0, NULL, 0, NULL, NULL),
(57, 'đã thích bài viết của bạn', 5, 3, NULL, NULL, NULL, 51, NULL, 0, 0, '2021-11-06 09:59:29', 25, 0, NULL, 0, NULL, NULL),
(58, 'đã thích bài viết của bạn', 5, 1, NULL, NULL, NULL, 52, NULL, 1, 1, '2021-11-06 09:59:32', 25, 0, NULL, 0, NULL, NULL),
(59, 'đã thích bài viết của bạn', 5, 1, NULL, NULL, NULL, 53, NULL, 1, 1, '2021-11-06 10:00:05', 25, 0, NULL, 0, NULL, NULL),
(60, 'đã thích bài viết của bạn', 5, 1, NULL, NULL, NULL, 54, NULL, 1, 1, '2021-11-07 01:25:02', 2, 0, NULL, 0, NULL, NULL),
(61, 'đã thích bài viết của bạn', 5, 1, NULL, NULL, NULL, 56, NULL, 0, 0, '2021-11-08 01:11:15', 2, 1, '2021-11-08 01:12:02', 2, NULL, NULL),
(62, 'đã thích bài viết của bạn', 5, 1, NULL, NULL, NULL, 57, NULL, 0, 0, '2021-11-08 01:12:04', 2, 1, '2021-11-08 01:12:07', 2, NULL, NULL),
(63, 'đã thích bài viết của bạn', 5, 1, NULL, NULL, NULL, 58, NULL, 0, 0, '2021-11-08 01:12:10', 2, 1, '2021-11-08 01:12:13', 2, NULL, NULL),
(64, 'đã bình luận về bài viết của bạn', 2, 1, NULL, 72, NULL, NULL, NULL, 1, 1, '2021-11-08 01:12:42', 2, 0, NULL, 0, NULL, NULL),
(65, 'đã thích bình luận của bạn', 6, 1, NULL, NULL, NULL, NULL, 67, 0, 0, '2021-11-08 01:13:05', 2, 1, '2021-11-08 01:13:08', 2, NULL, NULL),
(66, 'đã thích bình luận của bạn', 6, 1, NULL, NULL, NULL, NULL, 68, 0, 0, '2021-11-08 01:13:17', 2, 1, '2021-11-08 01:13:19', 2, NULL, NULL),
(67, 'đã thích bài viết của bạn', 5, 2, NULL, NULL, NULL, 60, NULL, 0, 0, '2021-11-08 05:07:02', 1, 0, NULL, 0, NULL, NULL),
(68, 'đã thích bài viết của bạn', 5, 3, NULL, NULL, NULL, 61, NULL, 0, 0, '2021-11-08 05:07:08', 1, 1, '2021-11-08 05:20:44', 1, NULL, NULL),
(69, 'đã bình luận về bài viết của bạn', 2, 3, NULL, 73, NULL, NULL, NULL, 0, 0, '2021-11-08 05:07:33', 1, 0, NULL, 0, NULL, NULL),
(70, 'đã thích bài viết của bạn', 5, 3, NULL, NULL, NULL, 62, NULL, 0, 0, '2021-11-08 05:20:45', 1, 1, '2021-11-08 05:20:50', 1, NULL, NULL),
(71, 'đã thích bài viết của bạn', 5, 3, NULL, NULL, NULL, 63, NULL, 0, 0, '2021-11-08 05:20:54', 1, 0, NULL, 0, NULL, NULL),
(72, 'đã thích bài viết của bạn', 5, 1, NULL, NULL, NULL, 64, NULL, 1, 0, '2021-11-08 05:22:50', 2, 0, NULL, 0, NULL, NULL),
(73, 'đã theo dõi bạn', 1, 26, NULL, NULL, 12, NULL, NULL, 0, 0, '2021-11-08 05:36:42', 1, 1, '2021-11-08 05:36:47', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `hashtag` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `rating` int(11) DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `category` int(11) NOT NULL,
  `location` bigint(20) NOT NULL,
  `post_image` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `post_video` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `view_quantity` bigint(20) NOT NULL DEFAULT '0',
  `like_quantity` bigint(20) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  `is_locked` tinyint(4) NOT NULL DEFAULT '0',
  `locked_by` bigint(20) DEFAULT NULL,
  `locked_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `title`, `hashtag`, `rating`, `content`, `category`, `location`, `post_image`, `post_video`, `view_quantity`, `like_quantity`, `created_at`, `created_by`, `updated_at`, `updated_by`, `is_deleted`, `deleted_at`, `deleted_by`, `is_locked`, `locked_by`, `locked_at`) VALUES
(1, 'gà rán Texas Chicken', NULL, 5, '<p><span class=\"text\">Nay thấy Texas chicken treo biển : \"d&ugrave;ng g&agrave; kh&ocirc;ng đ&ocirc;ng lạnh để chế biến\" nghe kh&aacute; hay n&ecirc;n v&agrave;o thử. 💸Combo m&igrave;nh 2 miếng g&agrave; chọn 1 miếng cay,1 kh&ocirc;ng + b&aacute;nh mật ong ,rau trộn v&agrave; ly nước refill gi&aacute; 89k. 🍗Miếng g&agrave; kh&ocirc;ng cay ăn vỏ mỏng gi&ograve;n,&iacute;t ngấm dầu ,b&ecirc;n trong phần thịt mọng nước v&agrave; vị ngọt thịt hơn hẳn trước. 🍗Miếng g&agrave; vị cay vị thơm hơn ,cay nhẹ nh&agrave;ng,tuy nhi&ecirc;n kh&aacute; mặn.Trong c&aacute;c loại miếng g&agrave; cay thấy đ&acirc;y vị mặn nhất hơn cả KFC,Mc donald. 🍗G&agrave; của texas miếng c&oacute; phần to nhỉnh hơn c&aacute;c miếng g&agrave; mọng nước của h&atilde;ng kh&aacute;c,v&agrave; kh&ocirc; hơn 1 ch&uacute;t n&ecirc;n bị cảm gi&aacute;c ng&aacute;n thịt ,kh&ocirc;ng hết nổi 2 miếng =&gt; g&oacute;i về cho con m&eacute;o vậy. 🥯B&aacute;nh phết mật ong rất ngon </span><span class=\"emoji-sizer emoji-outer \">👍</span><span class=\"text\"> 🥗Rau trộn nh&igrave;n kh&aacute; ch&aacute;n với h&igrave;nh quảng c&aacute;o nhưng ăn ổn. 🍅Tương c&agrave; kh&aacute; chua v&agrave; tương ớt kh&aacute; mặn </span><span class=\"emoji-sizer emoji-outer \">😃</span><span class=\"text\"> giống như tương của heniz nhưng được chỉnh vị lại đậm đ&agrave; hơn. 👉Tổng thể nếu ko lăn tăn về canh c tr&igrave;nh k m&atilde;i ăn g&agrave;,th&igrave; c&aacute;c bạn c&oacute; thể chọn Texas n&agrave;y ăn kh&aacute; ổn với gi&aacute;, m&agrave; kh&ocirc;ng cần chờ khuyến m&atilde;i.</span></p>', 1, 4, '\r\n[\"upload/post/1/img0.jpg\",\"upload/post/1/img1.jpg\",\"upload/post/1/img2.jpg\",\"upload/post/1/img3.jpg\",\"upload/post/1/img4.jpg\",\"upload/post/1/img5.jpg\",\"upload/post/1/img6.jpg\"]', NULL, 12302, 93, '2021-10-01 12:50:42', 2, NULL, NULL, 0, NULL, NULL, 1, 1, '2021-10-04 05:00:00'),
(2, 'Cơm trưa nhà hàng Las Vegas', NULL, 5, '<p>Trời h&ocirc;m nay mưa từ s&aacute;ng tới trưa,đang ko biết ăn g&igrave; lại lười ra ngo&agrave;i thấy nh&agrave; h&agrave;ng quen đang đ&oacute;ng để d&atilde;n c&aacute;ch chuyển qua phục vụ cơm trưa giao h&agrave;ng n&ecirc;n lựa rồi đặt 2 phần về. 💸M&igrave;nh chọn cơm b&ograve; x&agrave;o v&agrave; cơm sườn non kho gi&aacute; 45k 1 phần, freehip Q7 ,thiệt trời mưa n&agrave;y lười đi mua bắt qu&aacute;n ship hơi ngại m&agrave; quy định 3 phần mới ship m&igrave;nh đặt 2 cũng ok th&igrave; n&oacute;i chung đ&aacute;nh gi&aacute; phục phụ ok n&egrave;. Cơm nhận trong hộp chịu nhiệt,sạch sẽ v&agrave; kh&aacute; nặng tay,mở ra cơm nhiều (qu&aacute;n c&oacute; dặn ăn nhiều cơm sẽ cho th&ecirc;m bịch cơm nữa) trong c&oacute; cả rau x&agrave;o v&agrave; kim chi,1 miếng dưa hấu tr&aacute;ng miệng nữa. 👉phần sườn non kho : rất hấp dẫn sườn kho mềm cắn v&agrave;o thơm ngọt thịt ,ngon . 👉phần b&ograve; x&agrave;o rau củ : b&ograve; d&ugrave;ng thịt xịn n&egrave; cắt miếng ko nhỏ x&iacute;u như qu&aacute;n cơm,dầy hơn,x&agrave;o hơi nhạt kiểu giữ ngọt thịt h&ograve;a rau củ, chan nước tương mix sa tế cho k&egrave;m l&ecirc;n ăn th&igrave; vừa. 👉Cơm gạo ngon nấu hơi nh&atilde;o 1 ch&uacute;t . 👉kim chi ngon nha, hồi xưa m&igrave;nh ăn g&agrave; nướng x&ocirc;i ở đ&acirc;y m&ecirc; t&iacute;t m&oacute;n kim chi lu&ocirc;n d&ugrave; chỉ l&agrave; cho ăn k&egrave;m. 👉Ch&eacute;n canh nh&igrave;n kh&aacute; ưng bụng,nấu rau ko bị sống như nhiều qu&aacute;n cơm,rau nhiều n&egrave;. 👍Tổng thể chất lượng cho 8 điểm chất lượng / gi&aacute; tiền + 1 phục vụ kh&aacute; ok ship tận nơi cho trong m&ugrave;a dịch v&agrave; vệ sinh tốt .</p>', 1, 3, '[\"upload/post/2/img0.jpg\",\"upload/post/2/img1.jpg\",\"upload/post/2/img2.jpg\",\"upload/post/2/img3.jpg\",\"upload/post/2/img4.jpg\",\"upload/post/2/img5.jpg\"]', NULL, 1649, 34, '2021-10-01 12:50:42', 1, NULL, NULL, 0, NULL, NULL, 2, 1, '2021-10-05 12:07:32'),
(3, 'gà bao to của Bếp Chef Hó ó ó ó', NULL, 4, '<p>Xin ch&agrave;o cả nh&agrave; lại l&agrave; Bếu đ&acirc;y 🖐️🖐️🖐️</p>\r\n<p>H&ocirc;m nay Bếu đi ri-viu bữa cơm l&uacute;c 11g khuya 😭😭😭 Bếu đặt đồ ăn qu&aacute;n Wow chicken m&agrave; Bếu đ&atilde; từng r&igrave; -viu trước đ&oacute; ( https://app.riviu.vn/03ljsJM9 ) nhưng kh&ocirc;ng để &yacute; bị a shipper nhận rồi hủy kh&ocirc;ng giao thế l&agrave; lượn t&igrave;m qu&aacute;n kh&aacute;c trong v&ocirc; vọng. &Acirc;y ch&agrave; h&ecirc;n chọn mặt gửi v&agrave;ng được 1 qu&aacute;n m&agrave; t&ecirc;n qu&aacute;n nghe kh&aacute; ngộ nghĩnh \"Bếp Chef H&oacute;\".</p>\r\n<p>Kh&ocirc;ng uổng c&ocirc;ng đợi đến 11g đ&ecirc;m để đặt qu&aacute;n n&agrave;y. Qu&aacute;n n&agrave;y m&igrave;nh thấy tr&ecirc;n app th&igrave; 8g s&aacute;ng đ&atilde; mở b&aacute;n cho đến tận 1g khuya mới đ&oacute;ng cửa --&gt; Đ&acirc;y đ&uacute;ng l&agrave; qu&aacute;n cứu đ&oacute;i với c&aacute;c c&uacute; đ&ecirc;m. 👍👍👍&nbsp;</p>\r\n<p>Bếu xin giới thiệu sơ về menu của qu&aacute;n:<br />👉 M&igrave; g&agrave; tiềm<br />👉 Cơm g&agrave; nướng mật ong<br />👉 M&igrave; trộn đ&ugrave;i g&agrave; nướng BBQ<br />👉 Cơm g&agrave; nướng x&iacute; mụi<br />C&aacute;c bạn c&oacute; thể tham khảo menu v&agrave; đặt m&oacute;n tại đ&acirc;y: https://www.now.vn/ho-chi-minh/bep-chef-ho-mi-thit-nuong</p>\r\n<p>Như ti&ecirc;u đề Bếu đề cập g&agrave; ở qu&aacute;n n&agrave;y b&aacute;n rất to v&agrave; chất lượng 👍 Thịt g&agrave; được ướp gia vị rất đậm đ&agrave; từ da đến thịt, cơm th&igrave; rất dẻo, thơm. Kh&ocirc;ng biết sao chứ Bếu đặt m&igrave; nhưng lại giao l&agrave; nui trong khi menu qu&aacute;n lại kh&ocirc;ng c&oacute; nui 😂😂😂 nhưng...nhưng nui cũng được ướp sốt x&agrave;o n&oacute;ng ăn với g&agrave; rất ngon lắm nha Bếu nghĩ khi ăn m&igrave; trộn chắc qu&aacute;n cũng ướp vị giống nui. Đặc biệt, đồ ăn qu&aacute;n n&agrave;y được g&oacute;i trong giấy bạc đựng tr&ecirc;n c&aacute;i dĩa m&uacute;t n&ecirc;n khi giao cơm tới nơi g&agrave; vẫn c&ograve;n đủ ấm để d&ugrave;ng ngon miệng.</p>\r\n<p>Về gi&aacute; th&igrave; kh&ocirc;ng hẵn qu&aacute; mắc, đối với phần ăn n&agrave;y gi&aacute; tầm 45.000₫ - 55.000₫ cũng ổn.</p>\r\n<p>H&atilde;y follow Bếu Bếu để theo d&otilde;i nhiều qu&aacute;n ăn Ngon - Bổ - Rẻ nh&eacute; 🥰🥰🥰</p>\r\n<p>▪️▪️▪️▪️▪️▪️▪️▪️▪️▪️<br />Bếp Chef H&oacute; - M&igrave; Thịt Nướng<br />🏠 212B/D28/4 Nguyễn Tr&atilde;i, P. Nguyễn Cư Trinh, Quận 1, TP. HCM<br />⏰ 08:00 - 01:00<br />🌐 App đặt m&oacute;n: https://www.now.vn/ho-chi-minh/bep-chef-ho-mi-thit-nuong</p>', 1, 1, '[\"upload/post/3/img0.jpg\",\"upload/post/3/img1.jpg\",\"upload/post/3/img2.jpg\"]', NULL, 2349, 52, '2021-10-01 12:50:42', 2, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL),
(4, 'WELAX SPA - GỘI ĐẦU SIÊU THƯ GIÃN', NULL, 5, '<p>🌼 WELAX SPA 🌼<br />🌿 Spa đ&ocirc;ng y dưỡng sinh ở S&agrave;i G&ograve;n, full combo gội, đắp, ấn, nhấn chỉ c&oacute; 169k (75p).&nbsp;</p>\r\n<p>🍃<br />Vừa bước vào l&agrave; c&oacute; cảm t&igrave;nh r&ograve;i, nghe thấy tiếng nhạc du dương vs m&ugrave;i tinh dầu thi&ecirc;n nhi&ecirc;n thơm thơm, tone v&agrave;ng ấm ấm nữa.</p>\r\n<p>Ban đầu m&igrave;nh t&iacute;nh thử g&oacute;i gội đầu 25k hoi, m&agrave; thấy combo 19 bước c&oacute; 169k (bữa đ&oacute; giảm 20% c&ograve;n 131k) được 75 ph&uacute;t trọn g&oacute;i: gội đầu, đắp mặt nạ, massage trị liệu (cổ, vai, g&aacute;y, lưng, tay, đầu, mặt) hợp l&yacute; n&ecirc;n chốt lu&ocirc;n.</p>\r\n<p>🍃<br />MẶT: Đầu ti&ecirc;n l&agrave; được l&agrave;m sạch da, rồi tẩy tế b&agrave;o chết bằng Muối hạt mơ + Olive. Sau đ&oacute; massage bằng Kem tr&agrave; xanh, rồi cuối c&ugrave;ng l&agrave; chọn mặt nạ để đắp, b&ecirc;n n&agrave;y c&oacute; mặt nạ bơ, tr&agrave; xanh, đậu đỏ... m&agrave; m&igrave;nh kho&aacute;i bơ n&ecirc;n chọn lu&ocirc;n. &nbsp;Được massage mặ ph&ecirc; qu&aacute; n&ecirc;n m&igrave;nh ngủ 1 l&uacute;c 🙄 Bạn k&ecirc;u mới dậy, đ&uacute;ng ngại.</p>\r\n<p>🍃&nbsp;<br />ĐẦU: nh&acirc;n vi&ecirc;n tư vấn chất t&oacute;c cho m&igrave;nh, rồi m&igrave;nh chọn gội Bồ kết, b&ecirc;n n&agrave;y bồ kết handmade lu&ocirc;n. Vừa gội mấy chỉ vừa massage da đầu, nhạc &ecirc;m &aacute;i nữa, ph&ecirc; tập 2 🥺 Xong rửa tai, gội lại lần nữa, cuối c&ugrave;ng l&agrave; ủ cao bồ kết bạc h&agrave; m&aacute;t lạnh.</p>\r\n<p>🍃<br />BODY: Nhẹ cái đầu rồi thì tới Body. Ở đây massage l&agrave; trọn bộ cổ vai g&aacute;y lưng tay đầu lu&ocirc;n. Mấy chị massage đ&atilde; lắm, kiểu lực vừa m&agrave; đ&uacute;ng huyệt lu&ocirc;n. Trong l&uacute;c l&agrave;m nh&acirc;n vi&ecirc;n cứ hỏi thoải m&aacute;i chưa, lực ntn ok kh&ocirc;ng, thân thiện và dễ thương d&atilde; man.</p>\r\n<p>Kết thúc chương trình là sấy tóc cho kh&ocirc;, xoa tinh dầu, xịt dưỡng chất. Thấy t&oacute;c mượt trơn hẳn lu&ocirc;n.</p>\r\n<p>🍃<br />L&uacute;c ngồi chờ về được uống tr&agrave; hoa đậu biếc handmade thơm thơm nữa, chu đ&aacute;o thiệt sự.</p>\r\n<p>M&igrave;nh kể hơi chi tiết, tại kh&ocirc;ng nghĩ một combo với giá này mà được chỉnh chu từng chút vậy &aacute;.</p>\r\n<p>😊 Nchung combo ok qu&aacute; lu&ocirc;n. Gi&aacute; ổn &aacute;p vầy mn đi thư gi&atilde;n, xả stress l&agrave; hợp l&yacute; lu&ocirc;n &aacute;.&nbsp;</p>\r\n<p>--<br />𝐖𝐄𝐋𝐀𝐗 &nbsp;𝐒𝐏𝐀<br />◽️ Đ/c: 245 T&acirc;n Hương, P. T&acirc;n Qu&yacute;, Q. T&acirc;n Ph&uacute;, TPHCM&nbsp;</p>', 4, 2, '[\"upload/post/4/img0.jpg\",\"upload/post/4/img1.jpg\",\"upload/post/4/img2.jpg\",\"upload/post/4/img3.jpg\",\"upload/post/4/img4.jpg\",\"upload/post/4/img5.jpg\"]', NULL, 1771, 52, '2021-10-01 12:50:42', 1, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL),
(5, 'set shasimi cá hồi báo ngậy', NULL, 5, '<p><span class=\"text\">Thấy nay qu&aacute;n quen hưởng ứng việc chỉ thị mua đồ ăn mang về giảm 10% + free ship , th&igrave; m&igrave;nh cũng k&ecirc;u &iacute;t phile c&aacute; hồi chấm m&ugrave;ng tạt . 💸Phần n&agrave;y khoảng 3 lạng,th&aacute;i kh&aacute; dầy v&agrave; c&aacute; nay b&eacute;o ngậy ,thiệt hại 300k thấy l&agrave; vừa cho 3 người ăn c&ugrave;ng m&oacute;n kh&aacute;c. Ưu điểm l&agrave; c&aacute; rất tươi ,v&igrave; chưa chuẩn bị đ&aacute; kh&ocirc; kịp n&ecirc;n qu&aacute;n cho mượn c&aacute;i t&ocirc; đ&aacute; lu&ocirc;n bảo lần sau đặt giao th&igrave; sẽ lấy lại haha 👉Địa chỉ qu&aacute;n las vegas 192 Ng Văn Linh Quận 7. 👉Qu&aacute;n n&agrave;y đang k m&atilde;i 10% to&agrave;n bộ menu khi mua mang về +freeship khu vực Q4 v&agrave; Q7 ,kh&aacute; ổn mua ăn tại nh&agrave;. </span><span class=\"emoji-sizer emoji-outer \">👉</span><span class=\"text\"> link bản đồ dưới nh&eacute; c&aacute;c bạn c&oacute; số hotline đặt lu&ocirc;n: </span><a class=\"text-is-link\" target=\"_blank\" rel=\"noopener noreferrer\">https://g.page/Lasvegas-district7?share</a></p>', 1, 1, '[\"upload/post/5/img0.jpg\",\"upload/post/5/img1.jpg\",\"upload/post/5/img2.jpg\"]', NULL, 804, 15, '2021-10-01 14:50:05', 2, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL),
(6, '100 điểm đủ chưa', NULL, 5, 'Còn gì tuyệt vời hơn là một đêm trăng thanh mà ngồi uống nước trong một quán bar mà mình yêu thích Leon\r\nDịch cũng hơi lâu, sau kỳ này, khi hết dịch sẽ trở lại quán quen 1 bữa.\r\nQuán này được cái khá ấm cúng, tối tối đốt đèn cầy tạo nên không khí huyền ảo lắm mn ơi\r\nNước ở đây khá ngon, rất hợp với khẩu vị của mình. \r\nỞ đây thường mình hay gọi món cà phê sữa (buổi sáng) và nếu có ghé qua buổi tối sẽ dùng cocktail\r\nHighly recommend nha mn ơi\r\nLéon Bar - 112 Hồ Tùng Mậu, Quận 1', 2, 5, '[\"upload/post/6/img0.jpg\",\"upload/post/6/img1.jpg\"]', 'upload/post/6/vid1.mp4', 752, 15, '2021-10-01 14:51:19', 1, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL),
(7, 'Cuối tuần hay giữa tuần thì vẫn đi chill khi stress', NULL, 5, '<p><span class=\"text\">Stress qu&aacute; nhiều rủ ngay 2 đứa bạn đi chill thư gi&atilde;n lấy lại năng lượng t&iacute;ch cực. Qu&aacute;n n&agrave;y m&igrave;nh đến v&agrave;i lần. V&igrave; kh&ocirc;ng gian ở đ&acirc;y y&ecirc;n tĩnh, nhạc hợp l&yacute; để giải ph&oacute;ng ti&ecirc;u cực </span><span class=\"emoji-sizer emoji-outer \">🔥</span><span class=\"text\"> Concept qu&aacute;n kh&aacute; lạ so với c&aacute;c qu&aacute;n kh&aacute;c, c&oacute; nhiều g&oacute;c đẹp để chụp h&igrave;nh cực CHẤT 🖤 Buổi s&aacute;ng c&oacute; b&aacute;n cafe với gi&aacute; kh&aacute; ổn ở trung t&acirc;m quận 1 tầm 60k g&igrave; đấy </span><span class=\"emoji-sizer emoji-outer \">💸</span><span class=\"text\"> Buổi tối c&oacute; b&aacute;n cocktail v&agrave; shisha, c&oacute; th&ecirc;m bắp rang được refill thoải m&aacute;i nữa nha 👍🏻 </span><span class=\"emoji-sizer emoji-outer \">🏠</span><span class=\"text\"> L&Eacute;ON BAR - 112 Hồ T&ugrave;ng Mậu, Q1</span></p>', 2, 5, '[\"upload/post/7/img0.jpg\",\"upload/post/7/img1.jpg\",\"upload/post/7/img2.jpg\",\"upload/post/7/img3.jpg\",\"upload/post/7/img4.jpg\"]', 'upload/post/7/vid1.mp4', 801, 87, '2021-10-01 14:51:46', 2, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL),
(8, 'CÙNG NHAU VỀ BUÔN MA THUỘT 3N2D', NULL, 5, '<p>Nhắc đến Buôn Ma Thuột (hay còn gọi là Ban Mê Thuột) - một thành phố thuộc tỉnh Đắk Lắk - địa điểm nghe tên là mọi người nghĩ ngay đến: núi rừng, Coffee, những ngọn thác cao hùng vĩ….cộng thêm chút hiếu kì về thành phố lớn nhất vùng Tây Nguyên, nên nhóm mình đã quyết định xách balo lên khám phá ngay Buôn Mê trong 3N2Đ với những trải nghiệm thú vị.</p><p>Thời tiết cuối tháng 5, khi tụi mình đi thì khá nóng, cái nóng của vùng đất Tây Nguyên rất oi ả, lâu lâu được một vài cơn mưa rào nhẹ nhưng vẫn không xua đi được hết cái nắng nóng nơi đây. Kinh nghiệm cho các bạn nên mang theo đầy đủ: áo khoác, kem chống nắng, nón, dù…để bảo vệ sức khoẻ nha ?! Dưới đây là một chiếc Review nhỏ sau chuyến đi ngắn ngày của tụi mình, các bạn có thể tham khảo thử ha ???</p><p>---------------------------------------------------</p><p>1. DI CHUYỂN ĐẾN BUÔN MA THUỘT:</p><p>• Từ TP.Hồ Chí Minh – Buôn Ma Thuột: Nhóm mình chọn xe phòng nằm Tiến Oanh (Giá vé 300k/ng), xe có ghế nằm massage, chỗ nằm rộng, thoải mái ngủ cả chuyến đi, xe có đón ở dọc đường đi nên các bạn gọi điện và hỏi trước với bên nhà xe để hẹn chỗ đón trước nhé.</p><p>• Từ Buôn Ma Thuột – TP.Hồ Chí Minh: Lúc về thì bọn mình đi xe giường nằm Loan Sáng (Giá vé 220k/ng).</p><p>2. Ở ĐÂU TẠI BUÔN MA THUỘT:</p><p>Lần đi này, quyết định thuê căn hộ cho 7 người để được thoải mái, vì vậy mình đã chọn TV Homestay (4/17 Trung Trực, TP.Buôn Ma Thuột). Ở đây có 2 loại: căn hộ nhỏ 1tr2, căn lớn thì 1tr7, bọn mình book căn hộ lớn gồm: 3 phòng ngủ, 2 nhà vệ sinh, phòng rộng, có đầy đủ nội thất, bếp núc để nấu ăn, tủ lạnh, dầu gội, sữa tắm….đầy đủ hết (riêng nước suối thì 10k/chai). Giá phòng cho 3 ngày 2 đêm: 3tr (do được chị chủ hỗ trợ giảm giá thêm nữa chứ…). Điểm trừ duy nhất mà mình không thích khi ở đây là ban ngày, phòng khách và hai phòng trên lầu không được mát, hơi nóng, về đêm thì cũng đủ lạnh mà đó chỉ là vấn đề nhỏ, chị chủ dễ thương, hỗ trợ nhiệt tình nên cũng không vấn đề gì cả.</p><p>3. ĐI LẠI TRONG BUÔN MA THUỘT:</p><p>Ở Buôn Mê Thuột, bọn mình thuê xe máy di chuyển trong 3 ngày 2 đêm với giá 100k-120k/xe số, 150k/xe tay ga (do thuê 4 xe nên tụi mình deal được giá 100k/ngày :))))</p><p>4. CHƠI GÌ Ở BUÔN MA THUỘT</p><p>• Bảo tàng Thế giới Cà Phê - Nguyễn Đình Chiểu, Phường Tân Lợi, TP.Buôn Ma Thuột (địa điểm “đinh” mà ai đến Buôn Ma Thuột cũng phải đến tham quan rồi nè). Đến đây bạn tha hồ chụp ảnh với mọi ngóc ngách, đâu đâu lên hình cũng đẹp cả.<br>Giá vé vào cổng: 75k/1ng để tham quan khu vực phía trên, 100k tham quan khu tầng hầm, 150k/ng trọn gói cả hai khu nhé (nhưng theo mình thấy chỉ nên tham quan ở trên là đủ, vì dưới hầm khá nhỏ, không đặc sắc lắm).<br>Giá nước ở đây tầm ≈ trên 50k/ly cũng tạm ổn.</p><p>• Thác Dray Nur (nằm cách thành phố Buôn Ma Thuột 25 km đi theo quốc lộ 14) có độ cao khoảng 50m, dài 100m, đây là ngọn thác đẹp và hùng vĩ nhất khi đến với Tây Nguyên. Ở đây vừa có thác, vừa có hồ nước xanh, cầu treo nhỏ bắc ngang qua sông Serepôk để đến thác Dray Sp nên các bạn tha hồ sống ảo hoặc mang theo đồ ăn để làm bữa tiệc picnic cũng rất vui. Tuy nhiên, đường vào thác có nhiều đá dăm và bụi do xe tải chở đá nên hơi khó đi, nhất là khi mưa xuống sẽ trơn trượt, mọi người chú ý chạy cẩn thận nhé. Giá vé vào cổng: 30k/người</p><p>• Lee’s House (55 đường số 3 thuộc xã Cư Ê Bur, TP.Buôn Ma Thuột). Lee’s House là một Homestay nhưng được thiết kế, trang trí với nhiều tiểu cảnh để khách tham quan, chụp ảnh, tổ chức tiệc…Bọn mình đến đây tầm ≈11h trưa thì trời rất nóng, thời tiết oi bức, kinh nghiệm nên đến lúc sáng sớm hoặc buổi chiều ≈3h trở đi khi nắng chiều buông xuống sẽ mát mẻ, dễ chịu hơn để tha hồ chụp choẹt.<br>Giá vé khu phim trường: 100k/người (khu này dành cho chụp ảnh cưới là chính thôi nha).<br>Giá vé khu Homestay với bối cảnh Bali: 50k/người Bên trong Lee’s House cũng có bán đồ ăn, các bạn có thể thử món: cơm lam nướng trong ống nứa, gà nướng chấm muối ớt xanh, khá là ngon đấy!!!</p><p>• Hẻm HongKong - (Hẻm 188 AMa Khuê - TP. Buôn Ma Thuột). Thiết kế quán Trà sữa ở đây giống một đoạn đường trong phim HongKong nên rất rất nhiều góc để sống ảo luôn.<br>Về nước uống thì có các loại trà sữa ≈ 30k-40k/tuỳ loại, bánh bao ≈ 20k, xúc xích ≈ 45k/phần. Đồ ăn theo mình chỉ dừng lại là tạm được chứ không ngon lắm.</p><p>5. MỘT VÀI ĐIỂM NÊN ĐI NẾU CÓ THỜI GIAN:</p><p>• Chùa Sắc tứ Khải Đoan<br>• Núi Đá voi Yang Tao - Cách trung tâm thành phố 36km.<br>• Hồ Lắk - Cách trung tâm thành phố 50km. Từ Đá Voi Mẹ vô đây thêm khoảng 20km.<br>• Buôn Đôn (Bản Đôn) - Cách trung tâm 47km.<br>• Khu du lịch Troh Bư - Cách Bản đôn 31km đi theo hướng về lại thành phố. Giá vé 30k/ người.<br>• Khu du lịch sinh thái<br>• Hồ Ea Kao<br>• Khu du lịch Đồi Thông<br>• Tà Đùng Topview - được mệnh danh Vịnh Hạ Long Tây Nguyên.</p><p>6. ĂN GÌ Ở BUÔN MA THUỘT:</p><p>• Bún chìa Cô Chua (226 Nguyễn Tất Thành). Giá 1 tô bún chìa là: 65k/tô, bạn phải nên ăn thử món bún chìa này rồi vị là sự pha trộn giữa bún bò với bún mọc, những món còn lại mình thấy không ngon lắm. Ăn hết được cái chìa TO ĐÙNG thì cũng gian nan lắm nha.</p><p>• Nem nướng Thanh Loan (226 Y Jut, Thắng Lợi, Thành phố Buôn Ma Thuột). Nem nướng ở đây rất ngon</p>', 3, 6, '[\"upload/post/8/img0.jpg\",\"upload/post/8/img1.jpg\",\"upload/post/8/img2.jpg\",\"upload/post/8/img3.jpg\",\"upload/post/8/img4.jpg\",\"upload/post/8/img5.jpg\",\"upload/post/8/img6.jpg\",\"upload/post/8/img7.jpg\",\"upload/post/8/img8.jpg\",\"upload/post/8/img9.jpg\",\"upload/post/8/img10.jpg\",\"upload/post/8/img11.jpg\",\"upload/post/8/img12.jpg\",\"upload/post/8/img13.jpg\",\"upload/post/8/img14.jpg\",\"upload/post/8/img15.jpg\",\"upload/post/8/img16.jpg\"]', 'upload/post/8/vid1.mp4', 2110, 31, '2021-10-01 14:53:39', 1, '2021-07-27 09:58:26', 1, 0, NULL, NULL, 0, NULL, NULL),
(9, 'Bún riêu 66, nhà tôi 66 đời làm bún riêu ', NULL, 5, '<p>Nay mình lại phát hiện ra một tiệm bán Canh Bún - Bún Riêu siêu xịn với tuổi đời gần 30 năm, khách thì tấp nập mà đến giờ tui mới biết ^^, thiệt là quê mùa. ai mê Canh Bún như mình thì nên note lại địa điểm này nha.</p>\r\n\r\n<p>- Tiệm này tên là Quán Bún 66 - Bún Riêu, Canh Bún ở hẻm 287 Nguyễn Đình Chiểu. Chỗ này có nồi nước lèo cực hấp dẫn với nào là tàu hủ, cà chua, mọc, riêu cua, huyết, nhìn thôi là đã thấy thèm rồi ^^.</p>\r\n\r\n<p>- Canh bún hay Bún riêu đều đồng giá 40k/tô. Mình thì bị \"ghiền\" canh bún, nên là ăn thử canh bún thôi. Môt tô đầy topping, bún thì lạc trôi vì dàn topping quá khủng :)), nào là chả (tụi mình có thể yêu cầu chả khoanh hoặc chả cây nha), huyết, ốc, riêu cua (thích ăn mọc thì có thể kêu để mọc nè). Mình thì muốn ăn thử tất cả nên kêu thêm mọc với chả là 5k/cây.</p>\r\n\r\n<p>- Nước lèo nêm nếm vừa miệng, vị không quá đậm đà dễ ăn. Ai ăn mặn thì có thể để thêm mắm là vừa ngon luôn. Về phần topping thì tui thấy cái nào cũng ngon và chất lượng từ chả đến tàu hủ, ốc thì to ăn sần sật. Mọc ăn đầy thịt, sần sật. Riêu cua là loại trộn với trứng và thịt nên ăn tui không thích lắm, còn lại thì món nào cũng chất lượng.</p>\r\n\r\n<p> - Đặc biệt ở đây sẽ chấm với mắm gừng khá đặc sắc, tưởng không hợp nhưng nó lại hợp đến bất ngờ. Công nhận một điều là nước mắm gừng ở đây pha đậm đà, chấm ốc ăn ngon bá cháy. Phục vụ nhanh, dễ thương lắm nha. Quán có 2 chi nhánh lận, một cái nữa là ở Trần Bình Trọng, Q5.</p>', 1, 10, '[\"upload/post/9/img0.jpg\",\"upload/post/9/img1.jpg\",\"upload/post/9/img2.jpg\",\"upload/post/9/img3.jpg\",\"upload/post/9/img4.jpg\",\"upload/post/9/img5.jpg\",\"upload/post/9/img6.jpg\",\"upload/post/9/img7.jpg\",\"upload/post/9/img8.jpg\",\"upload/post/9/img9.jpg\",\"upload/post/9/img10.jpg\",\"upload/post/9/img11.jpg\"]', NULL, 0, 0, '2021-10-01 21:26:11', 1, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL),
(10, 'Cafe sữa tươi', NULL, 5, '<p>Ở đay có cafe sữa tươi uống cũng ổn ne , giá 20k/ly , vị béo béo ít ngọt . Có ly 1 lít nữa nha</p>\r\n\r\n<p>Địa chỉ : 82 vĩnh viễn q10</p>', 2, 11, '[\"upload/post/10/img0.jpg\"]', NULL, 0, 0, '2021-10-01 21:26:11', 1, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL),
(11, 'Xỉu up xỉu down với đĩa Ốc Xào Mắm Tỏi Tóp Mỡ núp chợ Phạm Văn Hai', NULL, 5, '<p>Ở nhà buồn miệng, trời thì cứ mưa lả chả, ta nói thèm đĩa Ốc Hương Xào Tóp Mỡ bên Chợ Phạm Văn Hai xỉu up xỉu down ????</p><p>- Hàng ốc bán cũng khá lâu rồi nè, nằm ở phía cuối khu chợ Phạm Văn Hai. Ở đây đặc biệt có món Ốc Xào Tóp Mỡ độc đáo mà ngon bá cháy</p><p>- Tui có gọi đĩa Ốc Hương, Cà Na với Móng Tay đều xào Mắm Tỏi Tóp Mỡ. Nêm nếm rất đậm đà luôn, ốc thấm đều gia vị mằn mặn, ngọt ngọt. Ốc khá to và tươi, tóp mỡ giòn rụm, được xào chung nên đậm đà ngon xỉu á</p><p>- Giá ở đây từ 40k-100k nha, phần mình gọi tổng cộng là 270k. Nếu đi đông, gọi tầm 4 5 đĩa là được tặng thêm 1 đĩa rau câu nhà làm xuất sắc luôn.</p><p>Ốc Thanh Thuý - Chợ Phạm Văn Hai<br>101 Tân Sơn Hoà, P2, Q.Tân Bình<br>16:00 - 23:00</p><p>-&nbsp;</p>', 1, 12, '[\"upload/post/11/img0.jpg\",\"upload/post/11/img1.jpg\",\"upload/post/11/img2.jpg\",\"upload/post/11/img3.jpg\",\"upload/post/11/img4.jpg\",\"upload/post/11/img5.jpg\",\"upload/post/11/img6.jpg\",\"upload/post/11/img7.jpg\",\"upload/post/11/img8.jpg\",\"upload/post/11/img9.jpg\",\"upload/post/11/img10.jpg\"]', NULL, 0, 0, '2021-10-01 09:41:13', 1, '2021-08-01 09:43:26', 1, 0, NULL, NULL, 0, NULL, NULL),
(12, 'Độc đáo BÁNH TRÁNG MỠ HÀNH THỊT BẰM thơm ngon có một không hai ở Sài Gòn', NULL, 5, '<p>Ở Sài Gòn không thiếu những hàng bán bánh tráng ngon, từ bánh tráng trộn đơn thuần, đến những món bánh tráng cực kỳ độc đáo. Nếu bánh tráng mỡ hành đã quá quen thuộc thì nay mình vừa tìm được một quán Bánh Tráng Trộn Mỡ Hành Tóp Mỡ Thịt Bằm một sự kết hợp vô cùng độc đáo.</p><p>- Xe bánh trộn của chị Như ở Phạm Văn Bạch cực nổi tiếng trong giới ăn vặt. Hàng của chị thì món ăn vô sốmà nổi tiếng nhất vẫn là Bánh Tráng Thịt Bằm 25k/bịch trộn rất vừa ăn, mix các loại gia vị và nguyên liệu lại với nhau cực kì bắt vị, cực dễ \'nghiện\'.&nbsp;</p><p>- Ngoài ra còn có Bánh Tráng Chiên (30k/ph) ăn cũng xỉu up xỉu down không kém nha, nước sốt chấm chua chua ngọt ngọt ăn kèm ăn \"dính\" lắm luôn ^^. Topping đi kèm thì nhiều thôi rồi, nào là Tóp mỡ thịt băm, xoài, trứng, tôm khô,...2 người ăn mới hết á.&nbsp;</p><p>- Chị chủ dễ thương lắm nè, một tay chị làm hết và cân cả quán luôn nhưng lúc nào chị cũng vui vẻ và thân thiện với khách.</p><p>Ăn Vặt Chị Như - Phạm Văn Bạch<br>553 Phạm Văn Bạch, Phường 15, Quận Tân Bình<br>17:00 - 22:00</p>', 1, 13, '[\"upload\\/post\\/12\\/img0.jpg\",\"upload\\/post\\/12\\/img1.jpg\",\"upload\\/post\\/12\\/img2.jpg\",\"upload\\/post\\/12\\/img3.jpg\",\"upload\\/post\\/12\\/img4.jpg\",\"upload\\/post\\/12\\/img5.jpg\",\"upload\\/post\\/12\\/img6.jpg\",\"upload\\/post\\/12\\/img7.jpg\"]', NULL, 0, 0, '2021-10-01 09:47:39', 1, '2021-10-08 04:00:49', 1, 0, NULL, NULL, 0, NULL, NULL),
(13, 'Hít hà đã miệng vì tô Mì Bò siu cay chất lượng gần 10 năm tuổi ở quận 11 ', NULL, 5, '<p>[Album]<br>Dạo này cuối tuần mình hay dậy sớm đi ăn sáng và cà phê nhẹ nhàng cùng tụi bạn nên phát hiện ra khá khá quán ăn sáng siêu xịn xò. Nay mình được dẫn đi quan quận 11 ăn Hủ Tiếu Mì Bò Cay, lần đầu ăn mà động lại cái vị không chê vào đâu được nên phải review ngay luôn nè.</p><p>- Quán này thâm niên cũng gần 10 năm, ai ở quận 11 chắc sẽ biết và đã nghe qua quán Hủ Tiếu Bò Cay Xóm Đất. Quán cực nổi tiếng vì ngon và chất lương nên khách cũng đông lắm.</p><p>- Giá hơi chát chút xíu tầm 60k/tô, bù lại chất lượng khỏi phải bàn, từ bò đến hương vị. Món này quan trọng nhất là phần bò ướp thấm vị, thịt bò có độ mềm vừa phải, phần thịt phải ngon thì khi nấu mới giữ được độ mềm và dai vùa phải. Vị món này thì sẽ như món bò kho mà tụi mình hay ăn, cay nhẹ, thơm ngũ vị hương nêm nếm đậm đà.</p><p>- Mình gọi 2 tô Mì Bò Cay, nếu gọi thêm thịt là 40k/ph nha. Bò ở đây cắt miếng to đùng, ăn tới đâu là \"phê\" tới đó luôn á, vị không cay lắm đâu nên mọi người đừng có ngại nha. Ăn sẽ the the, thơm, phần bò mềm ăn thích lắm. Với mình giá 60k/tô thật sự hợp lý cho một phần Mì Bò Cay chất lượng như vậy. \'<br>Mọi người thích ăn Bò Cay nên note lại quán này nha!!!</p><p>Bò Cay Xóm Đất - Hủ Tiếu Mì Khô Bạc Liêu<br>174A Xóm Đất, Q.11<br>6:00 - 11:00</p>', 1, 14, '[\"upload/post/13/img0.jpg\",\"upload/post/13/img1.jpg\",\"upload/post/13/img2.jpg\",\"upload/post/13/img3.jpg\",\"upload/post/13/img4.jpg\",\"upload/post/13/img5.jpg\",\"upload/post/13/img6.jpg\"]', NULL, 0, 0, '2021-10-01 10:05:39', 1, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL),
(14, 'BÁNH BAO NHÂN ĐỘC LẠ NGON RẺ TẠI VUA CUA', NULL, 5, '<p>Mùa này ở nhà, mua gì cũng phải cân nhắc. Nên phải chọn đồ ăn sao cho ngon mà để được lâu tốt. Nên thui, đặt vài set bánh bao của Vua Cua về nhà trữ đông ăn dần &nbsp;</p><p>Đã mua bánh bao Vua Cua là 2 cái này nhất định phải-phải-phải thử nè:&nbsp;<br>Bánh bao nhân tôm sốt trứng muối hải sản, hấp lên cái bánh nóng hổi. Hải sản ngập tràn, đẫm sốt trứng muối beo béo, thêm phô mai nữa. Đúng đỉnkkk&nbsp;<br>Bánh bao Gà xé sốt tiêu đen nó ngon dã man mấy ông ơi. Tưởng xốt tiêu đen chỉ có ăn bò ăn cua ai ngờ có trong bánh bao nữa. Ai ăn cay như tui đảm bảo thít. Nhân mằn mặn ngọt ngọt, thêm cái tiêu đen cay cay. Nói chung ăn một hơi 2 cái còn thèmm &nbsp;</p><p>Giải cứu thêm Khoai lang tím với bánh bao nhân tím lịm tìm sim, có phô mai chảy chảy beo béo. Nói chung hài lòng cái nhân ngọt này. Ngoài ra còn nhiều nhân khác cũng ngon hông kém, cứ lên fanpage Vua Cua tham khảo nhen.&nbsp;<br>Giá từ 76 cành tới 200 cành cho 1 túi 4 cái no í ẹ tuỳ loại nèeee</p><p>Điểm ưng là giao hàng nhanh. Shipper mặc đồ bảo hộ nhìn mà an tâm, đứng cách xa 2m. Mà dù có bịch kín mít, đeo khẩu trang, kính chắn vẫn đẹp trai. Cho xin info được hông? ????</p><p>À tui đặt qua website vuacua.vn nha!! Ai làm biếng gọi luôn 1900633387 cũng được cho lẹ</p>', 1, 15, '[\"upload/post/14/img0.jpg\",\"upload/post/14/img1.jpg\",\"upload/post/14/img2.jpg\",\"upload/post/14/img3.jpg\",\"upload/post/14/img4.jpg\",\"upload/post/14/img5.jpg\"]', NULL, 1, 1, '2021-10-01 23:39:13', 1, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL),
(15, 'Bún bò Sườn Cọng', NULL, 4, '<p>Độc lạ với 1 tô Bún bò Sườn Cọng khổng lồ dễ sợ.</p><p>- Vị trí: Quán nằm trong hẻm mà search gg đi theo cũng dễ tìm, k thì chạy tới đầu hẻm sẽ thấy có nhiều anh shipper chạy vô cứ đi theo cũng tới :)))Quán mở cửa từ 14h đến 22h nè nên h nào tiện thì ghé liền :)))</p><p>- Bún Bò Út Hằng<br>- 595/1 CMT8 Quận 10</p><p>- Giá cả &amp; Menu: Giá dao động từ 35k-60k, mà tô bình thường 40k đã có sườn cọng rồi nên cũng ok !</p><p>- Nhân viên: Phục vụ là ng trong gia đình nhưng mấy cô khá dễ thương và vui vẻ&nbsp;</p><p>- Không gian: Quán trong nhà nên cũng khôg quá nhiều chổ, tầm 5-10 bàn, nên các bạn đến ăn tranh thủ né những luac h ca điểm, nhất là bữa trưa ý đông cực kỳ. Ngoài ra cô còn có bán trên app deli nên ngại đông có thể đặt giao cho lành :)))</p><p>- Về món ăn: Mình gọi phần 2 tô (40k/tô), 1 tô bình thường và 1 tô có giò. Bún bò toàn là thịt bò chứ không như mấy chỗ khác mình từng ăn xen kẽ thịt heo, mà nhất là phần thịt nạm mình ăn rất vừa, không quá dai cũng k cứng. Còn phần nc dùng tuy hơi ngọt nhưng khá hợp khẩu vị của mình, mà bửa nọ mình order trên grab hơi trễ thì nc khá là mặn mà :))) Ai mà mê các kiểu cơm tấm sườn cọng này nọ thì bún bò sườn cọng ở đây sẽ thoả lắp nổi niềm, cô hầm nhừ vừa phải, sườn còn có mỡ béo béo cực kỳ đã. Cô bán siêu dễ thương thân thiện với tụi mình lúc đó. Đặc biệt, cô chủ còn có làm cả hành ngâm chua ăn kèm, rất hợp với bún bò. Sa tế thơm, cay, ngon miễn bàn luôn.</p><p>Đây là ý kiến cá nhân về trải nghiệm của riêng mình, bạn hãy đến và trải nghiệm rồi hashtag #bigmoutheatmore và ghé qua instagram @bigmoutheatmore để xem nhiều chia sẻ của mình hơn nhé.</p>', 1, 16, '[\"upload/post/15/img0.jpg\",\"upload/post/15/img1.jpg\",\"upload/post/15/img2.jpg\",\"upload/post/15/img3.jpg\",\"upload/post/15/img4.jpg\"]', NULL, 3, 1, '2021-10-01 23:55:21', 1, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL),
(16, 'Chị ơi cho em tô gân với sụn, không chả nha ', NULL, 5, '<p>Buổi sáng mà có gân với sườn cọng thì chắc chỉ có ở đây mới có thui á cả nhà , chứ ở O Vui , Cô Bi với bún bò 149 hẻm 273 Tô Hiến Thành &nbsp;toàn chiều mới có gân với sụn , mà một đứa như mình thì chỉ có : \" chị ơi cho em tô gân với sụn , ko chả nha \".Tô bún gân 30k + tô xí quách 35k , ăn no cành hông á</p>', 1, 16, '[\"upload/post/16/img0.jpg\",\"upload/post/16/img1.jpg\",\"upload/post/16/img2.jpg\"]', NULL, 4, 1, '2021-10-01 23:59:17', 1, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL),
(17, 'Bún bò sườn cọng Út Hằng', NULL, 4, '<p>Có ăn cơm tấm sườn cây r mà h mới biết có cả bún bò sườn cây. Sườn cây sườn cọng gì đấy ai gọi sao gọi, lạ đấy mà cứ có cảm giác gọi tô đặc biệt có 2 loại chả 60k nó cũng hoành tráng mà s ko có sườn, phải gọi thêm 1 tô sườn cọng 35k nữa ăn mới đủ phê, nói chung toàn xương là xương gặm gặm xíu hết cũng chán&nbsp;</p><p>Ngoài cái cọng ra thì tô này vừa miệng từ thịt tới nước lèo, đậm đà vừa miệng toii lun bưng ra nóng hổi làm 1 muỗng sa tế cái là húp sồn sột. Bún bò sườn cọng k thôi thì 45k, ăn xong mới biết free bún vs rau thêm biết thế ăn hết cái chừa nước bỏ bún rau vô ngập thành tô mới nó chắc bụng hơn k :))</p><p>Cô chủ dễ thương mà chắc cổ tên Út Hằng giống tên quán :))) cô bán từ 3h chìu tới 10h đêm, lễ vs chủ nhật cô bán sớm hơn 11h mở hàng òi, bên 595/1 CMT8 Q10 ngheng&nbsp;</p>', 1, 16, '[\"upload/post/17/img0.jpg\",\"upload/post/17/img1.jpg\",\"upload/post/17/img2.jpg\",\"upload/post/17/img3.jpg\"]', NULL, 16, 1, '2021-10-01 00:02:40', 3, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL),
(27, 'haha', NULL, 3, 'hello', 1, 16, '[\"upload\\/post\\/27\\/1636357772_ab6fbb51166f6c85867a.jpg\",\"upload\\/post\\/27\\/1636357772_cbd5bf8cd3966b86ceda.jpg\",\"upload\\/post\\/27\\/1636357772_e77ce295349f2391ec69.jpg\",\"upload\\/post\\/27\\/1636357772_30577d992b267ac7f3be.jpg\",\"upload\\/post\\/27\\/1636357772_1cd98323fc2781df8853.jpg\",\"upload\\/post\\/27\\/1636357772_30af0c09746fb43f0ad3.jpg\",\"upload\\/post\\/27\\/1636357772_d148bad1711e11e9690b.jpg\"]', NULL, 2, 1, '2021-10-08 01:49:31', 26, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post_hashtag`
--

CREATE TABLE `post_hashtag` (
  `id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `hashtag_id` bigint(20) NOT NULL,
  `hashtag_alias` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `post_hashtag`
--

INSERT INTO `post_hashtag` (`id`, `post_id`, `hashtag_id`, `hashtag_alias`) VALUES
(1, 8, 1, 'dulich'),
(2, 7, 1, 'dulich'),
(3, 6, 1, 'dulich'),
(4, 5, 2, 'anuong'),
(5, 4, 3, 'spa'),
(6, 3, 2, 'anuong'),
(7, 2, 2, 'anuong'),
(8, 1, 2, 'anuong'),
(9, 14, 5, 'vuacua'),
(10, 14, 2, 'anuong'),
(11, 12, 4, 'anvat'),
(12, 10, 2, 'anuong'),
(13, 11, 2, 'anuong'),
(14, 9, 7, 'bunrieu'),
(15, 9, 2, 'anuong'),
(16, 15, 2, 'anuong'),
(17, 15, 6, 'bunbo'),
(18, 16, 6, 'bunbo'),
(19, 17, 6, 'bunbo'),
(20, 13, 2, 'anuong'),
(21, 16, 2, 'anuong'),
(22, 17, 2, 'anuong'),
(23, 12, 2, 'AnUong');

-- --------------------------------------------------------

--
-- Table structure for table `post_report`
--

CREATE TABLE `post_report` (
  `id` bigint(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `report_title` int(11) NOT NULL,
  `report_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `post_report`
--

INSERT INTO `post_report` (`id`, `post_id`, `report_title`, `report_content`, `is_deleted`, `deleted_at`, `deleted_by`, `created_at`, `created_by`) VALUES
(1, 1, 1, 'Test', 1, NULL, NULL, '2021-10-06 21:50:47', 2),
(2, 1, 1, 'Test', 1, NULL, NULL, '2021-10-06 21:52:33', 2),
(3, 1, 1, 'Test2', 1, NULL, NULL, '2021-10-07 01:37:25', 2),
(4, 1, 2, 'Testtttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttttt', 1, NULL, NULL, '2021-10-08 01:26:42', 2),
(5, 2, 2, 'Test', 1, NULL, NULL, '2021-10-08 01:27:01', 2),
(6, 2, 2, 'Test', 1, NULL, NULL, '2021-10-08 01:27:47', 1),
(7, 3, 3, 'Test', 0, NULL, NULL, '2021-10-08 01:27:56', 1),
(8, 3, 3, 'Test', 0, NULL, NULL, '2021-10-09 00:55:56', 1),
(9, 1, 1, 'test', 1, '2021-10-12 05:02:27', 1, '2021-10-11 03:02:16', 2),
(10, 0, 0, NULL, 1, '2021-10-11 10:07:41', 0, '2021-10-11 03:07:41', 0);

-- --------------------------------------------------------

--
-- Table structure for table `post_violation`
--

CREATE TABLE `post_violation` (
  `id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `report_title_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_violation`
--

INSERT INTO `post_violation` (`id`, `post_id`, `report_title_id`) VALUES
(1, 1, 2),
(2, 1, 3),
(3, 2, 1),
(4, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `province`
--

CREATE TABLE `province` (
  `id` int(11) NOT NULL,
  `province_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_deleted` tinyint(1) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `province`
--

INSERT INTO `province` (`id`, `province_name`, `created_at`, `is_deleted`, `created_by`, `deleted_at`, `deleted_by`, `updated_at`, `updated_by`) VALUES
(1, 'Hà Nội', '2020-09-09 03:29:13', 0, 1, NULL, NULL, NULL, NULL),
(2, 'Hà Giang', '2020-09-09 03:29:55', 0, 1, NULL, NULL, NULL, NULL),
(3, 'Cao Bằng', '2020-09-09 03:30:30', 0, 1, NULL, NULL, NULL, NULL),
(4, 'Bắc Kạn', '2020-09-09 03:31:14', 0, 1, NULL, NULL, NULL, NULL),
(5, 'Tuyên Quang', '2020-09-09 03:32:04', 0, 1, NULL, NULL, NULL, NULL),
(6, 'Lào Cai', '2020-09-09 03:32:04', 0, 1, NULL, NULL, NULL, NULL),
(7, 'Điện Biên', '2020-09-09 03:32:04', 0, 1, NULL, NULL, NULL, NULL),
(8, 'Lai Châu', '2020-09-09 03:39:39', 0, 1, NULL, NULL, NULL, NULL),
(9, 'Sơn La', '2020-09-09 03:42:14', 0, 1, NULL, NULL, NULL, NULL),
(10, 'Yên Bái', '2020-09-09 03:42:45', 0, 1, NULL, NULL, NULL, NULL),
(11, 'Hòa Bình', '2020-09-09 03:43:07', 0, 1, NULL, NULL, NULL, NULL),
(12, 'Thái Nguyên', '2020-09-09 03:43:37', 0, 1, NULL, NULL, NULL, NULL),
(13, 'Lạng Sơn', '2020-09-09 03:47:59', 0, 1, NULL, NULL, NULL, NULL),
(14, 'Quảng Ninh', '2020-09-09 03:48:20', 0, 1, NULL, NULL, NULL, NULL),
(15, 'Bắc Giang', '2020-09-09 03:48:43', 0, 1, NULL, NULL, NULL, NULL),
(16, 'Phú Thọ', '2020-09-09 03:49:06', 0, 1, NULL, NULL, NULL, NULL),
(17, 'Vĩnh Phúc', '2020-09-09 03:49:37', 0, 1, NULL, NULL, NULL, NULL),
(18, 'Bắc Ninh', '2020-09-09 03:50:05', 0, 1, NULL, NULL, NULL, NULL),
(19, 'Hải Dương', '2020-09-09 03:50:38', 0, 1, NULL, NULL, NULL, NULL),
(20, 'Hải Phòng', '2020-09-09 03:50:56', 0, 1, NULL, NULL, NULL, NULL),
(21, 'Hưng Yên', '2020-09-09 03:51:18', 0, 1, NULL, NULL, NULL, NULL),
(22, 'Thái Bình', '2020-09-09 03:51:38', 0, 1, NULL, NULL, NULL, NULL),
(23, 'Hà Nam', '2020-09-09 03:51:55', 0, 1, NULL, NULL, NULL, NULL),
(24, 'Nam Định', '2020-09-09 03:52:14', 0, 1, NULL, NULL, NULL, NULL),
(25, 'Ninh Bình', '2020-09-09 03:52:34', 0, 1, NULL, NULL, NULL, NULL),
(26, 'Thanh Hóa', '2020-09-09 06:15:47', 0, 1, NULL, NULL, NULL, NULL),
(27, 'Nghệ An', '2020-09-09 06:16:10', 0, 1, NULL, NULL, NULL, NULL),
(28, 'Hà Tĩnh', '2020-09-09 06:16:37', 0, 1, NULL, NULL, NULL, NULL),
(29, 'Quảng Bình', '2020-09-09 06:16:57', 0, 1, NULL, NULL, NULL, NULL),
(30, 'Quảng Trị', '2020-09-09 06:17:13', 0, 1, NULL, NULL, NULL, NULL),
(31, 'Thừa Thiên Huế', '2020-09-09 06:17:40', 0, 1, NULL, NULL, NULL, NULL),
(32, 'Đà Nẵng', '2020-09-09 06:18:04', 0, 1, NULL, NULL, NULL, NULL),
(33, 'Quảng Nam', '2020-09-09 06:18:20', 0, 1, NULL, NULL, NULL, NULL),
(34, 'Quảng Ngãi', '2020-09-09 06:18:41', 0, 1, NULL, NULL, NULL, NULL),
(35, 'Bình Định', '2020-09-09 06:19:12', 0, 1, NULL, NULL, NULL, NULL),
(36, 'Phú Yên', '2020-09-09 06:19:27', 0, 1, NULL, NULL, NULL, NULL),
(37, 'Khánh Hòa', '2020-09-09 06:19:45', 0, 1, NULL, NULL, NULL, NULL),
(38, 'Ninh Thuận', '2020-09-09 06:20:05', 0, 1, NULL, NULL, NULL, NULL),
(39, 'Bình Thuận', '2020-09-09 06:20:23', 0, 1, NULL, NULL, NULL, NULL),
(40, 'Kon Tum', '2020-09-09 06:21:42', 0, 1, NULL, NULL, NULL, NULL),
(41, 'Gia Lai', '2020-09-09 06:21:56', 0, 1, NULL, NULL, NULL, NULL),
(42, 'Đắk Lắk', '2020-09-09 06:22:12', 0, 1, NULL, NULL, NULL, NULL),
(43, 'Đắk Nông', '2020-09-09 06:23:05', 0, 1, NULL, NULL, NULL, NULL),
(44, 'Lâm Đồng', '2020-09-09 06:23:18', 0, 1, NULL, NULL, NULL, NULL),
(45, 'Bình Phước', '2020-09-09 06:23:41', 0, 1, NULL, NULL, NULL, NULL),
(46, 'Tây Ninh', '2020-09-09 06:24:00', 0, 1, NULL, NULL, NULL, NULL),
(47, 'Bình Dương', '2020-09-09 06:24:16', 0, 1, NULL, NULL, NULL, NULL),
(48, 'Đồng Nai', '2020-09-09 06:24:34', 0, 1, NULL, NULL, NULL, NULL),
(49, 'Bà Rịa - Vũng Tàu', '2020-09-09 06:24:49', 0, 1, NULL, NULL, NULL, NULL),
(50, 'Hồ Chí Minh', '2020-09-09 06:25:04', 0, 1, NULL, NULL, NULL, NULL),
(51, 'Long An', '2020-09-09 06:25:26', 0, 1, NULL, NULL, NULL, NULL),
(52, 'Tiền Giang', '2020-09-09 06:25:44', 0, 1, NULL, NULL, NULL, NULL),
(53, 'Bến Tre', '2020-09-09 06:26:13', 0, 1, NULL, NULL, NULL, NULL),
(54, 'Trà Vinh', '2020-09-09 06:26:28', 0, 1, NULL, NULL, NULL, NULL),
(55, 'Vĩnh Long', '2020-09-09 06:26:43', 0, 1, NULL, NULL, NULL, NULL),
(56, 'Đồng Tháp', '2020-09-09 06:26:58', 0, 1, NULL, NULL, NULL, NULL),
(57, 'An Giang', '2020-09-09 06:27:15', 0, 1, NULL, NULL, NULL, NULL),
(58, 'Kiên Giang', '2020-09-09 06:27:34', 0, 1, NULL, NULL, NULL, NULL),
(59, 'Cần Thơ', '2020-09-09 06:27:47', 0, 1, NULL, NULL, NULL, NULL),
(60, 'Hậu Giang', '2020-09-09 06:28:19', 0, 1, NULL, NULL, NULL, NULL),
(61, 'Sóc Trăng', '2020-09-09 06:28:34', 0, 1, NULL, NULL, NULL, NULL),
(62, 'Bạc Liêu', '2020-09-09 06:28:49', 0, 1, NULL, NULL, NULL, NULL),
(63, 'Cà Mau', '2020-09-09 06:29:02', 0, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `id` int(10) NOT NULL,
  `ratingcount` int(10) DEFAULT NULL,
  `ratingtotal` int(10) DEFAULT NULL,
  `location_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `report_comment`
--

CREATE TABLE `report_comment` (
  `id` bigint(11) NOT NULL,
  `comment_id` bigint(20) NOT NULL,
  `account_id` bigint(20) NOT NULL,
  `report_title` int(11) NOT NULL,
  `report_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_approval` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `report_title`
--

CREATE TABLE `report_title` (
  `id` int(11) NOT NULL,
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `report_title`
--

INSERT INTO `report_title` (`id`, `title`, `created_at`, `created_by`, `updated_at`, `updated_by`, `is_deleted`, `deleted_at`, `deleted_by`) VALUES
(1, 'Hình ảnh / video đồi trụy', '2021-10-07 15:25:11', 1, NULL, NULL, 0, NULL, NULL),
(2, 'Nội dung sai lệch / tin giả', '2021-10-07 15:25:11', 1, NULL, NULL, 0, NULL, NULL),
(3, 'Bài viết rác / spam', '2021-10-07 15:25:59', 1, NULL, NULL, 0, NULL, NULL),
(4, 'Nội dung không liên quan đến địa điểm ', '2021-10-07 15:25:59', 1, NULL, NULL, 0, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth`
--
ALTER TABLE `auth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `follow`
--
ALTER TABLE `follow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_post`
--
ALTER TABLE `gallery_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hashtag`
--
ALTER TABLE `hashtag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `like_comment`
--
ALTER TABLE `like_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `like_post`
--
ALTER TABLE `like_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_hashtag`
--
ALTER TABLE `post_hashtag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_report`
--
ALTER TABLE `post_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_violation`
--
ALTER TABLE `post_violation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report_comment`
--
ALTER TABLE `report_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report_title`
--
ALTER TABLE `report_title`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `auth`
--
ALTER TABLE `auth`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `follow`
--
ALTER TABLE `follow`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `gallery_post`
--
ALTER TABLE `gallery_post`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `hashtag`
--
ALTER TABLE `hashtag`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `like_comment`
--
ALTER TABLE `like_comment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `like_post`
--
ALTER TABLE `like_post`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `post_hashtag`
--
ALTER TABLE `post_hashtag`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `post_report`
--
ALTER TABLE `post_report`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `post_violation`
--
ALTER TABLE `post_violation`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `province`
--
ALTER TABLE `province`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `report_comment`
--
ALTER TABLE `report_comment`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `report_title`
--
ALTER TABLE `report_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
