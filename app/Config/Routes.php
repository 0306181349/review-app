<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
// $routes->set404Override(function () {
// 	if(session('isLoggedIn')){
//     	echo view('/admin/main_view');
// 	}else{
// 		echo view('/admin/login');
// 	}
// });
$routes->set404Override('App\Controllers\Admin\HomeController::index');
$routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
// $routes->get('/', 'Home::index');
$routes->get('admin/login', 'Admin\AuthController::index');
$routes->post('admin/login', 'Admin\AuthController::login');
$routes->get('admin/logout', 'Admin\AuthController::logout');

$routes->post('login', 'AuthController::login');
$routes->post('check-phone', 'AuthController::checkPhone');
$routes->post('register', 'AuthController::register');
$routes->post('sign-up', 'AuthController::signUp');
$routes->get('refresh-token', 'AuthController::refreshToken');
$routes->get('logout', 'AuthController::logout');

// admin routes
$routes->group('admin', ['filter' => 'admin_auth'], function ($routes) {
	
	$routes->get('user-info', 'Admin\AccountController::userInfo');
	$routes->get('/', 'Admin\HomeController::index');
	$routes->group('users', function ($routes) {
		$routes->get('all', 'Admin\AccountController::index');
		$routes->get('show/(:num)', 'Admin\AccountController::show/$1');
		$routes->post('add', 'Admin\AccountController::create');
		$routes->delete('delete/(:num)', 'Admin\AccountController::delete/$1');
		$routes->post('update/(:num)', 'Admin\AccountController::update/$1');
		$routes->post('lock/(:num)', 'Admin\AccountController::lock/$1');
		$routes->post('changepass/(:num)', 'Admin\AccountController::changePass/$1');
	});	

	$routes->group('post', function ($routes) {
		$routes->get('all', 'Admin\PostController::index');
		$routes->get('show/(:num)', 'Admin\PostController::show/$1');
		$routes->post('add', 'Admin\PostController::create');
		$routes->post('update/(:num)', 'Admin\PostController::update/$1');
		$routes->delete('delete/(:num)', 'Admin\PostController::delete/$1');
		$routes->post('lock/(:num)', 'Admin\PostController::lock/$1');
		$routes->get('locked/all', 'Admin\PostController::showAllLockedPost');
		$routes->get('locked/(:num)', 'Admin\PostController::showLockedPost/$1');
		$routes->get('unlock-request/all', 'Admin\PostController::getAllPostUnlockRequest');
		$routes->get('unlock/(:num)', 'Admin\PostController::unlockPost/$1');
		$routes->get('unlock-reject/(:num)', 'Admin\PostController::unlockPostReject/$1');

	});
	
	$routes->group('comment', function ($routes) {
		$routes->get('all/(:num)', 'Admin\CommentController::getAllComment/$1');
		$routes->get('replies/(:num)', 'Admin\CommentController::getReplies/$1');
		$routes->delete('delete/(:num)', 'Admin\CommentController::delete/$1');
	});

	$routes->group('location', function ($routes) {
		$routes->get('all', 'Admin\LocationController::index');
		$routes->get('show/(:num)', 'Admin\LocationController::show/$1');
		$routes->post('add', 'Admin\LocationController::create');
		$routes->post('update/(:num)', 'Admin\LocationController::update/$1');
		$routes->delete('delete/(:num)', 'Admin\LocationController::delete/$1');
		$routes->get('verify/(:num)', 'Admin\LocationController::verify/$1');
	});

	$routes->group('dashboard', function ($routes) {
		$routes->get('all', 'Admin\DashboardController::index');
	});

	$routes->group('province', function ($routes) {
		$routes->get('all', 'Admin\ProvinceController::index');
		$routes->post('edit/(:num)', 'Admin\ProvinceController::edit/$1');
		$routes->post('add', 'Admin\ProvinceController::add');
		$routes->delete('delete/(:num)', 'Admin\ProvinceController::delete/$1');
	});

	$routes->group('category', function ($routes) {
		$routes->get('all', 'Admin\CategoryController::index');
		$routes->post('add', 'Admin\CategoryController::add');
		$routes->post('edit/(:num)', 'Admin\CategoryController::edit/$1');
		$routes->delete('delete/(:num)', 'Admin\CategoryController::delete/$1');
	});

	$routes->group('hashtag', function ($routes) {
		$routes->get('all', 'Admin\HashtagController::index');
		$routes->post('edit/(:num)', 'Admin\HashtagController::edit/$1');
		$routes->post('add', 'Admin\HashtagController::add');
		$routes->delete('delete/(:num)', 'Admin\HashtagController::delete/$1');
		$routes->get('post/(:num)', 'Admin\HashtagController::postByHashtag/$1');
	});

	$routes->group('report-title', function ($routes) {
		$routes->get('all', 'Admin\ReportController::getAllReportTitle');
		$routes->post('add', 'Admin\ReportController::addReportTitle');
		$routes->post('edit/(:num)', 'Admin\ReportController::editReportTitle/$1');
		$routes->delete('delete/(:num)', 'Admin\ReportController::deleteReportTitle/$1');
	});

	$routes->group('report', function ($routes) {
		$routes->group('post', function ($routes) {
			$routes->get('all', 'Admin\ReportController::getAllPostReport');
			$routes->get('(:num)', 'Admin\ReportController::getAllReportByPost/$1');
			$routes->delete('delete-report/(:num)', 'Admin\ReportController::deletePostReport/$1');
			$routes->delete('delete-all/(:num)', 'Admin\ReportController::deleteAllPostReport/$1');
		});
	});
	
});

// get data for react-select
$routes->group('select', function ($routes){
	$routes->get('location', 'Admin\LocationController::selectLocation');
	$routes->get('hashtag', 'Admin\HashtagController::selectHashtag');
	$routes->get('category', 'Admin\CategoryController::getAllCategory');
	$routes->get('province', 'Admin\ProvinceController::getAllProvince');
});

$routes->group('search', function ($routes){
	$routes->get('all', 'SearchController::searchAll');
	$routes->get('post', 'SearchController::searchPost');
	$routes->get('location', 'SearchController::searchLocation');
	$routes->get('user', 'SearchController::searchAccount');
	$routes->get('hashtag', 'SearchController::searchHashtag');
});

// user routes
$routes->group('post', function ($routes) {
	$routes->get('all', 'PostController::getPostDataByPage');
	$routes->get('detail', 'PostController::getPostDetail');
	// $routes->post('upload-image/(:num)' , 'PostController::uploadImage/$1' , ['filter' => 'JWT_auth']);
	$routes->post('create', 'PostController::createPost', ['filter' => 'JWT_auth']);
	$routes->post('edit', 'PostController::editPost', ['filter' => 'JWT_auth']);
	$routes->get('video/all', 'PostController::getVideoByPage');
	$routes->get('post-by-user', 'PostController::postByUser');
	$routes->get('add-view', 'PostController::addPostView');
	$routes->get('remove-post', 'GalleryController::removePostFromGallery');
	$routes->get('post-by-gallery', 'PostController::getPostByGallery');
	$routes->get('liked-post', 'PostController::getlikedPost', ['filter' => 'JWT_auth']);
	$routes->get('hashtag', 'PostController::getPostByHashtag');
});

// location routes
$routes->group('location', function ($routes){
	// $routes->get('search', 'LocationController::searchLocation');
	$routes->get('detail', 'LocationController::locationDetail');
	$routes->post('create', 'LocationController::createLocation', ['filter' => 'JWT_auth']);
	$routes->get('post', 'LocationController::postByLocation');
	$routes->get('top-location', 'LocationController::getTopLocationByMonth');
});

// comment routes
$routes->group('comment', function ($routes){
	$routes->get('all', 'CommentController::getCommentByPage');
	$routes->get('reply', 'CommentController::getReplyCommentByPage');
	$routes->get('detail', 'CommentController::detailComment');
	$routes->post('create', 'CommentController::addComment', ['filter' => 'JWT_auth']);
	$routes->post('add', 'CommentController::addComment', ['filter' => 'JWT_auth']);
	$routes->delete('delete', 'CommentController::deleteComment', ['filter' => 'JWT_auth']);
	$routes->get('like', 'CommentController::likeComment', ['filter' => 'JWT_auth']);
	$routes->get('unlike', 'CommentController::unlikeComment', ['filter' => 'JWT_auth']);
	$routes->post('edit', 'CommentController::editComment', ['filter' => 'JWT_auth']);
	$routes->get('delete', 'CommentController::deleteComment', ['filter' => 'JWT_auth']);
});

// user routes
$routes->group('user', function ($routes){
	$routes->get('profile', 'AccountController::accountProfile');
	$routes->get('detail', 'AccountController::accountDetail', ['filter' => 'JWT_auth']);
	$routes->get('follow', 'AccountController::follow', ['filter' => 'JWT_auth']);
	$routes->get('unfollow', 'AccountController::unfollow', ['filter' => 'JWT_auth']);
	$routes->get('like-post', 'AccountController::likePost', ['filter' => 'JWT_auth']);
	$routes->get('unlike-post', 'AccountController::unlikePost', ['filter' => 'JWT_auth']);
	// $routes->get('add-favorite-post', 'AccountController::addFavoritePost', ['filter' => 'JWT_auth']);
	// $routes->get('remove-favorite-post', 'AccountController::removeFavoritePost', ['filter' => 'JWT_auth']);
	$routes->post('change-pass', 'AccountController::changePass', ['filter' => 'JWT_auth']);
	$routes->post('edit-profile', 'AccountController::editProfile', ['filter' => 'JWT_auth']);
	$routes->post('change-avatar', 'AccountController::changeAvatar', ['filter' => 'JWT_auth']);
	$routes->get('list-following', 'AccountController::getListFollowing');
	$routes->get('list-follower', 'AccountController::getListFollower');
	// $routes->get('list-favorite-post', 'AccountController::getListFavoritePost', ['filter' => 'JWT_auth']);
	$routes->get('top-follower', 'AccountController::getTopFollower');
	$routes->get('top-user', 'AccountController::getTopUserByMonth');
});

// province
$routes->group('province', function ($routes){
	$routes->get('all', 'ProvinceController::getAllProvince');
});

// hashtag
$routes->group('hashtag', function ($routes){
	$routes->get('detail', 'HashtagController::hashtagDeltail');
});

// category
$routes->group('category', function ($routes){
	$routes->get('all', 'CategoryController::getAllCategory');
});

// notification
$routes->group('notification', ['filter' => 'JWT_auth'], function ($routes){
	$routes->get('count-unseen', 'NotificationController::countUnseenNotification');
	$routes->get('seen-all', 'NotificationController::checkSeenAllNotification');
	$routes->get('clicked', 'NotificationController::checkClickedNotification');
	$routes->get('clicked-all', 'NotificationController::checkClikedAllNotification');
	$routes->get('all', 'NotificationController::getAllNotificationByPage');
	$routes->get('get-new', 'NotificationController::getNewNotification');
	$routes->get('get-earlier', 'NotificationController::getEarlierNotification');
});

// gallery
$routes->group('gallery', function ($routes){
	$routes->get('all', 'GalleryController::getAllGallery');
	$routes->get('delete', 'GalleryController::deleteGallery', ['filter' => 'JWT_auth']);
	$routes->post('create', 'GalleryController::createGallery', ['filter' => 'JWT_auth']);
	$routes->post('edit', 'GalleryController::editGallery', ['filter' => 'JWT_auth']);
	$routes->get('set-privacy', 'GalleryController::setPrivacy', ['filter' => 'JWT_auth']);
	$routes->get('add-post', 'GalleryController::addPostToGallery', ['filter' => 'JWT_auth']);
	$routes->get('remove-post', 'GalleryController::removePostFromGallery', ['filter' => 'JWT_auth']);
});

// report 
$routes->group('report', function ($routes){
	$routes->get('report-title', 'ReportController::getAllReportTitle');
	$routes->post('post', 'ReportController::reportPost', ['filter' => 'JWT_auth']);
});





/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}


