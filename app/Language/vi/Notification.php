<?php
return [
	'noti_title'		=>	[
		NEW_FOLLOWER        =>  'đã theo dõi bạn',
		NEW_COMMENT         =>  'đã bình luận về bài viết của bạn',
		NEW_REPLY_COMMENT   =>  'đã trả lời về bình luận của bạn',
		NEW_POST            =>  'đã thêm một bài viết mới',
		LIKE_POST           =>  'đã thích bài viết của bạn',
		LIKE_COMMENT        =>  'đã thích bình luận của bạn',
		LOCK_POST			=>	'Bài viết của bạn đã bị khóa vì vi phạm chính sách cộng đồng',
		UNLOCK_POST			=>	'Bài viết của bạn đã được mở khóa',
		UNLOCK_POST_REJECT  =>  'Yêu cầu mở khóa của bạn đã không được phê duyệt',
	],
];