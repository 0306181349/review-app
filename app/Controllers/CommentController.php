<?php 
namespace App\Controllers;
use App\Services\Comment\CommentService;
use App\Services\Comment\UpdateCommentService;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use App\Validation\Rules\Comment\CommentRules;


class CommentController extends BaseController
{
    public function __construct(){
        $this->service = new CommentService();
        $this->updateService = new UpdateCommentService();
    }

    public function getCommentByPage(){
        // check if there is token in request header and valid that token
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse(
            [
                'status'     => 'Success',
                'pagination' => $data['pagination'],
                'data'       => $data['data'],
            ]
        );
    }

    public function getReplyCommentByPage(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);

        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong!'
            ], ResponseInterface::HTTP_BAD_REQUEST);
        }

        return $this->getResponse([
            'status'        => 'Success',
            'pagination'    => $response['pagination'],
            'data'          => $response['data'],
        ]);
    }

    public function addComment(){
        // Validation
        $validateRules = new CommentRules();
        
        $input = $this->getRequestInput($this->request);
        if (!$this->validateRequest($input, $validateRules->rules)) {
            return $this
                ->getResponse(
                    ['message' => $this->validator->getErrors()],
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }

        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);

        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong!'
            ], ResponseInterface::HTTP_BAD_REQUEST);
        }

        return $this->getResponse([
            'status' => 'Success',
            'data'   => $response,
        ]);
    }

    public function likeComment(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);
        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong bitch'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
        return $this->getResponse([
            'data' => [
                'status' => 'Success'
            ]
        ]);
    }

    public function unlikeComment(){  
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }    
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);
        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong bitch'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
        return $this->getResponse([
            'data' => [
                'status' => 'Success'
            ]
        ]);
    }

    public function editComment(){
        // Validation
        $validateRules = new CommentRules();
        
        $input = $this->getRequestInput($this->request);
        if (!$this->validateRequest($input, $validateRules->rules)) {
            return $this
                ->getResponse(
                    ['message' => $this->validator->getErrors()],
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }    
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);
        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong bitch'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
        return $this->getResponse([
            'status' => 'Success',
            'data'   => $response
        ]);
    }

    public function deleteComment(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }    
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);
        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong bitch'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
        return $this->getResponse([
            'data' => [
                'status' => 'Success'
            ]
        ]);
    }

    public function detailComment(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }    
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);
        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong bitch'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
        return $this->getResponse([
            'status' => 'Success',
            'data'   => $response,
        ]);
    }
}