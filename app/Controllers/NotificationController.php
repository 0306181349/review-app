<?php 
namespace App\Controllers;
use App\Services\Notification\NotificationService;
use App\Services\Notification\UpdateNotificationService;
use CodeIgniter\HTTP\ResponseInterface;

class NotificationController extends BaseController
{
    public function __construct(){
        $this->service = new NotificationService();
        $this->updateService = new UpdateNotificationService();
    }

    public function getAllNotificationByPage(){
        $userInfo = $this->getUserInfo();
        if(is_object($userInfo)){
            return $userInfo;
        }

        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);

        return $this->getResponse([
            'status'    => 'Success',
            'data'      =>  $data,
        ]);
    }

    public function countUnseenNotification(){
        $userInfo = $this->getUserInfo();
        if(is_object($userInfo)){
            return $userInfo;
        }

        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);

        return $this->getResponse([
            'data' => [
                'status' => 'Success',
                'count'  => $data,
            ]
        ]);
    }

    public function checkSeenAllNotification(){
        $userInfo = $this->getUserInfo();
        if(is_object($userInfo)){
            return $userInfo;
        }

        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);

        if($data !== true){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong',
                ], ResponseInterface::HTTP_BAD_REQUEST
            );
        }

        return $this->getResponse([
            'data' => [
                'status' => 'Success'
            ]
        ]);
    }

    public function checkClickedNotification(){
        $userInfo = $this->getUserInfo();
        if(is_object($userInfo)){
            return $userInfo;
        }

        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);

        if($data !== true){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong',
                ], ResponseInterface::HTTP_BAD_REQUEST
            );
        }

        return $this->getResponse([
            'data' => [
                'status' => 'Success'
            ]
        ]);
    }

    public function checkClikedAllNotification(){
        $userInfo = $this->getUserInfo();
        if(is_object($userInfo)){
            return $userInfo;
        }

        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);

        if($data !== true){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong',
                ], ResponseInterface::HTTP_BAD_REQUEST
            );
        }

        return $this->getResponse([
            'data' => [
                'status' => 'Success'
            ]
        ]);
    }

    public function getNewNotification(){
        $userInfo = $this->getUserInfo();
        if(is_object($userInfo)){
            return $userInfo;
        }

        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);

        return $this->getResponse([
            'status'    => 'Success',
            'data'      =>  $data,
        ]);
    }

    public function getEarlierNotification(){
        $userInfo = $this->getUserInfo();
        if(is_object($userInfo)){
            return $userInfo;
        }

        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);

        return $this->getResponse([
            'status'    => 'Success',
            'data'      =>  $data,
        ]);
    }
}