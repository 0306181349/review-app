<?php 
namespace App\Controllers;
use App\Services\Hashtag\HashtagService;
use App\Services\Hashtag\UpdateHashtagService;
use App\Services\Dashboard\DashboardService;
use CodeIgniter\HTTP\ResponseInterface;

class HashtagController extends BaseController
{
    public function __construct(){
        $this->service = new HashtagService();
        $this->updateService = new UpdateHashtagService();
    }

    public function hashtagDeltail(){
        
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Hashtag not found',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [
                'data'  =>  $data,
            ]
        );
    }
}