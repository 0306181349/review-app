<?php 
namespace App\Controllers;
use App\Services\Post\UpdatePostService;
use App\Services\Location\UpdateLocationService;
use App\Services\Account\UpdateAccountService;
use App\Services\Hashtag\UpdateHashtagService;

class SearchController extends BaseController
{
    public function searchPost(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        
        $updatePostService = new UpdatePostService();
        $updatePostService->setUser($userInfo);
        $updatePostService->setRequest($this->request);
        $data = $updatePostService->searchPost();
        
        return $this->getResponse(
            [
                'status'        => "Success",
                'pagination'    =>  $data['pagination'],
                'data'          =>  $data['data'],
            ]
        );
    }

    public function searchLocation(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }

        $updateLocationService = new UpdateLocationService();
        $updateLocationService->setUser($userInfo);
        $updateLocationService->setRequest($this->request);
        $data = $updateLocationService->searchLocation();
        
        return $this->getResponse(
            [
                'status'        => "Success",
                'pagination'    =>  $data['pagination'],
                'data'          =>  $data['data'],
            ]
        );
    }

    public function searchAccount(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }

        $updateAccountService = new UpdateAccountService();
        $updateAccountService->setUser($userInfo);
        $updateAccountService->setRequest($this->request);
        $data = $updateAccountService->searchAccount();
        
        return $this->getResponse(
            [
                'status'        => "Success",
                'pagination'    =>  $data['pagination'],
                'data'          =>  $data['data'],
            ]
        );
    }

    public function searchHashtag(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        
        $updateHashtagService = new UpdateHashtagService();
        $updateHashtagService->setUser($userInfo);
        $updateHashtagService->setRequest($this->request);
        $data = $updateHashtagService->searchHashtag();
        
        return $this->getResponse(
            [
                'status'        => "Success",
                'pagination'    =>  $data['pagination'],
                'data'          =>  $data['data'],
            ]
        );
    }

    public function searchAll(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        
        $data = [];
        $updatePostService = new UpdatePostService();
        $updatePostService->setUser($userInfo);
        $updatePostService->setRequest($this->request);
        $data['post'] = $updatePostService->searchPost()['data'];

        $updateLocationService = new UpdateLocationservice();
        $updateLocationService->setUser($userInfo);
        $updateLocationService->setRequest($this->request);
        $data['location'] = $updateLocationService->searchLocation()['data'];

        $updateAccountService = new UpdateAccountService();
        $updateAccountService->setUser($userInfo);
        $updateAccountService->setRequest($this->request);
        $data['user'] = $updateAccountService->searchAccount()['data'];

        $updateHashtagService = new UpdateHashtagService();
        $updateHashtagService->setUser($userInfo);
        $updateHashtagService->setRequest($this->request);
        $data['hashtag'] = $updateHashtagService->searchHashtag()['data'];
        
        return $this->getResponse(
            [
                'status'        => "Success",
                'data'          => $data
            ]
        );
    }
}
