<?php 
namespace App\Controllers;

use App\Models\AccountModel;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;
use ReflectionException;
use App\Validation\Rules\UserLoginRules;
use App\Validation\Rules\SignUpRules;
use App\Validation\Rules\RegisterRules;
use App\Validation\Rules\AccountRules;
use App\Services\Account\AccountService;
use App\Controllers\BaseController;
use App\Models\AuthModel;
use Config\Services;
use App\Services\Gallery\GalleryService;

class AuthController extends BaseController
{
    private function getJWTForUser($id, int $responseCode = ResponseInterface::HTTP_OK, $isRefresh = false){
        try {
            $accountService = new AccountService();
            $user = $accountService->findUserById($id);
            unset($user['password']);

            helper('jwt');

            $authModel = new AuthModel();
            $authData = $authModel->where(['user' => $id])->get()->getRow();

            $token = getSignedJWTForUser($id);
            $refreshToken = getRefreshJWTForUser($id);

            // the auth table can only insert and update data 
            // if there is already have data in db so we just update that row else we will insert a new row into db
            if(!$authData){
                $data = [
                    'user'          =>  $user['id'],
                    'access_token'  =>  $token,
                    'refresh_token' =>  $refreshToken['token'],
                    'expired_at'    =>  date('Y-m-d H:i:s', $refreshToken['exp']),
                ];
                $authModel->insert($data);
            }else{
                $data = [
                    'user'          =>  $user['id'],
                    'access_token'  =>  $token,
                    'refresh_token' =>  $refreshToken['token'],
                    'expired_at'    =>  date('Y-m-d H:i:s', $refreshToken['exp']),
                    'created_at'    =>  date('Y-m-d H:i:s'),
                ];
                $authModel->update($authData->id, $data);
            }
            if($isRefresh){
                return $this
                ->getResponse(
                    [
                        'status' => 'Success',
                        'data' => [
                            'access_token' => $token,
                            'refresh_token' => $refreshToken['token'],
                        ]
                    ]
                );
            }

            return $this
                ->getResponse(
                    [
                        'status' => 'User authenticated successfully',
                        'data'  => [
                            'user_id' => intval($user['id']),
                            'user_name' => $user['user_name'],
                            // 'email' => $user['email'],
                            // 'phone' => $user['phone'],
                            // 'first_name' => $user['first_name'],
                            // 'last_name' => $user['last_name'],
                            // 'gender' => $user['gender'],
                            // 'birthday' => $user['birthday'],
                            'avatar' => $user['avatar'],
                            // 'exp_point' => $user['exp_point'],
                            // 'rank' => $user['rank'],
                            'access_token' => $token,
                            'refresh_token' => $refreshToken['token'],
                        ],
                    ]
                );
        } catch (Exception $exception) {
            return $this
                ->getResponse(
                    [
                        'message' => $exception->getMessage(),
                    ],
                    $responseCode
                );
        }
    }
    public function register(){
        $validateRules = new RegisterRules();

        $input = $this->getRequestInput($this->request);
        $avatar = $this->request->getFile('avatar');
        if (!$this->validateRequest($input, $validateRules->rules(0))) {
            return $this
                ->getResponse(
                    $this->validator->getErrors(),
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        $accountService = new AccountService();
        $galleryService = new GalleryService();

        $input['created_by'] = 0;
        $input['password'] = password_hash($input['password'], PASSWORD_BCRYPT);

        $id = $accountService->saveData($input);
        $input['id'] = $id;

        if(!isset($avatar)){
            $input['avatar'] = 'assets/images/avatars/avatar.jpg';
            $input['created_by'] = $id;
            $accountService->saveData($input);
        }else{
            $ext = $avatar->getClientExtension();
            $avatarName = 'avatar.'.$ext;
            $avatar->move('upload/users/'.$id.'/avatar', $avatarName);
            $input['avatar'] = 'upload/users/'.$id.'/avatar/'.$avatarName;
            $input['created_by'] = $id;
            $accountService->saveData($input);
        }

        // create default gallery
        $galleryData = [
            'gallery_name' => "Xem sau",
            'created_by'   => $id,
            'is_default'   => IS_DEFAULT,
        ];

        $galleryService->saveData($galleryData);

        return $this->getJWTForUser($input['id'], ResponseInterface::HTTP_CREATED);
    }

    public function signUp(){
        // $validateRules = new SignUpRules();

        $input = $this->getRequestInput($this->request);

        $avatar = $this->request->getFile('avatar');

        $accountService = new AccountService();
        $galleryService = new GalleryService();
        $accountData = null;
        if(isset($input['f_uid'])){
            $accountData = $accountService->getModel()->where(['f_uid' => $input['f_uid']])->first();
        }
        if(isset($input['g_uid'])){
            $accountData = $accountService->getModel()->where(['g_uid' => $input['g_uid']])->first();
        }
        if($accountData == null){
            // if (!$this->validateRequest($input, $validateRules->rules(0))) {
            //     return $this
            //         ->getResponse(
            //             $this->validator->getErrors(),
            //             ResponseInterface::HTTP_BAD_REQUEST
            //         );
            // }
            $input['created_by'] = 0;
            $id = $accountService->saveData($input);
            $input['id'] = $id;

            // save avatar
            if(!isset($avatar)){
                $input['avatar'] = 'assets/images/avatars/avatar.jpg';
                $input['created_by'] = $id;
                $accountService->saveData($input);
            }else{
                $ext = $avatar->getClientExtension();
                $avatarName = 'avatar.'.$ext;
                $avatar->move('upload/users/'.$id.'/avatar', $avatarName);
                $input['avatar'] = 'upload/users/'.$id.'/avatar/'.$avatarName;
                $input['created_by'] = $id;
                $accountService->saveData($input);
            }
            
            // create default gallery
            $galleryData = [
                'gallery_name' => lang('Label.gallery_name'),
                'created_by'   => $id,
                'is_default'   => IS_DEFAULT,
            ];
            $galleryService->saveData($galleryData);

            // update firebase token (in case user reinstall app => firebase token will change so we need to update it in db)
            $data = [];
            $data['id'] = $id;
            $data['firebase_token'] = $input['firebase_token'];
            $accountService->saveData($data);

            return $this->getJWTForUser($input['id'], ResponseInterface::HTTP_CREATED);
        }else{
            // update firebase token (in case user reinstall app => firebase token will change so we need to update it in db)
            $data = [];
            $data['id'] = $accountData->id;
            $data['firebase_token'] = $input['firebase_token'] ?? '';
            $accountService->saveData($data);

            return $this->getJWTForUser($accountData->id, ResponseInterface::HTTP_CREATED);
        }
    }

    public function login(){
        $validateRules = new UserLoginRules();

        $input = $this->getRequestInput($this->request);

        if (!$this->validateRequest($input, $validateRules->rules)) {
            return $this
                ->getResponse(
                    [
                        'message'   =>    $this->validator->getErrors(),
                    ],
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        $accountService = new AccountService();
        $user = $accountService->findUserByPhone($input['phone']);

        if(!$user || !password_verify($input['password'], $user['password'])){
            return $this
                ->getResponse(
                    ['message'=>"Phone or password incorrect"],
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        // update firebase token (in case user reinstall app => firebase token will change so we need to update it in db)
        $data = [];
        $data['id'] = $user['id'];
        $data['firebase_token'] = $input['firebase_token'];
        $accountService->saveData($data);

       return $this->getJWTForUser($user['id']);
    }
 
    public function logout(){
        $userId = $this->getUserInfo();
        
        $authModel = new AuthModel();
        // $authData = $authModel->where(['user' => $user['id']])->delete();

        return $this->getResponse([
            'status' => 'Success'
        ]);
    }

    public function refreshToken(){
            $authenticationHeader = $this->request->getServer('HTTP_AUTHORIZATION');
        try {
            helper('jwt');
            $encodedToken = getJWTFromRequest($authenticationHeader);
            $user = validateJWTFromRequest($encodedToken);
            
            $authModel = new AuthModel();
            $authData = $authModel->where(['user' => $user['id']])->get()->getRow();
            
            if(strcmp($authData->refresh_token, $encodedToken) != 0){
                return Services::response()
                ->setJSON(
                    [
                        'message' => "Invalid token"
                    ]
                )
                ->setStatusCode(ResponseInterface::HTTP_UNAUTHORIZED);
            }
            return $this->getJWTForUser($user['id'], ResponseInterface::HTTP_OK, true);
        } catch (Exception $e) {
            return Services::response()
                ->setJSON(
                    [
                        'message' => $e->getMessage()
                    ]
                )
                ->setStatusCode(ResponseInterface::HTTP_UNAUTHORIZED);
        }
    }

    public function checkPhone(){
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;
        $validateRules = new AccountRules();

        $input = $this->getRequestInput($this->request);

        if (!$this->validateRequest($input, $validateRules->checkPhone)) {
            return $this
                ->getResponse(
                    [
                        'message'   =>    $this->validator->getErrors(),
                    ],
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        $data = $accountService->getInfo(
            [
                $accountAlias.'.*',
            ],
            [
                'phone' => $input['phone'],
            ]
        );
        if($data){
            return $this->getResponse(
                [
                    'data' => [
                        'phoneIsExisted' => true,
                    ],
                ],
                ResponseInterface::HTTP_OK
            );
        }else{
            return $this->getResponse(
                [
                    'data' => [
                        'phoneIsExisted' => false,
                    ],
                ],
                ResponseInterface::HTTP_OK
            );
        }
        
    }
}