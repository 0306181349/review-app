<?php 
namespace App\Controllers;

use App\Services\Post\PostService;
use App\Services\Post\UpdatePostService;
use App\Validation\Rules\Post\PostRules;
use App\Validation\Rules\Post\UpdatePostRules;
use App\Validation\Rules\Post\UploadImageRules;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Config\Services;
class PostController extends BaseController
{
    public function __construct(){
        $this->service = new PostService();
        $this->updateService = new UpdatePostService();
    }

    public function getPostDataByPage(){
        // check if there is token in request header and valid that token
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        // $data = $this->updateService->getPostDataWhenLoggedIn();
        $data = $this->updateService->getPostDataByPage();
        return $this->getResponse(
            [
                'status'     => "Success",
                'pagination' => $data['pagination'],
                'data'       => $data['data'],
            ]
        );
    }

    public function getVideoByPage(){
        // check if there is token in request header and valid that token
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse(
            [
                'status'     => "Success",
                'pagination' => $data['pagination'],
                'data'       => $data['data'],
            ]
        );
    }

    public function getPostDetail(){
        // check if there is token in request header and valid that token
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object
        if(is_object($userInfo)){
            return $userInfo;
        }
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Post not found',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [
                'data'  =>  $data,
            ]
        );
    }

    public function createPost(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }

        $validateRules = new PostRules();

        $input = $this->getRequestInput($this->request);

        if (!$this->validateRequest($input, $validateRules->rules)) {
            return $this
                ->getResponse(
                    $this->validator->getErrors(),
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        $this->updateService->setRequest($this->request);
        $this->updateService->setUser($userInfo);
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse(
            [
                'status'   =>   'Create post success',
                'data'     =>   $data,
            ]
        );
    }
    
    public function postByUser(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }

        $this->updateService->setRequest($this->request);
        $data = $this->updateService->postByUser($userInfo);

        return $this->getResponse([
            'status'        => 'Success',
            'pagination'    => $data['pagination'],
            'data'          => $data['data'],
        ]);
    }

    // public function getViewedPost(){
    //     $userInfo = $this->getUserInfo();
    //     // getUserInfo return an exception object => return that object 
    //     if(is_object($userInfo)){
    //         return $userInfo;
    //     }

    //     $this->updateService->setUser($userInfo);
    //     $this->updateService->setRequest($this->request);
    //     $data = $this->updateService->setAction(__FUNCTION__);

    //     return $this->getResponse([
    //         'status'        => 'Success',
    //         'pagination'    => $data['pagination'],
    //         'data'          => $data['data'],
    //     ]);
    // }

    public function getPostByGallery(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);

        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Gallery not found',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }

        return $this->getResponse([
            'status'        => 'Success',
            'pagination'    => $data['pagination'],
            'data'          => $data['data'],
        ]);
    }

    public function editPost(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        
        $validateRules = new UpdatePostRules();

        $input = $this->getRequestInput($this->request);

        if (!$this->validateRequest($input, $validateRules->rules)) {
            return $this
                ->getResponse(
                    [
                        'messgae' => $this->validator->getErrors()
                    ],
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }
        
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);

        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong!',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }

        return $this->getResponse([
            'status'        => 'Edit Success',
            'data'          => $data,
        ]);
    }

    public function addPostView(){
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);

        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong!',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }

        return $this->getResponse([
            'data' => [
                'status' => 'Success'
            ]
        ]);
    }

    public function getlikedPost(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);

        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong!',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }

        return $this->getResponse([
            'status'        => 'Success',
            'data'          => $data['data'],
        ]); 
    }

    public function getPostByHashtag(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);


        return $this->getResponse([
            'status'        => 'Success',
            'data'          => $data,
        ]); 
    }
}
