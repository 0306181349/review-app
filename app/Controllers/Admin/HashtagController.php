<?php 
namespace App\Controllers\Admin;
use App\Controllers\BaseController; 
use App\Services\Hashtag\HashtagService;
use App\Services\Hashtag\UpdateHashtagService;
use Config\Services;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;

class HashtagController extends BaseController
{
    public function __construct(){
        $this->service = new HashtagService();
        $this->updateService = new UpdateHashtagService();
		$this->session = Services::session();
    }

    public function selectHashtag(){
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse(
            [
                'status'          => 'Success',
                'hashtag_data'   =>  $data,
            ]
        );
    }

    public function index(){
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->getHashtagTableData();
        return $this->getResponse(
            [            
                'status'    =>  'Success',
                'hashtags'      =>  $data,
            ]
        );
    }

    public function edit($id){
        $userId = session('loginData')['id'];
        $this->updateService->setRequest($this->request);
        $this->updateService->setUser($userId);
        $this->updateService->setId($id);
        $data = $this->updateService->setAction(__FUNCTION__);
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [            
                'status'            =>  'Success',
                'hashtag_data'      =>  $data,
            ]
        );
    }

    public function delete($id){
        $userId = session('loginData')['id'];
        $this->updateService->setId($id);
        $this->updateService->setUser($userId);
        $data = $this->updateService->setAction(__FUNCTION__);
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [            
                'status'    =>  'Success',
                'data'      =>  $data,
            ]
        );
    }

    public function add(){
        $userId = session('loginData')['id'];
        $this->updateService->setRequest($this->request);
        $this->updateService->setUser($userId);
        $data = $this->updateService->setAction(__FUNCTION__);
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [            
                'status'            =>  'Success',
                'hashtag_data'      =>  $data,
            ]
        );
    }

    public function postByHashtag($id){
        $userId = session('loginData')['id'];
        $this->updateService->setRequest($this->request);
        $this->updateService->setId($id);
        $this->updateService->setUser($userId);
        $data = $this->updateService->setAction(__FUNCTION__);
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [            
                'status'            =>  'Success',
                'post_data'         =>  $data,
            ]
        );
    }
}