<?php 
namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Services\Account\AccountService;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use App\Services\Account\UpdateAccountService;
use Config\Services;
use Firebase\JWT\JWT;
use App\Validation\Rules\AccountRules;

class AccountController extends BaseController
{
    public function __construct(){
        $this->service = new AccountService();
        $this->updateService = new UpdateAccountService();
		$this->session = Services::session();
    }

    public function userInfo(){
        $userId = session('loginData')['id'];

        $accountAlias = $this->service->getModel()->alias;
        $data = $this->service->getInfo([$accountAlias.'.*'],['id' => $userId]);
        
        unset($data->password);
        return $this->getResponse(
            [
                'message'       =>   "Success",
                'user_data'     =>   $data
            ]
        );
    }

    public function index(){
        $userInfo = session('loginData')['id'];

        $accountModel = $this->service->getModel()->alias;
        $data = $this->service->getData([$accountModel.'.id', 'user_name', 'gender', 'email', 'phone'],[$accountModel.'.id !=' => $userInfo]);
        return $this->getResponse(
            [
                'message' => "Success",
                'users'   => $data
            ]
        );
    }

    public function show($id){
        $accountModel = $this->service->getModel()->alias;
        $data = $this->service->getInfo([$accountModel.'.*'],['id' => $id]);
        if($data == null){
            return $this->getResponse(
                ['errors'=>"Account doesn't exist"],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [
                'message'  => "Success",
                'user_data'     => $data
            ]
        );
    }

    public function create(){
        // Validation
        $validateRules = new AccountRules();
        $input = $this->getRequestInput($this->request);
        if (!$this->validateRequest($input, $validateRules->rules(0))) {
            return $this
                ->getResponse(
                    $this->validator->getErrors(),
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        //Get current request user id so we can set value to create_by field in db
        $userId = session('loginData')['id'];
        $this->updateService->setUser($userId);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);

        if(!$response){
            return $this->getResponse(
                ['errors'=>"Account doesn't exist"],
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }
        return $this->getResponse([
                'id' => $response
            ]
        );
    }


    public function delete($id){
        $userId = session('loginData')['id'];

        $this->updateService->setId($id);
        $this->updateService->setUser($userId);
        $response = $this->updateService->setAction(__FUNCTION__);
        
        if(!$response){
            return $this->getResponse(
                ['errors'=>"Account doesn't exist"],
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }
        return $this->getResponse(
            [
                $response
            ]
        );
    }

    public function update($id){
        //Validation
        $validateRules = new AccountRules();
        $input = $this->getRequestInput($this->request);
        if (!$this->validateRequest($input, $validateRules->rules($id))) {
            return $this
                ->getResponse(
                    $this->validator->getErrors(),
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        $userId = session('loginData')['id'];

        $this->updateService->setId($id);
        $this->updateService->setUser($userId);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);

        if(!$response){
            return $this->getResponse(
                ['errors'=>"Account doesn't exist"],
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }
        return $this->getResponse([
                'id' => $response
            ]
        );
    }

    public function lock($id){
        $userId = session('loginData')['id'];

        $this->updateService->setId($id);
        $this->updateService->setUser($userId);
        $response = $this->updateService->setAction(__FUNCTION__);
        
        if(!$response){
            return $this->getResponse(
                ['errors'=>"Account doesn't exist"],
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }
        return $this->getResponse($response);
    }

    public function changePass($id){
        $userId = session('loginData')['id'];
        
        $this->updateService->setId($id);
        $this->updateService->setRequest($this->request);
        $this->updateService->setUser($userId);
        
        $response = $this->updateService->setAction(__FUNCTION__);
        if(!$response){
            return $this->getResponse(
                ['errors'=>"Account doesn't exist"],
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }
        return $this->getResponse(
            [
                $response
            ]
        );
    }
}