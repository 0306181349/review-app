<?php 
namespace App\Controllers\Admin;
use App\Controllers\BaseController;
use App\Services\Dashboard\DashboardService;
use Config\Services;

class DashboardController extends BaseController
{
    public function __construct(){
        $this->service = new DashboardService();
		$this->session = Services::session();
    }

    public function index(){
        $data = $this->service->setAction(__FUNCTION__);
        return $this->getResponse([
            "dashboard_data" => $data,
        ]);
    }
}