<?php 
namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Services\Province\ProvinceService;
use App\Services\Province\UpdateProvinceService;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Config\Services;

class ProvinceController extends BaseController
{
    public function __construct(){
        $this->service = new ProvinceService();
        $this->updateService = new UpdateProvinceService();
		$this->session = Services::session();
    }

    public function index(){
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse([
            'status'    => 'Success',
            'province'  => $data
        ]);
    }

    public function searchProvince(){
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse(
            [
                'status' => 'Success',
                'data'   =>  $data,
            ]
        );
    }

    public function getAllProvince(){
        $data = $this->updateService->setAction(__FUNCTION__);
        
        return $this->getResponse([
            'status'    => 'Success',
            'province_data'  => $data
        ]);
    }

    public function show(){

    }

    public function add(){
        $userId = session('loginData')['id'];
        $this->updateService->setRequest($this->request);
        $this->updateService->setUser($userId);
        $data = $this->updateService->setAction(__FUNCTION__);
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [            
                'status'    =>  'Success',
                'province_data'      =>  $data,
            ]
        );
    }

    public function edit($id){
        $userId = session('loginData')['id'];
        $this->updateService->setRequest($this->request);
        $this->updateService->setUser($userId);
        $this->updateService->setId($id);
        $data = $this->updateService->setAction(__FUNCTION__);
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [            
                'status'    =>  'Success',
                'province_data'      =>  $data,
            ]
        );
    }

    public function delete($id){
        $userId = session('loginData')['id'];
        $this->updateService->setId($id);
        $this->updateService->setUser($userId);
        $data = $this->updateService->setAction(__FUNCTION__);
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [            
                'status'    =>  'Success',
                'data'      =>  $data,
            ]
        );
    }
}