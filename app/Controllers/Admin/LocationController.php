<?php 
namespace App\Controllers\Admin;
use App\Controllers\BaseController;
use App\Services\Location\LocationService;
use App\Services\Location\UpdateLocationService;
use Config\Services;

class LocationController extends BaseController
{
    public function __construct(){
        $this->service = new LocationService();
        $this->updateService = new UpdateLocationService();
		$this->session = Services::session();
    }

    public function index(){
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse(
            [
                'message' => "Success",
                'location'    => $data
            ]
        );
    }

    public function show($id){
        $this->updateService->setId($id);
        $data = $this->updateService->setAction(__FUNCTION__);
        if($data == null){
            return $this->getResponse(
                ['errors' => "Location doesn't exist"],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [
                'message'          =>  'Success',
                'location_data'    =>   $data
            ]
        );
    }
    
    public function create(){
        $input = $this->getRequestInput($this->request);

        //Get current request user id so we can set value to create_by field in db
        $userId = session('loginData')['id'];
        $this->updateService->setUser($userId);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);

        if(!$response){
            return $this->getResponse([
                'errors'=>"Post doesn't exist"
            ],
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }
        return $this->getResponse([
                'id' => $response
            ]
        );
    }

    public function update($id){

        $input = $this->getRequestInput($this->request);

        $userId = session('loginData')['id'];

        $this->updateService->setId($id);
        $this->updateService->setUser($userId);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);

        if(!$response){
            return $this->getResponse(
                ['errors'=>"Account doesn't exist"],
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }
        return $this->getResponse([
            'id' => $response
            ]
        );
    }

    public function delete($id){
        $userId = session('loginData')['id'];

        $this->updateService->setId($id);
        $this->updateService->setUser($userId);
        $response = $this->updateService->setAction(__FUNCTION__);
        
        if(!$response){
            return $this->getResponse(
                ['errors'=>"Post doesn't exist"],
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }
        return $this->getResponse(
            [
                $response
            ]
        );
    }

    public function selectLocation(){
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse(
            [
                'status'          => 'Success',
                'location_data'   =>  $data,
            ]
        );
    }

    public function verify($id){
        $userId = session('loginData')['id'];

        $this->updateService->setId($id);
        $this->updateService->setUser($userId);
        $response = $this->updateService->setAction(__FUNCTION__);

        if(!$response){
            return $this->getResponse(
                ['errors'=>"Post doesn't exist"],
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }
        return $this->getResponse(
            $response
        );
    }   
}