<?php 
namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Services\Report\ReportTitle\UpdateReportTitleService;
use App\Services\Report\PostReport\PostReportService;
use App\Validation\Rules\Report\ReportPostRules;
use App\Services\Report\PostReport\UpdatePostReportService;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Config\Services;

class ReportController extends BaseController
{
    public function __construct(){
        $this->postReportService = new PostReportService();
        $this->updatePostReportService = new UpdatePostReportService();
        $this->updateReportTitleService = new UpdateReportTitleService();
		$this->session = Services::session();
    }

    public function getAllReportTitle(){
        $this->updateReportTitleService->setRequest($this->request);
        $data = $this->updateReportTitleService->getAllReportTitle(1);
        
        return $this->getResponse(
            [
                'status' => "Success",
                'report_title_data'  =>  $data,
            ]
        );
    }

    public function getAllPostReport(){
        $data = $this->updatePostReportService->setAction(__FUNCTION__);        
        return $this->getResponse(
            [
                'status'       => "Success",
                'report_data'  =>  $data,
            ]
        );
    }

    public function getAllReportByPost($id){
        $this->updatePostReportService->setPostId($id);
        $data = $this->updatePostReportService->setAction(__FUNCTION__);
        return $this->getResponse(
            [
                'status'        =>  "Success",
                'report_data'   =>  $data,
            ]
        );
    }

    public function deletePostReport($id){
        $this->updatePostReportService->setId($id);
        $userId = session('loginData')['id'];
        $this->updatePostReportService->setUser($userId);
        $data = $this->updatePostReportService->setAction(__FUNCTION__);
        
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong!',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }

        return $this->getResponse(
            [
                'status'        =>  "Success",
                'report_data'   =>  $data,
            ]
        );
    }

    public function deleteAllPostReport($id){
        $this->updatePostReportService->setPostId($id);
        $userId = session('loginData')['id'];
        $this->updatePostReportService->setUser($userId);
        $data = $this->updatePostReportService->setAction(__FUNCTION__);
        
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong!',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }

        return $this->getResponse(
            [
                'status'        =>  "Success",
                'report_data'   =>  $data,
            ]
        );
    }

    public function addReportTitle(){
        $userId = session('loginData')['id'];
        $this->updateReportTitleService->setRequest($this->request);
        $this->updateReportTitleService->setUser($userId);
        $data = $this->updateReportTitleService->add();
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [            
                'status'    =>  'Success',
                'report_title_data'      =>  $data,
            ]
        );
    }

    public function editReportTitle($id){
        $userId = session('loginData')['id'];
        $this->updateReportTitleService->setRequest($this->request);
        $this->updateReportTitleService->setId($id);
        $this->updateReportTitleService->setUser($userId);
        $data = $this->updateReportTitleService->edit();
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [            
                'status'    =>  'Success',
                'report_title_data'      =>  $data,
            ]
        );
    }

    public function deleteReportTitle($id){
        $userId = session('loginData')['id'];
        $this->updateReportTitleService->setId($id);
        $this->updateReportTitleService->setUser($userId);
        $data = $this->updateReportTitleService->delete();
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [            
                'status'    =>  'Success',
                'data'      =>  $data,
            ]
        );
    }
}