<?php

namespace App\Controllers\Admin;
use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

class HomeController extends BaseController
{
	public function index()
	{
		if(session('isLoggedIn')){
    		echo view('/admin/main_view');
		}else{
            // return $this->getResponse(
            //     ['errors' => "Post doesn't exist"],
            //     ResponseInterface::HTTP_UNAUTHORIZED
            // );
    		echo view('/admin/login');
		}
	}
}
