<?php
namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Services\Comment\CommentService;
use App\Services\Comment\UpdateCommentService;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Config\Services;

class CommentController extends BaseController
{
    public function __construct(){
        $this->service = new CommentService();
        $this->updateService = new UpdateCommentService();
		$this->session = Services::session();
    }

    public function getAllComment($id){
        $data = $this->updateService->setRequest($this->request);
        $data = $this->updateService->setPostId($id);
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse(
            [
                'status'         => "Success",
                'comment_data'   => $data
            ]
        );
    }

    public function getReplies($id){
        $this->updateService->setRequest($this->request);
        $this->updateService->setId($id);
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse(
            [
                'status'         => "Success",
                'replies_data'   => $data
            ]
        );
    }

    public function delete($id){
        $userId = session('loginData')['id'];
        $this->updateService->setRequest($this->request);
        $this->updateService->setId($id);
        $this->updateService->setUser($userId);
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse(
            [
                'status'         => "Success",
                'data'   => $data
            ]
        );
    }
}