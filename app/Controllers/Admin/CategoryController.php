<?php 
namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use App\Services\Category\CategoryService;
use App\Services\Category\UpdateCategoryService;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Config\Services;

class CategoryController extends BaseController
{
    public function __construct(){
        $this->service = new CategoryService();
        $this->updateService = new UpdateCategoryService();
		$this->session = Services::session();
    }

    public function index(){
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse(
            [
                'status'     => "Success",
                'category'   => $data
            ]
        );
    }

    public function getAllCategory(){
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse(
            [
                'status'            => "Success",
                'category_data'     => $data
            ]
        );
    }

    public function add(){
        $userId = session('loginData')['id'];
        $this->updateService->setRequest($this->request);
        $this->updateService->setUser($userId);
        $data = $this->updateService->setAction(__FUNCTION__);
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [            
                'status'    =>  'Success',
                'category_data'      =>  $data,
            ]
        );
    }

    public function edit($id){
        $userId = session('loginData')['id'];
        $this->updateService->setRequest($this->request);
        $this->updateService->setUser($userId);
        $this->updateService->setId($id);
        $data = $this->updateService->setAction(__FUNCTION__);
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [            
                'status'    =>  'Success',
                'category_data'      =>  $data,
            ]
        );
    }

    public function delete($id){
        $userId = session('loginData')['id'];
        $this->updateService->setId($id);
        $this->updateService->setUser($userId);
        $data = $this->updateService->setAction(__FUNCTION__);
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [            
                'status'    =>  'Success',
                'data'      =>  $data,
            ]
        );
    }
}