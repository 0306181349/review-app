<?php 
namespace App\Controllers\Admin;

use CodeIgniter\HTTP\Response;
use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use App\Services\Post\PostService;
use App\Services\Post\UpdatePostService;
use Config\Services;

class PostController extends BaseController
{
    public function __construct(){
        $this->service = new PostService();
        $this->updateService = new UpdatePostService();
		$this->session = Services::session();
    }

    public function index(){
        $data = $this->updateService->getPostTableData();
        return $this->getResponse(
            [
                'message' => "Success",
                'posts'    => $data
            ]
        );
    }

    public function show($id){
        $this->updateService->setId($id);
        $data = $this->updateService->setAction(__FUNCTION__);
        if($data == null){
            return $this->getResponse(
                ['errors' => "Post doesn't exist"],
                ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [
                'message'          =>  'Success',
                'post_data'        =>   $data
            ]
        );
    }

    public function create(){
        $input = $this->getRequestInput($this->request);

        //Get current request user id so we can set value to create_by field in db
        $userId = session('loginData')['id'];
        $this->updateService->setUser($userId);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);

        if(!$response){
            return $this->getResponse([
                'errors'=>"Post doesn't exist"
            ],
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }
        return $this->getResponse([
                'id' => $response
            ]
        );
    }
    
    public function delete($id){
        $userId = session('loginData')['id'];

        $this->updateService->setId($id);
        $this->updateService->setUser($userId);
        $response = $this->updateService->setAction(__FUNCTION__);
        
        if(!$response){
            return $this->getResponse(
                ['errors'=>"Post doesn't exist"],
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }
        return $this->getResponse(
            [
                $response
            ]
        );
    }

    public function update($id){
        $input = $this->getRequestInput($this->request);

        $userId = session('loginData')['id'];

        $this->updateService->setId($id);
        $this->updateService->setUser($userId);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);

        if(!$response){
            return $this->getResponse(
                ['errors'=>"Account doesn't exist"],
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }
        return $this->getResponse([
            'id' => $response
        ]);
    }

    public function lock($id){
        $userId = session('loginData')['id'];
        $this->updateService->setId($id);
        $this->updateService->setUser($userId);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);

        if(!$response){
            return $this->getResponse(
                ['errors'=>"Account doesn't exist"],
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }
        return $this->getResponse([
            'id' => $response
        ]);
    }

    public function showAllLockedPost(){
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse(
            [
                'message' => "Success",
                'posts'    => $data
            ]
        );
    }

    public function showLockedPost($id){
        $this->updateService->setId($id);
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse(
            [
                'message' => "Success",
                'post_data'    => $data
            ]
        );
    }

    public function getAllPostUnlockRequest(){
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse(
            [
                'message' => "Success",
                'posts'    => $data
            ]
        );
    }

    public function unlockPost($id){
        $userId = session('loginData')['id'];
        $this->updateService->setId($id);
        $this->updateService->setUser($userId);
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse(
            [
                'message' => "Success",
                'posts'    => $data
            ]
        );
    }

    public function unlockPostReject($id){
        $userId = session('loginData')['id'];
        $this->updateService->setId($id);
        $this->updateService->setUser($userId);
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse(
            [
                'message' => "Success",
                'posts'    => $data
            ]
        );
    }
}