<?php 
namespace App\Controllers;
use App\Services\Report\ReportTitle\UpdateReportTitleService;
use App\Services\Report\PostReport\PostReportService;
use App\Validation\Rules\Report\ReportPostRules;
use App\Services\Report\PostReport\UpdatePostReportService;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;

class ReportController extends BaseController
{
    public function __construct(){
        $this->postReportService = new PostReportService();
        $this->updatePostReportService = new UpdatePostReportService();
        $this->updateReportTitleService = new UpdateReportTitleService();
    }

    public function getAllReportTitle(){
        $this->updateReportTitleService->setRequest($this->request);
        $data = $this->updateReportTitleService->setAction(__FUNCTION__);
        
        return $this->getResponse(
            [
                'status' => "Success",
                'data'  =>  $data,
            ]
        );
    }

    public function reportPost(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        
        $validateRules = new ReportPostRules();

        $input = $this->getRequestInput($this->request);

        if (!$this->validateRequest($input, $validateRules->rules)) {
            return $this
                ->getResponse(
                    [
                        'messgae' => $this->validator->getErrors()
                    ],
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }
        
        $this->updatePostReportService->setUser($userInfo);
        $this->updatePostReportService->setRequest($this->request);
        $data = $this->updatePostReportService->setAction(__FUNCTION__);

        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong!',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        
        return $this->getResponse([
            'data' => [
                'status' => 'Success'
            ]
        ]);
    }
}