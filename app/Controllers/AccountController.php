<?php 
namespace App\Controllers;
use App\Services\Account\AccountService;
use App\Services\Account\UpdateAccountService;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use App\Validation\Rules\AccountRules;
use App\Services\Dashboard\DashboardService;

class AccountController extends BaseController
{
    public function __construct(){
        $this->service = new AccountService();
        $this->updateService = new UpdateAccountService();
    }

    public function accountDetail(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        $this->updateService->setUser($userInfo);
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse([
            'status' => "Success",
            'data'   => $data,
        ]);
    }
    public function accountProfile(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        $this->updateService->setRequest($this->request);
        $this->updateService->setUser($userInfo);
        $data = $this->updateService->setAction(__FUNCTION__);
        if(!$data){
            return $this->getResponse([
                'message' => "User not found",
            ]);
        }
        return $this->getResponse([
            'status' => "Success",
            'data'   => $data,
        ]);
    }

    public function follow(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);
        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong bitch'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
        return $this->getResponse([
            'data' => [
                'status' => 'Success'
            ]
        ]);
    }

    public function unfollow(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);
        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong bitch'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
        return $this->getResponse([
            'data' => [
                'status' => 'Success'
            ]
        ]);
    }

    public function likePost(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);
        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong bitch'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
        return $this->getResponse([
            'data' => [
                'status' => 'Success'
            ]
        ]);
    }

    public function unlikePost(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }

        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);
        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong bitch'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
        return $this->getResponse([
            'data' => [
                'status' => 'Success'
            ]
        ]);
    }

    public function addFavoritePost(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }

        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);

        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong bitch'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
        
        return $this->getResponse([
            'data' => [
                'status' => 'Success'
            ]
        ]);
    }

    public function removeFavoritePost(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);

        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong bitch'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
        
        return $this->getResponse([
            'data' => [
                'status' => 'Success'
            ]
        ]);

    }

    public function changePass(){
        // Validation
        $validateRules = new AccountRules();
        
        $input = $this->getRequestInput($this->request);
        if (!$this->validateRequest($input, $validateRules->changePass)) {
            return $this
                ->getResponse(
                    $this->validator->getErrors(),
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        $this->updateService->setId($this->getUserInfo());
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);

        switch ($response){
            case 'User not found!':
                return $this->getResponse([
                    'message' => $response
                ], ResponseInterface::HTTP_NOT_FOUND);
                break;
            case 'Password incorrect!':
                return $this->getResponse([
                    'message' => $response
                ], ResponseInterface::HTTP_BAD_REQUEST);
                break;
            default:
                return redirect()->route('refresh-token');
                break;
        }
    }

    public function changeAvatar(){
        // Valiadtion
        $validateRules = new AccountRules();
        
        $input = $this->request->getFiles();

        if (!$this->validateRequest($input, $validateRules->changeAvatar)) {
            return $this
                ->getResponse(
                    $this->validator->getErrors(),
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        $this->updateService->setId($this->getUserInfo());
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);
        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong bitch'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
        return $this
        ->getResponse([
            'data' => $response,
        ]); 
    }

    public function editProfile(){
        $this->updateService->setUser($this->getUserInfo());
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);
        
        return $this
        ->getResponse([
            'data' => $response,
        ]);
    }

    public function getListFollower(){
        // $userInfo = $this->getUserInfo();
        // if(is_object($userInfo)){
        //     return $userInfo;
        // }

        // $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);
        
        return $this
        ->getResponse([
            'status'        => "Success",
            'pagination'    => $data['pagination'],
            'data'          => $data['data'],
        ]);
    }

    public function getListFollowing(){
        // $userInfo = $this->getUserInfo();
        // if(is_object($userInfo)){
        //     return $userInfo;
        // }

        // $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);

        return $this
        ->getResponse([
            'status'        => "Success",
            'pagination'    => $data['pagination'],
            'data'          => $data['data'],
        ]);
    }

    public function getListFavoritePost(){
        $userInfo = $this->getUserInfo();
        if(is_object($userInfo)){
            return $userInfo;
        }

        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);
        
        return $this
        ->getResponse([
            'status'        => "Success",
            'pagination'    => $data['pagination'],
            'data'          => $data['data'],
        ]);
    }

    public function getTopFollower(){
        $userInfo = $this->getUserInfo();
        if(is_object($userInfo)){
            return $userInfo;
        }

        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);

        return $this
        ->getResponse([
            'status'        => "Success",
            'pagination'    => $data['pagination'],
            'data'          => $data['data'],
        ]);
    }

    public function getTopUserByMonth(){
        $userInfo = $this->getUserInfo();
        if(is_object($userInfo)){
            return $userInfo;
        }

        $dashboardService = new DashboardService();
        $dashboardService->setUser($userInfo);
        $dashboardService->setRequest($this->request);
        $data = $dashboardService->setAction(__FUNCTION__);

        return $this->getResponse([
            'status'        => 'Success',
            'data'          => $data,
        ]);
    }
}