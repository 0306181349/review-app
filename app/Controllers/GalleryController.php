<?php 
namespace App\Controllers;
use App\Services\Gallery\GalleryService;
use App\Services\Gallery\UpdateGalleryService;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use App\Validation\Rules\Gallery\GalleryRules;

class GalleryController extends BaseController
{
    public function __construct(){
        $this->service = new GalleryService();
        $this->updateService =  new UpdateGalleryService();
    }

    public function createGallery(){
        // Validation
        $validateRules = new GalleryRules();
        
        $input = $this->getRequestInput($this->request);
        if (!$this->validateRequest($input, $validateRules->rules)) {
            return $this
                ->getResponse(
                    [
                        'message' => $this->validator->getErrors(),
                    ],
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }

        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);

        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong!'
            ], ResponseInterface::HTTP_BAD_REQUEST);
        }
        return $this->getResponse([
            'status' => 'Success',
            'data'   => $response,
        ]);
    }

    public function editGallery(){
        // Validation
        $validateRules = new GalleryRules();
        
        $input = $this->getRequestInput($this->request);
        if (!$this->validateRequest($input, $validateRules->rules)) {
            return $this
                ->getResponse(
                    [
                        'message' => $this->validator->getErrors(),
                    ],
                    ResponseInterface::HTTP_BAD_REQUEST
                );
        }

        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }

        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);

        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong!'
            ], ResponseInterface::HTTP_BAD_REQUEST);
        }

        return $this->getResponse([
            'status' => 'Success',
            'data'   => $response,
        ]);
    }

    public function deleteGallery(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }

        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);

        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong!'
            ], ResponseInterface::HTTP_BAD_REQUEST);
        }

        return $this->getResponse([
            'data' => [
                'status' => "Success",
            ]
        ]);
    }

    public function getAllGallery(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }

        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);

        return $this->getResponse([
            'status' => 'Success',
            'data'   => $data,
        ]);
    }

    public function setPrivacy(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }

        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);

        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong!'
            ], ResponseInterface::HTTP_BAD_REQUEST);
        }

        return $this->getResponse([
            'data' => [
                'status' => 'Success',
            ]
        ]);
    }

    public function removePostFromGallery(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }

        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);

        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong!'
            ], ResponseInterface::HTTP_BAD_REQUEST);
        }

        return $this->getResponse([
            'data' => [
                'status' => 'Success'
            ]
        ]);
    }

    public function addPostToGallery(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        
        $this->updateService->setUser($userInfo);
        $this->updateService->setRequest($this->request);
        $response = $this->updateService->setAction(__FUNCTION__);

        if(!$response){
            return $this->getResponse([
                'message' => 'Something went wrong!'
            ], ResponseInterface::HTTP_BAD_REQUEST);
        }

        return $this->getResponse([
            'data'   => [
                'status' => 'Success',
            ]
        ]);
    }
}