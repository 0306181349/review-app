<?php 
namespace App\Controllers;
use App\Services\Category\CategoryService;
use App\Services\Category\UpdateCategoryService;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;

class CategoryController extends BaseController
{
    public function __construct(){
        $this->service = new CategoryService();
        $this->updateService = new UpdateCategoryService();
    }

    public function getAllCategory(){
        $data = $this->updateService->setAction(__FUNCTION__);
        
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [
                'status' => "Success",
                'data'   =>  $data,
            ]
        );
    }
}