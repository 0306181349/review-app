<?php 
namespace App\Controllers;
use App\Services\Province\ProvinceService;
use App\Services\Province\UpdateProvinceService;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;

class ProvinceController extends BaseController
{
    public function __construct(){
        $this->service = new ProvinceService();
        $this->updateService = new UpdateProvinceService();
    }

    public function getAllProvince(){
        $data = $this->updateService->setAction(__FUNCTION__);
        
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [
                'status' => "Success",
                'data'  =>  $data,
            ]
        );

    }
}