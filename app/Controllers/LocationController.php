<?php 
namespace App\Controllers;
use App\Services\Location\LocationService;
use App\Services\Location\UpdateLocationService;
use App\Services\Dashboard\DashboardService;
use CodeIgniter\HTTP\ResponseInterface;

class LocationController extends BaseController
{
    public function __construct(){
        $this->service = new LocationService();
        $this->updateService = new UpdateLocationService();
    }

    public function createLocation(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        $this->updateService->setRequest($this->request);
        $this->updateService->setUser($userInfo);
        $data = $this->updateService->setAction(__FUNCTION__);
        return $this->getResponse(
            [
                'status'    => 'Create location success',
                'data'      => $data,
            ]
        );
    }

    // public function searchLocation(){
    //     $this->updateService->setRequest($this->request);
    //     $data = $this->updateService->setAction(__FUNCTION__);
    //     return $this->getResponse(
    //         [
    //             'status' => 'Success',
    //             'data'   =>  $data,
    //         ]
    //     );
    // }

    public function locationDetail(){
        $this->updateService->setId($this->request->getGet('id'));
        $data = $this->updateService->setAction(__FUNCTION__);
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Location not found',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [
                'data'  =>  $data,
            ]
        );
    }

    public function postByLocation(){
        $userInfo = $this->getUserInfo();
        // getUserInfo return an exception object => return that object 
        if(is_object($userInfo)){
            return $userInfo;
        }
        $this->updateService->setUser($userInfo);
        $this->updateService->setId($this->request->getGet('location'));
        $this->updateService->setRequest($this->request);
        $data = $this->updateService->setAction(__FUNCTION__);
        if(!$data){
            return $this->getResponse(
                [
                    'message'  =>  'Something went wrong!',
                ], ResponseInterface::HTTP_NOT_FOUND
            );
        }
        return $this->getResponse(
            [
                'status'     => "Success",
                'pagination' => $data['pagination'],
                'data'       => $data['data'],
            ]
        );
    }

    public function getTopLocationByMonth(){
        $dashboardService = new DashboardService();
        $dashboardService->setRequest($this->request);
        $data = $dashboardService->setAction(__FUNCTION__);

        return $this->getResponse([
            'status'        => 'Success',
            'data'          => $data,
        ]);
    }
}