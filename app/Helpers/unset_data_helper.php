<?php

function unsetObject($fields, $data){
    $newData = $data;
    foreach($fields as $key => $field){
        unset($newData->$field);
    }
    return $newData;
}