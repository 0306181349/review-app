<?php

function convertNum($num){
    if(!is_numeric($num)){
       return $num; 
    }else{
        if(floatval($num) && intval($num) != floatval($num)){
            return floatval($num);
        }
        return intval($num);
    }
}
/** @param: object or array
 * 
 */
function convertNumData($data){
    if(is_object($data)){
        foreach($data as $key => $value){
            if($key != 'phone'){
                $data->$key = convertNum($value);
            }
        }
    }
    if(is_array($data)){
        foreach($data as $key => $value){
            if($key != "phone"){
                $data[$key] = convertNum($value);
            }
        }
    }
    return $data;
}
