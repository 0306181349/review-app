<?php

use Config\Services;
use Firebase\JWT\JWT;
use App\Services\Account\AccountService;

function getJWTFromRequest($authenticationHeader): string
{
    if (is_null($authenticationHeader)) { //JWT is absent
        throw new Exception('Missing or invalid JWT in request');
    }
    //JWT is sent from client in the format Bearer XXXXXXXXX
    return explode(' ', $authenticationHeader)[1];
}

function validateJWTFromRequest(string $encodedToken)
{
    $key = Services::getSecretKey();
    $decodedToken = JWT::decode($encodedToken, $key, ['HS256']);
    $accountService = new AccountService();
    // If the user was not found, the User Model throws an exception which is caught and returned to the user as an HTTP_UNAUTHORIZED (401) response
    return $accountService->findUserById($decodedToken->id);
}

function getSignedJWTForUser($id)
{
    $issuedAtTime = time();
    $tokenTimeToLive = getenv('JWT_TIME_TO_LIVE');
    $tokenExpiration = $issuedAtTime + $tokenTimeToLive;
    $payload = [
        'id'  => $id,
        'iat' => $issuedAtTime,
        'exp' => $tokenExpiration,
    ];

    $jwt = JWT::encode($payload, Services::getSecretKey());
    return $jwt;
}

function getRefreshJWTForUser($id)
{
    $issuedAtTime = time();
    $tokenTimeToLive = getenv('REFRESH_JWT_TIME_TO_LIVE');
    $tokenExpiration = $issuedAtTime + $tokenTimeToLive;
    $payload = [
        'id'  => $id,
        'iat' => $issuedAtTime,
        'exp' => $tokenExpiration,
    ];

    $jwt = JWT::encode($payload, Services::getRefreshSecretKey());
    return ['token' => $jwt, 'exp' => $tokenExpiration];
}
