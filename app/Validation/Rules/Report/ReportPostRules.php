<?php 
namespace App\Validation\Rules\Report;

class ReportPostRules
{
    public $rules = [
        'report_title'  =>  'required|numeric',
        'post_id'       =>  'required|numeric',
    ];
}