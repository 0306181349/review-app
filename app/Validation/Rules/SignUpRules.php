<?php 
namespace App\Validation\Rules;

class SignUpRules 
{
    public function rules($id){
        return [
            'uid'   =>  'trim|required|is_unique[account.uid,id,'. $id .']',
        ];
    }
}