<?php 
namespace App\Validation\Rules;

class RegisterRules
{
    public function rules($id){
        return [
            'phone'             =>  'trim|required|max_length[20]|min_length[9]|is_unique[account.phone,id,'. $id .']',
            'password'          =>  'trim|required',
            'user_name'         =>  'required',
            'firebase_token'    =>  'trim|required',
        ];
    }
}