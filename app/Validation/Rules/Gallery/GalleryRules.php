<?php 
namespace App\Validation\Rules\Gallery;

class GalleryRules
{
    public $rules = [
        'gallery_name'  =>  'required',
        'is_private'    =>  'in_list[0,1]'
    ];
}