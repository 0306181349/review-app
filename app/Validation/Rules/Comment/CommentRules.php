<?php 
namespace App\Validation\Rules\Comment;

class CommentRules
{
    public $rules = [
        'content'       =>  'required',
    ];
}