<?php 
namespace App\Validation\Rules;

class AccountRules 
{
    public function rules($id){
        return [
            'email' => 'trim|required|max_length[20]|min_length[6]|is_unique[account.email,id,'. $id .']',
        ];
    }

    public $changePass = [
        'old_pass'      => 'trim|required|min_length[8]',
        'new_pass'      => 'trim|required|min_length[8]',
        'confirm_pass'  => 'trim|required|matches[new_pass]',
    ];

    public $changeAvatar = [
        'avatar' => 'is_image[avatar]',
    ];

    public $checkPhone = [
        'phone' => 'required|min_length[10]|numeric',
    ];
}