<?php 
namespace App\Validation\Rules\Location;

class LocationRules
{
    public $rules = [
        'title'         =>  'required',
        'content'       =>  'required',
        'location'      =>  'required|numeric',
        'rating'        =>  'required|numeric',
        'province'      =>  'required|numeric',
    ];
}