<?php 
namespace App\Validation\Rules\Post;

class UpdatePostRules
{
    public $rules = [
        'title'                 =>  'required',
        'content'               =>  'required',
        'category'              =>  'required|numeric',
        'rating'                =>  'required|numeric|less_than[6]',
        // 'uploaded_image'        =>  'required',
    ];
}