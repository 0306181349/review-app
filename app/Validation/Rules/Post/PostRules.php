<?php 
namespace App\Validation\Rules\Post;

class PostRules
{
    public $rules = [
        'title'         =>  'required',
        'content'       =>  'required',
        'location'      =>  'required|numeric',
        'category'      =>  'required|numeric',
        'rating'        =>  'required|numeric|less_than[6]',
    ];
}