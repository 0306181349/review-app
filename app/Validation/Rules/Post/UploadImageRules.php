<?php 
namespace App\Validation\Rules\Post;

class UploadImageRules
{
    public $rules = [
        'image[]'   =>  'required',
    ];
}