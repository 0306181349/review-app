<?php 
namespace App\Services\Province;
use App\Services\Province\Province;

class UpdateProvinceService
{   
    private $request;
    private $userId = 0;
	private $id;

    public function __construct(){
        $this->service = new ProvinceService();
        helper('convert_num');
    }

    //set modify account id
    public function setId($id){
		return $this->id = $id;
	}

    //set user id who is modifying the account info 
    public function setUser($userId){
        return $this->userId = $userId;
    }
    
	public function setRequest($request){
		return $this->request = $request;
	}

    public function setAction($action){
        return $this->{$action}();
    }

    public function index(){
        $provinceAlias = $this->service->getModel()->alias;
        $selects = [
            $provinceAlias.'.id',
            $provinceAlias.'.province_name',
            $provinceAlias.'.created_at',
        ];
        $data = $this->service->getData($selects);
        return $data;
    }

    public function getAllProvince(){
        $provinceAlias = $this->service->getModel()->alias;
        $selects = [
            $provinceAlias.'.id AS province_id',
            $provinceAlias.'.province_name',
        ];
        
        $data = $this->service->getData($selects, []);
        foreach($data as $key => $value){
            $value = convertNumData($value);
        }

        return $data;
    }

    public function searchProvince(){
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;
        $key = $this->request->getGet('key');
        $selects = [
            'id AS location_id',
            'location_name',
            'address',
        ];
        $like = ['location_name' => $key];
        $data = $this->service->createQuery()->select($selects)->like($like)->get()->getResultObject();
        foreach($data as $key => $value){
            $postData = $postService->getInfo([$postAlias.".post_image"], ["location" => $value->location_id], 0, 1);
            $value = convertNumData($value);
            if($postData){
                $value->location_image = [json_decode($postData->post_image, true)[0]];
            }else{
                $value->location_image = [];
            }
            $value->post_by_location = $postService->countResult(['location' => $value->location_id]);
        }
        return $data;
    }

    public function getProvinceData(){
        $provinceAlias = $this->service->getModel()->alias;
        
        $data = $this->service->getInfo(
            [
                $provinceAlias.'.id',
                $provinceAlias.'.province_name',
                $provinceAlias.'.created_at',
            ], 
            [
                'id' => $this->id,
            ]
        );
        return $data;
    }

    public function add(){
        $input = $this->request->getPost();
        if (empty($input)) {
			//convert request body to associative array
			$input = json_decode($this->request->getBody(), true);
		}
        $hashtagAlias = $this->service->getModel()->alias;

        $data = [
            'province_name' => $input['province_name'],
            'created_by' => $this->userId,
        ];

        $this->id = $this->service->saveData($data);
        return $this->getProvinceData();
    }

    public function edit(){
        $input = $this->request->getPost();
        if (empty($input)) {
			//convert request body to associative array
			$input = json_decode($this->request->getBody(), true);
		}
        $provinceAlias = $this->service->getModel()->alias;
        $data = $this->service->getInfo(
            [
                $provinceAlias.'.id'
            ],
            [
                'id' => $this->id
            ]
        );
        if(!$data){
            return false;
        }
        $data = [
            'id' => $this->id,
            'province_name' => $input['province_name'],
            'updated_by' => $this->userId,
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        $this->id = $this->service->saveData($data);
        return $this->getProvinceData();
    }

    public function delete(){
        $hashtagAlias = $this->service->getModel()->alias;

        $data = $this->service->getInfo(
            [
                $hashtagAlias.'.id'
            ],
            [
                'id' => $this->id
            ]
        );
        if(!$data){
            return false;
        }
        $data = [
            'id' => $this->id,
            'is_deleted' => DEL_FLG_ON,
            'deleted_by' => $this->userId,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
        $this->service->saveData($data);
        return true;
    }
}