<?php 
namespace App\Services\Comment;
use App\Models\CommentModel;
use App\Models\AccountModel;
use App\Models\PostModel;

class CommentService 
{
    public function __construct(){
        $this->model = new CommentModel();
        $this->db = \Config\Database::connect();
    }

    public function getModel(){
        return $this->model;
    }

    public function createQuery(){
        $accountModel = new AccountModel();
        $postModel = new PostModel();

        $commentTable = $this->model->table;
        $commentAlias = $this->model->alias;

        $accountTable = $accountModel->table;
        $accountAlias = $accountModel->alias;

        $postTable = $postModel->table;
        $postAlias = $postModel->alias;

        $builder = $this->db->table($this->model->table.' AS '.$this->model->alias)
                            ->join($accountTable.' AS '.$accountAlias, $commentAlias.'.created_by = '.$accountAlias.'.id')
                            ->join($postTable.' AS '.$postAlias, $commentAlias.'.post_id = '.$postAlias.'.id')
                            ->where($this->model->alias.'.is_deleted', DEL_FLG_OFF);
        return $builder;
    }

    public function getData($selects = [], $where = [], $page = 0, $limit = 0){
        if($page != 0){
            $page = $page * $limit;
        }

        $postAlias = $this->model->alias;
        $postTable = $this->model->table;
        $query = $this->createQuery()
                                    ->select($selects)
                                    ->where($where)
                                    ->limit($limit, $page)
                                    ->orderBy($postAlias.'.id', 'DESC');
        $get = $query->get();
        return $get->getResultObject();
    }

	// get row information
	public function getInfo($selects = [], $where = []){
		$model_alias = $this->model->alias;

		$query = $this->createQuery()
						->select($selects);
		if(is_array($where) && !empty($where)){
			$query = $this->buildCondition($query, $where);
		}
		return $query->get()->getRow();
	}
    
    // cout result
    public function countResult($where = []){
        $query = $this->createQuery();
        if(!empty($where)){
		    if(is_array($where)){
		    	$query = $this->buildCondition($query, $where);
		    }else{
                $query->where($where);
            }
        }
        return $query->countAllResults();
    }
    
	// set condition
	protected function buildCondition($query, $condition){
		foreach($condition as $field=>$val){
			switch ($field) {
				default:
					if($val !== ''){
						$query->where($this->model->alias.".".$field, $val);
					}
					break;
			}
		}
		return $query;
	}

    
	public function saveData($data){
		$id = $data['id'] ?? false;
		if($id){
			unset($data['id']);
			$this->model->update($id, $data);
		}else{
			$id = $this->model->insert($data);
		}
		return $id;
	}
}