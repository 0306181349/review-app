<?php 
namespace App\Services\Comment;
use App\Services\Comment\CommentService;
use App\Services\Account\AccountService;
use App\Services\Like\LikeCommentService;
use App\Services\Post\PostService;
use App\Models\CommentModel;
use App\Services\Notification\UpdateNotificationService;
use App\Services\Notification\NotificationService;

class UpdateCommentService
{
    protected $id = 0;
	protected $postId = 0;
    protected $userId = 0;
	protected $request;

    
    public function __construct(){
        $this->service = new CommentService();
        helper('convert_num');
    }

    //set modify account id
    public function setId($id){
		return $this->id = $id;
	}

    public function setPostId($id){
        return $this->postId = $id;
    }

    //set user id who is modifying the account info 
    public function setUser($userId){
        return $this->userId = $userId;
    }
    
	public function setRequest($request){
		return $this->request = $request;
	}

    public function setAction($action){
        return $this->{$action}();
    }

    public function getSigleComment(){
        $commentAlias = $this->service->getModel()->alias;
        $accountService = new AccountService();
        $likeCommentService = new LikeCommentService();
        $likeCommentAlias = $likeCommentService->getModel()->alias;
        $commentAlias = $this->service->getModel()->alias;
        $accountAlias = $accountService->getModel()->alias;

        $data = $this->service->getInfo(
            [
                $commentAlias.'.id AS comment_id',
                $commentAlias.'.content',
                $commentAlias.'.created_at AS comment_date',
                $commentAlias.'.created_by AS user',
                $commentAlias.'.comment_image',
                $accountAlias.'.id AS user_id',
                $accountAlias.'.user_name',
                $accountAlias.'.avatar',
            ],
            [
                'id' => $this->id,
            ]
        );
        $data->user = convertNumData([
            'user_id'   =>  $data->user_id,
            'user_name' =>  $data->user_name,
            'avatar'    =>  $data->avatar,
        ]);

        $data->like = $likeCommentService->countResult([$likeCommentAlias.'.comment_id' => $data->comment_id]);
        $data->num_of_reply = $this->service->countResult(['comment_id' => $data->comment_id]);
        
        if($this->userId){
            $data->isLiked = false;
            if($likeCommentService->getModel()->where(['comment_id' => $data->comment_id, 'account_id' => $this->userId])->first()){
                $data->isLiked = true;
            }
        }

        unset($data->user_id);
        unset($data->user_name);
        unset($data->avatar);
        
        return convertNumData($data);
    }

    public function getAllComment(){
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;

        $accountService = new AccountService();
        $likeCommentService = new LikeCommentService();
        $likeCommentAlias = $likeCommentService->getModel()->alias;
        $commentAlias = $this->service->getModel()->alias;
        $accountAlias = $accountService->getModel()->alias;

                
        $selects = [
            $commentAlias.'.id AS comment_id',
            $commentAlias.'.content',
            $commentAlias.'.comment_id AS reply_to',
            $commentAlias.'.created_at AS comment_date',
            $commentAlias.'.created_by AS user',
            $commentAlias.'.comment_image',
            $accountAlias.'.id AS user_id',
            $accountAlias.'.user_name',
            $accountAlias.'.avatar',
        ];

        $where = [
            $commentAlias.'.post_id' => $this->postId,
            $commentAlias.'.comment_id' => NULL,
        ];

        $data = $this->service->getData($selects, $where, $page, $limit);

        foreach($data as $key => $value){
            if($this->service->getInfo(
                [
                    $commentAlias.'.id'
                ],
                [
                    'comment_id' => $value->comment_id
                ]
            )){
                $value->is_loaded = false;
            }else{
                $value->is_loaded = true;
            }
        }

        return $data;
    }

    public function getReplies(){
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;

        $accountService = new AccountService();
        $likeCommentService = new LikeCommentService();
        $likeCommentAlias = $likeCommentService->getModel()->alias;
        $commentAlias = $this->service->getModel()->alias;
        $accountAlias = $accountService->getModel()->alias;

                
        $selects = [
            $commentAlias.'.id AS comment_id',
            $commentAlias.'.content',
            $commentAlias.'.comment_id AS reply_to',
            $commentAlias.'.created_at AS comment_date',
            $commentAlias.'.created_by AS user',
            $commentAlias.'.comment_image',
            $accountAlias.'.id AS user_id',
            $accountAlias.'.user_name',
            $accountAlias.'.avatar',
        ];

        $where = [
            $commentAlias.'.comment_id' => $this->id,
        ];

        $data = $this->service->getData($selects, $where, $page, $limit);

        foreach($data as $key => $value){
            $value->is_loaded = false;
        }

        return $data;
    }

    public function delete(){
        $commentAlias = $this->service->getModel()->alias;
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;

        $commentData = $this->service->getInfo(
            [
                $commentAlias.'.id',
                $commentAlias.'.comment_image',
                $commentAlias.'.created_by AS comment_author',
                $postAlias.'.created_by AS post_author'
            ],
            [
                'id' => $this->id,
            ]
        );

        $replyCommentData = $this->service->getData(
            [
                $commentAlias.'.id',
            ],
            [
                'comment_id' => $this->id,
            ]
        );

        foreach($replyCommentData as $key => $value){
            $data = [
                'id'         => $value->id,
                'is_deleted' => DEL_FLG_ON,
                'deleted_by' => $this->userId,
                'deleted_at' => date('Y-m-d H:i:s'),
            ];
            $this->service->saveData($data);
        }

        if(isset($commentData->comment_image)){
            @unlink($commentData->comment_image);
        }

        $data = [
            'id'         => $commentData->id,
            'is_deleted' => DEL_FLG_ON,
            'deleted_by' => $this->userId,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];

        $this->service->saveData($data);
        return true;
    }

    public function getCommentByPage(){
        $post_id = is_numeric($this->request->getGet('post')) ? $this->request->getGet('post') : 0;
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;

        $accountService = new AccountService();
        $likeCommentService = new LikeCommentService();
        $likeCommentAlias = $likeCommentService->getModel()->alias;
        $commentAlias = $this->service->getModel()->alias;
        $accountAlias = $accountService->getModel()->alias;
        
        $selects = [
            $commentAlias.'.id AS comment_id',
            $commentAlias.'.content',
            $commentAlias.'.created_at AS comment_date',
            $commentAlias.'.created_by AS user',
            $commentAlias.'.comment_image',
            $accountAlias.'.id AS user_id',
            $accountAlias.'.user_name',
            $accountAlias.'.avatar',
        ];

        $where = [
            $commentAlias.'.post_id' => $post_id,
            $commentAlias.'.comment_id' => NULL,
        ];

        $data = $this->service->getData($selects, $where, $page, $limit);
        $countResult = 0;
        foreach($data as $key => $value){
            $countResult ++;

            if($value->comment_image == null){
                $value->comment_image = "";
            }
            
            $value = convertNumData($value);
            
            $value->user = [
                'user_id'   =>  $value->user_id,
                'user_name' =>  $value->user_name,
                'avatar'    =>  $value->avatar,
            ];

            $value->like = $likeCommentService->countResult([$likeCommentAlias.'.comment_id' => $value->comment_id]);
            $value->num_of_reply = $this->service->countResult(['comment_id' => $value->comment_id]);

            if($this->userId){
                $value->isLiked = false;
                if($likeCommentService->getModel()->where(['comment_id' => $value->comment_id, 'account_id' => $this->userId])->first()){
                    $value->isLiked = true;
                }
            }
            
            unset($value->user_id);
            unset($value->user_name);
            unset($value->avatar);
        }
        
        $pagination = [
            'page'  => $page,
            'limit' => $limit,
            'total' => $countResult,
        ];
        
        $pagination = convertNumData($pagination);
        
        return['data' => $data, 'pagination' => $pagination];
    }

    public function getReplyCommentByPage(){
        $comment_id = is_numeric($this->request->getGet('comment')) ? $this->request->getGet('comment') : "0";
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;

        $accountService = new AccountService();
        $likeCommentService = new LikeCommentService();
        $likeCommentAlias = $likeCommentService->getModel()->alias;
        $commentAlias = $this->service->getModel()->alias;
        $accountAlias = $accountService->getModel()->alias;
        
        $selects = [
            $commentAlias.'.id AS comment_id',
            $commentAlias.'.content',
            $commentAlias.'.created_at AS comment_date',
            $commentAlias.'.created_by AS user',
            $commentAlias.'.comment_image',
            $accountAlias.'.id AS user_id',
            $accountAlias.'.user_name',
            $accountAlias.'.avatar',
        ];

        $where = [
            $commentAlias.'.comment_id' => $comment_id,
        ];
        
        $data = $this->service->getData($selects, $where, $page, $limit);
        $countResult = 0;
        foreach($data as $key => $value){
            $countResult ++;

            if($value->comment_image == null){
                $value->comment_image = "";
            }
            
            $value = convertNumData($value);
            
            $value->user = [
                'user_id'   =>  $value->user_id,
                'user_name' =>  $value->user_name,
                'avatar'    =>  $value->avatar,
            ];
            $value->like = $likeCommentService->countResult([$likeCommentAlias.'.comment_id' => $value->comment_id]);
            if($this->userId){
                $value->isLiked = false;
                if($likeCommentService->getModel()->where(['comment_id' => $value->comment_id, 'account_id' => $this->userId])->first()){
                    $value->isLiked = true;
                }
            }
            
            unset($value->user_id);
            unset($value->user_name);
            unset($value->avatar);
        }
        $pagination = [
            'page'  => $page,
            'limit' => $limit,
            'total' => $countResult,
        ];
        
        $pagination = convertNumData($pagination);
        
        return['data' => $data, 'pagination' => $pagination];

    }

    private function replyComment($input, $image){
        $updateNotificationService = new UpdateNotificationService();
        $commentAlias = $this->service->getModel()->alias;
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;

        // check if the user is reply on their comment (in that case notification should not be fired)
        $isReplyByMySelf = false;
        $commentData = $this->service->getInfo([$commentAlias.'.*'], ['id' => $input['comment_id'], 'post_id' => $input['post_id']]);
        if(!$commentData){
            return false;
        }
        $type = NEW_REPLY_COMMENT;
        if($commentData->created_by == $this->userId){
            $isReplyByMySelf = true;
        }

        $input['created_by'] = $this->userId;
        $this->id = $this->service->saveData($input);

        if($image && $image->isValid()){
            $input['id'] = $this->id;
            $imageName = $image->getRandomName();
            $image->move('upload/comment/'.$this->id, $imageName);
            $input['comment_image'] = 'upload/comment/'.$this->id.'/'.$imageName;
            $this->service->saveData($input);
            unset($input['id']);
        }

        $created_at = $this->service->getInfo(
            [$commentAlias.'.created_at'], //select
            ['id' => $this->id] // where condition
        );
        $input['comment_date'] = $created_at->created_at;

        $user_data = $accountService->getInfo(
            [$accountAlias.'.id AS user_id', $accountAlias.'.avatar', $accountAlias.'.user_name'], //select
            ['id' => $this->userId] // where condition
        );
        $user_data = convertNumData($user_data);
        $input['user'] = $user_data;
        $input['comment_id'] = $this->id;
        unset($input['created_by']);

        // create and push notification 
        if(!$isReplyByMySelf){
            $updateNotificationService->setType($type);
            $updateNotificationService->setUser($this->userId);
            $updateNotificationService->setComment($this->id);
            $updateNotificationService->createNotification();
        }
        
        return convertNumData($input);
    }

    private function addCommentToPost($input, $image, $commentToMyPost){
        $updateNotificationService = new UpdateNotificationService();
        $commentAlias = $this->service->getModel()->alias;
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;

        $type = NEW_COMMENT;
        $input['created_by'] = $this->userId;
        $this->id = $this->service->saveData($input);

        if($image && $image->isValid()){
            $input['id'] = $this->id;
            $imageName = $image->getRandomName();
            $image->move('upload/comment/'.$this->id, $imageName);
            $input['comment_image'] = 'upload/comment/'.$this->id.'/'.$imageName;
            $this->service->saveData($input);
            unset($input['id']);
        }

        $commentAlias = $this->service->getModel()->alias;
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;

        $created_at = $this->service->getInfo(
            [$commentAlias.'.created_at'], //select
            ['id' => $this->id] // where condition
        );
        $input['comment_date'] = $created_at->created_at;

        $user_data = $accountService->getInfo(
            [$accountAlias.'.id AS user_id', $accountAlias.'.avatar', $accountAlias.'.user_name'], //select
            ['id' => $this->userId] // where condition
        );
        $user_data = convertNumData($user_data);
        $input['user'] = $user_data;
        $input['comment_id'] = $this->id;
        unset($input['created_by']);
        
        // create and push notification 
        if(!$commentToMyPost){
            $updateNotificationService->setType($type);
            $updateNotificationService->setUser($this->userId);
            $updateNotificationService->setComment($this->id);
            $updateNotificationService->createNotification();
        }

        return convertNumData($input);

    }

    public function addComment(){
        $input = $this->request->getPost();
        $image = $this->request->getFile('image');
        $updateNotificationService = new UpdateNotificationService();

        // check if the user is comment on their post (in that case notification should not be fired)
        $commentToMyPost = false;

        // check if post exist or not
        if(isset($input['post_id'])){
            $postService = new PostService();
            $postAlias = $postService->getModel()->alias;
            $postData = $postService->getInfo([$postAlias.'.*'], ['id' => $input['post_id'], 'is_locked' => LOCK_FLG_OFF]); 
            if(!$postData){
                return false;
            }
            if($postData->created_by == $this->userId){
                $commentToMyPost = true;
            }
        }
        // check if comment exist or not
        if(isset($input['comment_id'])){
            return $this->replyComment($input, $image);
        }
        return $this->addCommentToPost($input, $image, $commentToMyPost);
    }

    public function likeComment(){
        $commentId = $this->request->getGet("comment");
        $likeCommentService = new LikeCommentService();
        $commentAlias = $this->service->getModel()->alias;
        $updateNotificationService = new UpdateNotificationService();
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;

        $selects = [
            $commentAlias.'.id as comment_id',
            $commentAlias.'.created_by',
            $commentAlias.'.post_id',
        ];

        $commentData = $this->service->getInfo($selects, ['id' => $commentId]);

        if(!$commentData){
            return false;
        }

        $likePostData = $likeCommentService->getInfo(['*'], ['comment_id' => $commentId, 'account_id' => $this->userId]);

        // if the user already like this comment 
        if($likePostData){
            return false;
        }
        
        $postData = $postService->getInfo(
            [
                $postAlias.'.is_locked',
            ],
            [
                'id' => $commentData->post_id,
            ]
        );
        if(!$postData || $postData->is_locked != LOCK_FLG_OFF){
            return false;
        }

        $data = [
            'account_id'    => $this->userId,
            'comment_id'    => $commentId
        ];
        $likeCommentId = $likeCommentService->saveData($data);

        // create and push noti
        if($commentData->created_by != $this->userId){
            $updateNotificationService->setType(LIKE_COMMENT);
            $updateNotificationService->setUser($this->userId);
            $updateNotificationService->setLikeComment($likeCommentId);
            $updateNotificationService->createNotification();
        }
        return $likeCommentId;
    }

    public function unlikeComment(){
        $commentId = $this->request->getGet("comment");
        $likeCommentService = new LikeCommentService();
        $notificationService = new NotificationService();
        $notificationAlias = $notificationService->getModel()->alias;

        $likeCommentData = $likeCommentService->getModel()->where(['comment_id' => $commentId])->first();
        if(!$likeCommentData){
            return false;
        }
        
        $notificationData = $notificationService->getInfo(
            [
                $notificationAlias.'.id'
            ],
            [
                'like_comment_id' => $likeCommentData->id,
            ]
        );
        if($notificationData){
            $notificationService->saveData(
                [
                    'id'            => $notificationData->id,
                    'deleted_by'    => $this->userId,
                    'deleted_at'    => date('Y-m-d H:i:s'),
                    'is_deleted'    => DEL_FLG_ON, 
                ]
            );
        }
        $likeCommentService->getModel()->delete($likeCommentData->id);
        return true;
    }

    public function editComment(){
        $this->id = is_numeric($this->request->getGet('id')) ? $this->request->getGet('id') : 0;
        $input = $this->request->getPost();
        $image = $this->request->getFile('image');
        $commentAlias = $this->service->getModel()->alias;
        $input['id'] = $this->id;

        $commentData = $this->service->getInfo(
            [
                $commentAlias.'.*',
            ],
            [
                'id' => $this->id,
                'created_by' => $this->userId,
            ]
        );
        
        if(!$commentData){
            return false;
        }

        if($image && $image->isValid()){
            @unlink($commentData->comment_image);
            $imageName = $image->getRandomName();
            $image->move('upload/comment/'.$this->id, $imageName);
            $input['comment_image'] = 'upload/comment/'.$this->id.'/'.$imageName;
            $this->service->saveData($input);
        }
        
        $input['updated_at'] = date('Y-m-d H:i:s');
        $input['updated_by'] = $this->userId;
        $this->service->saveData($input);
        return $this->getSigleComment();
    }

    public function deleteComment(){
        $this->id = is_numeric($this->request->getGet('id')) ? $this->request->getGet('id') : 0;
        $commentAlias = $this->service->getModel()->alias;
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;

        $commentData = $this->service->getInfo(
            [
                $commentAlias.'.id',
                $commentAlias.'.comment_image',
                $commentAlias.'.created_by AS comment_author',
                $postAlias.'.created_by AS post_author'
            ],
            [
                'id' => $this->id,
            ]
        );

        if(!$commentData || ( $this->userId != $commentData->comment_author && $this->userId != $commentData->post_author) ){
            return false;
        }

        $replyCommentData = $this->service->getData(
            [
                $commentAlias.'.id',
            ],
            [
                'comment_id' => $this->id,
            ]
        );

        foreach($replyCommentData as $key => $value){
            $data = [
                'id'         => $value->id,
                'is_deleted' => DEL_FLG_ON,
                'deleted_by' => $this->userId,
                'deleted_at' => date('Y-m-d H:i:s'),
            ];
            $this->service->saveData($data);
        }

        if(isset($commentData->comment_image)){
            @unlink($commentData->comment_image);
        }

        $data = [
            'id'         => $commentData->id,
            'is_deleted' => DEL_FLG_ON,
            'deleted_by' => $this->userId,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];

        $this->service->saveData($data);
        return true;
    }

    public function detailComment(){
        $this->id = is_numeric($this->request->getGet('id')) ? $this->request->getGet('id') : 0;
        $commentAlias = $this->service->getModel()->alias;
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;
        $likeCommentService = new LikeCommentService();
        $likeCommentAlias = $likeCommentService->getModel()->alias;

        $commentData = $this->service->getInfo(
            [
                $commentAlias.'.id AS comment_id',
                $commentAlias.'.content',
                $commentAlias.'.post_id',
                $commentAlias.'.created_at AS comment_date',
                $commentAlias.'.created_by AS user',
                $commentAlias.'.comment_image',
                $accountAlias.'.id AS user_id',
                $accountAlias.'.user_name',
                $accountAlias.'.avatar',
            ],
            [
                'id' => $this->id,
            ]
        );

        if(!$commentData){
            return false;
        }

        if($commentData->comment_image == null){
            $commentData->comment_image = "";
        }
        
        $commentData = convertNumData($commentData);
        
        $commentData->user = [
            'user_id'   =>  $commentData->user_id,
            'user_name' =>  $commentData->user_name,
            'avatar'    =>  $commentData->avatar,
        ];
        $commentData->like = $likeCommentService->countResult([$likeCommentAlias.'.comment_id' => $commentData->comment_id]);
        if($this->userId){
            $commentData->isLiked = false;
            if($likeCommentService->getModel()->where(['comment_id' => $commentData->comment_id, 'account_id' => $this->userId])->first()){
                $commentData->isLiked = true;
            }
        }
        
        unset($commentData->user_id);
        unset($commentData->user_name);
        unset($commentData->avatar);

        return $commentData;
    }
}