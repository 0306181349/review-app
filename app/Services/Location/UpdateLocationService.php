<?php 
namespace App\Services\Location;
use App\Services\Location\LocationService;
use App\Services\Province\ProvinceService;
use App\Services\Account\AccountService;
use App\Services\Follow\FollowService;
use App\Models\LocationModel;
use App\Services\Post\PostService;

class UpdateLocationService
{
    
    private $request;
    private $userId = 0;
	private $id;

    public function __construct(){
        helper('convert_num');
        $this->service = new LocationService();
    }

    //set modify account id
    public function setId($id){
		return $this->id = $id;
	}

    //set user id who is modifying the account info 
    public function setUser($userId){
        return $this->userId = $userId;
    }
    
	public function setRequest($request){
		return $this->request = $request;
	}

    public function setAction($action){
        return $this->{$action}();
    }

    public function index(){
        $locationAlias = $this->service->getModel()->alias;
        $postService = new PostService();
        $selects = [
            $locationAlias.'.id',
            $locationAlias.'.location_name',
            $locationAlias.'.open_time',
            $locationAlias.'.closed_time',
        ];
        $data = $this->service->getData($selects);
        foreach($data as $key => $value){
            $value->rating = $postService->selectAvg($postService->getModel()->alias.'.rating', ['location' => $value->id])->rating;
            $value->rating = round($value->rating, 1);
        }
        return $data;
    }

    public function show(){
        $provinceService = new ProvinceService();
        $provinceAlias = $provinceService->getModel()->alias;
        $locationAlias = $this->service->getModel()->alias;
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;
        $data = $this->service->getInfo(
            [
                $locationAlias.'.id',
                $locationAlias.'.location_name',
                $locationAlias.'.open_time',
                $locationAlias.'.closed_time',
                $locationAlias.'.lowest_price',
                $locationAlias.'.highest_price',
                $locationAlias.'.address',
                $locationAlias.'.phone_number',
                $locationAlias.'.is_verify',
                $locationAlias.'.verified_by',

                $provinceAlias.'.id AS province_id',
                $provinceAlias.'.province_name',
            ],
            [
                'id' => $this->id,
            ]
        );

        if($data->is_verify == 1){
            $data->verified_by = $accountService->getInfo(
                [
                    $accountAlias.".user_name"
                ],
                [
                    'id' => $data->verified_by
                ]
            )->user_name;
            $data->is_verify = true;
        }else{
            $data->is_verify = false;
        }

        return $data;
    }

    public function verify(){
        $locationAlias = $this->service-> getModel()->alias;
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;

        $locationData = $this->service->getInfo(
            [
                $locationAlias.'.is_verify',
            ],
            [
                'id' => $this->id,
            ]
        );
        $isVerify = false;
        if($locationData->is_verify == VER_FLG_OFF){
            $isVerify = true;
            $data = [
                'id' => $this->id,
                'is_verify' => VER_FLG_ON,
                'verified_by' => $this->userId,
            ];
        }else{
            $data = [
                'id' => $this->id,
                'is_verify' => VER_FLG_OFF,
                'verified_by' => NULL,
            ];
        }
        $this->service->saveData($data);
        return [
            'is_verify' => $isVerify,
            'verify_by' => $accountService->getInfo(
                [
                    $accountAlias.'.user_name',
                ],
                [
                    'id' => $this->userId
                ]
            )->user_name,
        ];
    }

    public function locationDetail(){
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;
        $locationModel = $this->service->getModel();
        $locationAlias = $locationModel->alias;
        $provinceService = new ProvinceService();
        $provinceAlias = $provinceService->getModel()->alias;
        $selects = [
            $locationAlias.'.id AS location_id',
            $locationAlias.'.open_time AS open_time',
            $locationAlias.'.closed_time AS closed_time',
            $locationAlias.'.lowest_price AS lowest_price',
            $locationAlias.'.highest_price AS highest_price',
            $locationAlias.'.phone_number AS phone',
            $locationAlias.'.province AS province_id',
            $locationAlias.'.location_name',
            $locationAlias.'.address',
        ];
        $where = [
            'id' => $this->id
        ];
        
        $data = $this->service->getInfo($selects, $where);
        $provinceData = $provinceService->getInfo([$provinceAlias.'.id AS province_id', $provinceAlias.'.province_name'], ['id' => $data->province_id]);
        $provinceData = convertNumData($provinceData);
        $data->province = $provinceData;
        unset($data->province_id);
        $data->location_rating = $postService->selectAvg($postService->getModel()->alias.'.rating', ['location'=>$this->id])->rating;
        $data->post_by_location = $postService->countResult(['location'=>$this->id]);
        $data = convertNumData($data);
        $data->location_image = [];

        // get 3 lastest post image
        $topThreePostData = $postService->getData([$postAlias.".post_image"], [$postAlias.".location" => $this->id], 0, 3);

        foreach($topThreePostData as $key => $value){
            foreach(json_decode($value->post_image) as $key => $value){
                array_push($data->location_image, $value);
            }
        }

        $data->location_rating = round($data->location_rating, 1);
        return $data;
    }

    public function createLocation(){
        $data = $this->request->getPost();
        $image = $this->request->getFiles();
        if (empty($data)) {
			//convert request body to associative array
			$data = json_decode($this->request->getBody(), true);
		}
        $data['created_by'] = $this->userId;
        $this->id = $this->service->saveData($data);

        if(isset($image) && isset($image['location_image'])){
            $data['location_image'] = [];
            $data['id'] = $this->id;
            foreach($image['location_image'] as $key => $value){
                $ext = $value->getClientExtension();
                $imgName = $value->getRandomName();
                $path = 'upload/location/'.$this->id;
                $value->move($path, $imgName);
                array_push($data['location_image'], $path.'/'.$imgName);
            }
            $data['location_image'] = json_encode($data['location_image']);
            $this->id = $this->service->saveData($data);
        }
        $locationData = $this->locationDetail();
        if($locationData->location_image){
            $locationData->location_image = [$locationData->location_image[0]];
        }
        return $locationData;
    }

    public function searchLocation(){
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;
        $key = $this->request->getGet('key');
        $province = is_numeric($this->request->getGet('province')) ? $this->request->getGet('province') : '0';
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 3;

        $locationAlias = $this->service->getModel()->alias;

        $selects = [
            $locationAlias.'.id AS location_id',
            $locationAlias.'.location_name',
            $locationAlias.'.address',
        ];

        if($province){
            $data = $this->service->createQuery()->select($selects)
                                  ->like([$locationAlias.'.location_name' => $key])
                                  ->where([$locationAlias.'.province' => $province])
                                  ->limit($limit, $page != 0 ? $page * $limit : $page)
                                  ->orderBy($locationAlias.'.id', 'DESC')
                                  ->get()
                                  ->getResultObject();
        }else{
            $data = $this->service->createQuery()->select($selects)
                                  ->like([$locationAlias.'.location_name' => $key])
                                  ->limit($limit, $page != 0 ? $page * $limit : $page)
                                  ->orderBy($locationAlias.'.id', 'DESC')
                                  ->get()
                                  ->getResultObject();
        }

        $countResult = 0;
        foreach($data as $key => $value){
            $countResult ++;
            $postData = $postService->getInfo([$postAlias.".post_image"], ["location" => $value->location_id], 0, 1);
            $value = convertNumData($value);
            if($postData && $postData->post_image != NULL){
                $value->location_image = [json_decode($postData->post_image, true)[0]];
            }else{
                $value->location_image = [];
            }
            $value->post_by_location = $postService->countResult(['location' => $value->location_id]);
            
            $value->location_rating = $postService->selectAvg($postService->getModel()->alias.'.rating', ['location'=>$value->location_id])->rating;
            $value->location_rating = round($value->location_rating, 1);
        }
        
        $pagination = [
            'page'  => $page,
            'limit' => $limit,
            'total' => $countResult,
        ];
        $pagination = convertNumData($pagination);

        return ['pagination' => $pagination, 'data' => $data];
    }

    public function create(){
        $data = $this->request->getPost();
        if (empty($data)) {
			//convert request body to associative array8
			$data = json_decode($this->request->getBody(), true);
		}
        
        $data['created_by'] = $this->userId;
        $data['created_at'] = date('Y-m-d H:i:s');
        $this->id = $this->service->saveData($data);
        return $this->id;
    }

    public function update(){
        $data = $this->request->getPost();
        if (empty($data)) {
			//convert request body to associative array
			$data = json_decode($this->request->getBody(), true);
		}
        $data['id'] = $this->id;
        $data['updated_by'] = $this->userId;
        $data['updated_at'] = date('Y-m-d H:i:s');
        $this->id = $this->service->saveData($data);
        return $this->id;
    }
    
    public function delete(){
        $obj = $this->service->getInfo(["*"],["id" => $this->id]);
        if(empty($obj)){
            return false;
        }
        $data = [
            "id"            =>  $this->id,
            'is_deleted'    =>  DEL_FLG_ON,
            'deleted_at'    =>  date('Y-m-d H:i:s'),
            'deleted_by'    =>  $this->userId,
        ];
        $this->id = $this->service->saveData($data);
        return ['message' => "Delete success!"];
    }

    public function postByLocation(){
        $page = $this->request->getGet("page");
        $limit = $this->request->getGet("limit");

        $postService = new PostService();
        $accountService = new AccountService();
        $followService = new FollowService();
        $accountAlias = $accountService->getModel()->alias;
        $postAlias = $postService->getModel()->alias;
        $selects = [
            $postAlias.'.id AS post_id',
            $postAlias.'.title AS post_title',
            $postAlias.'.content AS post_content',
            $postAlias.'.rating AS post_rating',
            $postAlias.'.created_at AS post_date',
            $postAlias.'.location AS location_id',
            $postAlias.'.post_image',
            $postAlias.'.created_by AS user_id',
        ];
        $data = $postService->getData($selects, [$postAlias.'.location' => $this->id], $page, $limit);
        $countResult = 0;

        foreach($data as $key => $value){
            $value->user = $accountService->getInfo(
                //selects
                [
                    $accountAlias.'.id AS user_id,',
                    $accountAlias.'.user_name',
                    $accountAlias.'.avatar',
                ], 
                // where
                [
                    'id' => $value->user_id
                ]
            );
            $countResult++;
            $value->post_image = json_decode($value->post_image);
            if($this->userId){
                $value->user->isFollowed = false;
                if($followService->getModel()->where(['account_id_1' => $this->userId, 'account_id_2' => $value->user_id])->first()){
                    $value->user->isFollowed = true;
                }
            }
            unset($value->user_id);
            unset($value->location_id);
            $value = convertNumData($value);
            $value->user = convertNumData($value->user);
        }

        $pagination = [
            'page'  => $page,
            'limit' => $limit,
            'total' => $countResult,
        ];
        
        $pagination = convertNumData($pagination);
        return ['data' => $data, 'pagination' => $pagination];

    }

    public function selectLocation(){
        $key = $this->request->getGet('key');
        $locationAlias = $this->service->getModel()->alias;
        $selects = [
            $locationAlias.'.id AS location_id',
            $locationAlias.'.location_name',
        ];
        $like = ['location_name' => $key];
        $data = $this->service->createQuery()->select($selects)->like($like)->get()->getResultObject();
        
        return $data;
    }
}