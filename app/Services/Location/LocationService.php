<?php 
namespace App\Services\Location;
use App\Models\LocationModel;
use App\Services\Province\ProvinceService;
use App\Services\Post\PostService;
use App\Services\Notification\NotificationService;
use Exception;

class LocationService 
{
    public function __construct(){
        $this->model = new LocationModel();
        $this->db = \Config\Database::connect();
    }

    public function getModel(){
        return $this->model;
    }

    public function createQuery(){
		$provinceService = new ProvinceService();
		$provinceTable = $provinceService->getModel()->table;
		$provinceAlias = $provinceService->getModel()->alias;
        $builder = $this->db->table($this->model->table.' AS '.$this->model->alias)
							->join($provinceTable.' AS '.$provinceAlias, $this->model->alias.'.province = '.$provinceAlias.'.id')
                            ->where($this->model->alias.'.is_deleted', DEL_FLG_OFF);
        return $builder;
    }

    public function getData($selects = [], $where = []){
        $accountAlias = $this->model->alias;
        $accountTable = $this->model->table;
        $query = $this->createQuery()
                                    ->select($selects)
                                    ->where($where)
                                    ->orderBy($accountAlias.'.id', 'DESC');
        $get = $query->get();
        return $get->getResultObject();
    }

	// get row information
	public function getInfo($selects = [],$where = []){
		$model_alias = $this->model->alias;
        
		$query = $this->createQuery()
						->select($selects);
		if(is_array($where) && !empty($where)){
			$query = $this->buildCondition($query, $where);
		}
		return $query->get()->getRow();
	}
    
	// set condition
	protected function buildCondition($query, $condition){
		foreach($condition as $field=>$val){
			switch ($field) {
				default:
					if($val !== ''){
						$query->where($this->model->alias.".".$field, $val);
					}
					break;
			}
		}
		return $query;
	}
	
    // cout result
	public function countResult($where=[]){
        $query = $this->createQuery();
        if(!empty($where)){
		    if(is_array($where)){
		    	$query = $this->buildCondition($query, $where);
		    }else{
                $query->where($where);
            }
        }
        return $query->countAllResults();
    }
    
	public function saveData($data){
		$id = $data['id'] ?? false;
		if($id){
			if(isset($data['is_deleted']) && $data['is_deleted'] == DEL_FLG_ON){
				$this->deleteRef($data);
			}
			$this->model->update($id, $data);
		}else{
			$id = $this->model->insert($data);
		}
		return $id;
	}

	public function deleteRef($data){
		$postService = new PostService();
		$postAlias = $postService->getModel()->alias;
		$postModel = $postService->getModel();
		$postData = $postService->getData(
			[
				$postAlias.'.id'
			],
			[
				$postAlias.'.location' => $data['id'],
			]
		);
		foreach($postData as $key => $value){
			$insertData = [
				'id' => $value->id,
				'is_deleted' => DEL_FLG_ON,
				'deleted_at' => date('Y-m-d H:i:s'),
				'deleted_by' => $data['deleted_by']
			];
			$postService->saveData($insertData);
		}
		return true;
	}
}