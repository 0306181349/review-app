<?php
namespace App\Services\Report\ReportTitle;
use App\Services\Report\ReportTitle\ReportTitleService;
use App\Services\Report\PostReport\PostReportService;

class UpdateReportTitleService
{
    private $request;
    private $userId = 0;
	private $id;

    public function __construct(){
        helper('convert_num');
        $this->service = new ReportTitleService();
    }

    //set modify account id
    public function setId($id){
		return $this->id = $id;
	}

    //set user id who is modifying the account info 
    public function setUser($userId){
        return $this->userId = $userId;
    }
    
	public function setRequest($request){
		return $this->request = $request;
	}

    public function setAction($action){
        return $this->{$action}();
    }

    public function getAllReportTitle($isAdmin = 0){
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;
        
        $reportTitleAlias = $this->service->getModel()->alias;
        $data = $this->service->getData(
            [
                $reportTitleAlias.'.id AS report_title_id',
                $reportTitleAlias.'.title AS report_title',
            ],
            [

            ],
            $page,
            $limit
        );
        
        if($isAdmin){
            foreach($data as $key => $value){
                $value->report_num = 0;
            }
        }else{
            foreach($data as $key => $value){
                $value = convertNumData($value);
            }
        }
        return $data;
    }

    public function getReportTitleData(){
        $reportTitleAlias = $this->service->getModel()->alias;
        $data = $this->service->getinfo(
            [
                $reportTitleAlias.'.id AS report_title_id',
                $reportTitleAlias.'.title AS report_title',
            ],
            [
                'id' => $this->id,
            ],  
        );

        return $data;
    }

    public function add(){
        $input = $this->request->getPost();
        if (empty($input)) {
			//convert request body to associative array
			$input = json_decode($this->request->getBody(), true);
		}

        $data = [
            'title' => $input['report_title'],
            'created_by'    => $this->userId,
        ];

        $this->id = $this->service->saveData($data);
        return $this->getReportTitleData();
    }

    public function edit(){
        $input = $this->request->getPost();
        if (empty($input)) {
			//convert request body to associative array
			$input = json_decode($this->request->getBody(), true);
		}
        $provinceAlias = $this->service->getModel()->alias;
        $data = $this->service->getInfo(
            [
                $provinceAlias.'.id'
            ],
            [
                'id' => $this->id
            ]
        );
        if(!$data){
            return false;
        }
        $data = [
            'id'            => $this->id,
            'title' => $input['report_title'],
            'updated_by'    => $this->userId,
            'updated_at'    => date('Y-m-d H:i:s'),
        ];

        $this->id = $this->service->saveData($data);
        return $this->getReportTitleData();
    }

    public function delete(){
        $reportTitleAlias = $this->service->getModel()->alias;

        $data = $this->service->getInfo(
            [
                $reportTitleAlias.'.id'
            ],
            [
                'id' => $this->id
            ]
        );
        if(!$data){
            return false;
        }
        $data = [
            'id' => $this->id,
            'is_deleted' => DEL_FLG_ON,
            'deleted_by' => $this->userId,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
        $this->service->saveData($data);
        return true;
    }
}