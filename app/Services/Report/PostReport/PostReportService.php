<?php 
namespace App\Services\Report\PostReport;
use App\Models\PostReportModel;
use App\Services\Post\PostService;
use App\Services\Account\AccountService;
use App\Services\Report\ReportTitle\ReportTitleService;

class PostReportService
{
    public function __construct(){
        $this->model = new PostReportModel();
        $this->db = \Config\Database::connect();
    }

    public function getModel(){
        return $this->model;
    }

    public function createQuery(){
		$postService = new PostService();
		$postTable = $postService->getModel()->table;
		$postAlias = $postService->getModel()->alias;
		$reportTitleService = new ReportTitleService();
		$reportTitleAlias = $reportTitleService->getModel()->alias;
		$reportTitleTable = $reportTitleService->getModel()->table;
		$accountService = new AccountService();
		$accountAlias = $accountService->getModel()->alias;
		$accountTable = $accountService->getModel()->table;

        $builder = $this->db->table($this->model->table.' AS '.$this->model->alias)
						->join($postTable.' AS '.$postAlias, $this->model->alias.'.post_id = '.$postAlias.'.id AND '.$postAlias.'.is_deleted = '.DEL_FLG_OFF)
						->join($reportTitleTable.' AS '.$reportTitleAlias, $this->model->alias.'.report_title = '.$reportTitleAlias.'.id AND '.$reportTitleAlias.'.is_deleted = '.DEL_FLG_OFF)
						->join($accountTable.' AS '.$accountAlias, $this->model->alias.'.created_by = '.$accountAlias.'.id AND '.$accountAlias.'.is_deleted = '.DEL_FLG_OFF)
						->where($this->model->alias.'.is_deleted', DEL_FLG_OFF);
        return $builder;
    }

    public function getData($selects = [], $where = []){
        $accountAlias = $this->model->alias;
        $accountTable = $this->model->table;
        $query = $this->createQuery()
                                    ->select($selects)
                                    ->where($where)
                                    ->orderBy($accountAlias.'.id', 'DESC');
        $get = $query->get();
        return $get->getResultObject();
    }

	// get row information
	public function getInfo($selects = [], $where = []){
		$model_alias = $this->model->alias;

		$query = $this->createQuery()
						->select($selects);
		if(is_array($where) && !empty($where)){
			$query = $this->buildCondition($query, $where);
		}
		return $query->get()->getRow();
	}
    
    // cout result
    public function countResult($where=[]){
        $builder = $this->createQuery();
        $builder->where($where);
        return $builder->countAllResults();
    }

	// set condition
	protected function buildCondition($query, $condition){
		foreach($condition as $field=>$val){
			switch ($field) {
				default:
					if($val !== ''){
						$query->where($this->model->alias.".".$field, $val);
					}
					break;
			}
		}
		return $query;
	}

    
	public function saveData($data){
		$id = $data['id'] ?? false;
		if($id){
			unset($data['id']);
			$this->model->update($id, $data);
		}else{
			$id = $this->model->insert($data);
		}
		return $id;
	}
}