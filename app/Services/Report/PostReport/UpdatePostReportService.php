<?php 
namespace App\Services\Report\PostReport;
use App\Services\Report\PostReport\PostReportService;
use App\Services\Report\ReportTitle\ReportTitleService;
use App\Services\Post\PostService;
use App\Services\Account\AccountService;

class UpdatePostReportService 
{
    
    private $request;
    private $userId = 0;
	private $id;
    private $postId;

    public function __construct(){
        $this->service = new PostReportService();
    }

    //set modify account id
    public function setId($id){
		return $this->id = $id;
	}

    //set user id who is modifying the account info 
    public function setUser($userId){
        return $this->userId = $userId;
    }
    
	public function setRequest($request){
		return $this->request = $request;
	}

    public function setAction($action){
        return $this->{$action}();
    }

    public function setPostId($postId){
        return $this->postId = $postId;
    }

    public function reportPost(){
        $data = $this->request->getPost();
        if (empty($data)) {
			//convert request body to associative array
			$data = json_decode($this->request->getBody(), true);
		}
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;
        $reportTitleService = new ReportTitleService();
        $reportTitleAlias =  $reportTitleService->getModel()->alias;

        $postData = $postService->getInfo(
            [
                $postAlias.'.id',
            ],
            [
                'id' => $data['post_id'],
                'is_locked' => LOCK_FLG_OFF,
            ]
        );

        $reportTitleData = $reportTitleService->getInfo(
            [
                $reportTitleAlias.'.id'
            ],
            [
                'id' => $data['report_title']
            ]
        );

        if(!$postData || !$reportTitleData){
            return false;
        }

        $data['created_by'] = $this->userId;
        $data['created_at'] = date('Y-m-d H:i:s');

        return $this->service->saveData($data);
    }
    
	public function getAllPostReport(){
		$reportPostAlias = $this->service->getModel()->alias;
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;
		$reportTitleService = new ReportTitleService();
		$reportTitleAlias = $reportTitleService->getModel()->alias;

        $selects = [
            $reportPostAlias.'.id',
            $reportTitleAlias.'.title',
            $reportPostAlias.'.report_content',
            $reportPostAlias.'.report_content',
        ];
        $data = $this->service->createQuery()->select('COUNT( '.$postAlias.'.id ) AS post_report_num, '
                                                               .$postAlias.'.id AS post_id, '
                                                               .$postAlias.'.title AS post_title')
                                            ->groupBy($postAlias.'.id')
                                            ->get()
                                            ->getResultObject();
        return $data;
	}

    public function getAllReportByPost(){
        $postReportAlias = $this->service->getModel()->alias;
        $reportTitleService = new ReportTitleService();
        $reportTitleAlias = $reportTitleService->getModel()->alias;
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;
        $data = $this->service->getData(
            [
                $reportTitleAlias.'.title AS report_title',
                $postReportAlias.'.id AS report_id',
                $postReportAlias.'.report_content',
                $postReportAlias.'.created_at',
                $accountAlias.'.id AS account_id',
                $accountAlias.'.avatar',
                $accountAlias.'.user_name',
            ],
            [
                $postReportAlias.'.post_id' => $this->postId
            ]
        );

        return $data;
    }

    public function deletePostReport(){
        $postReportAlias = $this->service->getModel()->alias;
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;
        $postData = $postService->getInfo(
            [
                $postAlias.'.id',
            ],
            [
                'id' => $this->id,
            ]
        );
        $postReportData = $this->service->getInfo(
            [
                $postReportAlias.'.id',
            ],
            [
                'id' => $this->id,
            ]
        );

        if(!$postReportData || !$postData){
            return false;
        }

        $data = [
            "id"            =>  $this->id,
            'is_deleted'    =>  DEL_FLG_ON,
            'deleted_at'    =>  date('Y-m-d H:i:s'),
            'deleted_by'    =>  $this->userId,
        ];
        $this->id = $this->service->saveData($data);
        return true;
    }

    public function deleteAllPostReport(){
        $postReportAlias = $this->service->getModel()->alias;

        $postReportData = $this->service->getData(
            [
                $postReportAlias.'.id',
            ],
            [
                $postReportAlias.'.post_id' => $this->postId,
            ]
        );
        $id = [];
        foreach($postReportData as $key => $value){
            array_push($id, $value->id);
        }

        $data = [
            "id"            =>  $id,
            'is_deleted'    =>  DEL_FLG_ON,
            'deleted_at'    =>  date('Y-m-d H:i:s'),
            'deleted_by'    =>  $this->userId,
        ];
        $this->id = $this->service->saveData($data);
        return true;
    }
}