<?php 
namespace App\Services\Post;
use App\Models\PostModel;
use App\Models\AccountModel;
use App\Models\ProvinceModel;
use App\Models\LocationModel;
use App\Models\CategoryModel;
use App\Services\Like\LikePostService;
use App\Services\Notification\NotificationService;

class PostService 
{
    public function __construct(){
        $this->model = new PostModel();
        $this->db = \Config\Database::connect();
        $this->accountModel = new AccountModel();
        $this->locationModel = new LocationModel();
        $this->provinceModel = new ProvinceModel();
        $this->categoryModel = new CategoryModel();
    }

    public function getModel(){
        return $this->model;
    }

    public function createQuery(){
        $postTable = $this->model->table;
        $postAlias = $this->model->alias;

        $accountTable = $this->accountModel->table;
        $accountAlias = $this->accountModel->alias;

        $locationTable = $this->locationModel->table;
        $locationAlias = $this->locationModel->alias;

        $provinceTable = $this->provinceModel->table;
        $provinceAlias = $this->provinceModel->alias;

        $categoryTable = $this->categoryModel->table;
        $categoryAlias = $this->categoryModel->alias;

        $likePostService = new LikePostService();
        $likePostTable = $likePostService->getModel()->table;
        $likePostAlias = $likePostService->getModel()->alias;

        $builder = $this->db->table($postTable.' AS '.$postAlias)
                            ->join($accountTable.' AS '.$accountAlias, $postAlias.'.created_by = '.$accountAlias.'.id')
                            // ->join($likePostTable. ' AS '.$likePostAlias, $postAlias.'.id = '.$likePostAlias.'.post_id', 'left')
                            ->join($locationTable. ' AS '.$locationAlias, $postAlias.'.location = '.$locationAlias.'.id')
                            ->join($categoryTable. ' AS '.$categoryAlias, $postAlias.'.category = '.$categoryAlias.'.id')
                            ->join($provinceTable. ' AS '.$provinceAlias, $locationAlias.'.province = '.$provinceAlias.'.id')
                            ->where($this->model->alias.'.is_deleted', DEL_FLG_OFF);
        return $builder;
    }

    public function getData($selects = [], $where = [], $page = 0, $limit = 0){
        if($page != 0){
            $page = $page * $limit;
        }

        $postAlias = $this->model->alias;
        $postTable = $this->model->table;
        $query = $this->createQuery()
                                    ->select($selects)
                                    ->where($where)
                                    ->limit($limit, $page)
                                    ->orderBy($postAlias.'.id', 'DESC');
        $get = $query->get();
        return $get->getResultObject();
    }

	// get row information
	public function getInfo($selects = [], $where = []){
		$model_alias = $this->model->alias;
		$query = $this->createQuery()
						->select($selects);
		if(is_array($where) && !empty($where)){
			$query = $this->buildCondition($query, $where);
		}
		return $query->get()->getRow();
	}

    public function saveData($data){
		$id = $data['id'] ?? false;
		if($id){
			unset($data['id']);
			$this->model->update($id, $data);
		}else{
			$id = $this->model->insert($data);
		}
		return $id;
	}
    
    // cout result
    public function countResult($where=[]){
        $query = $this->createQuery();
        if(!empty($where)){
		    if(is_array($where)){
		    	$query = $this->buildCondition($query, $where);
		    }else{
                $query->where($where);
            }
        }
        return $query->countAllResults();
    }

    //select avg column 
    public function selectAvg($col, $where=[]){
        $query = $this->createQuery();
		if(is_array($where) && !empty($where)){
			$query = $this->buildCondition($query, $where);
		}
        return $query->selectAvg($col)->get()->getRow();
    }

    // set condition
	protected function buildCondition($query, $condition){
		foreach($condition as $field=>$val){
			switch ($field) {
				default:
					if($val !== ''){
						$query->where($this->model->alias.".".$field, $val);
					}
					break;
			}
		}
		return $query;
	}
}