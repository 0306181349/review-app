<?php 
namespace App\Services\Post;
use App\Services\Post\PostService;
use App\Services\Post\ViewedPostService;
use App\Services\Account\AccountService;
use App\Services\Location\LocationService;
use App\Services\Follow\FollowService;
use App\Services\Like\LikePostService;
use App\Services\Gallery\GalleryPostService;
use App\Services\Gallery\GalleryService;
use App\Services\Comment\CommentService;
use App\Services\Category\CategoryService;
use App\Services\Hashtag\HashtagService;
use App\Services\Hashtag\PostHashtagService;
use App\Models\AccountModel;
use App\Models\PostModel;
use App\Models\LocationModel;
use App\Models\ProvinceModel;
use App\Models\CategoryModel;
use App\Services\Notification\UpdateNotificationService;
use App\Services\Dashboard\DashboardService;
use App\Services\Post\PostViolationService;
use App\Services\Report\ReportTitle\ReportTitleService;
use App\Services\Report\PostReport\PostReportService;

class UpdatePostService
{
    protected $id = 0;
    protected $userId = 0;
	protected $request;

    public function __construct(){
        $this->service = new PostService();
        helper('convert_num');
        helper('unset_data');
    }

    //set modify account id
    public function setId($id){
		return $this->id = $id;
	}

    //set user id who is modifying the account info 
    public function setUser($userId){
        return $this->userId = $userId;
    }
    
	public function setRequest($request){
		return $this->request = $request;
	}

    public function setAction($action){
        return $this->{$action}();
    }

    public function show(){
        $postAlias = $this->service->getModel()->alias;
        $locationServcie = new LocationService();
        $postHashtagService = new PostHashtagService();
        $hashtagService = new HashtagService();
        $categoryService = new CategoryService();
        $locationAlias = $locationServcie->getmodel()->alias;
        $categoryAlias = $categoryService->getModel()->alias;
        $postHashtagAlias = $postHashtagService->getModel()->alias;
        $hashtagAlias = $hashtagService->getModel()->alias;
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;

        $selects= [
            $postAlias.'.id as post_id',
            $postAlias.'.title as title',
            $postAlias.'.content as content',
            $postAlias.'.hashtag as hashtag',
            $postAlias.'.rating as rating',
            $postAlias.'.post_video',
            $postAlias.'.view_quantity as view_quantity',
            $postAlias.'.like_quantity as like_quantity',
            $postAlias.'.post_image as post_image',
            $postAlias.'.location as location_id',
            $postAlias.'.category AS category_id',
            $accountAlias.'.user_name as created_by',

            $locationAlias.'.location_name as location_name',
            
            $categoryAlias.'.category_name',
        ];

        $data = $this->service->getInfo($selects, ['id' => $this->id]);
        if(!$data){
            return false;
        }

        $postHashtagData = $postHashtagService->getData(
            [
                $postHashtagAlias.'.hashtag_id',
            ],
            [
                $postHashtagAlias.'.post_id' => $this->id,
            ]
        );
        $data->hashtag = [];
        $data->show_hashtag = '';
        
        foreach($postHashtagData as $key => $value){
            $hashtagData = $hashtagService->getInfo(
                [
                    $hashtagAlias.'.hashtag AS label',
                    $hashtagAlias.'.id AS value',
                ],
                [
                    'id' => $value->hashtag_id,
                ]
            );
            array_push($data->hashtag, $hashtagData);
            $data->show_hashtag = $data->show_hashtag.$hashtagData->label.' ';
        }
        $data->post_image = json_decode($data->post_image, true);

        return $data;
    }

    public function getPostTableData(){
        $postModel = new PostModel();
        $locationModel = new LocationModel();
        $select = [
            $postModel->alias.'.id AS id',
            $postModel->alias.'.title AS title',
            $postModel->alias.'.view_quantity AS view_quantity',
            $postModel->alias.'.like_quantity AS like_quantity',
            $locationModel->alias.'.location_name AS location',
        ];
        return $this->service->getData($select, [$postModel->alias.'.is_locked' => LOCK_FLG_OFF]);
    }

    public function postByUser($requestUser = 0 /* used when call function from another service */){
        $page = 0;
        $limit = 10;

        if($requestUser){
            $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
            $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;
        }
        $this->userId ? : $this->userId = is_numeric($this->request->getGet('user')) ? $this->request->getGet('user') : 0;

        $postAlias = $this->service->getModel()->alias;

        $selects = [
            $postAlias.'.id AS post_id',
            $postAlias.'.title AS post_title',
            $postAlias.'.post_image'
        ];

        if($requestUser == $this->userId){
            $where = [
                $postAlias.'.created_by' => $this->userId,
            ];
        }else{
            $where = [
                $postAlias.'.created_by' => $this->userId,
                $postAlias.'.is_locked'  => LOCK_FLG_OFF,
            ];
        }

        $data = $this->service->getData($selects, $where, $page, $limit);
        $countResult = 0;

        foreach($data as $key => $value){
            $countResult++;
            $value->post_image = [ json_decode($value->post_image)[0] ];
            $value = convertNumData($value);
        }

        $pagination = [
            'page'  => $page,
            'limit' => $limit,
            'total' => $countResult,
        ];

        $pagination = convertNumData($pagination);
        return ['data' => $data, 'pagination' => $pagination];
    }

    public function standardizeDataForPostAll($data){
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;
        $likePostService = new LikePostService();
        $followService = new FollowService();
        $countResult = 0;
        foreach($data as $key => $value){
            $countResult++;
            $value = convertNumData($value);
            $value->user = [
                'user_id'   =>  $value->user_id,
                'user_name' =>  $value->user_name,
                'avatar'    =>  $value->avatar,
            ];
            $value->like = $likePostService->countResult('post_id = '.$value->post_id);

            $value->isLiked = false;
            $value->user['isFollowed'] = false;

            if($likePostService->getModel()->where(['post_id' => $value->post_id, 'account_id' => $this->userId])->first()){
                $value->isLiked = true;
            }
            if($followService->getModel()->where(['account_id_1' => $this->userId, 'account_id_2' => $value->user_id])->first()){
                $value->user['isFollowed'] = true;
            }

            if($value->post_image != null){
                $value->post_image = json_decode($value->post_image, true);
                $value->post_image = [$value->post_image[0]];
            }else{
                $value->post_image = [];
            }
            
            $unsetFileds = [
                'user_id',
                'user_name',
                'avatar',
            ];

            $value = unsetObject($unsetFileds, $value);
        }

        $pagination = [
            'page'  => $page,
            'limit' => $limit,
            'total' => $countResult,
        ];
        $pagination = convertNumData($pagination);
        return ['data' => $data, 'pagination' => $pagination];
    }

    public function getPostDataWhenLoggedIn(){
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;
        $category = $this->request->getGet('category');
        $province = $this->request->getGet('province');
        if($page != 0){
            $page = $page * $limit;
        }

        $accountModel = new AccountModel();
        $postModel = new PostModel();
        $locationModel = new LocationModel();
        $provinceModel = new ProvinceModel();
        $categoryModel = new CategoryModel();
        $accountAlias = $accountModel->alias;
        $likePostService = new LikePostService();
        $likePostAlias = $likePostService->getModel()->alias;
        $likePostTable = $likePostService->getModel()->table;
        $followService = new FollowService();
        $followAlias = $followService->getModel()->alias;
        $followTable = $followService->getModel()->table;
        $dashboardService = new DashboardService();
        $date = $dashboardService->getFirstDayAndLastDayOfLastMonth();

        $select = [
            $postModel->alias.'.id AS post_id',
            $postModel->alias.'.post_image AS post_image',
            $postModel->alias.'.title AS post_title',
            $postModel->alias.'.category AS category',
            $postModel->alias.'.rating AS rating',

            // $provinceModel->alias.'.id AS province_id',
            // $provinceModel->alias.'.province_name AS province_name',

            $categoryModel->alias.'.category_name AS category_name',
            $categoryModel->alias.'.id AS category_id',
            
            $accountModel->alias.'.id AS user_id',
            $accountModel->alias.'.user_name',
            $accountModel->alias.'.avatar',
        ];
        $where = [
            $postModel->alias.'.is_locked' => LOCK_FLG_OFF,
            $followAlias.'.account_id_1' => $this->userId,
            $postModel->alias.'.created_by !=' => $this->userId,
        ];


        if($province){
            $where[$locationModel->alias.'.province'] = $province;
        }

        if($category){
            $where[$postModel->alias.'.category'] = $category;
        }
        
        $query = $this->service->createQuery()
                                ->join($followTable.' AS '.$followAlias, $accountAlias.'.id = '.$followAlias.'.account_id_2')
                                ->select($select)
                                ->where($where)
                                ->where($postModel->alias.".created_at BETWEEN '".$date[0]."' AND '".$date[1]."'")
                                ->groupBy($postModel->alias.'.id')
                                ->orderBy($postModel->alias.'.id', 'DESC');

        $data = $query
                ->limit($limit, $page)
                ->get()->getResultObject();
        $numOfRecord = count($data);
        if($numOfRecord >= $limit){
            return $this->standardizeDataForPostAll($data);
        }
        if($numOfRecord == 0){
            var_dump("=0");
            $numOfFollowingPostPage = count($data)/$limit;
            $pageToGet = ($page - $numOfFollowingPostPage - 1) * $limit; //$page begin at 0 => need to -1
            var_dump($numOfFollowingPostPage);
            var_dump($pageToGet);
            
            $where = [
                $postModel->alias.'.is_locked' => LOCK_FLG_OFF,
                $postModel->alias.'.created_by !=' => $this->userId,
            ];
            $query = $this->service->createQuery()
                                    ->join($followTable.' AS '.$followAlias, $accountAlias.'.id = '.$followAlias.'.account_id_2', 'left')
                                    ->select($select)
                                    ->where($where)
                                    ->groupBy($postModel->alias.'.id')
                                    ->orderBy($postModel->alias.'.id', 'DESC');
            $data = $query
                    ->limit($limit, $pageToGet)
                    ->get()->getResultObject();
            return $this->standardizeDataForPostAll($data);
        }
        if($numOfRecord < $limit){
            var_dump("<limit");
            
            $where = [
                $postModel->alias.'.is_locked' => LOCK_FLG_OFF,
                $postModel->alias.'.created_by !=' => $this->userId,
            ];
            $query = $this->service->createQuery()
                                    ->select($select)
                                    ->where($where)
                                    ->where("NOT EXIST(".$query->getLastQuery().")")
                                    ->groupBy($postModel->alias.'.id')
                                    ->orderBy($postModel->alias.'.id', 'DESC');
            $remainData = $query
                ->limit($limit, 0)
                ->get()->getResultObject();
                var_dump($remainData);
                var_dump($data);
            $data = array_merge($data, $remainData);
            return $this->standardizeDataForPostAll($data);
        }
    }

    public function getPostDataByPage(){
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;
        $category = $this->request->getGet('category');
        $province = $this->request->getGet('province');

        $accountModel = new AccountModel();
        $postModel = new PostModel();
        $locationModel = new LocationModel();
        $provinceModel = new ProvinceModel();
        $categoryModel = new CategoryModel();
        $likePostService = new LikePostService();

        $select = [
            $postModel->alias.'.id AS post_id',
            $postModel->alias.'.post_image AS post_image',
            $postModel->alias.'.title AS post_title',
            $postModel->alias.'.category AS category',
            $postModel->alias.'.rating AS rating',

            // $provinceModel->alias.'.id AS province_id',
            // $provinceModel->alias.'.province_name AS province_name',

            $categoryModel->alias.'.category_name AS category_name',
            $categoryModel->alias.'.id AS category_id',
            
            $accountModel->alias.'.id AS user_id',
            $accountModel->alias.'.user_name',
            $accountModel->alias.'.avatar',
        ];
        $where = [
            $postModel->alias.'.is_locked' => LOCK_FLG_OFF
        ];

        if($this->userId){
            $where[$postModel->alias.'.created_by !=' ] = $this->userId;
        }

        if($province){
            $where[$locationModel->alias.'.province'] = $province;
        }

        if($category){
            $where[$postModel->alias.'.category'] = $category;
        }
        
        $data = $this->service->getData($select, $where, $page, $limit);
        $countResult = 0;

        foreach($data as $key => $value){
            $countResult++;
            $value = convertNumData($value);
            $value->user = [
                'user_id'   =>  $value->user_id,
                'user_name' =>  $value->user_name,
                'avatar'    =>  $value->avatar,
            ];
            $value->like = $likePostService->countResult('post_id = '.$value->post_id);

            if($this->userId){
                $likePostService = new LikePostService();
                $followService = new FollowService();
                $value->isLiked = false;
                $value->user['isFollowed'] = false;
                if($likePostService->getModel()->where(['post_id' => $value->post_id, 'account_id' => $this->userId])->first()){
                    $value->isLiked = true;
                }
                if($followService->getModel()->where(['account_id_1' => $this->userId, 'account_id_2' => $value->user_id])->first()){
                    $value->user['isFollowed'] = true;
                }
            }
            if($value->post_image != null){
                $value->post_image = json_decode($value->post_image, true);
                $value->post_image = [$value->post_image[0]];
            }else{
                $value->post_image = [];
            }
            unset($value->user_id);
            unset($value->user_name);
            unset($value->avatar);
        }

        $pagination = [
            'page'  => $page,
            'limit' => $limit,
            'total' => $countResult,
        ];
        $pagination = convertNumData($pagination);
        return ['data' => $data, 'pagination' =>$pagination];
    }

    public function getVideoByPage(){
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;

        $likePostService = new LikePostService();
        $accountService = new AccountService();
        $postAlias = $this->service->getModel()->alias; 
        $accountAlias = $accountService->getModel()->alias;
        $commentService = new CommentService();
        $postHashtagService = new PostHashtagService();
        $postHashtagAlias = $postHashtagService->getModel()->alias;
        $hashtagService = new HashtagService();
        $hashtagAlias = $hashtagService->getModel()->alias;

        $selectsPost = [
            $postAlias.'.id AS post_id',
            $postAlias.'.title AS post_title',
            $postAlias.'.post_video',
            $postAlias.'.location AS location_id',
            $postAlias.'.content AS post_content',
            $postAlias.'.created_at AS post_date',
            $accountAlias.'.id AS user_id',
            $accountAlias.'.user_name',
            $accountAlias.'.avatar',
        ];

        $wherePost = [
            $postAlias.'.post_video !=' => 'NULL',
            $postAlias.'.is_locked' => LOCK_FLG_OFF,
        ];

        if($this->userId){
            $where[$postAlias.'.created_by !=' ] = $this->userId;
        }

        $data = $this->service->getData($selectsPost, $wherePost, $page, $limit);
        $countResult = 0;

        foreach($data as $key => $value){
            $countResult++;
            $value = convertNumData($value);
            $value->user = [
                'user_id'   =>  $value->user_id,
                'user_name' =>  $value->user_name,
                'avatar'    =>  $value->avatar,
            ];
            $value->comment_quantity = $commentService->countResult(['post_id = ' => $value->post_id, 'comment_id' => NULL]);
            
            $value->hashtag = [];
            $postHashtagData = $postHashtagService->getData(
                [
                    $postHashtagAlias.'.hashtag_id',
                ],
                [
                    $postHashtagAlias.'.post_id' => $value->post_id,
                ]
            );
            foreach($postHashtagData as $key => $hashtag){
                array_push(
                    $value->hashtag, 
                    convertNumData(
                        $hashtagService->getInfo(
                            [
                                $hashtagAlias.'.id AS hashtag_id',
                                $hashtagAlias.'.hashtag',
                            ],
                            [
                                'id' => $hashtag->hashtag_id,
                            ]
                        )
                    )
                );
            }

            if($this->userId){
                $likePostService = new LikePostService();
                $galleryPostService = new GalleryPostService();
                $galleryService = new GalleryService();
                $galleryAlias = $galleryService->getModel()->alias;
                $galleryPostAlias = $galleryPostService->getModel()->alias;    
                $followService = new FollowService();

                $value->isLiked = false;
                $value->isSaved = false;
                $value->user["isFollowed"] = false;

                if($likePostService->getModel()->where(['post_id' => $value->post_id, 'account_id' => $this->userId])->first()){
                    $value->isLiked = true;
                }

                if($galleryPostService->getData(
                    [
                        $galleryPostAlias.'.id',
                    ],
                    [
                        $galleryPostAlias.'.post_id' => $value->post_id, 
                        $galleryAlias.'.created_by' => $this->userId
                    ]
                )){
                    $value->isSaved = true;
                }

                if($followService->getModel()->where(['account_id_1' => $this->userId, 'account_id_2' => $value->user_id])->first()){
                    $value->user['isFollowed'] = false;
                }
            }
            $value->like = $likePostService->countResult('post_id = '.$value->post_id);
            unset($value->user_id);
            unset($value->user_name);
            unset($value->avatar);
        }
        
        $pagination = [
            'page'  => $page,
            'limit' => $limit,
            'total' => $countResult,
        ];
        $pagination = convertNumData($pagination);

        return ['data' => $data, 'pagination' =>$pagination];
    }

    public function getPostDetail($id = 0){
        if($id){
            $post_id = $id;
        }else{
            $post_id = $this->request->getGet('id');
        }

        $accountService = new AccountService();
        $accountModel = $accountService->getModel();
        $postModel = new PostModel();
        $provinceModel = new ProvinceModel();
        $locationService = new LocationService();
        $commentService = new CommentService();
        $locationModel = $locationService->getModel();
        $categoryModel = new CategoryModel();
        $likePostService = new LikePostService();
        $postHashtagService = new PostHashtagService();
        $postHashtagAlias = $postHashtagService->getModel()->alias;
        $hashtagService = new HashtagService();
        $hashtagAlias = $hashtagService->getModel()->alias;
        $lockedPostViolationService = new PostViolationService();
        $lockedPostViolationAlias = $lockedPostViolationService->getModel()->alias;
        $reportTitleService = new ReportTitleService();
        $reportTitleAlias = $reportTitleService->getModel()->alias;

        $selectPost = [
            $postModel->alias.'.id AS post_id',
            $postModel->alias.'.location',
            $postModel->alias.'.post_image AS post_image',
            $postModel->alias.'.rating AS post_rating',
            $postModel->alias.'.title AS post_title',
            $postModel->alias.'.created_at AS post_date',
            $postModel->alias.'.view_quantity AS view',
            $postModel->alias.'.content AS post_content',
            $postModel->alias.'.is_locked',

            $accountModel->alias.'.id AS user_id',

            // $provinceModel->alias.'.id AS province_id',
            // $provinceModel->alias.'.province_name AS province_name',
            
            $categoryModel->alias.'.id AS category_id',
            $categoryModel->alias.'.category_name AS category_name',
        ];

        $data = $this->service->getInfo($selectPost, ['id' => $post_id]);
        $data->like = $likePostService->countResult('post_id = '.$data->post_id);

        if($data == null){
            return false;
        }
        $data = convertNumData($data);

        if($data->post_image == null){
            $data->post_image = [];
        }else{
            $image = json_decode($data->post_image, true);
            $data->post_image = $image;
        }

        $selectLocation = [
            $locationModel->alias.'.id AS location_id',
            $locationModel->alias.'.location_name',
            $locationModel->alias.'.address',
        ];
        $whereLocation = [
            'id' => $data->location,
        ];

        if($data->is_locked != LOCK_FLG_OFF){
            $lockedReasonData = $lockedPostViolationService->getData(
                [
                    $reportTitleAlias.'.title',
                ],
                [
                    $lockedPostViolationAlias.'.post_id' => $data->post_id,
                ]
            );
            $data->locked_reason = [];
            foreach($lockedReasonData as $key => $value){
                array_push($data->locked_reason, $value->title);
            }
        }

        $locationData = $locationService->getInfo($selectLocation, $whereLocation);
        $locationData->location_rating = $this->service->selectAvg($this->service->getModel()->alias.'.rating', ['location'=>$locationData->location_id])->rating;
        $locationData = convertNumData($locationData);
        $locationData->location_rating = round($locationData->location_rating, 1);
        $postData = $this->service->getInfo([$this->service->getModel()->alias.".post_image"], ["location" => $locationData->location_id]);

        if($postData){
            $locationData->location_image = [json_decode($postData->post_image, true)[0]];
        }else{
            $locationData->location_image = [];
        }

        $locationData->post_by_location = $this->service->countResult(["location" => $locationData->location_id]);
        $data->location = $locationData;

        if(!$id){
            $selectUser = [
                $accountModel->alias.'.id AS user_id',
                $accountModel->alias.'.user_name',
                $accountModel->alias.'.rank',
                $accountModel->alias.'.avatar',
            ];
            $whereUser = [
                'id' => $data->user_id,
            ];
            $userData = $accountService->getInfo($selectUser, $whereUser);
            if($userData->avatar == null){
                $userData->avatar = "";
            }
            $userData = convertNumData($userData);

            $data->user = $userData;
        }

        $data->comment_quantity = $commentService->countResult('post_id = '.$post_id.' AND comment_id IS NULL');
        $data->hashtag = [];
        $data->hashtag = $postHashtagService->getData(
            [
                $postHashtagAlias.'.hashtag_id',
                $postHashtagAlias.'.hashtag_alias AS hashtag',
            ],
            [
                $postHashtagAlias.'.post_id' => $data->post_id,
            ]
        );
        foreach($data->hashtag as $key => $value){
            $value = convertNumData($value);
        }


        unset($data->user_id);

        if($this->userId){
            $followService = new FollowService();
            $likePostService = new LikePostService();
            $galleryPostService = new GalleryPostService();
            $galleryService = new GalleryService();
            $galleryAlias = $galleryService->getModel()->alias;
            $galleryPostAlias = $galleryPostService->getModel()->alias;

            if(!$id){
                $data->user->isFollowed = false;
                if($followService->getModel()->where(['account_id_1' => $this->userId, 'account_id_2' => $userData->user_id])->first()){
                    $data->user->isFollowed = true;
                }
            }
            $data->isLiked = false;
            $data->isSaved = false;
            if($likePostService->getModel()->where(['account_id' => $this->userId, 'post_id' => $data->post_id])->first()){
                $data->isLiked = true;
            }
            
            if($galleryPostService->getData(
                [
                    $galleryPostAlias.'.id',
                ],
                [
                    $galleryPostAlias.'.post_id' => $data->post_id, 
                    $galleryAlias.'.created_by' => $this->userId
                ]
            )){
                $data->isSaved = true;
            }
        }

        return $data;
    }

    public function createPost(){
        $data = $this->request->getPost();
        $files = $this->request->getFiles();

        if (empty($data)) {
			//convert request body to associative array
			$data = json_decode($this->request->getBody(), true);
		}

        $hashtagService = new HashtagService();
        $hashtagAlias = $hashtagService->getModel()->alias;
        $postHashtagService = new PostHashtagService();
        $postHashtagAlias = $postHashtagService->getModel()->alias;

        $data['created_by'] = $this->userId;
        $data['created_at'] = date('Y-m-d H:i:s');
        
        $this->id = $this->service->saveData($data);

        if(isset($files) && isset($files['image'])){
            $data['post_image'] = [];
            $data['id'] = $this->id;
            foreach($files['image'] as $key => $value){
                if($value->isValid()){
                    $imgName = $value->getRandomName();
                    $path = 'upload/post/'.$this->id;
                    $value->move($path, $imgName);
                    array_push($data['post_image'], $path.'/'.$imgName);
                }
            }
            $postImage = $data['post_image'];
            $data['post_image'] = json_encode($data['post_image']);
            $this->id = $this->service->saveData($data);

        }
        if(isset($files) && isset($files['video']) && $files['video']->isValid()){
            $data['id'] = $this->id;
            $ext = $files['video']->getClientExtension();
            $vidName = 'vid0.'.$ext;
            $path = 'upload/post/'.$this->id;
            $files['video']->move($path, $vidName);
            $data['post_video'] = $path.'/'.$vidName;
            $this->id = $this->service->saveData($data);
        }

        if(isset($data['hashtag'])){
            $hashtag = explode(" ", $data['hashtag']);
            foreach($hashtag as $key => $value){
                // find the hashtag that is the same as hashtag user insert
                $hashtagData = $hashtagService->getModel()->where(['hashtag' => strtolower($value)])->first();
                if($hashtagData){// insert data into post_hashtag table
                    $postHashtagService->saveData([
                        'post_id' => $this->id,
                        'hashtag_id' => $hashtagData->id,
                        'hashtag_alias' => $value,
                    ]);
                }else{// insert data in both hashtag table and post_hashtag table
                    $hashtag_id = $hashtagService->saveData([
                        'hashtag' => strtolower($value),
                        'created_by' => $this->userId
                    ]);
                    $postHashtagService->saveData([
                        'post_id'    => $this->id,
                        'hashtag_id' => $hashtag_id,
                        'hashtag_alias' => $value,
                    ]);
                }
            }
        }

        // create and push notification to the followers
        $updateNotificationService = new UpdateNotificationService();
        $updateNotificationService->setType(NEW_POST);
        $updateNotificationService->setUser($this->userId);
        $updateNotificationService->setPost($this->id);
        $updateNotificationService->createNotification();

        return $this->getPostDetail($this->id);
    }
    
    public function lock(){
        $data = $this->request->getPost();
        if (empty($data)) {
			//convert request body to associative array
			$data = json_decode($this->request->getBody(), true);
		}

        $lockedPostViolationService = new PostViolationService();
        $reportTitleService = new ReportTitleService();
        $reportTitleAlias = $reportTitleService->getModel()->alias;
        $postAlias = $this->service->getModel()->alias; 
        $postReportService = new PostReportService();
        $postReportAlias = $postReportService->getModel()->alias;
        
        $postData = $this->service->getInfo(
            [
                $postAlias.'.id',
            ],
            [
                'id' => $this->id,
            ]
        );

        if(!$postData){
            return false;
        }
        $checkReportTitleFlag = false;
        foreach($data['report_title'] as $key => $value){
            if(!$reportTitleService->getInfo([$reportTitleAlias.'.id'], ['id' => $value])){
                continue;
            }else{
                $checkReportTitleFlag = true;
                $saveData = [
                    'post_id' => $this->id,
                    'report_title_id' => $value,
                ];
                $lockedPostViolationService->saveData($saveData);
            }
        }

        if($checkReportTitleFlag){
            $data = [
                "id"            =>  $this->id,
                'is_locked'     =>  LOCK_FLG_ON,
                'locked_at'     =>  date('Y-m-d H:i:s'),
                'locked_by'     =>  $this->userId,
            ];
            $postReportData = $postReportService->getData(
                [
                    $postReportAlias.'.id',
                ],
                [
                    $postReportAlias.'.post_id' => $this->id,
                ]
            );
            foreach($postReportData as $key => $value){
                $savePostReportData = [
                    'id'         => $value->id,
                    'is_deleted' => DEL_FLG_ON,
                    'deleted_at' => date('Y-m-d H:i:s'),
                    'deleted_by' => $this->userId,
                ];
                $postReportService->saveData($savePostReportData);
            }

            
            // create and push notification to the followers
            $updateNotificationService = new UpdateNotificationService();
            $updateNotificationService->setType(LOCK_POST);
            $updateNotificationService->setUser($this->userId);
            $updateNotificationService->setPost($this->id);
            $updateNotificationService->createNotification();

            return $this->service->saveData($data);
        }
        return;
    }

    public function unlockPost(){
        $postAlias = $this->service->getModel()->alias;
        $postViolationService = new PostViolationService();
        $postviolationModel = $postViolationService->getModel();

        $postData = $this->service->getInfo(
            [
                $postAlias.'.id',
            ],
            [
                'id' => $this->id,
            ]
        );

        if(!$postData){
            return false;
        }

        $data = [
            'id' => $this->id,
            'is_locked' => LOCK_FLG_OFF,
            'locked_by' => NULL,
            'locked_at' => NULL,
        ];

        $postviolationModel->where('post_id', $this->id)->delete();

        // create and push notification to the followers
        $updateNotificationService = new UpdateNotificationService();
        $updateNotificationService->setType(UNLOCK_POST);
        $updateNotificationService->setUser($this->userId);
        $updateNotificationService->setPost($this->id);
        $updateNotificationService->createNotification();

        return $this->service->saveData($data);
    }

    public function unlockPostReject(){
        $postAlias = $this->service->getModel()->alias;
        $postData = $this->service->getInfo(
            [
                $postAlias.'.id',
            ],
            [
                'id' => $this->id,
            ]
        );
        if(!$postData){
            return false;
        }
        $data = [
            'id' => $this->id,
            'is_locked' => LOCK_FLG_ON,
            'locked_by' => $this->userId,
            'locked_at' => date('Y-m-d H:i:s'),
        ];
        
        // create and push notification to the followers
        $updateNotificationService = new UpdateNotificationService();
        $updateNotificationService->setType(UNLOCK_POST_REJECT);
        $updateNotificationService->setUser($this->userId);
        $updateNotificationService->setPost($this->id);
        $updateNotificationService->createNotification();

        return $this->service->saveData($data);
    }

    public function showAllLockedPost(){
        $postAlias = $this->service->getModel()->alias;
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;
        $data = $this->service->createQuery()->select(
                                                [
                                                    $postAlias.'.id AS id',
                                                    $postAlias.'.title AS title',
                                                    $postAlias.'.locked_at',
                                                    $postAlias.'.locked_by AS admin_id',
                                                ]
                                            )
                                          ->where(
                                                [
                                                    $postAlias.'.is_locked' => LOCK_FLG_ON
                                                ]
                                          )
                                          ->orderBy($postAlias.'.locked_at', 'DESC')
                                          ->get()
                                          ->getResultObject();

        foreach($data as $key => $value){
            $value->locked_by = $accountService->getInfo(
                [
                    $accountAlias.'.user_name',
                ],
                [
                    'id' => $value->admin_id,
                ]
            )->user_name;
        }

        return $data;
    }

    public function showLockedPost(){
        $postAlias = $this->service->getModel()->alias;
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;
        $locationService = new LocationService();
        $locationAlias = $locationService->getModel()->alias;
        $categoryService = new CategoryService();
        $categoryAlias = $categoryService->getModel()->alias;
        $postViolationService = new PostViolationService();
        $postViolationAlias = $postViolationService->getModel()->alias;
        $reportTitleService = new ReportTitleService();
        $reportTitleAlias = $reportTitleService->getModel()->alias;
        $postHashtagService = new PostHashtagService();
        $postHashtagAlias = $postHashtagService->getModel()->alias;
        $hashtagService = new HashtagService();
        $hashtagAlias = $hashtagService->getModel()->alias;

        $data = $this->service->createQuery()->select(
                                                [
                                                    $postAlias.'.id as post_id',
                                                    $postAlias.'.title as title',
                                                    $postAlias.'.content as content',
                                                    $postAlias.'.hashtag as hashtag',
                                                    $postAlias.'.rating as rating',
                                                    $postAlias.'.view_quantity as view_quantity',
                                                    $postAlias.'.like_quantity as like_quantity',
                                                    $postAlias.'.post_image as post_image',
                                                    $postAlias.'.location as location_id',
                                                    $postAlias.'.category AS category_id',
                                                    $postAlias.'.is_locked',
                                                    $postAlias.'.locked_by AS admin_id',
                                        
                                                    $locationAlias.'.location_name as location_name',
                                                    
                                                    $categoryAlias.'.category_name',
                                                ]
                                            )
                                          ->where(
                                                [
                                                    $postAlias.'.is_locked !=' => LOCK_FLG_OFF,
                                                    $postAlias.'.id'        => $this->id,
                                                ]
                                          )
                                          ->orderBy($postAlias.'.locked_at', 'DESC')
                                          ->get()
                                          ->getRow();
    
        if(!$data){
            return false;
        }
                                
        $postHashtagData = $postHashtagService->getData(
            [
                $postHashtagAlias.'.hashtag_id',
            ],
            [
                $postHashtagAlias.'.post_id' => $this->id,
            ]
        );
        $data->hashtag = [];
        $data->show_hashtag = '';
        
        foreach($postHashtagData as $key => $value){
            $hashtagData = $hashtagService->getInfo(
                [
                    $hashtagAlias.'.hashtag AS label',
                    $hashtagAlias.'.id AS value',
                ],
                [
                    'id' => $value->hashtag_id,
                ]
            );
            array_push($data->hashtag, $hashtagData);
            $data->show_hashtag = $data->show_hashtag.$hashtagData->label.' ';
        }
        $data->post_image = json_decode($data->post_image, true);

        $data->locked_by = $accountService->getInfo(
            [
                $accountAlias.'.user_name',
            ],
            [
                'id' => $data->admin_id,
            ]
        )->user_name;
        
        $postViolationData = $postViolationService->getData(
            [
                $postViolationAlias.'.report_title_id',
                $reportTitleAlias.'.title',
            ],
            [
                $postViolationAlias.'.post_id' => $this->id,
            ]
        );
        $data->post_violation = [];
        foreach($postViolationData as $key => $value){
            array_push($data->post_violation, $value->title);
        }
        if($data->is_locked == UNLOCK_REQUESTED){
            $data->unlock_requested = true;
        }else{
            $data->unlock_requested = false;
        }
        return $data;
    }

    public function getAllPostUnlockRequest(){
        $postAlias = $this->service->getModel()->alias;
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;
        $select = [
        ];
        $data = $this->service->createQuery()->select(
                                                [
                                                    $postAlias.'.id AS id',
                                                    $postAlias.'.title AS title',
                                                    $postAlias.'.locked_at',
                                                    $postAlias.'.locked_by AS admin_id',
                                                ]
                                            )
                                          ->where(
                                                [
                                                    $postAlias.'.is_locked' => UNLOCK_REQUESTED
                                                ]
                                          )
                                          ->orderBy($postAlias.'.locked_at', 'DESC')
                                          ->get()
                                          ->getResultObject();

        foreach($data as $key => $value){
            $value->locked_by = $accountService->getInfo(
                [
                    $accountAlias.'.user_name',
                ],
                [
                    'id' => $value->admin_id,
                ]
            )->user_name;
        }

        return $data;
    }

    public function editPost(){
        $this->id = is_numeric($this->request->getGet('id')) ? $this->request->getGet('id') : 0;
        $data = $this->request->getPost();
        if (empty($data)) {
			//convert request body to associative array
			$data = json_decode($this->request->getBody(), true);
		}
        $files = $this->request->getFiles();
        $postAlias = $this->service->getModel()->alias;
        $postHashtagService = new PostHashtagService();
        $postHashtagAlias = $postHashtagService->getModel()->alias;
        $hashtagService = new HashtagService();
        $postData = $this->service->getInfo(
            [
                $postAlias.'.*',
            ],
            [
                'id' => $this->id,
            ]
        );
        if(!$postData){
            return false;
        }

        if(isset($data['location'])){
            unset($data['location']);
        }

        if(isset($data['is_locked'])){
            unset($data['is_locked']);
        }
        // get all post image
        $postImages = json_decode($postData->post_image);
        $data['post_image'] = [];
        $data['id'] = $this->id;
        if($postImages){
            $remainImages = explode(' ', $data['uploaded_image']);
            foreach($remainImages as $key => $value){
                if($remainImages[$key] == ""){
                    unset($remainImages[$key]);
                }else{
                    $remainImages[$key] = trim($value, " ");
                }
            }
            // if user delete some image(s) => remove that image
            foreach($postImages as $key => $image){
                if(array_search($image, $remainImages) === false){
                    @unlink($image);
                }else{
                    array_push($data['post_image'], $image);
                }
            }
        }

        // if any new image uploaded by user => move that image(s) to folder and add the uri into db 
        if(isset($files) && isset($files['new_image'])){
            foreach($files['new_image'] as $key => $value){
                if($value->isValid()){
                    $imgName = $value->getRandomName();
                    $path = 'upload/post/'.$this->id;
                    $value->move($path, $imgName);
                    array_push($data['post_image'], $path.'/'.$imgName);
                }
            }
        }
        if(isset($data['post_video'])){
            unset($data['post_video']);
        }
        if(isset($files) && isset($files['video']) && $files['video']->isValid()){
            if($postData->post_video){
                @unlink($postData->post_video);
            }
            $ext = $files['video']->getClientExtension();
            $vidName = 'vid0.'.$ext;
            $path = 'upload/post/'.$this->id;
            $files['video']->move($path, $vidName);
            $data['post_video'] = $path.'/'.$vidName;
        }

        $postHashtagData = $postHashtagService->getData(
            [   
                $postHashtagAlias.'.id',
                $postHashtagAlias.'.hashtag_id',
            ],
            [
                $postHashtagAlias.'.post_id' => $this->id,
            ]
        );

        if(!isset($data['hashtag']) || $data['hashtag'] == ""){
            foreach($postHashtagData as $key => $value){
                $postHashtagService->getModel()->delete($value->id);
            }
        }else{
            $uploadedHashtags = explode(' ', $data['hashtag']);
            foreach($uploadedHashtags as $key => $value){
                if($uploadedHashtags[$key] == ""){
                    unset($uploadedHashtags[$key]);
                }else{
                    $uploadedHashtags[$key] = trim($value, " ");
                }
            }
            $insertHashtag = [];
            $numOfUploadedHashtag = count($uploadedHashtags);
            $numOfHashtag = count($postHashtagData);
    
            if($numOfUploadedHashtag == $numOfHashtag){
                foreach($postHashtagData as $key => $value){
                    $hashtagData = $hashtagService->getModel()->where(['hashtag' => strtolower($uploadedHashtags[$key])])->first();
                    if(!$hashtagData){
                        $hashtag_id = $hashtagService->saveData([
                            'hashtag'    => strtolower($uploadedHashtags[$key]),
                            'created_by' => $this->userId,
                        ]);
                    }else{
                        $hashtag_id = $hashtagData->id;
                    }
                    
                    $postHashtagService->saveData(
                        [
                            'id'         => $value->id,
                            'hashtag_id' => $hashtag_id,
                            'post_id'    => $this->id,
                            'hashtag_alias' => $uploadedHashtags[$key],
                        ]
                    );
                }
            }else{
                if($numOfUploadedHashtag > $numOfHashtag){
                    foreach($uploadedHashtags as $key => $value){
                        $hashtagData = $hashtagService->getModel()->where(['hashtag' => strtolower($uploadedHashtags[$key])])->first();
                        if(!$hashtagData){
                            $hashtag_id = $hashtagService->saveData([
                                'hashtag'    => strtolower($uploadedHashtags[$key]),
                                'created_by' => $this->userId
                            ]);
                        }else{
                            $hashtag_id = $hashtagData->id;
                        }
                        if($key < $numOfHashtag){
                            $postHashtagService->saveData(
                                [
                                    'id'         => $postHashtagData[$key]->id,
                                    'hashtag_id' => $hashtag_id,
                                    'post_id'    => $this->id,
                                    'hashtag_alias' => $uploadedHashtags[$key],
                                ]
                            );
                        }else{
                            $postHashtagService->saveData(
                                [
                                    'hashtag_id' => $hashtag_id,
                                    'post_id'    => $this->id,
                                    'hashtag_alias' => $uploadedHashtags[$key],
                                ]
                            );
                        }
                    }
                }else{
                    foreach($postHashtagData as $key => $value){
                        if($key < $numOfUploadedHashtag){
                            $hashtagData = $hashtagService->getModel()->where(['hashtag' => $uploadedHashtags[$key]])->first();
                            if(!$hashtagData){
                                $hashtag_id = $hashtagService->saveData([
                                    'hashtag'    => $uploadedHashtags[$key],
                                    'created_by' => $this->userId
                                ]);
                            }else{
                                $hashtag_id = $hashtagData->id;
                            }
                            $postHashtagService->saveData(
                                [
                                    'id'         => $value->id,
                                    'hashtag_id' => $hashtag_id,
                                    'post_id'    => $this->id,
                                    'hashtag_alias' => $uploadedHashtags[$key],
                                ]
                            );
                        }else{
                            $postHashtagService->getModel()->delete($value->id);
                        }
                    }
                }
            }
        }
        $data['id'] = $this->id;
        $data['post_image'] = json_encode($data['post_image']);
        $data['updated_by'] = $this->userId;
        $data['updated_at'] = date('Y-m-d H:i:s');
        if($postData->is_locked != LOCK_FLG_OFF){
            // unlock request
            $data['is_locked'] = UNLOCK_REQUESTED;
        }
        $this->id = $this->service->saveData($data);

        return $this->getPostDetail($this->id);
    }

    public function create(){
        $data = $this->request->getPost();
        $files = $this->request->getFiles();
        // var_dump($images);
        if (empty($data)) {
			//convert request body to associative array8
			$data = json_decode($this->request->getBody(), true);
		}
        // save data first
        $data['created_by'] = $this->userId;
        $data['created_at'] = date('Y-m-d H:i:s');
        // $inputData = [
        //     'created_by' => $this->userId,
        //     'created_at' => date('Y-m-d H:i:s'),
        //     'title'      => $data['title'],
        //     'title'      => $data['rating'],
        //     'title'      => $data['content'],
        //     'title'      => $data['category'],

        // ];
        $this->id = $this->service->saveData($data);

        if(isset($data['hashtags'])){
            $postHashtagService = new PostHashtagService();
            $postHashtagAlias = $postHashtagService->getModel()->alias;
            $hashtags = explode(',', $data['hashtags']);

            foreach($hashtags as $key => $hashtag){
                $hashtagData = [
                    'post_id'    => $this->id,
                    'hashtag_id' => $hashtag,
                ];
                $postHashtagService->saveData($hashtagData);
            }
        }

        // then check if uploaded files is exist or not if yes move image to upload folder and save the link into database
        if(isset($files) && isset($files['images'])){
            $data['post_image'] = [];
            $data['id'] = $this->id;
            foreach($files['images'] as $key => $value){
                $imgName = $value->getRandomName();
                $path = 'upload/post/'.$this->id;
                $value->move($path, $imgName);
                array_push($data['post_image'], $path.'/'.$imgName);
            }
            $postImage = $data['post_image'];
            $data['post_image'] = json_encode($data['post_image']);
            $this->id = $this->service->saveData($data);
        }
        
        return $this->id;
    }
    
    public function delete(){
        $obj = $this->service->getInfo(["*"],["id" => $this->id]);
        if(empty($obj)){
            return false;
        }
        $postAlias = $this->service->getModel()->alias;
        $postData = $this->service->getInfo(
            [
                $postAlias.'.*',
            ],  
            [
                'id' => $this->id,
            ]
        );
        $postImages = json_decode($postData->post_image);
        foreach($postImages as $key => $value){
            @unlink($value);
        }
        // if($postImages != NULL){
        //     rmdir('upload/post/'.$this->id);
        // }
        $data = [
            "id"            =>  $this->id,
            'is_deleted'    =>  DEL_FLG_ON,
            'deleted_at'    =>  date('Y-m-d H:i:s'),
            'deleted_by'    =>  $this->userId,
        ];
        $this->id = $this->service->saveData($data);
        return ['message' => "Delete success!"];
    }

    public function update(){
        $data = $this->request->getPost();
        if (empty($data)) {
			//convert request body to associative array
			$data = json_decode($this->request->getBody(), true);
		}
        $files = $this->request->getFiles();
        $postAlias = $this->service->getModel()->alias;
        $postHashtagService = new PostHashtagService();
        $postHashtagAlias = $postHashtagService->getModel()->alias;

        $postData = $this->service->getInfo(
            [
                $postAlias.'.*',
            ],
            [
                'id' => $this->id,
            ]
        );
        if(!$postData){
            return false;
        }

        // get all post image
        $postImages = json_decode($postData->post_image);
        $data['post_image'] = [];
        if($postImages){
            $remainImages = explode(',', $data['remain_images']);
            // if user delete some image(s) => remove that image
            foreach($postImages as $key => $image){
                if(array_search($image, $remainImages) === false){
                    @unlink($image);
                }else{
                    array_push($data['post_image'], $image);
                }
            }
        }

        // if any new image uploaded by user => move that image(s) to folder and add the uri into db 
        if(isset($files) && isset($files['images'])){
            foreach($files['images'] as $key => $value){
                $imgName = $value->getRandomName();
                $path = 'upload/post/'.$this->id;
                $value->move($path, $imgName);
                array_push($data['post_image'], $path.'/'.$imgName);
            }
        }

        $postHashtagData = $postHashtagService->getData(
            [   
                $postHashtagAlias.'.id',
                $postHashtagAlias.'.hashtag_id',
            ],
            [
                $postHashtagAlias.'.post_id' => $this->id,
            ]
        );

        $uploadedHashtags = explode(',', $data['hashtags']);
        $insertHashtag = [];

        foreach($postHashtagData as $key => $value){
            $index = array_search($value->hashtag_id, $uploadedHashtags);
            if($index === false){
                $postHashtagService->getModel()->delete($value->id);
            }else{
                unset($uploadedHashtags[$index]);
            }
        }

        foreach($uploadedHashtags as $key => $value){
            if($value == ""){
                break;
            }
            $insertData = [
                'hashtag_id' => $value,
                'post_id'    => $this->id,
            ];
            $postHashtagService->saveData($insertData);
        }


        $data['id'] = $this->id;
        $data['post_image'] = json_encode($data['post_image']);
        $data['updated_by'] = $this->userId;
        $data['updated_at'] = date('Y-m-d H:i:s');
        $this->id = $this->service->saveData($data);

        return $this->id;
    }

    public function searchPost(){
        $key = $this->request->getGet('key');
        $province = is_numeric($this->request->getGet('province')) ? $this->request->getGet('province') : 0;
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 3;

        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;
        $likePostService = new LikePostService();
        $locationServcie = new LocationService();
        
        $postAlias = $this->service->getModel()->alias;
        $locationAlias = $locationServcie->getModel()->alias;

        $selects = [
            $postAlias.'.id AS post_id',
            $postAlias.'.title AS post_title',
            $postAlias.'.post_image',
            $postAlias.'.created_by',
        ];
        if($province){
            $data = $this->service->createQuery()->select($selects)
                                  ->like([$postAlias.".title" => $key])
                                  ->where([$locationAlias.'.province' => $province, $postAlias.'.is_locked' => LOCK_FLG_OFF])
                                  ->limit($limit, $page != 0 ? $page * $limit : $page)
                                  ->orderBy($postAlias.'.id', 'DESC')
                                  ->get()
                                  ->getResultObject();
        }else{
            $data = $this->service->createQuery()->select($selects)
                                  ->like([$postAlias.".title" => $key])
                                  ->where([$postAlias.'.is_locked' => LOCK_FLG_OFF])
                                  ->limit($limit, $page != 0 ? $page * $limit : $page)
                                  ->orderBy($postAlias.'.id', 'DESC')
                                  ->get()
                                  ->getResultObject();
        }

        $countResult = 0;
        foreach($data as $key => $value){
            $countResult ++;
            $value->user = $accountService->getInfo(
                [
                    $accountAlias.'.id AS user_id',
                    $accountAlias.'.avatar',
                    $accountAlias.'.user_name',
                ],
                [
                    'id' => $value->created_by,
                ]
            );
            $value->user = convertNumData($value->user);
            if($this->userId){
                $value->isLiked = false;
                if($likePostService->getModel()->where(['account_id' => $this->userId, 'post_id' => $value->post_id])->first()){
                    $value->isLiked = true;
                }
            }
            if($value->post_image){
                $value->post_image = [json_decode($value->post_image, 1)[0]];
            }else{
                $value->post_image = [];
            }
            
            $value->like = $likePostService->countResult('post_id = '.$value->post_id);

            unset($value->created_by);
            $value = convertNumData($value);
        }
        
        $pagination = [
            'page'  => $page,
            'limit' => $limit,
            'total' => $countResult,
        ];
        $pagination = convertNumData($pagination);

        return ['pagination' => $pagination, 'data' => $data];
    }

    public function getViewedPost(){
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;

        $postAlias = $this->service->getModel()->alias;
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;
        $viewedPostService = new ViewedPostService();
        $viewedPostAlias = $viewedPostService->getModel()->alias;

        $data = $viewedPostService->getData(
            [
                // select
                $viewedPostAlias.'.post_id',
                $viewedPostAlias.'.created_by',
                $postAlias.'.post_image',
                $postAlias.'.title',
            ],
            [
                // where
                $viewedPostAlias.'.created_by' => $this->userId,
            ]
        );
        $countResult = 0;
        foreach($data as $key => $value){
            $countResult ++;
            $value->user = $accountService->getInfo(
                [
                    $accountAlias.'.id AS user_id',
                    $accountAlias.'.user_name',
                    $accountAlias.'.avatar',
                ],
                [
                    'id' => $value->created_by,
                ],
                $page,
                $limit
            );
            $value = convertNumData($value);
            $value->user = convertNumData($value->user);
        }

        $pagination = [
            'page'  => $page,
            'limit' => $limit,
            'total' => $countResult,
        ];
        $pagination = convertNumData($pagination);

        return ['pagination' => $pagination, 'data' => $data];
    }

    public function addPostView(){
        $this->id = is_numeric($this->request->getGet('post')) ? $this->request->getGet('post') : 0;
        if(!$this->id){
            return false;
        }

        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;

        $postData = $postService->getInfo(
            [
                // select
                $postAlias.'.id',
                $postAlias.'.view_quantity'
            ],
            [
                // where
                'id' => $this->id
            ]
        );
        return $postService->saveData([
                    'id' => $postData->id,
                    'view_quantity' => $postData->view_quantity += 1,
        ]);
    }

    public function getPostByGallery(){
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;
        $gallery = is_numeric($this->request->getGet('gallery')) ? $this->request->getGet('gallery') : 0;
        
        $likePostService = new LikePostService();
        $postAlias = $this->service->getModel()->alias;
        $galleryPostService = new GalleryPostService();
        $galleryService = new GalleryService();
        $galleryPostAlias = $galleryPostService->getModel()->alias;
        $galleryAlias = $galleryService->getModel()->alias;
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;
        $galleryPostData = $galleryPostService->getData(
            [
                $galleryPostAlias.'.post_id',
                $galleryAlias.'.is_private',
                $galleryAlias.'.created_by',
                $postAlias.'.post_image AS post_image',
                $postAlias.'.is_locked',
                $postAlias.'.title AS post_title',
                $postAlias.'.created_by AS post_created_by',
            ],
            [
                $galleryPostAlias.'.gallery_id' => $gallery,
            ],
            $page,
            $limit
        );

        $countResult = 0;
        foreach($galleryPostData as $key => $value){
            if($value->is_private == IS_PRIVATE && $value->created_by != $this->userId){
                return false;
            }
            $countResult ++;
            $value->post_image = [json_decode($value->post_image, true)[0]];
            $value = convertNumData($value);
            $value->user = $accountService->getInfo(
                [
                    $accountAlias.'.id AS user_id',
                    $accountAlias.'.avatar',
                    $accountAlias.'.user_name',
                ],
                [
                    'id' => $value->post_created_by,
                ]
            );
            if($this->userId){
                $value->isLiked = false;
                if($likePostService->getModel()->where(['account_id' => $this->userId, 'post_id' => $value->post_id])->first()){
                    $value->isLiked = true;
                }
            }
            if($value->is_locked != LOCK_FLG_OFF && $value->created_by != $this->userId){
                unset($value);

            }
            $value->like = $likePostService->countResult('post_id = '.$value->post_id);
            $value->user = convertNumData($value->user);
            unset($value->created_by);
            unset($value->post_created_by);
        }
        
        $pagination = [
            'page'  => $page,
            'limit' => $limit,
            'total' => $countResult,
        ];
        $pagination = convertNumData($pagination);

        return ['data' => $galleryPostData, 'pagination' => $pagination];
    }
    
    public function getPostByPrivateGallery(){
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;
        $gallery = is_numeric($this->request->getGet('gallery')) ? $this->request->getGet('gallery') : 0;
        
        $likePostService = new LikePostService();
        $postAlias = $this->service->getModel()->alias;
        $galleryPostService = new GalleryPostService();
        $galleryService = new GalleryService();
        $galleryPostAlias = $galleryPostService->getModel()->alias;
        $galleryAlias = $galleryService->getModel()->alias;
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;

        $galleryPostData = $galleryPostService->getData(
            [
                $galleryPostAlias.'.post_id',
                $postAlias.'.post_image AS post_image',
                $postAlias.'.title AS post_title',
                $postAlias.'.created_by',
            ],
            [
                $galleryPostAlias.'.gallery_id' => $gallery,
                $galleryAlias.'.is_private' => IS_PRIVATE,
                $galleryAlias.'.created_by' => $this->userId,
            ],
            $page,
            $limit
        );

        $countResult = 0;
        foreach($galleryPostData as $key => $value){
            $countResult ++;
            $value->post_image = [json_decode($value->post_image, true)[0]];
            $value = convertNumData($value);
            $value->user = $accountService->getInfo(
                [
                    $accountAlias.'.id AS user_id',
                    $accountAlias.'.avatar',
                    $accountAlias.'.user_name',
                ],
                [
                    'id' => $value->created_by,
                ]
            );
            if($this->userId){
                $value->isLiked = false;
                if($likePostService->getModel()->where(['account_id' => $this->userId, 'post_id' => $value->post_id])->first()){
                    $value->isLiked = true;
                }
            }
            $value->user = convertNumData($value->user);
            unset($value->created_by);
        }        
        
        $pagination = [
            'page'  => $page,
            'limit' => $limit,
            'total' => $countResult,
        ];
        $pagination = convertNumData($pagination);

        return ['data' => $galleryPostData, 'pagination' => $pagination];
    }

    public function getLikedPost(){
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;

        $postAlias = $this->service->getModel()->alias;
        $likePostService = new LikePostService();
        $likePostAlias = $likePostService->getModel()->alias;

        $selects = [
            $postAlias.'.id AS post_id',
            $postAlias.'.title AS post_title',
            $postAlias.'.post_image'
        ];

        $where = [
            $likePostAlias.'.account_id' => $this->userId,
        ];

        
        $data = $likePostService->getData($selects, $where, $page, $limit);
        $countResult = 0;

        foreach($data as $key => $value){
            $countResult++;
            $value->post_image = [ json_decode($value->post_image)[0] ];
            $value = convertNumData($value);
        }

        $pagination = [
            'page'  => $page,
            'limit' => $limit,
            'total' => $countResult,
        ];

        $pagination = convertNumData($pagination);
        return ['data' => $data, 'pagination' => $pagination];
    }

    public function getPostByHashtag(){
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;
        $hashtag = is_numeric($this->request->getGet('id')) ? $this->request->getGet('id') : 0;
        
        $postHashtagService = new PostHashtagService();
        $postHashtagAlias = $postHashtagService->getModel()->alias;
        $postAlias = $this->service->getModel()->alias;
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;
        $likePostService = new LikePostService();
        $postHashtagData = $postHashtagService->getData(
            [
                $postAlias.'.id AS post_id',
                $postAlias.'.title AS post_title',
                $postAlias.'.post_image',

                $accountAlias.'.id AS user_id',
                $accountAlias.'.user_name',
                $accountAlias.'.avatar',
            ],
            [
                $postHashtagAlias.'.hashtag_id' => $hashtag,
            ],
            $page,
            $limit
        );
        foreach($postHashtagData as $key => $value){
            $value->user = convertNumData([
                'user_id'   =>  $value->user_id,
                'user_name' =>  $value->user_name,
                'avatar'    =>  $value->avatar,
            ]);

            $value->like = $likePostService->countResult('post_id = '.$value->post_id);
            $value->post_image = [ json_decode($value->post_image)[0] ];

            if($this->userId){
                $likePostService = new LikePostService();
                $value->isLiked = false;
                if($likePostService->getModel()->where(['account_id' => $this->userId, 'post_id' => $value->post_id])->first()){
                    $value->isLiked = true;
                }
            }

            $unsetFileds = [
                'user_id',
                'user_name',
                'avatar'
            ];
            $value = unsetObject($unsetFileds, $value);
            $value = convertNumData($value);
        }
        return $postHashtagData;
    }
}
