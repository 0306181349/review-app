<?php 
namespace App\Services\Post;
use App\Models\PostViolationModel;
use App\Services\Report\ReportTitle\ReportTitleService;

class PostViolationService
{
    public function __construct(){
        $this->model = new PostViolationModel();
        $this->db = \Config\Database::connect();
    }

    public function getModel(){
        return $this->model;
    }

    public function createQuery(){
        $postTable = $this->model->table;
        $postAlias = $this->model->alias;
        
        $reportTitleService = new ReportTitleService();
        $reportTitleTable = $reportTitleService->getModel()->table;
        $reportTitleAlias = $reportTitleService->getModel()->alias;

        $builder = $this->db->table($postTable.' AS '.$postAlias)
                            ->join($reportTitleTable.' AS '.$reportTitleAlias, $postAlias.'.report_title_id = '.$reportTitleAlias.'.id');
        return $builder;
    }

    public function getData($selects = [], $where = [], $page = 0, $limit = 0){
        if($page != 0){
            $page = $page * $limit;
        }

        $postAlias = $this->model->alias;
        $postTable = $this->model->table;
        $query = $this->createQuery()
                                    ->select($selects)
                                    ->where($where)
                                    ->limit($limit, $page)
                                    ->orderBy($postAlias.'.id', 'DESC');
        $get = $query->get();
        return $get->getResultObject();
    }

	// get row information
	public function getInfo($selects = [], $where = []){
		$model_alias = $this->model->alias;
		$query = $this->createQuery()
						->select($selects);
		if(is_array($where) && !empty($where)){
			$query = $this->buildCondition($query, $where);
		}
		return $query->get()->getRow();
	}

    public function saveData($data){
		$id = $data['id'] ?? false;
		if($id){
			unset($data['id']);
			$this->model->update($id, $data);
		}else{
			$id = $this->model->insert($data);
		}
		return $id;
	}
    
    // cout result
    public function countResult($where=[]){
        $query = $this->createQuery();
        if(!empty($where)){
		    if(is_array($where)){
		    	$query = $this->buildCondition($query, $where);
		    }else{
                $query->where($where);
            }
        }
        return $query->countAllResults();
    }

    //select avg column 
    public function selectAvg($col, $where=[]){
        $query = $this->createQuery();
		if(is_array($where) && !empty($where)){
			$query = $this->buildCondition($query, $where);
		}
        return $query->selectAvg($col)->get()->getRow();
    }

    // set condition
	protected function buildCondition($query, $condition){
		foreach($condition as $field=>$val){
			switch ($field) {
				default:
					if($val !== ''){
						$query->where($this->model->alias.".".$field, $val);
					}
					break;
			}
		}
		return $query;
	}
}