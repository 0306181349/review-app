<?php 
namespace App\Services\Hashtag;
use App\Services\Hashtag\HashtagService;
use App\Services\Hashtag\PostHashtagService;
use App\Services\Account\AccountService;
use App\Services\Post\PostService;
use App\Services\Location\LocationService;

class UpdateHashtagService
{
    protected $id = 0;
    protected $userId = 0;
	protected $request;

    public function __construct(){
        $this->service = new HashtagService();
        helper('convert_num');
    }
    
    //set modify account id
    public function setId($id){
		return $this->id = $id;
	}

    //set user id who is modifying the account info 
    public function setUser($userId){
        return $this->userId = $userId;
    }
    
	public function setRequest($request){
		return $this->request = $request;
	}

    public function setAction($action){
        return $this->{$action}();
    }

    public function searchHashtag(){
        $key = $this->request->getGet('key');
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;

        $hashtagAlias = $this->service->getModel()->alias;
        $postHashtagService = new PostHashtagService();

        $selects = [
            $hashtagAlias.'.id AS hashtag_id',
            $hashtagAlias.'.hashtag',
        ];

        $data = $this->service->createQuery()->select($selects)
                                             ->like([$hashtagAlias.".hashtag" => $key])
                                             ->limit($limit, $page != 0 ? $page * $limit : $page)
                                             ->orderBy($hashtagAlias.'.id', 'DESC')
                                             ->get()
                                             ->getResultObject();
        
        $countResult = 0;
        foreach($data as $key => $value){
            $countResult ++;
            $value->numOfPost = $postHashtagService->countResult('hashtag_id = '.$value->hashtag_id);
            $value = convertNumData($value);
        }

        $pagination = [
            'page'  => $page,
            'limit' => $limit,
            'total' => $countResult,
        ];
        
        $pagination = convertNumData($pagination);
        return ['pagination' => $pagination, 'data' => $data];
    }

    public function selectHashtag(){
        $key = $this->request->getGet('key');
        $hashtagAlias = $this->service->getModel()->alias;
        $selects = [
            $hashtagAlias.'.id AS hashtag_id',
            $hashtagAlias.'.hashtag',
        ];
        $like = [
            'hashtag' => $key
        ];

        $data = $this->service->createQuery()->select($selects)->like($like)->get()->getResultObject();
        
        return $data;
    }

    public function getHashtagTableData(){
        $hashtagAlias = $this->service->getModel()->alias;
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;
        $postHashtagService = new PostHashtagService();

        $data = $this->service->getData(
            [
                $hashtagAlias.'.id AS hashtag_id',
                $hashtagAlias.'.hashtag',
                $hashtagAlias.'.created_at',
                $accountAlias.'.id AS user_id',
                $accountAlias.'.user_name AS created_by',
            ]
        );

        foreach($data as $key => $value){
            $value->post_quantity = $postHashtagService->countResult(
                [
                    'hashtag_id' => $value->hashtag_id,
                ]
            );
        }

        return $data;
    }

    public function getHashtagData(){
        $hashtagAlias = $this->service->getModel()->alias;
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;
        $postHashtagService = new PostHashtagService();

        $data = $this->service->getInfo(
            [
                $hashtagAlias.'.id AS hashtag_id',
                $hashtagAlias.'.hashtag',
                $hashtagAlias.'.created_at',
                $accountAlias.'.id AS user_id',
                $accountAlias.'.user_name AS created_by',
            ],
            [
                'id' => $this->id,
            ]
        );
        $data->post_quantity = $postHashtagService->countResult(
            [
                'hashtag_id' => $data->hashtag_id,
            ]
        );

        return $data;
    }

    public function add(){
        $input = $this->request->getPost();
        if (empty($input)) {
			//convert request body to associative array
			$input = json_decode($this->request->getBody(), true);
		}
        $hashtagAlias = $this->service->getModel()->alias;

        $data = [
            'hashtag' => $input['hashtag'],
            'created_by' => $this->userId,
        ];

        $this->id = $this->service->saveData($data);
        return $this->getHashtagData();
    }

    public function edit(){
        $input = $this->request->getPost();
        if (empty($input)) {
			//convert request body to associative array
			$input = json_decode($this->request->getBody(), true);
		}
        $hashtagAlias = $this->service->getModel()->alias;
        $data = $this->service->getInfo(
            [
                $hashtagAlias.'.id'
            ],
            [
                'id' => $this->id
            ]
        );
        if(!$data){
            return false;
        }
        $data = [
            'id' => $this->id,
            'hashtag' => $input['hashtag'],
            'updated_by' => $this->userId,
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        $this->id = $this->service->saveData($data);
        return $this->getHashtagData();
    }

    public function delete(){
        $postHashtagService = new PostHashtagService();
        $hashtagAlias = $this->service->getModel()->alias;

        $data = $this->service->getInfo(
            [
                $hashtagAlias.'.id'
            ],
            [
                'id' => $this->id
            ]
        );
        if(!$data){
            return false;
        }
        $data = [
            'id' => $this->id,
            'is_deleted' => DEL_FLG_ON,
            'deleted_by' => $this->userId,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];

        $this->id = $this->service->saveData($data);
        $postHashtagService->getModel()->where('hashtag_id', $this->id)->delete();
        return true;
    }

    public function hashtagDeltail(){
        $this->id = is_numeric($this->request->getGet('id')) ? $this->request->getGet('id') : 0;
        $postHashtagService = new PostHashtagService();
        $hashtagAlias = $this->service->getModel()->alias;
        $data = $this->service->getInfo(
            [
                $hashtagAlias.'.id AS hashtag_id',
                $hashtagAlias.'.hashtag',
            ],
            [
                'id' => $this->id,
            ]
        );
        if(!$data){
            return false;
        }
        $data->numOfPost = $postHashtagService->countResult(
            [
                'hashtag_id' => $this->id,
            ]
        );
        return convertNumData($data);
    }

    public function postByHashtag(){
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;
        $postHashtagService = new PostHashtagService();
        $postHashtagAlias = $postHashtagService->getModel()->alias;
        $locationService = new LocationService();
        // $select = [
        //     $postModel->alias.'.id AS id',
        //     $postModel->alias.'.title AS title',
        //     $postModel->alias.'.view_quantity AS view_quantity',
        //     $postModel->alias.'.like_quantity AS like_quantity',
        //     $locationModel->alias.'.location_name AS location',
        // ];
        $data =  $postHashtagService->getData(
            [
                $postAlias.'.id AS id',
                $postAlias.'.title AS title',
                $postAlias.'.view_quantity AS view_quantity',
                $postAlias.'.like_quantity AS like_quantity',
                $postAlias.'.location',
            ],
            [
                $postAlias.'.is_locked' => LOCK_FLG_OFF,
                $postHashtagAlias.'.hashtag_id' => $this->id,
            ]
        );
        foreach($data as $key => $value){
            $value->location = $locationService->getInfo(
                [
                    'location_name'
                ],
                [
                    'id' => $value->location,
                ]
            )->location_name;
        }
        return $data;
    }
}