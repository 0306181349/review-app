<?php 
namespace App\Services\Hashtag;
use App\Models\HashtagModel;
use App\Models\AccountModel;

class HashtagService
{
    
    public function __construct(){
        $this->model = new HashtagModel();
        $this->db = \Config\Database::connect();
    }


    public function getModel(){
        return $this->model;
    }

    public function createQuery(){
		$accountModel = new AccountModel();
		$accountTable = $accountModel->table;
		$accountAlias = $accountModel->alias;

        $builder = $this->db->table($this->model->table.' AS '.$this->model->alias)
							->join($accountTable.' AS '.$accountAlias, $this->model->alias.'.created_by = '.$accountAlias.'.id')
                            ->where($this->model->alias.'.is_deleted', DEL_FLG_OFF);
        return $builder;
    }

    public function getData($selects = [], $where = [], $page = 0, $limit = 0){
        if($page != 0){
            $page = $page * $limit;
        }

        $accountAlias = $this->model->alias;
        $accountTable = $this->model->table;
        $query = $this->createQuery()
                                    ->select($selects)
                                    ->where($where)
                                    ->limit($limit, $page)
                                    ->orderBy($accountAlias.'.id', 'DESC');
        $get = $query->get();
        return $get->getResultObject();
    }

	// get row information
	public function getInfo($selects = [], $where = []){
		$model_alias = $this->model->alias;

		$query = $this->createQuery()
						->select($selects);
		if(is_array($where) && !empty($where)){
			$query = $this->buildCondition($query, $where);
		}
		return $query->get()->getRow();
	}
    
	// set condition
	protected function buildCondition($query, $condition){
		foreach($condition as $field=>$val){
			switch ($field) {
				default:
					if($val !== ''){
						$query->where($this->model->alias.".".$field, $val);
					}
					break;
			}
		}
		return $query;
	}

	// cout result
	public function countResult($where = []){
		$query = $this->createQuery();
		if(!empty($where)){
			if(is_array($where)){
				$query = $this->buildCondition($query, $where);
			}else{
				$query->where($where);
			}
		}
		return $query->countAllResults();
	}
    
	public function saveData($data){
		$id = $data['id'] ?? false;
		if($id){
			unset($data['id']);
			$this->model->update($id, $data);
		}else{
			$id = $this->model->insert($data);
		}
		return $id;
	}
}