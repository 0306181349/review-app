<?php 

namespace App\Services\Gallery;
use App\Services\Gallery\GalleryService;
use App\Services\Gallery\GalleryPostService;
use App\Services\Post\PostService;
use App\Services\Account\AccountService;
use App\Services\Like\LikePostService;

class UpdateGalleryService
{   
    protected $id = 0;
    protected $userId = 0;
	protected $request;
    
    public function __construct(){
        helper('convert_num');
        $this->galleryService = new GalleryService();
        $this->galleryPostService = new GalleryPostService();
    }

    //set modify account id
    public function setId($id){
		return $this->id = $id;
	}

    //set user id who is modifying the account info 
    public function setUser($userId){
        return $this->userId = $userId;
    }
    
	public function setRequest($request){
		return $this->request = $request;
	}

    public function setAction($action){
        return $this->{$action}();
    }

    public function getAllGallery(){ // get all gallery
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;
        $user = is_numeric($this->request->getGet('user')) ? $this->request->getGet('user') : "0";

        $where = []; // where condition in select query
        $galleryAlias = $this->galleryService->getModel()->alias;

        if($this->userId == $user){ // if the user id that encoded in token equal to the user id in url param
                                    // => that user is on their own gallery list so we can get all the gallery
                                    // belong to that user whether it private or not 
            $where = [
                $galleryAlias.'.created_by' => $this->userId,
            ];
        }else{ // else that user is on another user gallery list so we cannot showup the private gallery
            $where = [
                $galleryAlias.'.created_by' => $user,
                $galleryAlias.'.is_private' => IS_PUBLIC
            ];
        }
        
        $data = $this->galleryService->getData(
            [
                // selects
                $galleryAlias.'.id AS gallery_id',
                $galleryAlias.'.gallery_name AS gallery_name',
                $galleryAlias.'.cover_photo AS cover_photo',
                $galleryAlias.'.description AS description',
                $galleryAlias.'.is_private AS is_private',
            ],
            $where,
            $page,
            $limit
        );
        foreach($data as $key => $value){
            $value = convertNumData($value);

            $value->num_of_post = $this->galleryPostService->countResult(
                [
                    'gallery_id' => $value->gallery_id,
                ]
            );
            if(!$value->is_private){
                $value->is_private = false;
            }else{
                $value->is_private = true;
            }
        }

        return $data;
    }

    public function getGalleryDetail($id = 0){
        $this->id = $id;

        $galleryAlias = $this->galleryService->getModel()->alias;
        $galleryPostAlias = $this->galleryPostService->getModel()->alias;
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;

        $galleryData = $this->galleryService->getInfo(
            [
                // select
                $galleryAlias.'.id AS gallery_id',
                $galleryAlias.'.cover_photo AS cover_photo',
                $galleryAlias.'.gallery_name AS gallery_name',
                $galleryAlias.'.description AS description',
                $galleryAlias.'.is_private AS is_private',
            ],
            [
                // where
                'id' => $this->id,
                'created_by' => $this->userId,
            ]
        );

        if($galleryData->is_private == IS_PRIVATE){
            $galleryData->is_private = true;
        }else{
            $galleryData->is_private = false;
        }
        $galleryData = convertNumData($galleryData);
        return $galleryData;
    }

    public function getPostByGallery(){
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;
        
        $likePostService = new LikePostService();
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;
        $galleryPostAlias = $this->galleryPostService->getModel()->alias;
        $galleryAlias = $this->galleryService->getModel()->alias;

        $galleryPostData = $this->galleryPostService->getData(
            [
                $galleryPostAlias.'.post_id',
                $galleryAlias.'.is_private',
                $postAlias.'.post_image AS post_image',
                $postAlias.'.title AS post_title',
                $postAlias.'.created_by',
            ],
            [
                $galleryPostAlias.'.gallery_id' => $this->id,
            ],
            $page,
            $limit
        );


        foreach($galleryPostData as $key => $value){
            if($value->is_private == IS_PRIVATE && $value->created_by != $this->userId){
                // if the gallery is private and the user requested to that gallery is not the owner 
                return false;
            }
            $value->post_image = [json_decode($value->post_image, true)[0]];
            $value = convertNumData($value);
            $value->user = $accountService->getInfo(
                [
                    $accountAlias.'.id AS user_id',
                    $accountAlias.'.avatar',
                    $accountAlias.'.user_name',
                ],
                [
                    'id' => $value->created_by,
                ]
            );
            if($this->userId){
                $value->isLiked = false;
                if($likePostService->getModel()->where(['account_id' => $this->userId, 'post_id' => $value->post_id])->first()){
                    $value->isLiked = true;
                }
            }
            $value->user = convertNumData($value->user);
            unset($value->created_by);
        }
        return $galleryPostData;
    }
    
    public function createGallery(){
        $data = $this->request->getPost();
        $file = $this->request->getFile('cover_photo');

        if (empty($data)) {
			//convert request body to associative array
			$data = json_decode($this->request->getBody(), true);
		}

        $data['is_default'] = IS_NOT_DEFAULT;
        $data['created_by'] = $this->userId;
        $this->id = $this->galleryService->saveData($data);

        if(isset($file)){
            $imgName = $file->getRandomName();
            $path = 'upload/gallery/'.$this->id;
            $file->move($path, $imgName);
            $data['cover_photo'] = 'upload/gallery/'.$this->id.'/'.$imgName;
            $data['id'] = $this->id;
            $this->id = $this->galleryService->saveData($data);
        }
        
        return $this->getGalleryDetail($this->id);
    }

    public function editGallery(){
        $this->id = is_numeric($this->request->getGet('id')) ? $this->request->getGet('id') : 0;
        $data = $this->request->getPost();
        $file = $this->request->getFile('cover_photo');

        if (empty($data)) {
			//convert request body to associative array
			$data = json_decode($this->request->getBody(), true);
		}

        $data['is_default'] = IS_NOT_DEFAULT;
        
        $galleryAlias = $this->galleryService->getModel()->alias;
        $galleryData = $this->galleryService->getInfo(
            [
                $galleryAlias.'.*',
            ],
            [
                'id'         => $this->id,
                'is_default' => IS_NOT_DEFAULT,
            ]
        );
        if(!$galleryData || $galleryData->created_by != $this->userId){ // if gallery data is null or that gallery is not belong to this user
            return false;
        }

        $data['updated_by'] = $this->userId;
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['id'] = $this->id;
        $this->id = $this->galleryService->saveData($data);

        if(isset($file)){
            @unlink($galleryData->cover_photo);
            $imgName = $file->getRandomName();
            $path = 'upload/gallery/'.$this->id;
            $file->move($path, $imgName);
            $data['cover_photo'] = 'upload/gallery/'.$this->id.'/'.$imgName;
            $data['id'] = $this->id;
            $this->id = $this->galleryService->saveData($data);
        }

        return $this->getGalleryDetail($this->id);
    }

    public function deleteGallery(){
        $this->id = is_numeric($this->request->getGet('gallery')) ? $this->request->getGet('gallery') : '0';

        $galleryAlias = $this->galleryService->getModel()->alias;

        $data = $this->galleryService->getInfo(
            [
                // selects
                $galleryAlias.'.id'
            ],
            [
                'id'         => $this->id,
                'created_by' => $this->userId,
                'is_default' => (string)IS_NOT_DEFAULT,
            ]
        );
        
        if(!$data){
            return false;
        }

        $this->removeAllPostFromGallery();
        return $this->galleryService->getModel()->delete($data->id);
    }   

    public function setPrivacy(){
        $this->id = is_numeric($this->request->getGet('gallery')) ? $this->request->getGet('gallery') : "0";
        $is_private = is_numeric($this->request->getGet('is_private')) ? $this->request->getGet('is_private') : 0;
        $galleryAlias = $this->galleryService->getModel()->alias;

        $data = $this->galleryService->getInfo(
            [
                // selects
                $galleryAlias.'.*'
            ],
            [
                'id'         => $this->id,
                'created_by' => $this->userId,
            ]
        );

        if(!$data){
            return false;
        }
        
        if($is_private == IS_PRIVATE){
            $insertData = [
                'id'         => $data->id,
                'is_private' => IS_PRIVATE
            ];
        }else{
            $insertData = [
                'id'         => $data->id,
                'is_private' => IS_PUBLIC
            ];
        }

        $this->galleryService->saveData($insertData);
        return true;
    }

    public function addPostToGallery(){
        $this->id = is_numeric($this->request->getGet('gallery')) ? $this->request->getGet('gallery') : "0";
        $post_id = is_numeric($this->request->getGet('post')) ? $this->request->getGet('post') : "0";
        
        $galleryAlias = $this->galleryService->getModel()->alias;
        $galleryPostAlias = $this->galleryPostService->getModel()->alias;
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;

        $postData = $postService->getInfo(
            [
                $postAlias.'.id',
            ],
            [
                'id' => $post_id,
            ]
        );

        $galleryData = $this->galleryService->getInfo(
            [   
                $galleryAlias.'.id',
            ],
            [
                'id'         => $this->id,
                'created_by' => $this->userId,
            ]
        );

        $galleryPostData = $this->galleryPostService->getData(
            [
                $galleryPostAlias.'.id',
            ],
            [
                $galleryPostAlias.'.post_id' => $post_id,
                $galleryAlias.'.created_by' => $this->userId,
            ]
        );

        if(!$postData || !$galleryData || !empty($galleryPostData)){ // if post doesn't exist or gallery doesn't exist or that post already in gallery
            return false;
        }

        $insertData = [
            'post_id' => $postData->id,
            'gallery_id' => $galleryData->id,
        ];
        
        $this->galleryPostService->saveData($insertData);
        return true;
    }

    public function removePostFromGallery(){
        $post_id = is_numeric($this->request->getGet('post')) ? $this->request->getGet('post') : '0';

        $galleryAlias = $this->galleryService->getModel()->alias;
        $galleryPostAlias = $this->galleryPostService->getModel()->alias;

        $galleryPostData = $this->galleryPostService->getData(
            [  
                $galleryPostAlias.'.id',
            ],
            [
                $galleryAlias.'.created_by'  => $this->userId,
                $galleryPostAlias.'.post_id' => $post_id,
            ]
        );

        if(empty($galleryPostData)){ // gallery post doesn't exist 
            return false;
        }

        return $this->galleryPostService->getModel()->delete($galleryPostData[0]->id);
    }

    public function removePostFromAllGallery(){
        //$this->id = is_numeric($this->request->getGet('gallery')) ? $this->request->getGet('gallery') : 0;
        $post_id = is_numeric($this->request->getGet('post')) ? $this->request->getGet('post') : 0;

        $galleryAlias = $this->galleryService->getModel()->alias;
        $galleryPostAlias = $this->galleryPostService->getModel()->alias;

        $galleryPostData = $this->galleryPostService->getData(
            [
                $galleryPostAlias.'.id',
            ],
            [
                $galleryPostAlias.'.post_id' => $post_id,
                $galleryAlias.'.created_by' => $this->userId,
            ]
        );

        if(!$galleryPostData){
            return false;
        }

        foreach($galleryPostData as $key => $value){
            $result = $this->galleryPostService->getModel()->delete($value->id);
            if(!$result){ // if the query somehow get error 
                return $result;
            }
        }
        return $result;
    }

    // remove all post from a specific gallery (use with delete gallery)
    private function removeAllPostFromGallery(){
        $galleryAlias = $this->galleryService->getModel()->alias;
        $galleryPostAlias = $this->galleryPostService->getModel()->alias;

        $galleryPostData = $this->galleryPostService->getData(
            [
                $galleryPostAlias.'.id',
            ],
            [
                $galleryPostAlias.'.gallery_id' => $this->id,
                $galleryAlias.'.created_by' => $this->userId,
            ]
        );

        foreach($galleryPostData as $key => $value){
            if(!$this->galleryPostService->getModel()->delete($value->id)){
                return false;
            }
        }
        return true;
    }
}