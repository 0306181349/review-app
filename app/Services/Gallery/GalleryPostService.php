<?php 
namespace App\Services\Gallery;
use App\Models\GalleryPostModel;
use App\Services\Post\PostService;
use App\Services\Gallery\GalleryService;

class GalleryPostService 
{
    public function __construct(){
        $this->model = new GalleryPostModel();
        $this->db = \Config\Database::connect();
    }

    public function getModel(){
        return $this->model;
    }

    public function createQuery(){
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;
        $postTable = $postService->getModel()->table;
		
		$galleryService = new GalleryService();
		$galleryTable = $galleryService->getModel()->table;
		$galleryAlias = $galleryService->getModel()->alias;

        $builder = $this->db->table($this->model->table.' AS '.$this->model->alias)
                            ->join($postTable.' AS '.$postAlias, $this->model->alias.'.post_id = '.$postAlias.'.id')
							->join($galleryTable.' AS '.$galleryAlias, $this->model->alias.'.gallery_id = '.$galleryAlias.'.id')
                            ->where($postAlias.'.is_deleted', DEL_FLG_OFF);
        return $builder;
    }

    public function getData($selects = [], $where = [], $page = 0, $limit = 0){
        if($page != 0){
            $page = $page * $limit;
        }

        $accountAlias = $this->model->alias;
        $accountTable = $this->model->table;
        $query = $this->createQuery()
                                    ->select($selects)
                                    ->where($where)
                                    ->limit($limit, $page)
                                    ->orderBy($accountAlias.'.id', 'DESC');
        $get = $query->get();
        return $get->getResultObject();
    }

	// get row information
	public function getInfo($selects = [], $where = []){
		$model_alias = $this->model->alias;

		$query = $this->createQuery()
						->select($selects);
		if(is_array($where) && !empty($where)){
			$query = $this->buildCondition($query, $where);
		}
		return $query->get()->getRow();
	}
    
	// set condition
	protected function buildCondition($query, $condition){
		foreach($condition as $field=>$val){
			switch ($field) {
				default:
					if($val !== ''){
						$query->where($this->model->alias.".".$field, $val);
					}
					break;
			}
		}
		return $query;
	}
	
	// cout result
	public function countResult($where=[]){
		$query = $this->createQuery();
		if(!empty($where)){
			if(is_array($where)){
				$query = $this->buildCondition($query, $where);
			}else{
				$query->where($where);
			}
		}
		return $query->countAllResults();
	}

	public function saveData($data){
		$id = $data['id'] ?? false;
		if($id){
			unset($data['id']);
			$this->model->update($id, $data);
		}else{
			$id = $this->model->insert($data);
		}
		return $id;
	}
}