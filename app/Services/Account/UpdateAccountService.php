<?php 
namespace App\Services\Account;
use App\Services\Account\AccountService;
use CodeIgniter\HTTP\IncomingRequest;
use App\Services\Post\PostService;
use App\Services\Follow\FollowService;
use App\Services\Like\LikePostService;
use App\Services\Province\ProvinceService;
use App\Services\Favorite\FavoriteService;
use App\Services\Post\UpdatePostService;
use App\Services\Notification\UpdateNotificationService;
use App\Services\Notification\NotificationService;

class UpdateAccountService 
{
    protected $id = 0;
    protected $userId = 0;
	protected $request;
    
    public function __construct(){
        $this->service = new AccountService();
    }

    //set modify account id
    public function setId($id){
		return $this->id = $id;
	}

    //set user id who is modifying the account info 
    public function setUser($userId){
        return $this->userId = $userId;
    }
    
	public function setRequest($request){
		return $this->request = $request;
	}

    public function setAction($action){
        return $this->{$action}();
    }

    public function delete(){
        $obj = $this->service->getInfo(["id" => $this->id]);
        if(empty($obj)){
            return false;
        }
        @unlink($obj->avarta);
        $data = [
            "id"            =>  $this->id,
            'is_deleted'    =>  DEL_FLG_ON,
            'deleted_at'    =>  date('Y-m-d H:i:s'),
            'deleted_by'    =>  $this->userId,
        ];
        $this->id = $this->service->saveData($data);
        return ['message' => "Delete success!"];
    }

    public function update(){
        $data = $this->request->getPost();
        $avatar = $this->request->getFile("avatar");

        if (empty($data)) {
			//convert request body to associative array
			$data = json_decode($this->request->getBody(), true);
		}

        $accountAlias = $this->service->getModel()->alias;
        $userData = $this->service->getInfo(
            [
                $accountAlias.'.*'
            ],
            [
                'id' => $this->id,
            ]   
        );

        if(!$userData){
            return false;
        }

        if(isset($avatar) && $avatar->isValid()){
            @unlink($userData->avatar);
            $avatarName = $avatar->getRandomName();
            $avatar->move('upload/users/'.$this->id.'/avatar', $avatarName);
    
            $data = [];
            $data['id'] = $this->id;
            $data['avatar'] = 'upload/users/'.$this->id.'/avatar/'.$avatarName;
        }
        
        if(isset($data['password'])){
            $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
        }
        
        $data['id'] = $this->id;
        $data['updated_by'] = $this->userId;
        $data['updated_at'] = date('Y-m-d H:i:s');
        $this->id = $this->service->saveData($data);
        return $this->id;
    }

    public function lock(){
        $obj = $this->service->getInfo(['*'],["id" => $this->id]);
        if(empty($obj)){
            return false;
        }
        if($obj->is_locked == 0){
            $data = [
                "id"            =>  $this->id,
                'is_locked'     =>  LOCK_FLG_ON,
                'updated_at'    =>  date('Y-m-d H:i:s'),
                'updated_by'    =>  $this->userId,
            ];
            $this->id = $this->service->saveData($data);
            return ['message' => "Lock success!", 'is_locked' => 1];
        }else{
            $data = [
                "id"            =>  $this->id,
                'is_locked'     =>  LOCK_FLG_OFF,
                'updated_at'    =>  date('Y-m-d H:i:s'),
                'updated_by'    =>  $this->userId,
            ];
            $this->id = $this->service->saveData($data);
            return ['message' => "Unlock success!", "is_locked" => 0];
        }
    }

    public function create(){
        $data = $this->request->getPost();
        $file = $this->request->getFiles();
        if (empty($data)) {
			//convert request body to associative array8
			$data = json_decode($this->request->getBody(), true);
		}
        $data['id'] = $this->id;
        $data['created_by'] = $this->userId;
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['avatar'] = "assets/images/avatars/avatar.jpg";
        $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
        $this->id = $this->service->saveData($data);
        return $this->id;
    }

    public function changePass(){
        $data = $this->request->getPost();
        if (empty($data)) {
			//convert request body to associative array
			$data = json_decode($this->request->getBody(), true);
		}
        $obj = $this->service->getInfo(['*'], ["id" => $this->id]);
        if(empty($obj)){
            return 'User not found!';
        }

        if(!password_verify($data['old_pass'], $obj->password)){
            return 'Password incorrect!';
        }

        $data = [
            "id"            =>  $this->id,
            'password'      =>  password_hash($data['new_pass'], PASSWORD_BCRYPT),
            'updated_at'    =>  date('Y-m-d H:i:s'),
            'updated_by'    =>  $this->id,
        ];
        $this->id = $this->service->saveData($data);
        return "Update success!";
    }

    public function accountProfile(){
        helper('convert_num');
        $this->id = $this->request->getGet('id');
        $postService = new PostService();
        $followService = new FollowService();
        $alias = $this->service->getModel()->alias;
        $postAlias = $postService->getModel()->alias;

        // id of the user want to query 
        $selects = [
            $alias.'.id as user_id',
            $alias.'.user_name',
            $alias.'.phone',
            $alias.'.first_name',
            $alias.'.last_name',
            $alias.'.avatar',
            $alias.'.rank',
            $alias.'.bio',
        ];

        $data = $this->service->getInfo($selects, ['id' => $this->id]);
        if($data == null){
            return false;
        }
        $data->followers = $followService->countResult(['account_id_2' => $this->id]);
        $data->following = $followService->countResult(['account_id_1' => $this->id]);

        if($this->userId){// user request is logged in
            // get isFollowed to indentify that the current user(which is perform this request) is followed this user or not 
            $data->isFollowed = false;
            if($followService->getModel()->where(['account_id_1' => $this->userId, 'account_id_2' => $this->id])->first()){
                $data->isFollowed = true;
            }
        }
        $data->post_by_user = $postService->countResult(["created_by" => $data->user_id]);
        $data = convertNumData($data);
        return $data;
    }

    public function accountDetail(){
        helper('convert_num');
        $postService = new PostService();
        $followService = new FollowService();
        $alias = $this->service->getModel()->alias;
        $postAlias = $postService->getModel()->alias;
        $provinceService = new ProvinceService();
        $provinceAlias = $provinceService->getModel()->alias;
        $selects = [
            $alias.'.id as user_id',
            $alias.'.user_name',
            $alias.'.email',
            $alias.'.phone',
            $alias.'.password',
            $alias.'.province AS province_id',
            $alias.'.first_name',
            $alias.'.last_name',
            $alias.'.gender',
            $alias.'.birthday',
            $alias.'.avatar',
            $alias.'.exp_point',
            $alias.'.rank',
            $alias.'.bio',
            // $provinceAlias.'.province_name AS province_name',
        ];
        $data = $this->service->getInfo($selects, ['id' => $this->userId]);
        $provinceData = $provinceService->getInfo(['id AS province_id', 'province_name'],['id' => $data->province_id]);
        convertNumData($provinceData);

        $data->passIsNull = false;
        if($data->password == NULL){
            $data->passIsNull = true;
        }

        unset($data->password);
        unset($data->province_id);

        $data->province = $provinceData;
        $data->followers = $followService->countResult(['account_id_2' => $this->userId]);
        $data->following = $followService->countResult(['account_id_1' => $this->userId]);

        $data->post_by_user = $postService->countResult(["created_by" => $data->user_id]);
        $data = convertNumData($data);
        return $data;
    }
    
    public function follow(){
        $followService = new FollowService();
        $updateNotificationService = new UpdateNotificationService();

        $this->id = $this->request->getGet("user");
        if($this->id == $this->userId){
            return false;
        }

        $followerData = $this->service->getInfo(['id', 'followers'], ['id' => $this->id]);
        $followingData = $this->service->getInfo(['id', 'following'], ['id' => $this->userId]);

        if(!$followerData){
            return false;
        }
        
        $followData = $followService->getInfo(['*'], ['account_id_1' => $this->userId, 'account_id_2' => $this->id]);
        if($followData){
            return false;
        }
        $data = [
            'account_id_1' => $this->userId,
            'account_id_2' => $this->id,
        ];
        
        $this->service->saveData([
            'id'        => $followerData->id,
            'followers' => $followerData->followers += 1,
        ]);

        $this->service->saveData([
            'id'        => $followingData->id,
            'following' => $followingData->following += 1,
        ]);
        
        $followId = $followService->saveData($data);

        // create and push notification 
        $updateNotificationService->setUser($this->userId);
        $updateNotificationService->setFollow($followId);
        $updateNotificationService->setType(NEW_FOLLOWER);
        $updateNotificationService->createNotification();

        return $followId;
    }

    public function unfollow(){
        $followService = new FollowService();
        $notificationService = new NotificationService();
        $notificationAlias = $notificationService->getModel()->alias;
        $this->id = $this->request->getGet("user");
        if($this->id == $this->userId){
            return false;
        }
        
        $followerData = $this->service->getInfo(['id', 'followers'], ['id' => $this->id]);
        $followingData = $this->service->getInfo(['id', 'following'], ['id' => $this->userId]);

        $followData = $followService->getModel()->where(['account_id_1' => $this->userId, 'account_id_2' => $this->id])->first();
        if(!$followData){
            return false;
        }
        
        $notificationData = $notificationService->getInfo(
            [
                $notificationAlias.'.id'
            ],
            [
                'follow_id' => $followData->id,
            ]
        );
        $notificationService->saveData(
            [
                'id'            => $notificationData->id,
                'deleted_by'    => $this->userId,
                'deleted_at'    => date('Y-m-d H:i:s'),
                'is_deleted'    => DEL_FLG_ON, 
            ]
        );
        
        $this->service->saveData([
            'id'        => $followerData->id,
            'followers' => $followerData->followers -= 1,
        ]);
        $this->service->saveData([
            'id'        => $followingData->id,
            'following' => $followingData->following -= 1,
        ]);

        $followService->getModel()->delete($followData->id);
        return true;
    }

    public function likePost(){
        $postId = $this->request->getGet("post");
        $likePostService = new LikePostService();
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;
        $updateNotificationService = new UpdateNotificationService();

        $selects = [
            $postAlias.'.id as post_id',
            $postAlias.'.like_quantity',
            $postAlias.'.created_by',
        ];

        $postData = $postService->getInfo($selects, ['id' => $postId, 'is_locked' => LOCK_FLG_OFF]);

        // check if post exist or not
        if(!$postData){
            return false;
        }

        $likePostData = $likePostService->getInfo(['*'], ['post_id' => $postId, 'account_id' => $this->userId]);

        if($likePostData){
            return false;
        }
        
        $postService->saveData([
            'id'            => $postData->post_id,
            'like_quantity' => $postData->like_quantity += 1,
        ]);
        $data = [
            'account_id' => $this->userId,
            'post_id'    => $postId
        ];

        $likePostId = $likePostService->saveData($data);

        // create and push noti if the one whose like post is not the author 
        if($postData->created_by != $this->userId){
            $updateNotificationService->setUser($this->userId);
            $updateNotificationService->setLikePost($likePostId);
            $updateNotificationService->setType(LIKE_POST);
            $updateNotificationService->createNotification();
        }

        return $likePostId;
    }
    
    public function unlikePost(){
        $likePostService = new LikePostService();
        $postId = $this->request->getGet("post");
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;
        $notificationService = new NotificationService();
        $notificationAlias = $notificationService->getModel()->alias;

        $selects = [
            $postAlias.'.id as post_id',
            $postAlias.'.like_quantity',
        ];

        $postData = $postService->getInfo($selects, ['id' => $postId]);

        $likePostData = $likePostService->getModel()->where(['account_id' => $this->userId, 'post_id' => $postId])->first();
        if(!$likePostData){
            return false;
        }

        $notificationData = $notificationService->getInfo(
            [
                $notificationAlias.'.id'
            ],
            [
                'like_post_id' => $likePostData->id,
            ]
        );
        if($notificationData){
            $notificationService->saveData(
                [
                    'id'            => $notificationData->id,
                    'deleted_by'    => $this->userId,
                    'deleted_at'    => date('Y-m-d H:i:s'),
                    'is_deleted'    => DEL_FLG_ON, 
                ]
            );
        }
        
        $postService->saveData([
            'id'            => $postData->post_id,
            'like_quantity' => $postData->like_quantity -= 1,
        ]);

        $likePostService->getModel()->delete($likePostData->id);
        return true;
    }

    public function addFavoritePost(){
        $postId = $this->request->getGet("post");
        $favoriteService = new FavoriteService();
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;
        $postData = $postService->getInfo(['*'], ['id' => $postId, 'locked_post' => LOCK_FLG_OFF]);

        if(!$postData){
            return false;
        }

        $favoritetData = $favoriteService->getInfo(['*'], ['post_id' => $postId, 'account_id' => $this->userId]);
        if(!$favoritetData){
            $data = [
                'account_id' => $this->userId,
                'post_id'    => $postId
            ];
            return $favoriteService->saveData($data);
        }

        return;
    }

    public function removeFavoritePost(){
        $favoriteService = new FavoriteService();
        $postId = $this->request->getGet("post");
        $favoriteData = $favoriteService->getModel()->where(['account_id' => $this->userId, 'post_id' => $postId]);
        if($favoriteData){
            $favoriteData->delete();
            return true;
        }
        return false;
    }

    public function changeAvatar(){
        $avatar = $this->request->getFile('avatar');
        $accountAlias = $this->service->getModel()->alias;
        $userData = $this->service->getInfo([$accountAlias.'.*'], ['id' => $this->id]);
        if($avatar && $avatar->isValid()){
            @unlink($userData->avatar);
            $avatarName = $avatar->getRandomName();
            $avatar->move('upload/users/'.$this->id.'/avatar', $avatarName);
    
            $data = [];
            $data['id'] = $this->id;
            $data['avatar'] = 'upload/users/'.$this->id.'/avatar/'.$avatarName;
            $this->service->saveData($data);
    
            return ['avatar' => $data['avatar']];
        }
        return false;
    }

    public function editProfile(){
        $avatar = $this->request->getFile('avatar');
        $input = $this->request->getPost();
        
        if (empty($input)) {
			//convert request body to associative array
			$input = json_decode($this->request->getBody(), true);
		}
        $accountAlias = $this->service->getModel()->alias;
        $data = $input;
        $data['id'] = $this->id;
        $userData = $this->service->getInfo([$accountAlias.'.*'], ['id' => $this->userId]);

        if(!empty($input)){
            foreach($input as $key => $value){
                if($key == 'password' || $key == 'email' || $key == 'phone' || $key == 'g_id' || $key == 'f_id' ){
                    unset($data[$key]);
                }
            }
        }

        if($avatar && $avatar->isValid()){
            @unlink($userData->avatar);
            $avatarName = $avatar->getRandomName();
            $avatar->move('upload/users/'.$this->id.'/avatar', $avatarName);
            $data['avatar'] = 'upload/users/'.$this->id.'/avatar/'.$avatarName;
        }

        $data['updated_by'] = $this->id;
        $data['updated_at'] = date('Y-m-d H:i:s');
        $this->id = $this->service->saveData($data);

        return $this->accountDetail();
    }

    public function getListFollowing(){
        helper('convert_num');
        $this->id = $this->request->getGet('user');
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;

        $accountAlias = $this->service->getModel()->alias;
        $followService = new FollowService();
        $followAlias = $followService->getModel()->alias;
        $accountAlias = $this->service->getModel()->alias;

        $selects = [
            $accountAlias.'_2.id AS user_id',
            $accountAlias.'_2.user_name AS user_name',
            $accountAlias.'_2.avatar AS avatar',
        ];
        $where = [
            $followAlias.'.account_id_1' => $this->id,
        ];

        $data = $followService->getData($selects, $where, $page, $limit);
        $countResult = 0;
        foreach($data as $key => $value){
            $countResult ++;
            $value->followers = $followService->countResult(
                // where statement
                [
                    'account_id_2' => $this->id,
                ]
            );
            $value = convertNumData($value);
        }
        
        $pagination = [
            'page'  => $page,
            'limit' => $limit,
            'total' => $countResult,
        ];
        $pagination = convertNumData($pagination);

        return ['data' => $data, 'pagination' =>$pagination];
    }   

    public function getListFollower(){
        helper('convert_num');
        $this->id = $this->request->getGet('user');
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;

        $accountAlias = $this->service->getModel()->alias;
        $followService = new FollowService();
        $followAlias = $followService->getModel()->alias;
        $accountAlias = $this->service->getModel()->alias;

        $selects = [
            $accountAlias.'_1.id AS user_id',
            $accountAlias.'_1.user_name AS user_name',
            $accountAlias.'_1.avatar AS avatar',
        ];
        $where = [
            $followAlias.'.account_id_2' => $this->id,
        ];

        $data = $followService->getData($selects, $where, $page, $limit);

        $countResult = 0;
        foreach($data as $key => $value){
            $countResult ++;
            $value->followers = $followService->countResult(
                // where statement
                [
                    'account_id_2' => $this->id,
                ]
            );
            $value = convertNumData($value);
        }
        $pagination = [
            'page'  => $page,
            'limit' => $limit,
            'total' => $countResult,
        ];
        $pagination = convertNumData($pagination);

        return ['data' => $data, 'pagination' =>$pagination];
    }

    public function getListFavoritePost(){
        helper('convert_num');
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;

        $favoriteService = new FavoriteService();
        $favoriteAlias = $favoriteService->getModel()->alias;
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;

        $selectsFavorite = [
            $favoriteAlias.'.post_id',
        ];
        $whereFavorite = [
            $favoriteAlias.'.account_id' => $this->userId,
        ];

        $favoriteData = $favoriteService->getData($selectsFavorite, $whereFavorite, $page, $limit);

        $postData = [];
        $countResult = 0;
        foreach($favoriteData as $key => $value){
            $countResult ++;
            $data = $postService->getInfo(
                [// select
                    $postAlias.'.id AS post_id',
                    $postAlias.'.title AS post_title',
                    $postAlias.'.post_image'
                ], 
                [//where
                    'id' => $value->post_id,
                ]
            );
            $data->post_image = [json_decode($data->post_image)[0]];
            array_push($postData, $data);
        }

        $pagination = [
            'page'  => $page,
            'limit' => $limit,
            'total' => $countResult,
        ];
        $pagination = convertNumData($pagination);

        return ['data' => $postData, 'pagination' => $pagination];
    }
    
    public function getTopFollower(){
        helper('convert_num');
        $followService = new FollowService();
        $followAlias = $followService->getModel()->alias;
        $accountAlias = $this->service->getModel()->alias;
        $postService = new PostService();
        $updatePostService = new UpdatePostService();
        $postAlias = $postService->getModel()->alias; 
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;

        if($page != 0 && $limit != 0){
            $page = $page * $limit;
            $builder = $followService->createQuery();
            $builder->selectCount($followAlias.'.account_id_2', 'followers')
                    ->select($followAlias.'.account_id_2 AS account_id')
                    ->groupBy($followAlias.'.account_id_2')
                    ->orderBy('followers' , "DESC")
                    ->limit($limit, $page);
            $followData = $builder->get()->getResultObject();
        }else{
            $builder = $followService->createQuery();
            $builder->selectCount($followAlias.'.account_id_2', 'followers')
                    ->select($followAlias.'.account_id_2 AS account_id')
                    ->groupBy($followAlias.'.account_id_2')
                    ->orderBy('followers' , "DESC");
            $followData = $builder->get()->getResultObject();
        }

        $userData = [];
        $countResult = 0;
        foreach($followData as $key => $value){
            // if user is logged in and that user is on the top follower user then don't return that user data
            if($value->account_id == $this->userId){
                continue;
            }

            $data = $this->service->getInfo(
                [
                    $accountAlias.'.id AS user_id',
                    $accountAlias.'.user_name',
                    $accountAlias.'.avatar',
                ],
                [
                    'id' => $value->account_id,
                ]
            );
            
            if($this->userId){
                $data->isFollowed = false;
                if($followService->getModel()->where(['account_id_1' => $this->userId, 'account_id_2' => $data->user_id])->first()){
                    $data->isFollowed = true;
                }
            }

            $updatePostService->setUser($data->user_id);
            $data->listPost = $updatePostService->postByUser()['data'];

            $data->followers = $value->followers;
            $data = convertNumData($data);
            $data->listPost = convertNumData($data->listPost);
            array_push($userData, $data);
            $countResult ++;
        }

        $pagination = [
            'page'  => $page,
            'limit' => $limit,
            'total' => $countResult,
        ];
        $pagination = convertNumData($pagination);

        return ['data' => $userData, 'pagination' => $pagination];
    }

    public function searchAccount(){
        helper('convert_num');
        $key = $this->request->getGet('key');
        $province = is_numeric($this->request->getGet('province')) ? $this->request->getGet('province') : "0";
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 3;

        $accountAlias = $this->service->getModel()->alias;
        $followService = new FollowService();

        $selects = [
            $accountAlias.'.id AS user_id',
            $accountAlias.'.user_name',
            $accountAlias.'.avatar',
            $accountAlias.'.rank',
        ];

        if($province){
            $data = $this->service->createQuery()->select($selects)
                                  ->like([$accountAlias.'.user_name' => $key])
                                  ->where([$accountAlias.'.province' => $province])
                                  ->limit($limit, $page != 0 ? $page * $limit : $page)
                                  ->orderBy($accountAlias.'.id', 'DESC')
                                  ->get()
                                  ->getResultObject();
        }else{
            $data = $this->service->createQuery()->select($selects)
                                  ->like([$accountAlias.'.user_name' => $key])
                                  ->limit($limit, $page != 0 ? $page * $limit : $page)
                                  ->orderBy($accountAlias.'.id', 'DESC')
                                  ->get()
                                  ->getResultObject();
        }

        $countResult = 0;                                  
        foreach($data as $key => $value){
            $countResult ++;
            if($this->userId){
                $value->isFollowed = false;
                if($followService->getModel()->where(['account_id_1' => $this->userId, 'account_id_2' => $value->user_id])->first()){
                    $value->isFollowed = true;
                }
            }
            $value->followes = $followService->countResult('account_id_2 = '.$value->user_id);
            $value = convertNumData($value);
        }
        
        $pagination = [
            'page'  => $page,
            'limit' => $limit,
            'total' => $countResult,
        ];
        $pagination = convertNumData($pagination);

        return ['pagination' => $pagination, 'data' => $data];
    }
}
