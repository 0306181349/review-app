<?php 
namespace App\Services\Category;
use App\Services\Category\CategoryService;

class UpdateCategoryService
{
    protected $id = 0;
    protected $userId = 0;
	protected $request;

    
    public function __construct(){
        $this->service = new CategoryService();
        helper('convert_num');
    }

    //set modify account id
    public function setId($id){
		return $this->id = $id;
	}

    //set user id who is modifying the account info 
    public function setUser($userId){
        return $this->userId = $userId;
    }
    
	public function setRequest($request){
		return $this->request = $request;
	}

    public function setAction($action){
        return $this->{$action}();
    }

    public function index(){
        $categoryAlias = $this->service->getModel()->alias;
        $selects = [
            $categoryAlias.'.id',
            $categoryAlias.'.category_name',
            $categoryAlias.'.created_at',
        ];
        $data = $this->service->getData($selects);
        return $data;
    }

    public function getAllCategory(){
        $categoryAlias = $this->service->getModel()->alias;
        $selects = [
            $categoryAlias.'.id AS category_id',
            $categoryAlias.'.category_name',
        ];
        
        $data = $this->service->getData($selects, []);
        foreach($data as $key => $value){
            $value = convertNumData($value);
        }

        return $data;
    }

    public function getCategoryData(){
        $categoryAlias = $this->service->getModel()->alias;
        $selects = [
            $categoryAlias.'.id',
            $categoryAlias.'.category_name',
            $categoryAlias.'.created_at',
        ];
        $where = [
            'id' => $this->id,
        ];
        $data = $this->service->getInfo($selects, $where);
        return $data;
    }
    
    public function add(){  
        $input = $this->request->getPost();
        if (empty($input)) {
			//convert request body to associative array
			$input = json_decode($this->request->getBody(), true);
		}

        $data = [
            'category_name' => $input['category_name'],
            'created_by'    => $this->userId,
        ];

        $this->id = $this->service->saveData($data);
        return $this->getCategoryData();
    }

    public function edit(){
        $input = $this->request->getPost();
        if (empty($input)) {
			//convert request body to associative array
			$input = json_decode($this->request->getBody(), true);
		}
        $provinceAlias = $this->service->getModel()->alias;
        $data = $this->service->getInfo(
            [
                $provinceAlias.'.id'
            ],
            [
                'id' => $this->id
            ]
        );
        if(!$data){
            return false;
        }
        $data = [
            'id'            => $this->id,
            'category_name' => $input['category_name'],
            'updated_by'    => $this->userId,
            'updated_at'    => date('Y-m-d H:i:s'),
        ];

        $this->id = $this->service->saveData($data);
        return $this->getCategoryData();
    }

    public function delete(){
        $categoryAlias = $this->service->getModel()->alias;

        $data = $this->service->getInfo(
            [
                $categoryAlias.'.id'
            ],
            [
                'id' => $this->id
            ]
        );
        if(!$data){
            return false;
        }
        $data = [
            'id' => $this->id,
            'is_deleted' => DEL_FLG_ON,
            'deleted_by' => $this->userId,
            'deleted_at' => date('Y-m-d H:i:s'),
        ];
        $this->service->saveData($data);
        return true;
    }
}