<?php 
namespace App\Services\Dashboard;
use App\Services\Post\PostService;
use App\Services\Like\LikePostService;
use App\Services\Account\AccountService;
use App\Services\Location\LocationService;
use App\Services\Province\ProvinceService;
use App\Services\Follow\FollowService;

class DashboardService
{
    protected $id = 0;
    protected $userId = 0;
	protected $request;

    public function __construct(){
        helper('convert_num');
        $this->service = new PostService();
    }

    //set modify account id
    public function setId($id){
		return $this->id = $id;
	}

    //set user id who is modifying the account info 
    public function setUser($userId){
        return $this->userId = $userId;
    }
    
	public function setRequest($request){
		return $this->request = $request;
	}

    public function setAction($action){
        return $this->{$action}();
    }

    public function index(){
        $activeUser = $this->getTopUserByPost(3); // get top 3 user by there post
        $topPost = $this->getTopPostByLike(3); // get top 3 post by like
        $topProvince = $this->getTopProvinceByPost(6);
        // var_dump($activeUser);
        // die();
        $date = $this->getPreMonthDays();
        $labels = [];
        $province_labels = [];
        $province_values = [];
        foreach($date as $key => $value){
            array_push($labels, substr($value[0], 5)." - ".substr($value[1], 5));
            $date[$key][0] = date('Y-m-d H:i:s', strtotime($value[0]));
            $date[$key][1] = date('Y-m-d H:i:s', strtotime($value[1].' 23:59:59'));
        }

        foreach($topProvince as $key => $value){
            array_push($province_labels, $value->province_label);
            array_push($province_values, $value->post_by_province);
        }

        $data = [
            'total_location'        => $this->getTotalLocation(),
            'total_post'            => $this->getTotalPost(),
            'total_user'            => $this->getTotalUser(),
            'user_report_labels'    => $labels,
            'user_report_values'    => $this->getNewUserPreMonth($date),
            'post_report_labels'    => $labels,
            'post_report_values'    => $this->getNewPostPreMonth($date),
            'top_user'              => $activeUser,
            'top_post'              => $topPost,
            'top_province_label'    => $province_labels,
            'top_province_values'   => $province_values,
            'this_month_post'       => $this->getLastMonthPost(),
            'this_month_user'       => $this->getLastMonthUser(),
            'this_month_location'   => $this->getLastMonthLocation(),
        ];
        return $data;
    }

    public function getFirstDayAndLastDayOfLastMonth(){
        $lastMonth = date("m") - 1;
        $thisYear = date("Y"); // or last year if last month == 12 
        if($lastMonth == 12){
            $thisYear -= 1;
        }
        $numOfDaysOfLastMonth = cal_days_in_month(CAL_GREGORIAN, $lastMonth, $thisYear);
        $beginDate = date("Y-m-d", strtotime($thisYear.'-'.$lastMonth.'-1'));
        $endDate = date("Y-m-d", strtotime($thisYear.'-'.$lastMonth.'-'.$numOfDaysOfLastMonth));
        return [
            $beginDate,
            $endDate
        ];
    }

    public function getLastMonthPost(){
        $postService = new PostService();
        
        $date = $this->getFirstDayAndLastDayOfLastMonth();
        $where = $postService->getModel()->alias.".created_at BETWEEN '".$date[0]."' AND '".$date[1]."'";

        $data = $postService->countResult($where);
        return $data;
    }

    public function getLastMonthLocation(){
        $locationService = new LocationService();

        $date = $this->getFirstDayAndLastDayOfLastMonth();
        $where = $locationService->getModel()->alias.".created_at BETWEEN '".$date[0]."' AND '".$date[1]."'";
        $data = $locationService->countResult($where);
        return $data;
    }

    public function getLastMonthUser(){
        $accountService = new AccountService();

        $date = $this->getFirstDayAndLastDayOfLastMonth();

        $where = $accountService->getModel()->alias.".created_at BETWEEN '".$date[0]."' AND '".$date[1]."'";
        $data = $accountService->countResult($where);
        return $data;
    }

    public function getTotalUser(){
        $accountService = new AccountService();
        return $accountService->countResult();
    }
 
    public function getTotalPost(){
        $postService = new PostService();
        return $postService->countResult();
    }

    public function getTotalLocation(){
        $locationService = new LocationService();
        return $locationService->countResult();
    }

    /**
     * This function will return a couple of date of monday or first day of month
     * and the date of sunday or the last day of month as an array 
     * Example in August 2021 return array will be 
     * [
     *   08-01-2021, 08-01-2021,
     *   08-02-2021, 08-08-2021,
     *   08-09-2021, 08-15-2021,
     *   08-16-2021, 08-22-2021,
     *   08-23-2021, 08-29-2021,
     *   08-30-2021, 08-31-2021,
     * ]
     * As we use these date to genarate the data for dashboard so the month would be current month - 1
     */
    public function getPreMonthDays(){
        $today = date('Y-m-d');
        $datestring = $today.' first day of last month';
        $dt = date_create($datestring);
        $previousMonth = $dt->format('m');
        // $previousMonth = date('m', strtotime("-1 months"));
        $current_year = date('Y');  
        $first_day_of_month = $current_year."-".$previousMonth.'-'."01";
        $last_day_of_month = date("Y-m-t", strtotime($first_day_of_month));
        
        // begin at first day of last month
        $day_1 = $first_day_of_month;
        $day_2 = $first_day_of_month;
        $day_arr = [];
        while(true){
            $date_of_week = date('w', strtotime($day_1));
            if($date_of_week == 0){
                array_push($day_arr, [$day_1, $day_1]);
                $day_1 = date("Y-m-d", strtotime($day_2. " +1 day"));
                continue;
            }
            $day_2 = date("Y-m-d", strtotime($day_1. " +".(7 - $date_of_week)." days"));

            if( $day_2 >= $last_day_of_month ){
                array_push($day_arr, [$day_1, date("Y-m-d", strtotime($last_day_of_month))]);
                break;
            }
            array_push($day_arr, [$day_1, $day_2]);
            $day_1 = date("Y-m-d", strtotime($day_2. " +1 day"));
        }
        return $day_arr;
    }

    public function getNewUserPreMonth($days = []){
        if(empty($days)){
            return;
        }
        $accountService = new AccountService();
        $data = [];
        foreach($days as $key => $value){
            $where = $accountService->getModel()->alias.".created_at BETWEEN '".$value[0]."' AND '".$value[1]."'";
            array_push($data, $accountService->countResult($where));
        } 
        return $data;
    }

    public function getNewPostPreMonth($days = []){
        if(empty($days)){
            return;
        }
        $postService = new PostService();
        $data = [];
        foreach($days as $key => $value){
            $where = $postService->getModel()->alias.".created_at BETWEEN '".$value[0]."' AND '".$value[1]."'";
            array_push($data, $postService->countResult($where));
        } 
        return $data;
    }

    public function getTopUserByPost($top){
        $postService = new PostService();
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;

        $data = $postService->createQuery()->select('COUNT( '.$accountAlias.'.id ) AS post_quantity, '
                                                             .$accountAlias.'.id AS account_id, '
                                                             .$accountAlias.'.user_name, '
                                                             .$accountAlias.'.avatar, '
                                                             .$accountAlias.'.created_at')
                                              ->groupBy($accountAlias.'.id')
                                              ->orderBy('post_quantity', 'DESC')
                                              ->limit($top, 0)
                                              ->get()
                                              ->getResultObject();
        return $data;
    }

    public function getTopPostByLike($top){
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;
        $likePostService = new LikePostService();

        $data = $likePostService->createQuery()->select('COUNT( '.$postAlias.'.id ) AS like_quantity, '
                                                             .$postAlias.'.id AS post_id, '
                                                             .$postAlias.'.title, '
                                                             .$postAlias.'.created_at, '
                                                             .$postAlias.'.post_image')
                                              ->groupBy($postAlias.'.id')
                                              ->orderBy('like_quantity', 'DESC')
                                              ->limit($top, 0)
                                              ->get()
                                              ->getResultObject();

        foreach($data as $key => $value){
            if($value->post_image){
                $value->post_image = json_decode($value->post_image)[0];
            }
        }
        return $data;
    }

    public function getTopProvinceByPost($top){
        $provinceService = new ProvinceService();
        $provinceAlias = $provinceService->getModel()->alias;
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;

        $data = $postService->createQuery()->select('COUNT( '.$provinceAlias.'.id ) AS post_by_province, '
                                                             .$provinceAlias.'.id province_id, '
                                                             .$provinceAlias.'.province_name AS province_label')
                                            ->groupBy($provinceAlias.'.id')
                                            ->orderBy('post_by_province', 'DESC')
                                            ->limit($top, 0)
                                            ->get()
                                            ->getResultObject();

        return $data;
    }

    public function getTopLocationByMonth(){
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 10;
        
        if($page != 0){
            $page = $page * $limit;
        }
        
        $date = $this->getFirstDayAndLastDayOfLastMonth();
        $locationService = new LocationService();
        $locationAlias = $locationService->getModel()->alias;
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;
        $likePostService = new LikePostService();
        $likePostAlias = $likePostService->getModel()->alias;
        
        $data = $postService->createQuery()->select(
                                                    [
                                                        $locationAlias.'.id AS location_id',
                                                        // 'COUNT('.$likePostAlias.'.post_id) AS like_quantity',
                                                        'COUNT('.$postAlias.'.id) AS num_of_post',
                                                        $locationAlias.'.location_name',
                                                        $locationAlias.'.address',
                                                        'AVG('.$postAlias.'.rating) AS location_rating',  // avg rating of location
                                                    ]
                                                )
                                               ->where($postAlias.".created_at BETWEEN '".$date[0]."' AND '".$date[1]."'")
                                                // ->groupBy('like_quantity')
                                               ->groupBy($locationAlias.'.id')
                                               ->orderBy('location_rating', 'DESC')
                                            //    ->having('num_of_post > 1')
                                               ->orderBy($locationAlias.'.id', 'DESC')
                                               ->limit($limit, $page)
                                               ->get()
                                               ->getResultObject();

        foreach($data as $key => $value){
            $postData = $postService->getInfo([$postAlias.".post_image"], ["location" => $value->location_id]);
            if($postData && $postData->post_image != NULL){
                $value->location_image = [json_decode($postData->post_image, true)[0]];
            }else{
                $value->location_image = [];
            }
            $value = convertNumData($value);
            $value->location_rating = round($value->location_rating, 1);
        }              
        return $data;                      
    }

    public function getTopUserByMonth(){
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 10;
        
        if($page != 0){
            $page = $page * $limit;
        }

        $date = $this->getFirstDayAndLastDayOfLastMonth();
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;
        $likePostService = new LikePostService();
        $likePostAlias = $likePostService->getModel()->alias;
        $followService = new FollowService();
        $likePostService = new LikePostService();
        $likePostTable = $likePostService->getModel()->table;
        $likePostAlias = $likePostService->getModel()->alias;
        $data = $postService->createQuery()->join($likePostTable. ' AS '.$likePostAlias, $postAlias.'.id = '.$likePostAlias.'.post_id', 'left')
                                           ->select(
                                                [
                                                    $accountAlias.'.id AS account_id',
                                                    $accountAlias.'.user_name',
                                                    $accountAlias.'.avatar',
                                                    'COUNT('.$likePostAlias.'.id) AS like_quantity',
                                                ]
                                           )
                                           ->where($postAlias.".created_at BETWEEN '".$date[0]."' AND '".$date[1]."'")
                                           ->groupBy($accountAlias.'.id')
                                           ->orderBy('like_quantity', 'DESC')
                                           ->limit($limit, $page)
                                           ->get()
                                           ->getResultObject();

        foreach($data as $key => $value){
            $numOfImage = 0;
            $post_image = [];
            $value->post_image = [];
            $user = $value;
            if($this->userId){// user request is logged in
                // get isFollowed to indentify that the current user(which is perform this request) is followed this user or not 
                $value->isFollowed = false;
                if($followService->getModel()->where(['account_id_1' => $this->userId, 'account_id_2' => $value->account_id])->first()){
                    $value->isFollowed = true;
                }
            }
            while(true){
                $postData = $postService->getData([$postAlias.".post_image"], [$postAlias.".created_by" => $user->account_id], 1, $numOfImage);
                if($postData[0] && $postData[0]->post_image != NULL){
                    $image = json_decode($postData[0]->post_image, true);
                    foreach($image as $key => $value2){
                        array_push($post_image, $value2);
                        $numOfImage++;
                        if($numOfImage == 4){
                            break 2;
                        }
                    }
                }else{
                    if(!$postData[0]){
                        break;
                    }
                    continue;
                }
            }
            $value->post_image = $post_image;
            $value = convertNumData($value);
        }                 
        return $data;         
    }
}