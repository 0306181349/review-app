<?php 
namespace App\Services\Notification;
use App\Services\Notification\NotificationService;
use App\Services\Account\AccountService;
use App\Services\Post\PostService;
use App\Services\Comment\CommentService;
use App\Services\Like\LikePostService;
use App\Services\Like\LikeCommentService;
use App\Services\Follow\FollowService;

class UpdateNotificationService
{
    protected $id = 0;
    protected $userId = 0;
	protected $request;
    protected $type = 0;
    protected $postId = 0;
    protected $commentId = 0;
    protected $followId = 0;
    protected $likePostId = 0;
    protected $likeCommentId = 0;

    public function __construct(){
        $this->service = new NotificationService();
        helper('convert_num');
    }
    
    //set modify account id
    public function setId($id){
		return $this->id = $id;
	}

    //set user id who is modifying the account info 
    public function setUser($userId){
        return $this->userId = $userId;
    }
    
    public function setType($type){
        return $this->type = $type;
    }

    public function setPost($postId){
        return $this->postId = $postId;
    }

    public function setComment($commentId){
        return $this->commentId = $commentId;
    }

    public function setFollow($followId){
        return $this->followId = $followId;
    }

    public function setLikePost($likePostId){
        return $this->likePostId = $likePostId;
    }

    public function setLikeComment($likeCommentId){
        return $this->likeCommentId = $likeCommentId;
    }

	public function setRequest($request){
		return $this->request = $request;
	}

    public function setAction($action){
        return $this->{$action}();
    }

    public function getAllNotificationByPage($flag = 0){
        $page = is_numeric($this->request->getGet('page')) ? $this->request->getGet('page') : 0;
        $limit = is_numeric($this->request->getGet('limit')) ? $this->request->getGet('limit') : 0;

        $notificationAlias = $this->service->getModel()->alias;
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;
        $postService = new PostService();
        $postAlias = $postService->getModel()->alias;
        $commentService = new CommentService();
        $commentAlias = $commentService->getModel()->alias;
        $likePostService = new LikePostService();
        $likePostAlias = $likePostService->getModel()->alias;
        $likeCommentService = new LikeCommentService();
        $likeCommentAlias = $likeCommentService->getModel()->alias;

        switch($flag){
            case 1:
                $where = [
                    $notificationAlias.'.account_id' => $this->userId,
                    $notificationAlias.'.created_at >=' => date("Y-m-d")." 00:00:00",
                ];
                break;
            case 2:
                $where = [
                    $notificationAlias.'.account_id' => $this->userId,
                    $notificationAlias.'.created_at < ' => date("Y-m-d")." 00:00:00",
                ];
                break;
            default:
                $where = [
                    $notificationAlias.'.account_id' => $this->userId,
                ];
                break;
        }
        
        $data = $this->service->getData(
            [
                // select
                $notificationAlias.'.id AS notification_id',
                $notificationAlias.'.title AS notification_title',
                $notificationAlias.'.type',
                $notificationAlias.'.clicked',
                $notificationAlias.'.seen',
                $notificationAlias.'.follow_id',
                $notificationAlias.'.post_id',
                $notificationAlias.'.comment_id',
                $notificationAlias.'.like_comment_id',
                $notificationAlias.'.like_post_id',
                $notificationAlias.'.created_by',
                $notificationAlias.'.created_at',
                $notificationAlias.'.account_id',

            ],
            $where,
            $page, 
            $limit
        );

        foreach($data as $key => $value){
            $value = convertNumData($value);

            if($value->clicked){
                $value->clicked = true;
            }else{
                $value->clicked = false;
            }

            if($value->seen){
                $value->seen = true;
            }else{
                $value->seen = false;
            }

            $value->user = convertNumData($accountService->getInfo(
                [
                    $accountAlias.'.id AS user_id',
                    $accountAlias.'.user_name',
                    $accountAlias.'.avatar'
                ],
                [
                    'id' => $value->created_by,
                ]
            ));
            switch($value->type){
                case NEW_FOLLOWER:
                    unset($value->comment_id);
                    unset($value->post_id);
                    // some one followed you
                    break;
                case NEW_COMMENT: 
                    // some one commented on your post
                    $commentData = convertNumData($commentService->getInfo(
                        [
                            // select
                            $postAlias.'.id AS post_id',
                            $commentAlias.'.id AS comment_id'
                        ],
                        [
                            // where
                            'id' => $value->comment_id
                        ]
                    ));
                    $value->post_id = $commentData->post_id;

                    $value->comment_id = $commentData->comment_id;
                    break;
                case NEW_REPLY_COMMENT: 
                    // some one replied your comment
                    $commentData = convertNumData($commentService->getInfo(
                        [
                            // select
                            $postAlias.'.id AS post_id',
                            // $commentAlias.'.id AS comment_id',
                            $commentAlias.'.comment_id',
                        ],
                        [
                            // where
                            'id' => $value->comment_id
                        ]
                    ));
                    $value->post_id = $commentData->post_id;

                    $value->comment_id = $commentData->comment_id;

                    // $value->reply_comment_id = $commentData->reply_comment_id;
                    break;
                case NEW_POST:
                    // your friend added a new post
                    $value->post_id = convertNumData($postService->getInfo(
                        [
                            $postAlias.'.id AS post_id',
                        ],
                        [
                            'id' => $value->post_id
                        ]
                    ))->post_id;
                    unset($value->comment_id);
                    break; 
                case LIKE_POST:
                    // some one liked your post
                    $value->post_id = convertNumData($likePostService->getInfo(
                        [
                            $likePostAlias.'.post_id'
                        ],
                        [
                            'id' => $value->like_post_id,
                        ]
                    ))->post_id;

                    unset($value->comment_id);
                    break;
                case LIKE_COMMENT:
                    // some one liked your comment
                    $likeCommentData = $likeCommentService->getInfo(
                        [
                            $likeCommentAlias.'.id AS like_comment_id',
                            $likeCommentAlias.'.comment_id AS comment_id',
                        ],
                        [
                            'id' => $value->like_comment_id
                        ]
                    );
                    $commentData = convertNumData($commentService->getInfo(
                        [
                            $postAlias.'.id AS post_id',
                            $commentAlias.'.id AS comment_id',
                            $commentAlias.'.comment_id AS reply_comment_id',
                        ],
                        [
                            'id' => $likeCommentData->comment_id,
                        ]
                    ));
                    if($commentData->reply_comment_id != NULL){
                        $value->comment_id = $commentData->reply_comment_id;
                    }else{
                        $value->comment_id = $commentData->comment_id;
                    }
                    $value->post_id = $commentData->post_id;
                    
                    break;
                case LOCK_POST:
                    $postData = convertNumData($postService->getInfo(
                        [
                            $postAlias.'.id AS post_id',
                        ],
                        [
                            'id' => $value->post_id
                        ]
                    ));
                    $value->post_id = $postData->post_id;
                    unset($value->user);
                    unset($value->comment_id);
                    break;
                case UNLOCK_POST:
                    $postData = convertNumData($postService->getInfo(
                        [
                            $postAlias.'.id AS post_id',
                        ],
                        [
                            'id' => $value->post_id
                        ]
                    ));
                    $value->post_id = $postData->post_id;
                    unset($value->user);
                    unset($value->comment_id);
                    break;
                case UNLOCK_POST_REJECT:
                    $postData = convertNumData($postService->getInfo(
                        [
                            $postAlias.'.id AS post_id',
                        ],
                        [
                            'id' => $value->post_id
                        ]
                    ));
                    $value->post_id = $postData->post_id;
                    unset($value->user);
                    unset($value->comment_id);
                    break;
                default: 
                    break;
            }
            
            unset($value->follow_id);
            unset($value->created_by);
            unset($value->like_comment_id);
            unset($value->like_post_id);
            unset($value->account_id);
        }
        return $data;
    }

    public function getEarlierNotification(){
        return $this->getAllNotificationByPage(2);
    }

    public function getNewNotification(){
        return $this->getAllNotificationByPage(1);
    }

    public function countUnseenNotification(){
        $notificationAlias = $this->service->getModel()->alias;

        $data = $this->service->countResult(
            [
                //where
                $notificationAlias.'.account_id' => $this->userId,
                $notificationAlias.'.seen' => UNSEEN,
            ]
        );

        return $data;
    }

    public function checkClikedAllNotification(){
        $notificationAlias = $this->service->getModel()->alias;

        $builder = $this->service->createQuery()
                                 ->where(
                                     [
                                         $notificationAlias.'.account_id' => $this->userId,
                                         $notificationAlias.'.clicked'    => UNCLICKED
                                     ]
                                 )
                                 ->update(
                                     [
                                         $notificationAlias.'.clicked' => CLICKED,
                                     ]
                                 );
        
        return $builder;
    }

    public function checkClickedNotification(){
        $noti_id = is_numeric($this->request->getGet('noti')) ? $this->request->getGet('noti') : '0';
        $notificationAlias = $this->service->getModel()->alias;

        $builder = $this->service->createQuery()
                                 ->where(
                                     [
                                         $notificationAlias.'.account_id' => $this->userId,
                                         $notificationAlias.'.id'    => $noti_id
                                     ]
                                 )
                                 ->update(
                                     [
                                         $notificationAlias.'.clicked' => CLICKED,
                                     ]
                                 );
        
        return $builder;
    }

    public function checkSeenAllNotification(){
        $notificationAlias = $this->service->getModel()->alias;

        $builder = $this->service->createQuery()
                                 ->where(
                                     [
                                         $notificationAlias.'.account_id' => $this->userId,
                                         $notificationAlias.'.seen'    => UNSEEN
                                     ]
                                 )
                                 ->update(
                                     [
                                         $notificationAlias.'.seen' => SEEN,
                                     ]
                                 );
        
        return $builder;
    }

    public function createNotification(){
        $accountService = new AccountService();
        $accountAlias = $accountService->getModel()->alias;

        switch($this->type){
            case NEW_FOLLOWER:
                // followed people receive noti
                $followService = new FollowService();
                $followAlias = $followService->getModel()->alias;

                // get the follow data that's already insert into follow table 
                $followData = $followService->getInfo(
                    [
                        // selects
                        $followAlias.'.*',
                    ],
                    [
                        'id' => $this->followId,
                    ]
                );

                // set up data to insert into noti table
                $data = [
                    'type'       => NEW_FOLLOWER,
                    'account_id' => $followData->account_id_2,
                    'created_by' => $followData->account_id_1,
                    'follow_id'  => $followData->id,
                    'title'      => lang('Notification.noti_title.'.NEW_FOLLOWER),
                ];
                $this->id = $this->service->saveData($data);

                // get info of the trigger noti account
                $userInfo = $accountService->getInfo(
                    [
                        $accountAlias.'.id AS user_id',
                        $accountAlias.'.user_name',
                        $accountAlias.'.avatar',
                    ],
                    [
                        'id' => $data['created_by'],
                    ]
                );

                // set up data to prepare fire noti
                $pushNotiData = [
                    'notification_id'       =>  $this->id,
                    'notification_title'    =>  $data['title'],
                    // get info of the trigger noti account
                    'user_id'               =>  $userInfo->user_id,
                    'user_name'             =>  $userInfo->user_name,
                    'avatar'                =>  $userInfo->avatar,  
                    'screen'                =>  USER_SCREEN,
                    // get firebase token of the receive noti account
                    'to'                    =>  $accountService->getInfo(
                                                    [
                                                        $accountAlias.'.firebase_token',
                                                    ],
                                                    [
                                                        'id' => $data['account_id'],
                                                    ]
                    ),
                ];
                $this->pushNotification($pushNotiData);
                break;
            case NEW_COMMENT:
                // post owner receive noti
                $commentService = new CommentService();
                $commentAlias = $commentService->getModel()->alias;
                $postService = new PostService();
                $postAlias = $postService->getModel()->alias;

                // get comment data that's already insert into comment table and also get the author of the post which have this comment
                $commentData = $commentService->getInfo(
                    [
                        // selects
                        $commentAlias.'.*',
                        $postAlias.'.created_by AS post_author',
                        $postAlias.'.id AS post_id',
                    ],
                    [
                        'id' => $this->commentId,
                    ]
                );

                // set up data to insert into noti table
                $data = [
                    'type'       => NEW_COMMENT,
                    'account_id' => $commentData->post_author,
                    'created_by' => $commentData->created_by,
                    'comment_id' => $commentData->id,
                    'title'      => lang('Notification.noti_title.'.NEW_COMMENT),
                ];
                $this->id = $this->service->saveData($data);

                // get info of the trigger noti account
                $userInfo = $accountService->getInfo(
                    [
                        $accountAlias.'.id AS user_id',
                        $accountAlias.'.user_name',
                        $accountAlias.'.avatar',
                    ],
                    [
                        'id' => $data['created_by'],
                    ]
                );

                // set up data to prepare fire noti
                $pushNotiData = [
                    'notification_id'       =>  $this->id,
                    'notification_title'    =>  $data['title'],
                    // get info of the trigger noti account
                    'user_id'               =>  $userInfo->user_id,
                    'user_name'             =>  $userInfo->user_name,
                    'avatar'                =>  $userInfo->avatar,
                    'post_id'               =>  $commentData->post_id,
                    'comment_id'            =>  $commentData->id,
                    'screen'                =>  POST_SCREEN,
                    // get firebase token of the receive noti account
                    'to'                    =>  $accountService->getInfo(
                                                    [
                                                        $accountAlias.'.firebase_token',
                                                    ],
                                                    [
                                                        'id' => $data['account_id'],
                                                    ]
                    ),
                ];
                // push noti to the author
                $this->pushNotification($pushNotiData);

                break;
            case NEW_REPLY_COMMENT:
                // comment owner receive noti 
                $commentService = new CommentService();
                $commentAlias = $commentService->getModel()->alias;
                $postService = new PostService();
                $postAlias = $postService->getModel()->alias;

                // get reply comment data 
                $repCommentData = $commentService->getInfo(
                    [
                        // selects
                        $commentAlias.'.*',
                    ],
                    [
                        // where
                        'id' => $this->commentId,
                    ]
                );

                // get data of the comment which is replied by $repCommentData
                $commentData = $commentService->getInfo(
                    [
                        //selects
                        $commentAlias.'.*',
                        $postAlias.'.id AS post_id',
                    ],
                    [
                        // where 
                        'id' => $repCommentData->comment_id,
                    ]
                );

                // set up data to insert into noti table
                $data = [
                    'type'       => NEW_REPLY_COMMENT,
                    'account_id' => $commentData->created_by,
                    'created_by' => $repCommentData->created_by,
                    'comment_id' => $repCommentData->id,
                    'title'      => lang('Notification.noti_title.'.NEW_REPLY_COMMENT),
                ];
                $this->id = $this->service->saveData($data);

                // get info of the trigger noti account
                $userInfo = $accountService->getInfo(
                    [
                        $accountAlias.'.id AS user_id',
                        $accountAlias.'.user_name',
                        $accountAlias.'.avatar',
                    ],
                    [
                        'id' => $data['created_by'],
                    ]
                );

                // set up data to prepare fire noti
                $pushNotiData = [
                    'notification_id'       =>  $this->id,
                    'notification_title'    =>  $data['title'],
                    // get info of the trigger noti account
                    'user_id'               =>  $userInfo->user_id,
                    'user_name'             =>  $userInfo->user_name,
                    'avatar'                =>  $userInfo->avatar,
                    'post_id'               =>  $commentData->post_id,
                    'comment_id'            =>  $commentData->id,
                    'screen'                =>  POST_SCREEN,
                    // get firebase token of the receive noti account
                    'to'                    =>  $accountService->getInfo(
                                                    [
                                                        $accountAlias.'.firebase_token',
                                                    ],
                                                    [
                                                        'id' => $data['account_id'],
                                                    ]
                    ),
                ];
                // push noti to the author
                $this->pushNotification($pushNotiData);
                break;
            case NEW_POST:
                // all follower receive noti
                $followService = new FollowService();
                $followAlias = $followService->getModel()->alias;
                $accountService = new AccountService();
                $accountAlias = $accountService->getModel()->alias;
                $postService = new PostService();
                $postAlias = $postService->getModel()->alias;

                // get all user whose following the post author
                $followData = $followService->getData(
                    [
                        // selects
                        $followAlias.'.account_id_1',
                        $followAlias.'.account_id_2',
                        $accountAlias.'_1.id AS user_id_1',
                        $accountAlias.'_1.user_name AS user_name_1',
                        $accountAlias.'_1.avatar AS avatar_1',
                        $accountAlias.'_2.firebase_token',
                    ],
                    [
                        // where
                        $followAlias.'.account_id_1' => $this->userId,
                    ]
                );

                // get post data (post title in order to add into notification body)
                $postData = $postService->getInfo(
                    [
                        // selects
                        $postAlias.'.id AS post_id',
                        $postAlias.'.title AS post_title',
                    ],
                    [
                        // where
                        'id' => $this->postId,
                    ]
                );
                
                $postTitle = substr($postData->post_title, 0, strpos(wordwrap($postData->post_title, 20), "\n")).'...';
                foreach($followData as $key => $value){
                    // set up data to insert into noti table
                    $data = [
                        'type'         => NEW_POST,
                        'account_id'   => $value->account_id_2,
                        'created_by'   => $value->account_id_1,
                        'post_id'      => $postData->post_id,
                        'title'        => lang('Notification.noti_title.'.NEW_POST),
                    ];

                    $this->id = $this->service->saveData($data);

                     // set up data to prepare fire noti
                    $pushNotiData = [
                        'notification_id'       =>  $this->id,
                        'notification_title'    =>  $data['title'],
                        'user_id'               =>  $value->user_id_1,
                        'user_name'             =>  $value->user_name_1,
                        'avatar'                =>  $value->avatar_1,
                        'post_title'            =>  $postTitle,
                        'screen'                =>  POST_SCREEN,
                        // get firebase token of the receive noti account
                        'to'                    =>  $value,
                    ];

                    // push noti to the author
                    $this->pushNotification($pushNotiData);
                }
                break;
            case LIKE_POST:
                // post owner receive noti
                $likePostService = new LikePostService();
                $likePostAlias = $likePostService->getModel()->alias;
                $postService = new PostService();
                $postAlias = $postService->getModel()->alias;

                // get data from like post table
                $likePostData = $likePostService->getInfo(
                    [
                        $likePostAlias.'.*',
                        $postAlias.'.created_by AS post_author',
                    ],
                    [
                        'id' => $this->likePostId,
                    ]
                );

                // set up data to insert into noti table
                $data = [
                    'type'         => LIKE_POST,
                    'account_id'   => $likePostData->post_author,
                    'created_by'   => $likePostData->account_id,
                    'like_post_id' => $likePostData->id,
                    'title'        => lang('Notification.noti_title.'.LIKE_POST),
                ];
                $this->id = $this->service->saveData($data);

                // get info of the trigger noti account
                $userInfo = $accountService->getInfo(
                    [
                        $accountAlias.'.id AS user_id',
                        $accountAlias.'.user_name',
                        $accountAlias.'.avatar',
                    ],
                    [
                        'id' => $data['created_by'],
                    ]
                );

                // set up data to prepare fire noti
                $pushNotiData = [
                    'notification_id'       =>  $this->id,
                    'notification_title'    =>  $data['title'],
                    'user_id'               =>  $userInfo->user_id,
                    'user_name'             =>  $userInfo->user_name,
                    'avatar'                =>  $userInfo->avatar,
                    'post_id'               =>  $likePostData->post_id,
                    'screen'                =>  POST_SCREEN,
                    // get firebase token of the receive noti account
                    'to'                    =>  $accountService->getInfo(
                                                    [
                                                        $accountAlias.'.firebase_token',
                                                    ],
                                                    [
                                                        'id' => $data['account_id'],
                                                    ]
                    ),
                ];
                // push noti to the author
                $this->pushNotification($pushNotiData);
                
                break;
            case LIKE_COMMENT:
                // comment owner receive noti
                $likeCommentService = new LikeCommentService();
                $likeCommentAlias = $likeCommentService->getModel()->alias;
                $commentService = new CommentService();
                $commentAlias = $commentService->getModel()->alias;

                $likeCommentData = $likeCommentService->getInfo(
                    [
                        $likeCommentAlias.'.*',
                        $commentAlias.'.created_by AS comment_author',
                        $commentAlias.'.post_id AS post_id',
                    ],
                    [
                        'id' => $this->likeCommentId,
                    ]
                );

                // set up data to insert into noti table
                $data = [
                    'type'            => LIKE_COMMENT,
                    'account_id'      => $likeCommentData->comment_author,
                    'created_by'      => $likeCommentData->account_id,
                    'like_comment_id' => $likeCommentData->id,
                    'title'           => lang('Notification.noti_title.'.LIKE_COMMENT),
                ];
                $this->id = $this->service->saveData($data);

                // get info of the trigger noti account
                $userInfo = $accountService->getInfo(
                    [
                        $accountAlias.'.id AS user_id',
                        $accountAlias.'.user_name',
                        $accountAlias.'.avatar',
                    ],
                    [
                        'id' => $data['created_by'],
                    ]
                );

                // set up data to prepare fire noti
                $pushNotiData = [
                   'notification_id'       =>  $this->id,
                   'notification_title'    =>  $data['title'],
                   'user_id'               =>  $userInfo->user_id,
                   'user_name'             =>  $userInfo->user_name,
                   'avatar'                =>  $userInfo->avatar,
                   'post_id'               =>  $likeCommentData->post_id,
                   'comment_id'            =>  $likeCommentData->comment_id,
                   'screen'                =>  POST_SCREEN,
                   // get firebase token of the receive noti account
                   'to'                    =>  $accountService->getInfo(
                                                   [
                                                       $accountAlias.'.firebase_token',
                                                   ],
                                                   [
                                                       'id' => $data['account_id'],
                                                   ]
                   ),
                ];
                // push noti to the author
                $this->pushNotification($pushNotiData); 

                break;
            case LOCK_POST: 
                // user post get locked by admin 
                $postService = new PostService();
                $postAlias = $postService->getModel()->alias;

                $postData = $postService->getInfo(
                    [
                        $postAlias.'.id AS post_id',
                        $postAlias.'.title AS post_title',
                        $postAlias.'.created_by',
                    ],
                    [
                        'id' => $this->postId,
                    ]
                );

                // set up data to insert into noti table
                $data = [
                    'type'            => LOCK_POST,
                    'account_id'      => $postData->created_by,
                    'created_by'      => $this->userId,
                    'post_id'         => $postData->post_id,
                    'title'           => lang('Notification.noti_title.'.LOCK_POST),
                ];
                $this->id = $this->service->saveData($data);
                
                // get info of the trigger noti account
                $userInfo = $accountService->getInfo(
                    [
                        $accountAlias.'.id AS user_id',
                        $accountAlias.'.user_name',
                        $accountAlias.'.avatar',
                    ],
                    [
                        'id' => $data['created_by'],
                    ]
                );

                // set up data to prepare fire noti
                $pushNotiData = [
                    'notification_id'       =>  $this->id,
                    'notification_title'    =>  $data['title'],
                    'post_id'               =>  $postData->post_id,
                    'screen'                =>  POST_SCREEN,
                    // get firebase token of the receive noti account
                    'to'                    =>  $accountService->getInfo(
                                                    [
                                                        $accountAlias.'.firebase_token',
                                                    ],
                                                    [
                                                        'id' => $data['account_id'],
                                                    ]
                    ),
                ];
                 // push noti to the author
                 $this->pushNotification($pushNotiData); 
                break;
            case UNLOCK_POST:
                // user post unlocked
                $postService = new PostService();
                $postAlias = $postService->getModel()->alias;

                $postData = $postService->getInfo(
                    [
                        $postAlias.'.id AS post_id',
                        $postAlias.'.title AS post_title',
                        $postAlias.'.created_by',
                    ],
                    [
                        'id' => $this->postId,
                    ]
                );

                // set up data to insert into noti table
                $data = [
                    'type'            => UNLOCK_POST,
                    'account_id'      => $postData->created_by,
                    'created_by'      => $this->userId,
                    'post_id'         => $postData->post_id,
                    'title'           => lang('Notification.noti_title.'.UNLOCK_POST),
                ];
                $this->id = $this->service->saveData($data);
                
                // get info of the trigger noti account
                $userInfo = $accountService->getInfo(
                    [
                        $accountAlias.'.id AS user_id',
                        $accountAlias.'.user_name',
                        $accountAlias.'.avatar',
                    ],
                    [
                        'id' => $data['created_by'],
                    ]
                );

                // set up data to prepare fire noti
                $pushNotiData = [
                    'notification_id'       =>  $this->id,
                    'notification_title'    =>  $data['title'],
                    'post_id'               =>  $postData->post_id,
                    'screen'                =>  POST_SCREEN,
                    // get firebase token of the receive noti account
                    'to'                    =>  $accountService->getInfo(
                                                    [
                                                        $accountAlias.'.firebase_token',
                                                    ],
                                                    [
                                                        'id' => $data['account_id'],
                                                    ]
                    ),
                ];
                 // push noti to the author
                 $this->pushNotification($pushNotiData); 
                break;
            case UNLOCK_POST_REJECT:
                // user post unlocked
                $postService = new PostService();
                $postAlias = $postService->getModel()->alias;

                $postData = $postService->getInfo(
                    [
                        $postAlias.'.id AS post_id',
                        $postAlias.'.title AS post_title',
                        $postAlias.'.created_by',
                    ],
                    [
                        'id' => $this->postId,
                    ]
                );

                // set up data to insert into noti table
                $data = [
                    'type'            => UNLOCK_POST_REJECT,
                    'account_id'      => $postData->created_by,
                    'created_by'      => $this->userId,
                    'post_id'         => $postData->post_id,
                    'title'           => lang('Notification.noti_title.'.UNLOCK_POST_REJECT),
                ];
                $this->id = $this->service->saveData($data);

                // get info of the trigger noti account
                $userInfo = $accountService->getInfo(
                    [
                        $accountAlias.'.id AS user_id',
                        $accountAlias.'.user_name',
                        $accountAlias.'.avatar',
                    ],
                    [
                        'id' => $data['created_by'],
                    ]
                );

                // set up data to prepare fire noti
                $pushNotiData = [
                    'notification_id'       =>  $this->id,
                    'notification_title'    =>  $data['title'],
                    'post_id'               =>  $postData->post_id,
                    'screen'                =>  POST_SCREEN,
                    // get firebase token of the receive noti account
                    'to'                    =>  $accountService->getInfo(
                                                    [
                                                        $accountAlias.'.firebase_token',
                                                    ],
                                                    [
                                                        'id' => $data['account_id'],
                                                    ]
                    ),
                ];
                 // push noti to the author
                 $this->pushNotification($pushNotiData); 
                break;
            default:
                break;
        }
        return true;
    }

    private function pushNotification($data){
		$client = \Config\Services::curlrequest();
		$serverKey = env('FIREBASE_SERVER_KEY');
        $url = FIREBASE_PUSH_NOTI_URL;
        $to = $data['to']->firebase_token;

        switch ($this->type) {
            case NEW_POST:
                $noti = [
                    'title' => $data['user_name'].' '.$data['notification_title'],
                    'body'  => $data['post_title'],
                ];
                unset($data['post_title']);
                break;
            case LOCK_POST:
                $noti = [
                    'title' => $data['notification_title'],
                    'body'  => '',
                ];

                break;
                case UNLOCK_POST:
                    $noti = [
                        'title' => $data['notification_title'],
                        'body'  => '',
                    ];
    
                    break;
                case UNLOCK_POST_REJECT:
                    $noti = [
                        'title' => $data['notification_title'],
                        'body'  => '',
                    ];
    
                    break;
            default:
                $noti = [
                    'title' => $data['user_name'].' '.$data['notification_title'],
                    'body'  => '',
                ];

                break;
        }
        
        unset($data['to']);
        unset($data['notification_title']);
		$response = $client->request('POST', $url, [
			'headers' => [
					'Authorization' 	=> 'key ='.$serverKey,
					'Content-Type'     	=> 'application/json',
			],
			'json' => [
				'to' 			=> 	$to,
				'notification'	=> 	$noti,
				'data'			=>  $data,
			]
		]);
        // var_dump($data);
        // die();
		// echo $response->getStatusCode();
		// echo $response->getBody();
		// echo $response->getHeader('Content-Type');
		return true;
    }
}