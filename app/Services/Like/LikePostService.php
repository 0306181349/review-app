<?php 
namespace App\Services\Like;
use App\Models\LikePostModel;
use App\Services\Post\PostService;

class LikePostService
{
    public function __construct(){
        $this->model = new LikePostModel();
        $this->db = \Config\Database::connect();
    }

    public function getModel(){
        return $this->model;
    }

    public function createQuery(){
		$postService = new PostService();
		$postTable = $postService->getModel()->table;
		$postAlias = $postService->getModel()->alias;

        $builder = $this->db->table($this->model->table.' AS '.$this->model->alias)
							->join($postTable.' AS '.$postAlias, $this->model->alias.'.post_id = '.$postAlias.'.id');
        return $builder;
    }

    public function getData($selects = [], $where = []){
        $accountAlias = $this->model->alias;
        $accountTable = $this->model->table;
        $query = $this->createQuery()
                                    ->select($selects)
                                    ->where($where)
                                    ->orderBy($accountAlias.'.id', 'DESC');
        $get = $query->get();
        return $get->getResultObject();
    }

	// get row information
	public function getInfo($selects = [], $where = []){
		$model_alias = $this->model->alias;

		$query = $this->createQuery()
						->select($selects);
		if(is_array($where) && !empty($where)){
			$query = $this->buildCondition($query, $where);
		}
		return $query->get()->getRow();
	}
    
	// set condition
	protected function buildCondition($query, $condition){
		foreach($condition as $field=>$val){
			switch ($field) {
				default:
					if($val !== ''){
						$query->where($this->model->alias.".".$field, $val);
					}
					break;
			}
		}
		return $query;
	}
	
	// cout result
	public function countResult($where = []){
		$query = $this->createQuery();
		if(!empty($where)){
			if(is_array($where)){
				$query = $this->buildCondition($query, $where);
			}else{
				$query->where($where);
			}
		}
		return $query->countAllResults();
	}
    
	public function saveData($data){
		$id = $data['id'] ?? false;
		if($id){
			unset($data['id']);
			$this->model->update($id, $data);
		}else{
			$id = $this->model->insert($data);
		}
		return $id;
	}
}