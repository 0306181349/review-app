<?php 
namespace App\Models;
use CodeIgniter\Model;
class LikeCommentModel extends Model
{
    public $table = "like_comment";
    public $alias = "lkctm";
    
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $useSoftDeletes = false;

    protected $useTimestamps = false;
    // protected $createdField = "created_at";
    // protected $updatedField = "";
    // protected $deletedField = "";
    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

    protected $allowedFields = [
        'id',
        'comment_id',
        'account_id',
    ];
}