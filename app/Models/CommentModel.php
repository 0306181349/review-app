<?php 
namespace App\Models;
use CodeIgniter\Model;
class CommentModel extends Model
{
    public $table = "comment";
    public $alias = "ctm";
    
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $useSoftDeletes = true;

    protected $useTimestamps = false;
    protected $createdField = "created_at";
    // protected $updatedField = "";
    protected $deletedField = "deleted_at";
    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

    protected $allowedFields = [
        'id',
        'post_id',
        'comment_id',
        'content',
        'comment_image',
        'created_at',
        'created_by',
        'is_deleted',
        'deleted_at',
        'deleted_by',
        'updated_at',
        'updated_by',
    ];
}
