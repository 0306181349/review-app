<?php 
namespace App\Models;
use CodeIgniter\Model;
class PostReportModel extends Model
{
    public $table = "post_report";
    public $alias = "prep";
    
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $useSoftDeletes = true;

    protected $useTimestamps = false;
    protected $createdField = "created_at";
    // protected $updatedField = "";
    // protected $deletedField = "";
    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

    protected $allowedFields = [
        'id',
        'post_id',
        'report_title',
        'report_content',
        'is_approval',
        'is_deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
    ];
}
