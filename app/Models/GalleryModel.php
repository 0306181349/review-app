<?php 

namespace App\Models;
use CodeIgniter\Model;

class GalleryModel extends Model
{
    public $table = "gallery";
    public $alias = "gal";
    
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $useSoftDeletes = false;

    protected $useTimestamps = false;
    protected $createdField = "created_at";
    // protected $updatedField = "";
    // protected $deletedField = "deleted_at";
    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

    protected $allowedFields = [
        'id',
        'gallery_name',
        'description',
        'cover_photo',
        'is_private',
        'is_default',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];
}
