<?php 
namespace App\Models;
use CodeIgniter\Model;

class PostHashtagModel extends Model
{
    
    public $table = "post_hashtag";
    public $alias = "phst";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $useSoftDeletes = false;

    protected $useTimestamps = false;
    // protected $createdField = "created_at";
    // protected $deletedField = "deleted_at";
    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;
    
    protected $allowedFields = [
        'id',
        'post_id',
        'hashtag_id',
        'hashtag_alias',
    ];
}