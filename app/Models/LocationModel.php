<?php 
namespace App\Models;
use CodeIgniter\Model;
class LocationModel extends Model
{
    public $table = "location";
    public $alias = "loc";
    
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $useSoftDeletes = true;

    protected $useTimestamps = true;
    protected $createdField = "created_at";
    protected $updatedField = "updated_at";
    protected $deletedField = "deleted_at";
    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

    protected $allowedFields = [
        'id',
        'location_name',
        'open_time',
        'closed_time',
        'lowest_price',
        'highest_price',
        'address',
        'province',
        'phone_number',
        'is_verify',
        'verified_by',
        'is_deleted',
        'deleted_at',
        'deleted_by',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];
}
