<?php 
namespace App\Models;
use CodeIgniter\Model;

class HashtagModel extends Model
{
    public $table = "hashtag";
    public $alias = "hst";
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $useSoftDeletes = true;

    protected $useTimestamps = false;
    protected $createdField = "created_at";
    protected $deletedField = "deleted_at";
    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;
    
    protected $allowedFields = [
        'id',
        'hashtag',
        'created_by',
        'created_at',
        'deleted_by',
        'deleted_at',
        'is_deleted',
        'updated_by',
        'updated_at',
    ];
}