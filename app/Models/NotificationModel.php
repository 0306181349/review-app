<?php 
namespace App\Models;
use CodeIgniter\Model;
class NotificationModel extends Model
{
    public $table = "notification";
    public $alias = "not";
    
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $useSoftDeletes = true;

    protected $useTimestamps = true;
    protected $createdField = "created_at";
    protected $updatedField = "";
    protected $deletedField = "";
    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

    protected $allowedFields = [
        'id',
        'title',
        'type',
        'follow_id',
        'account_id',
        'post_id',
        'comment_id',
        'like_comment_id',
        'like_post_id',
        'seen',
        'clicked',
        'created_at',
        'created_by',
        'is_deleted',
        'deleted_at',
        'deleted_by',
        'updated_at',
        'updated_by',
    ];
}