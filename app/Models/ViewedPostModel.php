<?php 
namespace App\Models;
use CodeIgniter\Model;
class ViewedPostModel extends Model
{
    public $table = "viewed_post";
    public $alias = "rti";
    
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $useSoftDeletes = true;

    // protected $useTimestamps = true;
    // protected $createdField = "created_at";
    // protected $updatedField = "";
    // protected $deletedField = "";
    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

    protected $allowedFields = [
        'id',
        'post_id',
        'created_by',
        'created_at',
    ];
}
