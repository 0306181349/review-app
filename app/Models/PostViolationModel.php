<?php 
namespace App\Models;
use CodeIgniter\Model;
class PostViolationModel extends Model
{
    public $table = "post_violation";
    public $alias = "locpstvio";
    
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $useSoftDeletes = false;

    protected $useTimestamps = false;
    // protected $createdField = "created_at";
    // protected $updatedField = "";
    // protected $deletedField = "";
    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

    protected $allowedFields = [
        'id',
        'post_id',
        'report_title_id',
    ];
}