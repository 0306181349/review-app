<?php 

namespace App\Models;
use CodeIgniter\Model;

class GalleryPostModel extends Model
{
    public $table = "gallery_post";
    public $alias = "gpts";
    
    protected $primaryKey = "id";
    protected $returnType = "object";
    protected $useSoftDeletes = false;

    protected $useTimestamps = false;
    protected $createdField = "created_at";
    // protected $updatedField = "";
    // protected $deletedField = "deleted_at";
    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

    protected $allowedFields = [
        'id',
        'gallery_id',
        'post_id',
    ];
}
